<style>
#event_people {
	display: block;
	overflow: hidden;
	width: auto;
	margin: 0px 10px 0px 0px;
	padding: 5px 2px 2px 2px;
}

#event_people > li {
	display: block;
	padding: 2px 0px 2px 4px;
	height: 30px;
	margin-bottom: 4px;
}

#event_people > li span {
	float: left;
	display: block;
	line-height: 30px;
	height: 30px;
	margin-right: 10px;
}

#event_people > li span.delete {
	height: 30px;
	line-height: 30px;
	margin-right: 10px;
	cursor: pointer;
}

#event_people > li span.name {
	width: 225px;
}
#event_people > li span.phone {
	width: 130px;
}
#event_people > li span.email {
	width: 250px;
}
#event_people > li span.status {
	width: 185px;
	float: right;
}

#event_people > li.paid {
	background: #f2faff;
	background: -moz-linear-gradient(top,  #f2faff 0%, #c0dcea 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2faff), color-stop(100%,#c0dcea));
	background: -webkit-linear-gradient(top,  #f2faff 0%,#c0dcea 100%);
	background: -o-linear-gradient(top,  #f2faff 0%,#c0dcea 100%);
	background: -ms-linear-gradient(top,  #f2faff 0%,#c0dcea 100%);
	background: linear-gradient(to bottom,  #f2faff 0%,#c0dcea 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2faff', endColorstr='#c0dcea',GradientType=0 );
	border: 1px solid #B2D7E8;
	margin-left: -1px;
	margin-top: -1px;
}

a.pay, a.checkin {
	background: #9c9c9c;
	background: -moz-linear-gradient(top,  #9c9c9c 0%, #797979 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#9c9c9c), color-stop(100%,#797979));
	background: -webkit-linear-gradient(top,  #9c9c9c 0%,#797979 100%);
	background: -o-linear-gradient(top,  #9c9c9c 0%,#797979 100%);
	background: -ms-linear-gradient(top,  #9c9c9c 0%,#797979 100%);
	background: linear-gradient(to bottom,  #9c9c9c 0%,#797979 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9c9c9c', endColorstr='#797979',GradientType=0 );
    border: 1px solid #232323;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    font-size: 14px;
    font-weight: normal;
    height: 30px;
    line-height: 30px;
	margin: 0px;
    overflow: visible;
    padding: 0px 10px 0px 10px;
    position: relative;
    text-align: center;
    text-decoration: none !important;
    text-shadow: 0 -1px 0 #000000;
    float: left;
    border-radius: 5px;
}

span.count {
	display: inline-block;
	padding: 1px 4px;
	border-radius: 15px;
	background: #468BBA;
	color: white;
	width: 20px;
	text-align: center;
}

a.pay.done, a.checkin.done {
	background: #DDDDDD;
	border: 1px solid #BBBBBB;
	color: #666;
	text-shadow: none;
}

a.pay {
	margin-right: 10px;
	width: 40px;
}

a.checkin {
	width: 90px;
	text-align: left;
}
</style>
<div class='background'>
<?php
echo form_open('/teesheets/save_teetime/'.$teetime_info->TTID,array('id'=>'teetime_form'));
//echo 'teesheet holes: '.$teesheet_holes.'<br/>';
//print_r($customer_info);
?>
<div id='radioset' class='teebuttons'>
	<input type='radio' value='teetime' id='type_teetime' name='event_type'  <?php echo ($teetime_info->type == 'teetime' || $teetime_info->type == '')?'checked':''; ?>><label id='teetime_label' for='type_teetime' >Tee Time</label>
    <input type='radio' value='tournament' id='type_tournament' name='event_type' <?php echo ($teetime_info->type == 'tournament')?'checked':''; ?>><label id='tournament_label' for='type_tournament'>Tournament</label>
    <input type='radio' value='league' id='type_league' name='event_type' <?php echo ($teetime_info->type == 'league')?'checked':''; ?>><label id='league_label' for='type_league'>League</label>
    <input type='radio' value='event' id='type_event' name='event_type' <?php echo ($teetime_info->type == 'event')?'checked':''; ?>><label id='event_label' for='type_event'>Event</label>
    <input type='radio' value='closed' id='type_closed' name='event_type' <?php echo ($teetime_info->type == 'closed')?'checked':''; ?>><label id='closed_label' for='type_closed'>Block</label>
    <!-- <input type='radio' value='shotgun' id='type_shotgun' name='event_type' <?php echo ($teetime_info->type == 'shotgun')?'checked':''; ?>><label id='shotgun_label' for='type_shotgun'>Shotgun</label> -->
    <div class='clear'></div>
</div>
<div id='closed_table' <?php echo ($teetime_info->type == 'closed')?'':"style='display:none'"?>>
	<div>
		<input tabindex=1 class='' id='closed_title' name='closed_title' placeholder='Title' value='<?php echo $teetime_info->title; ?>' style='width:160px, margin-right:5px;'/>
	</div>
	<div class='popup_divider'></div>
   <div>
       	<textarea tabindex=2 class='' placeholder='Details' id='closed_details' name='closed_details' style='width:94%;height:50px;'><?php echo $teetime_info->details; ?></textarea>
	</div>
	<div class='event_buttons_holder'>
  		<span class='saveButtons'>
			<button id='closed_save'>Save</button>
    	</span>
    	<button id='closed_delete' class='deletebutton'>Delete</button>
    	<div class='clear'></div>
    </div>
</div>
<div id='event_table'  <?php echo ($teetime_info->type == 'tournament' || $teetime_info->type == 'league' || $teetime_info->type == 'event')?'':"style='display:none'"?>>
	<div class='event_info'>
		<input class='' id='event_title' name='event_title' placeholder='Title' value='<?php echo $teetime_info->title; ?>' style='width:160px, margin-right:5px;'/>
    	<span class='flag_icon'></span>
	 	<span class='holes_buttonset'>
            <input type='radio' id='event_holes_9' name='event_holes' value='9' <?php echo (($teesheet_holes == '9' && $teetime_info->holes != '18') || $teetime_info->holes == '9')?'checked':''; ?>/>
            <label for='event_holes_9' id='event_holes_9_label'>9</label>
            <input type='radio' id='event_holes_18' name='event_holes' value='18'  <?php echo (($teesheet_holes == '18' && $teetime_info->holes != '9') || $teetime_info->holes == '18')?'checked':''; ?>/>
            <label for='event_holes_18' id='event_holes_18_label'>18</label>
        </span>
	 	<div class='player_buttons'>
	        <span class='players_icon'></span>
            <div class='players'>
            	<span class='players_buttonset'>
        			<input class='teetime_players' id='event_players' name='event_players' value='<?php echo $teetime_info->player_count; ?>'/>
        		</span>
    		</div>
            <span class='carts_icon'></span>
	        <div class='carts'>
            	<span class='carts_buttonset'>
    				<input class='teetime_carts' id='event_carts' name='event_carts' value='<?php echo $teetime_info->carts; ?>'/>
    			</span>
    		</div>
       	</div>
       	<div class='clear'></div>
       	<div class='popup_divider'></div>
       	<div class="row" style="overflow: hidden; padding-right: 15px;">
			<input name="event_person" id="event_person" value="" placeholder="Add players..." style='width: 220px; margin-right:5px; float: left;' />
			<?php echo form_dropdown('default_price_category', $price_classes, $teetime_info->default_price_category, 'style="float: right;"'); ?>
			<label for="event_cart_fee" style="font-weight: normal; float: right; margin-right: 15px;">
				<input name="default_cart_fee" type="hidden" value="0" />
				<input name="default_cart_fee" id="default_cart_fee" value="1" type="checkbox" <?php if($teetime_info->default_cart_fee > 0){ echo 'checked="checked"'; } ?> /> Cart Fee
			</label>
		</div>
		<h2 style="font-weight: bold; color: #666; margin-top: 5px; display: block;">
			<span class="count"><?php echo count($event_people); ?></span> Players
		</h2>
		<ul id="event_people">
			<?php if(!empty($event_people)){ ?>
			<?php foreach($event_people as $person){ ?>
			<li class="<?php if($person['paid'] == 1){ echo 'paid'; } ?>">
				<input type="hidden" name="event_people[]" value="<?php echo $person['person_id']; ?>" />
				<span class="clear_data delete" data-person-id="<?php echo $person['person_id']; ?>">x</span>
				<span class="name"><?php echo $person['last_name']; ?>, <?php echo $person['first_name']; ?></span>
				<span class="phone"><?php echo $person['phone_number']; ?></span>
				<span class="email"><?php echo $person['email']; ?></span>
				<span class="status">
					<a href="#" class="pay<?php if($person['paid'] == 1){ echo ' done'; } ?>">
						$ <?php if($person['paid'] == 1){ echo 'Paid'; }else{ echo 'Pay'; } ?>
					</a>
					<a href="#" class="checkin<?php if($person['checked_in'] == 1){ echo ' done'; } ?>">
						✔ <?php if($person['checked_in'] == 1){ echo 'Checked In'; }else{ echo 'Check In'; } ?>
					</a>
				</span>
			</li>
			<?php } } ?>
		</ul>
    </div>
    <div class='popup_divider'></div>
    <div>
       	<textarea tabindex=4 class='details_box' placeholder='Details' id='event_details' name='event_details' style='width:94%;height:50px;'><?php echo $teetime_info->details; ?></textarea>
    </div>
    <div class='event_buttons_holder'>
 		<span class='saveButtons'>
			<button id='event_save'>Save</button>
    	</span>
    	<button id='event_delete' class='deletebutton'>Delete</button>
     	<div class='clear'></div>
   </div>
</div>
<div id="shotgun_table" <?php echo ($teetime_info->type == 'shotgun')?'':"style='display:none'"?>>
	<div class="event_info">
		<input class='' id='shotgun_title' name='shotgun_title' placeholder='Title' value='<?php echo $teetime_info->title; ?>' style='width:160px, margin-right:5px;'/>
    	<span class='flag_icon'></span>
	 	<span class='holes_buttonset'>
            <input type='radio' id='shotgun_holes_9' name='shotgun_holes' value='9' <?php echo (($teesheet_holes == '9' && $teetime_info->holes != '18') || $teetime_info->holes == '9')?'checked':''; ?>/>
            <label for='shotgun_holes_9' id='shotgun_holes_9_label'>9</label>
            <input type='radio' id='shotgun_holes_18' name='shotgun_holes' value='18' <?php echo (($teesheet_holes == '18' && $teetime_info->holes != '9') || $teetime_info->holes == '18')?'checked':''; ?>/>
            <label for='shotgun_holes_18' id='shotgun_holes_18_label'>18</label>
        </span>
	 	<div class='player_buttons'>
	        <span class='players_icon'></span>
            <div class='players'>
            	<span class='players_buttonset'>
        			<input class='teetime_players' id='shotgun_players' name='shotgun_players' value='<?php echo $teetime_info->player_count; ?>'/>
        		</span>
    		</div>
            <span class='carts_icon'></span>
	        <div class='carts'>
            	<span class='carts_buttonset'>
    				<input class='teetime_carts' id='shotgun_carts' name='shotgun_carts' value='<?php echo $teetime_info->carts; ?>'/>
    			</span>
    		</div>
       	</div>
       	<div class='clear'></div>
	</div>
	<div class='popup_divider'></div>
	<table style="margin: 0px 0px 15px 15px;" id="shotgun_grid">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<?php for($player = 1; $player <= 5; $player++){ ?>
				<th>Player <?php echo $player; ?></th>
				<?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php for($hole = 1; $hole <= 9; $hole++){ ?>
			<tr>
				<td class="hole"><?php echo $hole; ?></td>
				<?php for($player = 1; $player <= 5; $player++){ ?>
				<td>
					<input type="text" class="shotgun-person" value="" placeholder="Add Player..." />
					<input type="hidden" name="shotgun_players[<?php echo $hole; ?>][<?php echo $player; ?>][person_id]" value="" />
				</td>
				<?php } ?>
			<tr>
			<?php } ?>
		</tbody>
	</table>
	<div>
		<textarea tabindex=4 class='details_box' placeholder='Details' id='shotgun_details' name='shotgun_details' style='width:94%;height:50px;'><?php echo $teetime_info->details; ?></textarea>
	</div>
	<div class='event_buttons_holder'>
		<span class='saveButtons'>
			<button id='shotgun_save'>Save</button>
		</span>
		<button id='shotgun_delete' class='deletebutton'>Delete</button>
		<div class='clear'></div>
   </div>
</div>
<div id='teetime_table' <?php echo ($teetime_info->type == 'teetime' || $teetime_info->type == '')?'':"style='display:none'"?>>
	<div id='teetime_people_row_1'>
		<div class='player_info'>
			<input tabindex=1 class='' id='teetime_title' name='teetime_title' placeholder='Last, First name' value='<?php echo ($teetime_info->person_name)?$teetime_info->person_name:$teetime_info->title; ?>' style='width:160px, margin-right:5px;'/>
		</div>	
			<div id="status_image" class="status_image <?php echo $image_status; ?>">
				<div class="customer_comment" id="customer_comment"><?php echo $customer_info[$teetime_info->person_id]->comments; ?></div>
			</div>
	   	<div class='player_info player_details'>	
	   		<input type='hidden' id='person_id' name='person_id' value='<?php echo $teetime_info->person_id; ?>'/>
	   		<input tabindex=2 type=text value='<?php echo $customer_info[$teetime_info->person_id]->email; ?>' id='email' name='email' placeholder='Email' class='ttemail'/>
	   		<input  tabindex=3 type=text value='<?php echo $customer_info[$teetime_info->person_id]->phone_number; ?>' id='phone' name='phone' placeholder='888-888-8888' class='ttphone'/>
	 		<input type='text' id='zip' name='zip' placeholder='Zip Code' size=7  value='<?php echo $customer_info[$teetime_info->person_id]->zip; ?>' />
   			<?php
				echo form_dropdown('price_class_1', $price_classes, $teetime_info->price_class_1);
			?>
	 	</div>
	 	<input type='checkbox' id='save_customer_1' name='save_customer_1' style='display:none'/>
		<!--label for='save_customer_1' title='Save to Customers'>
			<span class='checkbox_image' id='save_customer_1_image'></span>
    	</label>
   		<a class='expand_players down' href='#' title='Expand'>Expand</a-->
   		<div class='clear'></div>
   	</div>
   	<?php if ($this->config->item('mercury_id') || $this->config->item('ets_key')) { ?>
   	<div id='tee_time_credit_card_row'>
   		<a href='#' id='add_teetime_credit_card' class='colbox2'>Add Card</a>
   		<?php echo $this->Customer_credit_card->dropdown($teetime_info->person_id, $teetime_info->credit_card_id); ?>
   		<span id='charge_for_tee_time'>Charge No Show</span>
   		<input type='hidden' id='added_credit_card_id' value='-1' />
   		<div class='clear'></div>
   	</div>
   	<?php } ?>
   	<div class='popup_divider'></div>
    <div id='teetimes_row_2' >
        <div id='players' class='player_buttons'>
        	<span class='players_icon'></span>
	 		<span class='players_buttonset'>
    			<input type='radio' id='players_1' name='players' value='1' <?php echo ($teetime_info->player_count == '1' || $teetime_info->player_count == '0')?'checked':''; ?>/>
    			<label for='players_1' id='players_1_label'>1</label>
    			<input type='radio' id='players_2' name='players' value='2' <?php echo ($teetime_info->player_count == '2')?'checked':''; ?>/>
    			<label for='players_2' id='players_2_label'>2</label>
    			<input type='radio' id='players_3' name='players' value='3' <?php echo ($teetime_info->player_count == '3')?'checked':''; ?>/>
    			<label for='players_3' id='players_3_label'>3</label>
    			<input type='radio' id='players_4' name='players' value='4' <?php echo ($teetime_info->player_count == '4')?'checked':''; ?>/>
    			<label for='players_4' id='players_4_label'>4</label>
    			<input type='radio' id='players_5' name='players' value='5' <?php echo ($teetime_info->player_count == '5')?'checked':''; ?>/>
    			<label for='players_5' id='players_5_label'>5</label>
			</span>
			<?php echo form_hidden('current_player_count', $teetime_info->player_count);?>
		</div>
	    <div id='carts'>
	    	<span class='carts_icon'></span>
	 		<span class='carts_buttonset'>
				<input type='radio' id='carts_0' name='carts' value='0' <?php echo ($teetime_info->carts == '0')?'checked':''; ?>/>
				<label for='carts_0' id='carts_0_label'>0</label>
				<input type='radio' id='carts_1' name='carts' value='1' <?php echo ($teetime_info->carts == '1')?'checked':''; ?>/>
				<label for='carts_1' id='carts_1_label'>1</label>
				<input type='radio' id='carts_2' name='carts' value='2' <?php echo ($teetime_info->carts == '2')?'checked':''; ?>/>
				<label for='carts_2' id='carts_2_label'>2</label>
				<input type='radio' id='carts_3' name='carts' value='3' <?php echo ($teetime_info->carts == '3')?'checked':''; ?>/>
				<label for='carts_3' id='carts_3_label'>3</label>
				<input type='radio' id='carts_4' name='carts' value='4' <?php echo ($teetime_info->carts == '4')?'checked':''; ?>/>
				<label for='carts_4' id='carts_4_label'>4</label>
				<input type='radio' id='carts_5' name='carts' value='5' <?php echo ($teetime_info->carts == '5')?'checked':''; ?>/>
				<label for='carts_5' id='carts_5_label'>5</label>
			</span>
		</div>
		<span class='flag_icon'></span>
	 	<span class='holes_buttonset'>
	        <input type='radio' id='teetime_holes_9' name='teetime_holes' value='9' <?php echo (($teesheet_holes == '9' && $teetime_info->holes != '18') || $teetime_info->holes == '9')?'checked':''; ?>/>
	        <label for='teetime_holes_9' id='teetime_holes_9_label'>9</label>
	        <input type='radio' id='teetime_holes_18' name='teetime_holes' value='18'  <?php echo (($teesheet_holes == '18' && $teetime_info->holes != '9') || $teetime_info->holes == '18')?'checked':''; ?>/>
	        <label for='teetime_holes_18' id='teetime_holes_18_label'>18</label>
	    </span>

		<div>
			<button id='paid_players' disabled><?php echo $teetime_info->paid_player_count; ?> paid</button>
		</div>
		<div class="paid_carts">
			<button id='paid_carts' disabled><?php echo $teetime_info->paid_carts; ?> paid</button>
		</div>
		<div class="assigned_carts">
			<label for='assigned_carts' id='assigned_carts'>Assigned Carts :</label>
			<input type='text' id='assigned_carts_1' name="assigned_carts_1" value='<?php echo $teetime_info->cart_num_1; ?>' />
			<input type='text' id='assigned_carts_2' name="assigned_carts_2" value='<?php echo $teetime_info->cart_num_2; ?>' />
		</div>
		<div class='clear'></div>
		 <div class='event_controls_holder'>
		 	<!--a class='expand_players down' href='#' title='Expand'>Expand</a-->

	   		<input type='checkbox' value='1' name='expand_players' id='expand_players'/><span id='expand_players_text' style='font-size:14px; padding-left:5px;'>List all</span>
			<input type='checkbox' value='1' name='split_teetimes' id='split_teetimes' <?php if($this->config->item('auto_split_teetimes')) {?>checked<?php } ?>/><span id='split_teetime_text' style='font-size:14px; padding-left:5px;'>Split</span>
			<input type='checkbox' value='1' name='send_confirmation_emails' id='send_confirmation_emails' <?php if(($teetime_info->send_confirmation ? ($teetime_info->send_confirmation == 2 ? false : true): $this->config->item('send_reservation_confirmations'))) {?>checked<?php } ?>/>
			<span id='send_confirmation_emails_text' style='font-size:14px; padding-left:5px;'>Send Confirmation Emails</span>
		</div>
		<div class='marker_box'>
			<?php if ($teetime_info->teed_off_time != '0000-00-00 00:00:00') { ?>
			<span id='remove_teed_off_time' class='delete_item'>x</span><span id='teed_off_label'>Teed Off</span><input type='text' value='<?php echo date('h:i a', strtotime($teetime_info->teed_off_time)); ?>' id='teed_off_time' name='teed_off_time'/>
			<?php } ?>
			<?php if ($teetime_info->turn_time != '0000-00-00 00:00:00') { ?>
			<span id='remove_turn_time' class='delete_item'>x</span><span id='turn_label'>Turn</span><input type='text' value='<?php echo date('h:i a', strtotime($teetime_info->turn_time)); ?>' id='turn_time' name='turn_time'/>
			<?php } ?>
			<div class='clear'></div>
		</div>
	</div>
	<div id='teetimes_row_3'>
    	<div id='teetime_people_table_holder'>
    		<table id='teetime_people_table' style='display:none'>
    			<tbody>
    				<tr  id='teetime_people_row_label'>
    					<td colspan='5'>Additional players</td>
    				</tr>
    				<?php for ($i = 2; $i <= 5; $i++) { ?>
    				<tr id='teetime_people_row_<? echo $i?>' >
    					<td>
    						<span class='clear_data' onclick='clear_player_data(<? echo $i?>)'>x</span>
    					</td>
			        	<td>
			        		<input tabindex=<? echo (($i-1)*3)+1?> class='' placeholder='Last, First name' id='teetime_title_<? echo $i?>' name='teetime_title_<? echo $i?>' value='<?php $person_name = 'person_name_'.$i; echo $teetime_info->$person_name; ?>' style='width:160px, margin-right:5px;'/>
			        		<input type='hidden' id='person_id_<? echo $i?>' name='person_id_<? echo $i?>' value='<?php $person_id = 'person_id_'.$i; echo $teetime_info->$person_id; ?>'/>
			        	</td>
			            <td>
			        		<input tabindex=<? echo (($i-1)*3)+2?> type=text placeholder='Email' value='<?php echo $customer_info[$teetime_info->$person_id]->email; ?>' id='email_<? echo $i?>' name='email_<? echo $i?>' class='ttemail'/>
			        	</td>
			        	<td>
			        		<input  tabindex=<? echo (($i-1)*3)+3?> type=text placeholder='888-888-8888' value='<?php echo $customer_info[$teetime_info->$person_id]->phone_number; ?>' id='phone_<? echo $i?>' name='phone_<? echo $i?>' class='ttphone'/>
			        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        	</td>
			        	<td>
        			 		<?php
        			 			$price_class = "price_class_{$i}";
								echo form_dropdown("price_class_{$i}", $price_classes, $teetime_info->$price_class);
							?>
			        	</td>
			        	<!--td>
			        		<label for='save_customer_<? echo $i?>'>
			        			<span class='checkbox_image' id='save_customer_<? echo $i?>_image'></span>
				        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        		</label>
			        	</td-->
			        </tr>
			        <?php } ?>
    			</tbody>
    		</table>
   		</div>
	</div>
	<div class='popup_divider'></div>
    <div>
    	 <textarea tabindex=20 class='' placeholder='Details' id='details' name='teetime_details' style='width:94%;height:50px;'><?php echo $teetime_info->details; ?></textarea>
    </div>
   <div class='event_buttons_holder'>
   		<button id='teetime_delete' class='deletebutton' style='float:left;'>Delete</button>
    	<span class='saveButtons'>
        <?php if ($this->config->item('sales')) {?>
        	<span class='pay_icon'></span>
        <?php
        	for ($i = 1; $i <= 5; $i++) {
        		echo "<button class='purchase_button' id='purchase_$i'>$i</button>";
			} ?>
        <?php } else { ?>
			<span style="float: left; margin-right: 15px; padding-top: 5px; color: #444">Check In</span>
        	<?php for ($i = 1; $i <= 5; $i++) {
        		echo "<button class='purchase_button' id='checkin_$i'>$i</button>";
			} ?>        	
        	<input type='hidden' id='checkin' name='checkin' value='0'/>
        <?php } ?>
        </span>
        <button id='teetime_save' style='float:left;'><? echo $save_text; ?></button>
    	<button id='teetime_confirm' class='confirmButton' style='display:none'>Confirm</button>
        <button id='teetime_cancel' style='display:none' class='cancelbutton'>Cancel</button>
        <div class='clear'></div>
    </div>
<input type='hidden' id='purchase_quantity' name='purchase_quantity' value='0'/>
<input type='hidden' id='delete_teetime' name='delete_teetime' value='0'/>
<input type='hidden' name='start' value='<? echo $teetime_info->start; ?>'/>
<input type='hidden' name='end' value='<? echo $teetime_info->end; ?>'/>
<input type='hidden' name='teesheet_id' value='<? echo $teetime_info->teesheet_id; ?>'/>
<input type='hidden' name='side' value='<? echo $teetime_info->side; ?>'/>
<input type='hidden' name='paid_carts' value='<? echo $teetime_info->paid_carts; ?>'/>
<input type='hidden' name='paid_player_count' value='<? echo $teetime_info->paid_player_count; ?>'/>
<?php echo form_close(); ?>
</div>
<style>
	.carts_buttonset label.ui-button {
		padding:0px 5px 2px;
	}
	.holes_buttonset label.ui-button {
		padding:0px 5px 6px;
	}
	#zip {
		margin-right:5px;
		float:none;
	}
	#add_teetime_credit_card {
		line-height:23px;
		font-size:12px;
		width:56px;
	}
	select#teetime_rate_1 {
		font-size: 14px;
		font-weight: lighter;
		font-family: "Quicksand",Helvetica,sans-serif;
		height: 32px;
		-webkit-appearance: menulist;
		box-sizing: border-box;
		align-items: center;
		border: 1px solid;
		border-image-source: initial;
		border-image-slice: initial;
		border-image-width: initial;
		border-image-outset: initial;
		border-image-repeat: initial;
		white-space: pre;
		-webkit-rtl-ordering: logical;
		color: black;
		background-color: white;
		cursor: default;
	}
	span.carts_buttonset {
		width:290px;
	}
	span.flag_icon {
		margin-left: 0px;
		width: 44px;
		background-position: 4px -162px;
	}
	#colorbox select {
		font-size:16px;
	}
	#colorbox input {
		margin-right:5px;
	}
	#credit_card_id {
		margin-top:5px;
	}
	#charge_for_tee_time {
		font-size:10px;
		cursor:pointer;
	}
</style>
<script type='text/javascript'>
function event_person_row(data){
	var html = '<li id="event_person_'+data.person_id+'">' +
		'<input type="hidden" name="event_people[]" value="' + data.person_id + '" />' +
		'<span class="clear_data delete">x</span>' +
		'<span class="name">' + data.name + '</span>' +
		'<span class="phone">' + data.phone + '</span>' +
		'<span class="email">' + data.email + '</span>' +
		'<span class="status">' +
			'<a href="#" class="pay">$ Pay</a>' +
			'<a href="#" class="checkin">✔ Check In</a>' +
		'</span>' +
	'</li>';
	return html;
}

function clear_player_data(row) {
	if (row == 1)
	{
		$('#teetime_title').val('');
		$('#person_id').val('');
		$('#email').val('');
		$('#phone').val('');
		$('#phone').val('');
		$('#credit_card_id').replaceWith('<input type="hidden" id="credit_card_id" value="0" />');
	}
	else
	{
		$('#teetime_title_'+row).val('');
		$('#person_id_'+row).val('');
		$('#email_'+row).val('');
		$('#phone_'+row).val('');
	}
}
function added_credit_card(cc_id)
{
	$('#added_credit_card_id').val(cc_id);
	update_credit_card_dropdown(0,cc_id);
}
function update_credit_card_dropdown(customer_id, cc_id)
{
	customer_id = !customer_id || customer_id == undefined ? $('#person_id').val() : customer_id;
	cc_id = !cc_id || cc_id == undefined ? $('#added_credit_card_id').val() : cc_id;
	$.ajax({
       type: "POST",
       url: "index.php/teesheets/credit_card_dropdown/"+customer_id+'/'+cc_id,
       data: '',
       success: function(response){
       		$('#credit_card_id').replaceWith(response);
	        $.colorbox2.close();
	    },
        dataType:'html'
     });
}
function focus_on_title(title) {
	<?php if ($teetime_info->type == 'teetime' || $teetime_info->type == ''){
			$focus = 'teetime_title';
		}else if ($teetime_info->type == 'closed'){
			$focus = 'closed_title';
		}else{
			$focus = 'event_title';
		} ?>
	title = (title == undefined)?'<?php echo $focus;?>':title;
    $('#'+title).focus().select();
}
$.ui.autocomplete.prototype.options.autoSelect = true;
$(document).ready(function(){
	$('#add_teetime_credit_card').click(function(e){
		e.preventDefault();
		var person_id = $("#person_id").val();
		$.colorbox2({href:'index.php/teesheets/open_add_credit_card_window/<?=$teetime_info->TTID?>/'+person_id+'/width~600'})
	});
	$('#charge_for_tee_time').click(function (e){
		var card_to_charge = $('#credit_card_id').val();
		if (card_to_charge == 0)
		{
			alert('Please select a card to charge');
			return;
		}
		$.colorbox2({href:'index.php/teesheets/view_charge_card/<?=$teetime_info->TTID?>', width:500, onComplete:function(){
			$('#credit_card_label').html($('#credit_card_id option:selected').text());
			$('#card_to_charge').val(card_to_charge);
			$('#charge_person_id').val($('#person_id').val());
			$('#tee_time_id').val('<?=$teetime_info->TTID?>');
		}});
	});
	$('#teed_off_time').timepicker({'timeFormat':'h:mm tt', 'hour':'<?php echo date('h', strtotime($teetime_info->teed_off_time))?>', 'minute':'<?php echo date('i', strtotime($teetime_info->teed_off_time))?>'});
	$('#turn_time').timepicker({'timeFormat':'h:mm tt', 'hour':'<?php echo date('h', strtotime($teetime_info->turn_time))?>', 'minute':'<?php echo date('i', strtotime($teetime_info->turn_time))?>'});
	$('#remove_teed_off_time').click(function(){
		$.ajax({
	       type: "POST",
	       url: "index.php/teesheets/zero_teed_off/<?=$teetime_info->TTID?>",
	       data: "",
	       success: function(response){
		       Calendar_actions.update_teesheet(response.teetimes);
	           $('#remove_teed_off_time').remove();
	           $('#teed_off_label').remove();
	           $('#teed_off_time').remove();
		   },
		   dataType:'json'
	     });
	});
	$('#remove_turn_time').click(function(){
		$.ajax({
	       type: "POST",
	       url: "index.php/teesheets/zero_turn/<?=$teetime_info->TTID?>",
	       data: "",
	       success: function(response){
	           Calendar_actions.update_teesheet(response.teetimes);
           	   $('#remove_turn_time').remove();
	           $('#turn_label').remove();
	           $('#turn_time').remove();
		   },
		   dataType:'json'
	     });
	});
	$("#phone").mask2("(999) 999-9999");
	$("#phone_2").mask2("(999) 999-9999");
	$("#phone_3").mask2("(999) 999-9999");
	$("#phone_4").mask2("(999) 999-9999");
	$("#phone_5").mask2("(999) 999-9999");

	var submitting = false;
	$('#teetime_form').bind('keypress', function(e) {
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   //Do something
		   e.preventDefault();
		   $('#teetime_save').click();
		 }
    });
    $('#teetime_form').validate({
		submitHandler:function(form)
		{
			var type = $('input[name=event_type]:checked').val();
			var proceed_with_save = true;
			if ($('#delete_teetime').val() == 1)
			{
				proceed_with_save = confirm('Are you sure you want to delete this event?');
			}
			else if ((type == 'teetime' && $('#teetime_title').val() == '') ||
				    ((type == 'tournament' || type == 'league' || type == 'event') && $('#event_title').val() == '') ||
				     (type == 'closed' && $('#closed_title').val() == '') ||
					(type == 'shotgun' && $('#shotgun_title').val() == '')
				     )
				 {
				     alert('Please enter a name/title for this event');
				     proceed_with_save = false;
				 }
			if (proceed_with_save)
			{
				if (submitting) return;
				submitting = true;
				$(form).mask('<?php echo lang('common_wait'); ?>');

				$(form).ajaxSubmit({
					success:function(response)
					{
						Calendar_actions.update_teesheet(response.teetimes);
		                submitting = false;
		                post_teetime_form_submit(response);
		                if (response.email_confirmation != ''){
		                	$.each(response.email_confirmation, function( index, value ) {
		                		Calendar_actions.send_confirmation(value);
		                	});
		                }else{
		                	Calendar_actions.send_confirmation(response.send_confirmation);
		                }		
		                if (!response.second_time_available)
		                	alert('Warning: The reround overlaps existing tee times or events.');
		                if (response.go_to_register)
		                	window.location = '/index.php/sales';
		            	currently_editing = '';
		        		$.colorbox.close();
		            },
					dataType:'json'
				});
			}else{
				$('#delete_teetime').val(0) ;
			}
		},
		errorLabelContainer: '#error_message_box',
 		wrapper: 'li'
	});

	$('#teetime_name').focus().select();
	$('#expand_players').click(function(e){
    	var checked = $(e.target).attr('checked');
    	if (checked)
    	{
	    	$('#teetime_people_table').show();
	    }
    	else
    	{
	    	$('#teetime_people_table').hide();
	    }
    	//var x = $('#teetime_form').height();
	    //var y = $('#teetime_form').width();

	    $.colorbox.resize();
    });
    for (var q = 1; q <= 5; q++) {
		// Save customer event listeners
	    $('#save_customer_'+q).click(function() {
	    	if ($(this).attr('checked'))
	        	$(this).prev().addClass('checked');
	        else
	   	    	$(this).prev().removeClass('checked');
	    });
	}
	$('#purchase_1').click(function () {$('#purchase_quantity').val(1);});
	$('#purchase_2').click(function () {$('#purchase_quantity').val(2);});
	$('#purchase_3').click(function () {$('#purchase_quantity').val(3);});
	$('#purchase_4').click(function () {$('#purchase_quantity').val(4);});
	$('#purchase_5').click(function () {$('#purchase_quantity').val(5);});
	$('#checkin_1').click(function () {$('#checkin').val(1);});
	$('#checkin_2').click(function () {$('#checkin').val(2);});
	$('#checkin_3').click(function () {$('#checkin').val(3);});
	$('#checkin_4').click(function () {$('#checkin').val(4);});
	$('#checkin_5').click(function () {$('#checkin').val(5);});
	$('#teetime_delete').click(function(){$('#delete_teetime').val(1);});
	$('#event_delete').click(function(){$('#delete_teetime').val(1);});
	$('#shotgun_delete').click(function(){$('#delete_teetime').val(1);});
	$('#closed_delete').click(function(){$('#delete_teetime').val(1);});
	// Button icons and styles
	$('#paid_carts').button();
	$('#paid_players').button();
	$('#teetime_save').button();
    $('#event_save').button();
    $('#closed_save').button();
    $('#shotgun_save').button();
    $('#shotgun_delete').button();
    $('#teetime_delete').button();
    $('#event_delete').button();
    $('#closed_delete').button();
    $('.purchase_button').parent().buttonset();
    $('#teetime_checkin').button();
    $('#teetime_holes_9').button();
    $('#event_holes_9').button();
    $('#shotgun_holes_9').button();
    $('#teetime_confirm').button();
    $('#teetime_cancel').button();
    $('.teebuttons').buttonset();
    $('.holes_buttonset').buttonset();
    $('#players_0').button();
    $('#carts_0').button();
    $('.players_buttonset').buttonset();
    $('.carts_buttonset').buttonset();
    $('#players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#players_0_label').children().css('color','#e9f4fe');

    // Teetime name settings
	$( '#teetime_title' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/'); ?>',
		delay: 10,
		autoFocus: true,
		minLength: 0,
		search: function( event, ui ) {
			$('#status_image').removeClass('status_red').removeClass('status_yellow').removeClass('status_green').removeClass('customer_status');
		},
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#customer_comment').html(ui.item.comments);
			if (ui.item.status_flag == 1) {
				$('#status_image').addClass('status_red').addClass('customer_status');	
			}
			else if (ui.item.status_flag == 2) {
				$('#status_image').addClass('status_yellow').addClass('customer_status');	
			}
			else if (ui.item.status_flag == 3) {
				$('#status_image').addClass('status_green').addClass('customer_status');	
			}
			$('#teetime_title').val(ui.item.label);
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id').val(ui.item.value);
			$('#zip').val(ui.item.zip);
			$('#price_class_1').val(ui.item.price_class);
			update_credit_card_dropdown(ui.item.value, $('#added_credit_card_id').val());
		},
		focus: function(event, ui) {
			event.preventDefault();
			var card_swipe = $('#teetime_title').data('card_swipe');

			console.log('focused!!!');
			console.log('card_swipe '+card_swipe);

			if (card_swipe != undefined && card_swipe == 1)
			{
				var autocomplete = $( this ).data( "autocomplete" );

				console.log('autocomplete--------------');

			    if ( !autocomplete.options.autoSelect || autocomplete.selectedItem ) { return; }

			    autocomplete.widget().children( ".ui-menu-item:first" ).each(function() {
			        var item = $( this ).data( "item.autocomplete" );
			        autocomplete.selectedItem = item;
			    });
			    if ( autocomplete.selectedItem ) {
			        autocomplete._trigger( "select", event, { item: autocomplete.selectedItem } );
			    }
			    $(this).autocomplete('close');
			    $('#teetime_title').data('card_swipe', 0);
			}

			//$('#teetime_title').val(ui.item.label);
		}
	});
	$( '#teetime_title_2' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_2').val(ui.item.label);
			$('#email_2').val(ui.item.email).removeClass('teetime_email');
			$('#phone_2').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_2').val(ui.item.value);
			$('#price_class_2').val(ui.item.price_class);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_2').val(ui.item.label);
		}
	});
	$( '#teetime_title_3' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_3').val(ui.item.label);
			$('#email_3').val(ui.item.email).removeClass('teetime_email');
			$('#phone_3').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_3').val(ui.item.value);
			$('#price_class_3').val(ui.item.price_class);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_3').val(ui.item.label);
		}
	});
	$( '#teetime_title_4' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_4').val(ui.item.label);
			$('#email_4').val(ui.item.email).removeClass('teetime_email');
			$('#phone_4').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_4').val(ui.item.value);
			$('#price_class_4').val(ui.item.price_class);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_4').val(ui.item.label);
		}
	});
	$( '#teetime_title_5' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_5').val(ui.item.label);
			$('#email_5').val(ui.item.email).removeClass('teetime_email');
			$('#phone_5').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_5').val(ui.item.value);
			$('#price_class_5').val(ui.item.price_class);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_5').val(ui.item.label);
		}
	});
	$('#teetime_title').swipeable({allow_enter:false, auto_complete:'#teetime_title'});
	$('#teetime_title_2').swipeable({allow_enter:false, auto_complete:'#teetime_title_2'});
	$('#teetime_title_3').swipeable({allow_enter:false, auto_complete:'#teetime_title_3'});
	$('#teetime_title_4').swipeable({allow_enter:false, auto_complete:'#teetime_title_4'});
	$('#teetime_title_5').swipeable({allow_enter:false, auto_complete:'#teetime_title_5'});
	$('#teetime_title, #teetime_title_2, #teetime_title_3, #teetime_title_4, #teetime_title_5').keyup(function(){
		var focus = $(this);
		if (focus.val() == '')
		{
			var index = focus.attr('id').replace('teetime_title_', '');
			index = (index == 'teetime_title' ? 1 :index);

			clear_player_data(index);
		}
	});

	$('#event_person').autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/last_name'); ?>',
		delay: 150,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			var data = {};
			data.name = ui.item.label;
			data.phone = ui.item.phone_number;
			data.email = ui.item.email;
			data.person_id = ui.item.value;

			// If person is already in list, do nothing
			if($('#event_person_'+data.person_id).length > 0){
				$('#event_person').val('');
				return false;
			}

			$.post('<?php echo site_url('teesheets/save_event_person/'.$teetime_info->TTID); ?>', {'person_id':data.person_id},
				function(response){

			},'json');

			var html = event_person_row(data);
			$('#event_people').append(html);
			$('h2 > span.count').text( parseInt($('h2 > span.count').text()) + 1);
			$.colorbox.resize();
			$('#event_person').val('');
		},
		focus: function(event, ui) {
			event.preventDefault();
		}
	});

	$('#event_people').delegate('span.delete', 'click', function(e){
		var button = $(this);
		var person_id = button.data('person-id');
		var row = button.parents('li');
		row.remove();
		$('h2 > span.count').text( $('h2 > span.count').text() - 1);

		$.post('<?php echo site_url('teesheets/delete_event_person/'.$teetime_info->TTID); ?>', {'person_id':person_id},
			function(response){

		},'json');

		e.preventDefault();
		return false;
	});

	$('#event_people').delegate('a.checkin', 'click', function(e){
		if($(this).hasClass('done')){
			return false;
		}
		var person_id = $(this).parents('li').find('input').val();
		$.post('<?php echo site_url('teesheets/event_checkin/'.$teetime_info->TTID); ?>', {'person_id':person_id},
			function(response){

		},'json');
		$(this).addClass('done').text('✔ Checked In');
		e.preventDefault();
	});

	$('#event_people').delegate('a.pay', 'click', function(e){
		if($(this).hasClass('done')){
			return false;
		}
		var data = {};
		data.person_id = $(this).parents('li').find('input').val();
		data.person_name = $(this).parents('li').find('span.name').text();
		data.price_class = $('#default_price_category').val();
		data.event_players = $('#event_players').val();
		data.purchase_quantity = 1;

		if($('#event_holes_9').attr('checked')){
			data.teetime_holes = 9;
		}else{
			data.teetime_holes = 18;
		}
		data.start = '<? echo $teetime_info->start; ?>';

		data.carts = 0;
		data.paid_carts = 0;
		if($('#default_cart_fee:checked').attr('checked')){
			data.carts = 1;
		}

		$.post('<?php echo site_url('teesheets/event_pay/'.$teetime_info->TTID); ?>', data, function(response){
			if(response.success){
				window.location = SITE_URL + '/sales';
			}
		},'json');
		e.preventDefault();
	});

	$('input.shotgun-person').autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/last_name'); ?>',
		delay: 150,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			var data = {};
			data.name = ui.item.label;
			data.phone = ui.item.phone_number;
			data.email = ui.item.email;
			data.person_id = ui.item.value;
			$(this).val(data.name);
			$(this).siblings('input[type="hidden"]').val(data.person_id);
			$(this).hide().val('').after('<span class="person">' + data.name + '<a href="#" class="delete" style="color: red;">Clear</a></span>');
		},
		focus: function(event, ui) {
			event.preventDefault();
		}
	});

	$('#shotgun_grid').delegate('span.person a.delete', 'click',  function(e){
		var person = $(this).parents('span.person');
		person.siblings('input[type="text"]').val('').show();
		person.siblings('input[type="hidden"]').val('');
		person.remove();
		e.preventDefault();
	});

	// Phone number events
    $('#phone').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $('#zip').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#phone' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.label);
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
		}

	});
    $('#phone_2').keypress(function (e) {
    	$('#save_customer_2').attr('checked', true);
    	$('#save_customer_2').prev().addClass('checked');
    });
    $( '#phone_2' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_2').val(ui.item.name).removeClass('teetime_name');
			$('#email_2').val(ui.item.email).removeClass('teetime_email');
			$('#phone_2').val(ui.item.label);
			$('#person_id_2').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_2').val(ui.item.label);
		}

	});
    $('#phone_3').keypress(function (e) {
    	$('#save_customer_3').attr('checked', true);
    	$('#save_customer_3').prev().addClass('checked');
    });
    $( '#phone_3' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_3').val(ui.item.name).removeClass('teetime_name');
			$('#email_3').val(ui.item.email).removeClass('teetime_email');
			$('#phone_3').val(ui.item.label);
			$('#person_id_3').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_3').val(ui.item.label);
		}

	});
    $('#phone_4').keypress(function (e) {
    	$('#save_customer_4').attr('checked', true);
    	$('#save_customer_4').prev().addClass('checked');
    });
    $( '#phone_4' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_4').val(ui.item.name).removeClass('teetime_name');
			$('#email_4').val(ui.item.email).removeClass('teetime_email');
			$('#phone_4').val(ui.item.label);
			$('#person_id_4').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_4').val(ui.item.label);
		}

	});
    $('#phone_5').keypress(function (e) {
    	$('#save_customer_5').attr('checked', true);
    	$('#save_customer_5').prev().addClass('checked');
    });
    $( '#phone_5' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_5').val(ui.item.name).removeClass('teetime_name');
			$('#email_5').val(ui.item.email).removeClass('teetime_email');
			$('#phone_5').val(ui.item.label);
			$('#person_id_5').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_5').val(ui.item.label);
		}

	});
	// Email
	$('#email').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#email' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email').val(ui.item.label);
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email').val(ui.item.label);
		}

	});

	$('#email_2').keypress(function (e) {
    	$('#save_customer_2').attr('checked', true);
    	$('#save_customer_2').prev().addClass('checked');
    });
    $( '#email_2' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_2').val(ui.item.name).removeClass('teetime_name');
			$('#phone_2').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_2').val(ui.item.label);
			$('#person_id_2').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_2').val(ui.item.label);
		}

	});
	$('#email_3').keypress(function (e) {
    	$('#save_customer_3').attr('checked', true);
    	$('#save_customer_3').prev().addClass('checked');
    });
    $( '#email_3' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_3').val(ui.item.name).removeClass('teetime_name');
			$('#phone_3').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_3').val(ui.item.label);
			$('#person_id_3').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_3').val(ui.item.label);
		}

	});
	$('#email_4').keypress(function (e) {
    	$('#save_customer_4').attr('checked', true);
    	$('#save_customer_4').prev().addClass('checked');
    });
    $( '#email_4' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_4').val(ui.item.name).removeClass('teetime_name');
			$('#phone_4').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_4').val(ui.item.label);
			$('#person_id_4').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_4').val(ui.item.label);
		}

	});
	$('#email_5').keypress(function (e) {
    	$('#save_customer_5').attr('checked', true);
    	$('#save_customer_5').prev().addClass('checked');
    });
    $( '#email_5' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)	{
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			$('#teetime_title_5').val(ui.item.name).removeClass('teetime_name');
			$('#phone_5').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_5').val(ui.item.label);
			$('#person_id_5').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_5').val(ui.item.label);
		}
	});

    //Calendar_actions.event.make_styled_buttons();


	// CODE BELOW REQUIRES REVIEW

    $('#type_teetime').click(function(){
    	$('#teetime_table').show();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('teetime_title');
    });
    $('#type_tournament').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_league').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_event').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_closed').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').show();
    	$('#event_table').hide();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('closed_title');
    });
    $('#type_shotgun').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$('#shotgun_table').show();
    	$.colorbox.resize();
    	focus_on_title('shotgun_title');
    });

	$('.teetime_name').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_name');
	});
    $('.teetime_phone').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_phone');
	});
	$('.teetime_email').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_email');
	});
    $('.teetime_details').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_details');
	});
    $('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
});
</script>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?php echo $course_name ?> - Reservation Cancelled</title>
	<link href="<?php echo base_url(); ?>/css/dist/reservations.min.css" type="text/css" rel="stylesheet" />
	<style>
	body {
		background: url('<?php echo base_url(); ?>/images/grid_noise.png');
	}
	
	#unavailable {
		background-color: white;
		border: 1px solid #E0E0E0;
		padding: 20px;
	}
	
	h1 {
		text-align: center;
		line-height: 1em;
		margin: 0 0 0.5em 0;
	}
	
	div.well h3 {
		margin-top: 0px;
		margin-bottom: 1em;
	}
	
	label {
		display: block;
		float: left;
		width: 100px;
	}
	
	div.field {
		float: left;
	}
	
	div.col-md-6 {
		overflow: hidden;
	}
	
	.module {
		box-shadow: 1px 1px 1px 0px #CCC;
		border-top: 1px solid #DFDFDF; 
		border-left: 1px solid #DFDFDF;
		background-color: #FFFFFF;
		padding-bottom: 4em;
	}	
	</style>	
</head>
<body>
<nav class="navbar navbar-default" role="navigation" id="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo site_url('booking/'.$course_id); ?>" title="Go to online booking"><?php echo $course_name ?></a>
		</div>
	</div>
</nav>	
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if($success){ ?>
			<h1 class="text-success">Reservation Cancelled</h1>	
			<?php }else{ ?>
			<h1 class="text-danger">Cancellation Failed</h1>		
			<?php } ?>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<?php if($success){ ?>
			<p class="alert alert-success">Your reservation was cancelled successfully.</p>	
			<?php }else{ ?>
			<p class="alert alert-danger">There was a problem cancelling your reservation. Please contact ForeUp support at <a href="tel:(800) 929-5737">(800) 929-5737</a></p>
			<?php } ?>
		</div>
	</div>	
</div>
</body>
</html>

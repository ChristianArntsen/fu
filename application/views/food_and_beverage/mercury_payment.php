<script>
var data = {};
data.Payment_ID = '<?php echo $PaymentID; ?>';
var $ = window.parent.$;
var App = window.parent.App;
var wp = window.parent;

$.post(App.api_table + 'receipts/<?php echo $receipt_id; ?>/payments', data, function(payment){
	
	if(payment.sale_id){
		App.receipt.sale_id = "POS " + payment.sale_id;
	}
	// BUILD CREDIT CARD RECEIPT
	var receipt = App.receipts.get(<?php echo $receipt_id; ?>)

	// PRINT CREDIT CARD RECEIPT
	<?php if ($this->config->item('print_credit_card_receipt')) { ?>
		wp.print_mercury_signature_slip(payment);
	<?php } ?>
	
	// Add payment to receipt
	var receipt = App.receipts.get(<?php echo $receipt_id; ?>);
	if(receipt.get('payments').get(payment.type)){
		receipt.get('payments').get(payment.type).set(payment);
	}else{
		receipt.get('payments').add(payment);
	}
		
	if(receipt.isPaid()){
		receipt.printReceipt('sale');
		$.colorbox2.close();
	}

	// Close table (if everything is paid)
	App.closeTable();
	
},'json');
</script>

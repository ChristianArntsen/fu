<?php $this->load->view("partial/header");
$printer_name = $this->config->item('updated_printing')?'foreup':'foreup';
?>
<style>
#menubar_stats {
	overflow: hidden;
}

#menu_button_holder {
	width: 325px !important;
}

#menubar_stats {
	width: 700px !important;
}

#user_button_holder {
	padding: 0px;
	padding-right: 10px !important;
}

#stats_button_holder {
	width: auto !important;
}

#menubar_stats h2, #menubar_stats h3 {
	text-shadow: 1px 1px 3px #000 !important;
	font-size: 36px;
	float: left;
	margin-right: 30px;
	font-weight: normal;
}

#menubar_stats h3 {
	font-size: 30px;
	margin-top: 4px;
}

div.form_field label {
	line-height: 35px;
	padding: 0px 5px;
	font-size: 16px;
}

div.row {
	padding: 20px;
	overflow: hidden;
}

div.pay_box {
	display: block;
	float: none;
	overflow: hidden;
}

#cart_section {
	overflow: hidden;
}

#cbox2Content {
	background: none repeat scroll 0 0 #EFEFEF;
}

#cbox2title {
    background: -moz-linear-gradient(center top , #3CACD2, #3090C3, #2474B4) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-radius: 0 0 0 0;
    border-top: 1px solid #9FD7E9;
    color: #FFFFFF;
    font-size: 16px;
    font-weight: lighter;
    padding-left: 20px;
    text-align: left;
    text-shadow: -1px -1px 0 #000000;
}

#splitpayment_register .reg_item_seat {
	width: 6%;
}

#splitpayment_register .reg_item_name {
	width: 42%;
}

th.reg_item_name {
    width: 38%;
}

th.reg_item_seat, td.reg_item_seat {
	width: 10%;
	background-color: #F0F0F0;
	font-weight: bold;
}

th.reg_item_seat {
	text-indent: 0px !important;
}

th.reg_item_seat > div {
	margin-left: -6px;
}

td > a.edit_item.fnb_button,
td > a.send_message.fnb_button
 {
	margin: 2px !important;
	padding: 0px !important;
	line-height: 34px !important;
	height: 34px !important;
	float: right !important;
	width: 65px !important;
	text-align: center !important;
}

#register td.reg_item_total {
	padding-right: 0px;
	padding: 2px;
}

#register td {
	vertical-align: top !important;
	padding-top: 2px;
}

#register_box {
	max-height: 700px;
}

tbody > tr > td * {
	font-size: 14px;
}

tbody > tr.selected {
	background-color: #BED8FF !important;
}

tbody > tr.incomplete td, tbody > tr.selected.incomplete td {
	background-color: #C61F1F;
}

tbody > tr.incomplete * {
	color: white !important;
}

tbody > tr.ordered, tbody > tr.paid {
	background-color: #E5E5E5 !important;
}

tbody > tr.ordered a.edit_item,
tbody > tr.paid a.edit_item {
	display: none !important;
}

tbody > tr a.fnb_button.send_message {
	display: none !important;
}

tbody > tr.ordered a.fnb_button.send_message {
	display: block !important;
}

tbody td.reg_item_name > div {
	font-weight: bold;
}

tbody td.reg_item_name ul.modifiers {
	display: block;
	margin: 0px;
	padding: 0px;
}

ul.modifiers > li {
	padding: 4px 0px 4px 0px;
	font-size: 12px;
	line-height: 14px;
	display: block;
}

/* Table layout styles */
#layout_editor {
	display: block;
	position: relative;
	padding: 0px;
	margin: 0px;
	margin-top: 1px;
	height: 616px;
	width: auto;
	background: none;
	overflow: hidden;
}

#layout_editor div.palette {
	right: 0px;
	width: 150px;
	height: 616px;
	padding-top: 15px;
	background: white;
	border-left: 1px solid #DDD;
	z-index: 1;
	float: right;
}

#layout_editor div.tab_content {
	width: 984px;
	height: 590px;
	display: block;
	overflow: hidden;
	float: left;
}

#layout_editor div.tab_content > div {
	display: none;
}

#layout_editor div.tab_content > div.active {
	display: block;
}

div.palette > div.drag {
	margin: 25px auto 0px auto;
}

div.floor_layout > div.drag {
	position: absolute !important;
}

div.object {
	width: 75px;
	height: 75px;
}

div.object > span.label,
div.object > span.employee,
div.object > span.name,
div.object > input {
	display: block;
	text-align: center;
	top: 50%;
	width: 100%;
	float: none;
	position: absolute;
	margin-top: -7px;
}

div.object > input {
	margin: -12px auto 0px auto !important;
	padding: 2px !important;
	float: none;
	display: block;
	left: 15%;
	right: 15%;
}

div.object > span.label {
	font-weight: bold;
	font-size: 14px;
}

div.object > span.employee {
	margin-top: 14px;
}

div.object > span.name {
	top: 25%;
}

div.object > input {
	width: 60% !important;
	color: black;
	margin: 0 auto;
	margin-top: -12px;
}

div.object:hover > a.delete-object {
	display: block;
}

div.palette div.object:hover > a.delete-object {
	display: none !important;
}

div.object > a.delete-object {
	display: none;
	position: absolute;
	font-size: 20px;
	background-color: white;
	border: 1px solid #CCC;
	padding: 0px 5px 0px 5px;
	line-height: 20px;
	color: red;
	top: -4px;
	left: -4px;
}

/* Table SVG Styles */
div.object:hover {
	cursor: pointer;
}

div.object:hover .table,
div.object:hover .seat  {
	/*fill: red; */
}

div.object .table {
	fill: #FFFFFF;
}

div.object .seat {
	fill: #FFFFFF;
}

div.object.my-active .table,
div.object.my-active .seat {
	fill: #3483C9 !important;
}

div.object.my-active div.box {
	background-color: #3483C9 !important;
}

div.object.my-active span {
	color: white;
}

div.object.other-active span.label {
	color: #444
}

div.object.other-active .table,
div.object.other-active .seat {
	fill: #EFEFEF !important;
}

div.object.other-active div.box {
	background-color: #EFEFEF !important;
}

div.object > div.box {
	display: block;
	float: none;
	position: absolute;
	padding: 0px;
	margin: 0px;
	background-color: white;
	border: 1px solid black;
	top: 0px;
	left: 0px;
	right: 0px;
	bottom: 0px;
	width: 100%;
}

div.object.drag {
	width: 75px;
	height: 75px;
	position: relative;
}

div.floor_layout > div.object {
	background-size: 100% 100%;
	background-position: center center;
	background-repeat: no-repeat;
}

#layout_editor div.booth {
	background-image: url('images/restaurant_layout/booth.svg');
	width: 100px;
	height: 100px;
	display: block;
}

#layout_editor div.table_square_4 {
	background-image: url('images/restaurant_layout/table_square_4.svg');
	width: 100px;
	height: 100px;
	display: block;
}

#layout_editor div.table_round_4 {
	background-image: url('images/restaurant_layout/table_round_4.svg');
	width: 100px;
	height: 100px;
	display: block;
}

div.floor_layout {
	width: auto;
	height: 590px;
	display: block;
	background: none;
	z-index: 5;
	overflow: hidden;
}

/* Layout tabs */
#layout_editor ul.tabs {
	display: block;
	width: auto;
	height: 60px;
	background-color: #AAA;
	overflow: hidden;
	margin: 0px;
	padding: 0px;
}

#layout_editor ul.tabs > li.tab {
	width: auto;
	display: block;
	float: left;
	list-style-type: none;
	margin: 14px 10px 0px 0px;
	padding: 0px;
	position: relative;
}

#layout_editor ul.tabs > li.tab > a.tab {
	display: block;
	background-color: #E0E0E0;
	width: auto;
	padding: 15px 15px;
	font-size: 14px;
	color: #333;
	text-decoration: none;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
}

#layout_editor ul.tabs > li.tab > a.edit {
	position: absolute;
	top: 5px;
	left: 5px;
	margin: 0px;
	display: block;
	padding: 0px;
}

#layout_editor ul.tabs > li.tab.active > a.tab {
	background-color: #EFEFEF;
	font-weight: bold;
	box-shadow: 1px 1px 3px #555;
}

#layout_editor .button {
	display: block;
	float: left;
	margin: 0px 5px 0px 0px;
	background: #349AC5;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top, #349AC5, #4173B3);
	color: white;
	font-size: 14px;
	font-weight: normal;
	height: 32px;
	line-height: 32px;
	padding: 0px 20px;
	width: auto;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}

#layout_editor .button.new {
	float: left !important;
	margin-top: 15px;
	background: #4dad47;
	background: -moz-linear-gradient(top,  #4dad47 0%, #398235 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4dad47), color-stop(100%,#398235));
	background: -webkit-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -o-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -ms-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: linear-gradient(to bottom,  #4dad47 0%,#398235 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4dad47', endColorstr='#398235',GradientType=0 );
}

#colorbox input, #colorbox2 input {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #B1B1B1;
    border-radius: 5px;
    box-shadow: 0 2px 12px -6px #000000 inset;
    color: #666464;
    font-family: "Quicksand", Helvetica, sans-serif;
    font-size: 20px;
    font-weight: lighter;
    line-height: 20px;
    padding: 5px;
}

#recent_transactions {
	display: block !important;
	width: auto !important;
	overflow: hidden !important;
	max-height: none !important;
	background: none !important;
	padding: 10px !important;
}

#recent_transactions > li {
	background: white;
	display: block;
	border: 1px solid #E0E0E0;
	padding: 5px 8px;
	margin: 5px 0px 0px 0px;
	width: auto;
	overflow: hidden;
}

#recent_transactions > li * {
	font-size: 14px;
	line-height: 18px;
}

#recent_transactions > li div.info {
	display: block;
	float: left;
	overflow: hidden;
}

#recent_transactions > li div.info span.label {
	display: block;
	float: left;
	width: 70px;
	padding-right: 10px;
	text-align: right;
}

#recent_transactions > li div.info > div {
	display: block;
	overflow: hidden;
}

#recent_transactions > li h3 {
	font-size: 24px;
	line-height: 28px;
	color: #222;
	display: block;
}

#recent_transactions > li h5 {
	font-size: 16px;
	line-height: 20px;
	color: #666;
	font-weight: normal;
	display: block;
}

/** Keypad Styles **/
div.keypad {
	overflow: hidden;
	position: absolute;
	height: 190px;
	padding-top: 10px;
	width: 280px;
	background-color: white;
	box-shadow: 2px 2px 8px #222;
	z-index: 999999;
}

div.number_pad {
	overflow: hidden;
	margin: 0px auto;
	padding: 0px;
	width: 260px;
	display: block;
}

div.number_pad > a {
    cursor: pointer;
    display: block;
    float: left;
    height: 43px;
    width: 84px;
    color: white;
    font-size: 20px;
    font-weight: bold;
    text-align: center;
    line-height: 45px;
    padding: 0px;
    text-shadow: 0px 1px 1px #333;
    border-right: 1px solid rgba(0,0,0,0.25);
    border-bottom: 1px solid rgba(0,0,0,0.5);
    border-top: 1px solid rgba(255,255,255,0.5);

	background: #717882;
	background: -moz-linear-gradient(top,  #717882 0%, #4c5260 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#717882), color-stop(100%,#4c5260));
	background: -webkit-linear-gradient(top,  #717882 0%,#4c5260 100%);
	background: -o-linear-gradient(top,  #717882 0%,#4c5260 100%);
	background: -ms-linear-gradient(top,  #717882 0%,#4c5260 100%);
	background: linear-gradient(to bottom,  #717882 0%,#4c5260 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#717882', endColorstr='#4c5260',GradientType=0 );
}

div.number_pad > a.special {
	background: #dadbe0;
	background: -moz-linear-gradient(top,  #dadbe0 0%, #b3b6bd 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#dadbe0), color-stop(100%,#b3b6bd));
	background: -webkit-linear-gradient(top,  #dadbe0 0%,#b3b6bd 100%);
	background: -o-linear-gradient(top,  #dadbe0 0%,#b3b6bd 100%);
	background: -ms-linear-gradient(top,  #dadbe0 0%,#b3b6bd 100%);
	background: linear-gradient(to bottom,  #dadbe0 0%,#b3b6bd 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#dadbe0', endColorstr='#b3b6bd',GradientType=0 );
	text-shadow: none;
	color: #444;
}

div.keypad input {
	margin: 10px;
	display: block !important;
	width: auto !important;
}

#sale_details {
	float: none;
	display: block;
	width: auto;
}

a.fnb_button.payment-customer {
	width: 200px !important;
	display: block !important;
}

#food_and_beverage button.delete_button, #food_and_beverage a.delete_button {
	background: #d14d4d;
	background: -moz-linear-gradient(top,  #d14d4d 0%, #c03939 100%) !important;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d14d4d), color-stop(100%,#c03939));
	background: -webkit-linear-gradient(top,  #d14d4d 0%,#c03939 100%);
	background: -o-linear-gradient(top,  #d14d4d 0%,#c03939 100%);
	background: -ms-linear-gradient(top,  #d14d4d 0%,#c03939 100%);
	background: linear-gradient(to bottom,  #d14d4d 0%,#c03939 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d14d4d', endColorstr='#c03939',GradientType=0 );
    float: left;
}

#cboxClose, #cbox2Close {
	width: 30px;
	height: 30px;
	position: absolute;
	border-radius: 25px;
	background: none white;
	box-shadow: 1px 1px 5px rgba(0,0,0,0.25);
	top: 2px;
	right: 6px;
	border: 2px solid #2677B6;
	color: #2677B6 !important;
	font-weight: bold !important;
	text-align: center !important;
	font-size: 24px !important;
	float: none !important;
	text-indent: 0 !important;
}
</style>
<script>
$.colorbox.settings.close = '&times;';
$.colorbox.settings.closeButton = true;
$.colorbox2.settings.close = '&times;';
$.colorbox2.settings.closeButton = true;

function set_feedback(text, classname, keep_displayed, time_override)
{
	if(text!='' && text!=undefined)
	{
		$('#feedback_bar').removeClass();
		$('#feedback_bar').addClass(classname);
		$('#feedback_bar').html(text+"<span class='message_close_box'></span>");
		$('#feedback_bar').slideDown(250);
		var text_length = text.length;
		var text_lengthx = 0;
		if (time_override != ''&& time_override != undefined)
			text_lengthx = time_override;
		else
			text_lengthx = text_length*50;

		if(!keep_displayed)
		{
			$('#feedback_bar').show();

			setTimeout(function()
			{
				$('#feedback_bar').slideUp(250, function()
				{
					$('#feedback_bar').removeClass();
				});
			},text_lengthx);
		}
	}
	else
	{
		$('#feedback_bar').hide();
	}
}

function add_white_space(str_one, str_two, width)
{
       var width = width == undefined ? 46 : width;
       var strlen_one = str_one.length;
       var strlen_two = str_two.length;
       var white_space = '';
       var white_space_length = 0;
       if (strlen_one + strlen_two >= width)
               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
       else
               white_space_length = width - (strlen_one + strlen_two);

       for (var i = 0; i < white_space_length; i++)
               white_space += ' ';
       return str_one+white_space+str_two;
}

// JQuery - Keypad plugin
(function ($) {

	var template = '<div class="keypad" style="display: none; position: absolute;">' +
		'<div class="number_pad">' +
			'<a data-char="1">1</a>' +
			'<a data-char="2">2</a>' +
			'<a data-char="3">3</a>' +
			'<a data-char="4">4</a>' +
			'<a data-char="5">5</a>' +
			'<a data-char="6">6</a>' +
			'<a data-char="7">7</a>' +
			'<a data-char="8">8</a>' +
			'<a data-char="9">9</a>' +
			'<a class="special" data-char="enter">Enter</a>' +
			'<a data-char="0">0</a>' +
			'<a class="special" data-char="del">&larr;</a>' +
		'</div>' +
	'</div>';

	function format_number(chars, formatMoney){
		var number = chars.replace(/[^0-9]+/g, '');

		if(formatMoney){
			number = number / 100;
			var number = accounting.formatMoney(number, '');
		}
		return number;
	}

	function add_character(input, character){
		var characters = input.val() + character;
		var formatted = format_number(characters, input.data('settings').formatMoney);

		input.val(formatted);
		input.focus();
		return true;
	}

	function remove_character(input){
		var characters = input.val();
		characters = characters.substring(0, characters.length - 1);
		var formatted = format_number(characters,  input.data('settings').formatMoney);

		input.val(formatted);
		input.focus();
		return true;
	}

	function close(keypad){
		if($.fn.keypad.currentField){
			$.fn.keypad.currentField.trigger('keypad_close');
		}
		keypad.hide();
	}

	function focus_on_end(input){
		// If this function exists...
		if (input[0].setSelectionRange) {
		  // ... then use it (Doesn't work in IE)

		  // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
		  var len = input.val().length * 2;
		  input[0].setSelectionRange(len, len);

		} else {
			// ... otherwise replace the contents with itself
			// (Doesn't work in Google Chrome)
			input.val(input.val());
		}

		// Scroll to the bottom, in case we're in a tall textarea
		// (Necessary for Firefox and Google Chrome)
		input.scrollTop = 999999;
	}

	$.fn.keypad = function( options ) {

        // Default options
        var settings = $.extend({
            position: 'left',
            formatMoney: true,
            closeOnClick: true
        }, options );

        $(document).on('click', function(e){
 			if(!$(e.target).hasClass('has-keypad')
				&& !$(e.target).hasClass('keypad')
				&& !$(e.target).hasClass('number_pad_number')){

				if(keypad.is(':visible') && settings.closeOnClick){
					$.fn.keypad.currentField.trigger('keypad_close');
					close($('div.keypad'));

					if($.fn.keypad.currentField && $.fn.keypad.currentField.val() == ''){
						$.fn.keypad.currentField.val( $.fn.keypad.currentField.data('last-value') );
					}
				}
			}
		});

		// Place hidden keypad in page body
		var keypad = $(template);
		keypad.attr('id', 'jquery_keypad');
		$('#jquery_keypad').remove();
		$('body').append(keypad);

		keypad.on('click', 'a', function(e){
			var character = $(this).attr('data-char');
			var currentField = $.fn.keypad.currentField;
			if(character == 'enter'){
				close(keypad);

			}else if(character == 'del'){
				remove_character(currentField);

			}else{
				add_character(currentField, character);
			}
			if (currentField.hasClass('register_count')) {
				calculate_closing_amount();
			}
			return false;
		});

        // Attach keypad code to element on page
        return this.each(function(){

			$(this).addClass('has-keypad');
			$(this).data('settings', settings);

			$(this).click(function(e){
				$.fn.keypad.currentField = $(this);
				$(this).data('last-value', $(this).val()).val('');

				var height = parseInt($(this).height(), 10);
				var width = parseInt($(this).width(), 10);
				var padY = parseInt($(this).css('padding-top'), 10) + parseInt($(this).css('padding-bottom'), 10);
				var padX = parseInt($(this).css('padding-left'), 10) + parseInt($(this).css('padding-right'), 10);

				if(settings.position == 'right'){
					var top = $(this).offset().top + height + padY - 200;
					var left = $(this).offset().left + width + padX + 5;

				}else if(settings.position == 'bottom-right'){
					var top = $(this).offset().top;
					var left = $(this).offset().left + width + padX + 5;

				}else if(settings.position == 'left'){
					var top = $(this).offset().top + height + padY - 200;
					var left = $(this).offset().left - 200 - width + padX + 5;

				}else if(settings.position == 'bottom'){
					var top = $(this).offset().top + height + padY + 5;
					var left = $(this).offset().left + padX - 10;

				}else if(settings.position == 'top'){
					var top = $(this).offset().top + padY - 5;
					var left = $(this).offset().left + padX - 10;
				}

				$('div.keypad').css({'top':top, 'left':left}).show();
				focus_on_end( $(this) );
			});

			$(this).on('focus', function(event){
				focus_on_end( $(this) );
			});
		});
    };
}( jQuery ));
</script>
<script type='text/javascript' src='js/keyboard/jquery.keyboard.js'></script>
<script type='text/javascript' src='js/keyboard/qwerty.js'></script>
<script type='text/javascript' src='js/keyboard/form.js'></script>
<script type='text/javascript' src='js/keyboard/jquery.selection.js'></script>
<script type='text/javascript' src='js/moment.min.js'></script>

<script type='text/javascript' src='js/fnb.js'></script>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fnb.css?<?php echo APPLICATION_VERSION; ?>" />
<div style='display: none;'>
	<div id='fnb_login_container'>
		<?php $this->load->view('food_and_beverage/login');?>
	</div>
</div>
<?php
// If not logged in to F&B, display login window
if (!$this->session->userdata('fnb_logged_in')) { ?>
<script>
$(document).ready(function(){
	fnb.login.show();
});
</script>

<?php
// If no table is selected, show table layout
} else if (empty($table_number)){ ?>
<script>
	$(document).ready(function(){
		fnb.show_tables();
	});
</script>
<?php }else if(!empty($table_number)){ ?>
<script>
$(document).ready(function(){
	var tableOpen = moment('<?php echo $sale_time; ?>');
	$('#menubar_stats').html('<h2 class="table_num">Table #<?php echo $table_number; ?></h2>'+
		'<h3>Seated ' + tableOpen.fromNow() + ' (' + tableOpen.format('h:mma') + ')</h3>');
});
</script>
<?php } ?>

<style>
#attached_customer {
	display: block;
	float: none;
	overflow: hidden;
	width: auto;
	padding: 0px;
	margin: 0px;
}

.customer-header-bar {
	background: #E0E0E0;
	height: 37px;
	padding: 0px;
	margin: 0px;
	line-height: 37px;
	color: #444;
	font-size: 16px;
	padding: 0px 10px 0px 10px;
}

#customer_search_form {
	display: block;
	padding: 10px;
}

#customer_search_form input {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #B1B1B1;
	border-radius: 5px;
	box-shadow: 0 2px 12px -6px #000000 inset;
	color: #666464;
	font-family: "Quicksand", Helvetica, sans-serif;
	font-size: 20px;
	font-weight: lighter;
	line-height: 25px;
	padding: 5px;
	width: 180px;
}

#table_name {
	width: 140px;
	float: right;
}

#attached_customers {
	display: block;
	margin: 0px;
	padding: 0px 0px 15px 0px;
}

#attached_customers li {
	list-style-type: none;
	display: block;
	padding: 10px;
	margin: 0px;
	border-bottom: 1px solid #E0E0E0;
	overflow: hidden;
}

#attached_customers li a.remove {
	width: 75px !important;
	height: 35px !important;
	line-height: 35px !important;
	padding: 0px !important;
	text-align: center;
}

#attached_customers li.empty {
	font-size: 18px;
	font-weight: normal;
	color: #E0E0E0;
	text-align: left;
	line-height: 18px;
	border-bottom: none;
}

#attached_customers li:last-child {
	border-bottom: none !important;
}

ul.ui-autocomplete li.ui-menu-item {
	font-size: 16px;
	padding: 2px;
}

img.photo {
	float: left;
	width: 75px;
	height: 75px;
	margin-right: 15px;
	border-radius: 300px;
	border-color: #FFFFFF;
}

div.customer-info {
	float: left;
	min-height: 80px;
	overflow: hidden;
}

div.customer-info h4 {
	font-size: 20px;
	color: #333;
}

div.customer-info h4, div.customer-info > span {
	display: block;
	margin-bottom: 2px;
}

div.customer-info span.balance {
	float: right;
	text-align: right;
	display: block;
}
div.customer-info span.balance.negative {
	color: #C13A3A;
}

div.categories {
	width: 136px;
	float: left;
	margin-right: 4px;
	overflow: hidden;
	border-right: 1px solid #888;
}

div.sub-categories {
	float: left;
	overflow: hidden;
	width: 600px;
}

div.item-list {
	padding: 10px;
	display: block;
	overflow: hidden;
}

#food_and_beverage button.fnb_button {
	font-weight: normal;
	width: 130px;
	padding: 5px 10px;
	margin: 0px 5px 10px 0px;
	cursor: pointer;
    height: 55px !important;
    line-height: 16px !important;
}

button.fnb_button.subcategory {
	background: #36c17c !important;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#36c17c', endColorstr='#42b263') !important;
	background: -webkit-linear-gradient(top, #36c17c, #42b263) !important;
	background: -moz-linear-gradient(top, #36c17c, #42b263) !important;
}

button.fnb_button.selected {
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2), inset 0px 3px 66px 3px black !important;
}

div.quantity_buttons button.add {
	margin-left: 5px;
}

div.quantity_buttons button {
	display: block;
	float: left;
	margin: 0px;
	font-size: 16px;
	font-weight: bold;
	text-align: center;
	width: 40px;
}
</style>

<div id="feedback_bar"></div>
<div id='sales_register_holder'>
	<div id='cart_section'>
		<div class='pay_box'>
			<button id="send_to_kitchen" class="fnb_button">Send to Kitchen</button>
			<button id='payments_button' class='fnb_button'>Pay Now</button>
			<button id='open_cash_drawer' class='fnb_button fnb_open_cash_drawer'>Cash Drawer</button>
		</div>
		<div id="register_container" class="sales">
			<table>
		    	<tr>
		    		<td id="register_items_container" class='<?=$mode?>'>
						<div id="table_top" class="table_top_sale"></div>
						<!-- Register area -->
						<div class='fixed_top_table'>
							<div class='header-background'></div>

						<div id="register_holder">
							<div id="register_box">
								<table id="register" class="items">
									<!-- table header with labels -->
									<thead>
										<tr>
											<th class="reg_item_seat" id="reg_item_seat"><div class="header_cell header">Seat<span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_name" id="reg_item_name"><div class="header_cell header"><?php echo lang('sales_item_name'); ?><span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_price" id="reg_item_price"><div class="header_cell header"><?php echo lang('sales_price'); ?><span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_discount" id="reg_item_discount"><div class="header_cell header"><?php echo lang('sales_discount'); ?><span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_total" id="reg_item_total"><div class="header_cell header">&nbsp;<span class="sortArrow">&nbsp;</span></div></th>
										</tr>
									</thead>
									<!-- Items in the cart -->
									<tbody id="cart_contents">
									</tbody>
								</table>
							</div>

						    <div id='sale_details'>
				            	<table>
				            		<tr>
				            			<td>
							            	<div class='subtotal_box'>
					                        	<div class="left float"><?php echo lang('sales_sub_total'); ?>:</div>
						                        <div id="basket_total" class="right float"><?php echo to_currency($basket_subtotal); ?></div>
						                        <div class='clear'></div>
					                        </div>
					                        <div id="items_in_basket" class="left"><?php echo $items_in_basket; ?> Items</div>
							            </td>
						                <td>
							                <div id='taxes_holder'>
						                        <?php foreach($basket_taxes as $name=>$value) { ?>
						                        <div>
						                            <div class="right register_taxes"><?php echo to_currency($value); ?></div>
						                            <div class="left register_taxes"><?php echo $name; ?></div>
						                            <div class='clear'></div>
						                        </div>
						                        <?php }; ?>
							                </div>
							                <div id='taxable_box'>
							                	<div class='right'><?php echo form_checkbox(array('name'=>'is_taxable', 'id'=>'is_taxable', 'checked'=>$taxable_checked, 'onclick'=>"fnb.toggle_taxable(this)", "$taxable_disabled"=>"$taxable_disabled")); ?></div>
							                	<div class="left"><?php echo lang('sales_taxable'); ?></div>
							                    <div class='clear'></div>
						                    </div>
							                <div id="register_total">
					                            <div id="basket_final_total" class="right"><?php echo to_currency($basket_total); ?></div>
						                        <div class="left"><?php echo lang('sales_total'); ?>:</div>
					                        </div>
						                </td>
			          				</tr>
			          			</table>
				            </div>

							<div id='attached_customer'>
								<h3 class="customer-header-bar">Customers</h3>
								<form name="customer_search" id='customer_search_form' autocomplete="off">
									<input type="text" id="customer_search" name="search" value="" placeholder="Search customers..." autocomplete="off" />
									<input type="text" id="table_name" name="table_name" value="<?php echo htmlentities($name, ENT_QUOTES); ?>" placeholder="Table name" autocomplete="off" />
								</form>
								<ul id="attached_customers">
									<li class="empty">No customers attached</li>
								</ul>
							</div>
						</div>
						</div>
					</td>
				</tr>
			</table>
			<div id='totals_box'>
			</div>
		</div>
	</div>

	<!-- Start primary column -->
	<div class='main_column'>
		<div class='pay_box'>
			<button class='fnb_button' id='suspend_sale_button'>
				<?=lang('food_and_beverage_select_table');?>
			</button>
			<?php if($this->config->item('track_cash')) { ?>
			<button class='fnb_button fnb_close_register' id='close_register'>
				<?php echo lang('sales_close_register'); ?>
			</button>
			<?php } ?>
			<button class='fnb_button fnb_logout' id='fnb_logout'>
				Logout
			</button>
			<button class='fnb_button fnb_recent_transactions' id='recent_transactions_button'>
				Recent<br />Transactions
			</button>
			<button class='fnb_button delete_button' id='cancel_sale' style="float: right;">
				Cancel Sale
			</button>
		</div>

		<div class='item_menus'>
			<div class="categories">
				<?php foreach($items as $category => $subcats){
				$cat_hash = substr(md5($category), 0, 8); ?>

				<button id='section_<?php echo $cat_hash; ?>' data-category="<?php echo $cat_hash; ?>" class="menu_section_button fnb_button">
					<?php echo $category; ?>
				</button>
				<?php } ?>
			</div>

			<div class="sub-categories">
				<?php foreach($items as $category => $subcats){
				$cat_hash = substr(md5($category), 0, 8); ?>
				<div id='section_<?php echo $cat_hash; ?>_content' style="display: none;">

					<?php foreach($subcats as $subcat => $items){

					// If subcategory is blank, just display the items directly on page
					if($subcat == '-Uncategorized-'){
						foreach($items as $item){ ?>
						<button id="item_<?php echo $item['item_id']; ?>" class="fnb_button menu_item" data-item-id="<?php echo $item['item_id']; ?>" style="float: left;">
							<?php echo $item['name']; ?>
						</button>

					<?php } }else{
						$subcat_hash = substr(md5($subcat), 0, 8); ?>
						<button id="subcategory_<?php echo $subcatHash; ?>" class="fnb_button subcategory" data-subcategory="<?php echo $subcat; ?>" data-category="<?php echo $category; ?>" style="float: left;">
							<?php echo $subcat; ?>
						</button>
				<?php } } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script>
var giftcard_swipe = false;
sale_id = <?php echo (int) $sale_id; ?>;

$(document).ready(function(){

	// Show sub-category buttons
	$('.menu_section_button').click(function(e){
		var categoryHash = $(this).data('category');
		$(this).addClass('selected').siblings().removeClass('selected');
		$('#section_' +categoryHash+ '_content').show().siblings().hide();
		return false;
	});

	var gcn = $('#giftcard_number');
	gcn.keydown(function(event){
		// Allow: backspace, delete, tab, escape, and enter
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			 // Allow: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) ||
			 // Allow: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
		}
		else {
			// Ensure that it is a number and stop the keypress
			//if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				console.log('kc '+event.keyCode);
			var kc = event.keyCode;
			if (giftcard_swipe && !((kc >= 48 && kc <=57) || (kc >= 96 && kc <= 105) || kc == 13)) //Allow numbers only and enter
			{
				console.log('numbers only');
				event.preventDefault();
			}
			else if (kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/|| (event.shiftKey && kc == 53) /*percentage sign*/)
			{
				console.log('blocking special characters');
				giftcard_swipe = true;
				event.preventDefault();
			}
		}
	});

	// Scroll receipts RIGHT
	$('#split_payments a.right').live('click', function(e){
		var curVal = $('div.scroll-content').position().left;
		var numReceipts = parseInt($('div.scroll-content div.receipt-container').length);
		var totalReceiptWidth = numReceipts * 310;

		if(Math.abs(curVal - 620) < totalReceiptWidth){
			$('div.scroll-content').css("left", curVal - 620);
		}
		return false;
	});

	// Scroll receipts LEFT
	$('#split_payments a.left').live('click', function(e){
		var curVal = $('div.scroll-content').position().left;

		if(curVal <= -620){
			$('div.scroll-content').css("left", curVal + 620);
		}else{
			$('div.scroll-content').css("left", 0);
		}
		return false;
	});
});
</script>

<div id="table_layout" style="display: none;">
	<?php $this->load->view('food_and_beverage/layout'); ?>
</div>
<div id="select-employees" style="display: none;">
	<?php $this->load->view('food_and_beverage/select_employees'); ?>
</div>

<!-- Backbone Templates -->
<?php $this->load->view('food_and_beverage/js/templates/edit_item'); ?>
<?php $this->load->view('food_and_beverage/js/templates/cart_item'); ?>
<?php $this->load->view('food_and_beverage/js/templates/cart_totals'); ?>
<?php $this->load->view('food_and_beverage/js/templates/receipts'); ?>
<?php $this->load->view('food_and_beverage/js/templates/receipt'); ?>
<?php $this->load->view('food_and_beverage/js/templates/payment_window'); ?>
<?php $this->load->view('food_and_beverage/js/templates/payment'); ?>
<?php $this->load->view('food_and_beverage/js/templates/item_sides'); ?>
<?php $this->load->view('food_and_beverage/js/templates/item_modifiers'); ?>
<?php $this->load->view('food_and_beverage/js/templates/item_soup_salad'); ?>
<?php $this->load->view('food_and_beverage/js/templates/recent_transaction'); ?>
<?php $this->load->view('food_and_beverage/js/templates/recent_transaction_details'); ?>
<?php $this->load->view('food_and_beverage/js/templates/add_tip'); ?>
<?php $this->load->view('food_and_beverage/js/templates/change_issued'); ?>
<?php $this->load->view('food_and_beverage/js/templates/send_message'); ?>
<?php $this->load->view('food_and_beverage/js/templates/open_register'); ?>
<?php $this->load->view('food_and_beverage/js/templates/close_register'); ?>
<?php $this->load->view('food_and_beverage/js/templates/customer'); ?>
<?php $this->load->view('food_and_beverage/js/templates/customer'); ?>
<?php $this->load->view('food_and_beverage/js/templates/authorize'); ?>

<script>
// Currency/number format settings
accounting.settings = {
	currency: {
		symbol : "$",
		format: "%s%v",
		decimal : ".",
		thousand: ",",
		precision : 2
	},
	number: {
		precision : 0,
		thousand: ",",
		decimal : "."
	}
};

// Underscore init, custom functions
_.mixin({
	capitalize: function(str) {
		if(typeof(str) !== 'string'){ return str; }
		return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
	},

	round: function(value, places) {

		if(typeof(value) != 'number'){
			value = parseFloat(value);
		}
		if(isNaN(value)){
			value = 0;
		}
		if(!places){
			places = 2;
		}

		return +(Math.round(value + "e+" + places)  + "e-" + places);
	},

	// Filters parenthesis () and *. Handles characters not filtered
	// by browsers encodeURI() function, but still cause problems with Apache
	filterSpecialChars: function (str) {

		return str.replace(/['()]/g, escape). // i.e., %27 %28 %29
			replace(/\*/g, '%2A');
	}
});

var App = {};
App.employee_id = <?php echo (int) $employee_id; ?>;
App.employee_level = <?php echo (int) $employee_level; ?>;
App.table_num = "<?php echo $table_number; ?>";
App.sale_id = <?php echo (int) $suspended_sale_id; ?>;
App.table_name = "<?php echo htmlentities($name, ENT_QUOTES); ?>";
App.orders_made = <?php echo (int) $orders_made; ?>;
App.receipt_printer_ip = <?php echo (int) $receipt_printer_ip; ?>;
App.api = "index.php/api/food_and_beverage/";
App.api_root = "index.php/api/food_and_beverage/service/";
App.api_table = App.api_root + App.table_num + '/';
App.api_key = "no_limits";
App.receipt_ip = "<?=$this->config->item('webprnt_ip')?>";
App.hot_kitchen_ip = "<?=$this->config->item('webprnt_hot_ip')?>";
App.cold_kitchen_ip = "<?=$this->config->item('webprnt_cold_ip')?>";
App.track_cash = <?=(int) $this->config->item('track_cash')?>;

<?php
$member_account_name = lang('customers_member_account_balance');
if($this->config->item('member_balance_nickname')){
	$member_account_name = $this->config->item('member_balance_nickname');
}

$customer_account_name = lang('customers_account_balance');
if($this->config->item('customer_credit_nickname')){
	$customer_account_name = $this->config->item('customer_credit_nickname');
}
?>
App.member_account_name = '<?php echo $member_account_name; ?>';
App.customer_account_name = '<?php echo $customer_account_name; ?>';
App.use_ets_giftcards = <?php echo (int) $this->config->item('use_ets_giftcards'); ?>;

<?php if($this->config->item('ets_key')){ ?>
App.ets_credit_cards = true;
<?php }else{ ?>
App.ets_credit_cards = false;
<?php } ?>

<?php if($this->config->item('mercury_id') && $this->config->item('mercury_password')){ ?>
App.mercury_credit_cards = true;
<?php }else{ ?>
App.mercury_credit_cards = false;
<?php } ?>

App.receipt = {
	header: {
		course_name:'<?php echo addslashes($this->config->item('name'))?>',
    	address:'<?php echo addslashes(str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address'))))?>',
    	address_2:'<?php echo addslashes($this->config->item('city')).', '.$this->config->item('state').' '.$this->config->item('zip')?>',
    	phone:'<?php echo $this->config->item('phone')?>',
    	employee_name:'<?php echo addslashes($employee_info->last_name).', '.addslashes($employee_info->first_name); ?>',
    	customer:'',// Need to load this in from data
    	no_date:true
	},
	return_policy: "<?=addcslashes($this->config->item('return_policy'), implode('',array('"',"'")))?>",
	auto_print_receipts: "<?=$this->config->item('auto_print_receipts')?>",
	print_credit_card_receipt: "<?=$this->config->item('print_credit_card_receipt')?>",
	print_sales_receipt: "<?=$this->config->item('print_sales_receipt')?>",
	print_two_receipts: "<?=$this->config->item('print_two_receipts')?>",
	print_two_signature_slips: "<?=$this->config->item('print_two_signature_slips')?>",
	print_two_receipts_other: "<?=$this->config->item('print_two_receipts_other')?>",
	print_tip_line: "<?=$this->config->item('print_tip_line')?>",
	cash_register: "<?=$this->config->item('cash_register')?>"
};

// Override ajax function to always pass API key
$(document).ajaxSend(function(e, xhr, options){
    xhr.setRequestHeader("Api-key", App.api_key);

// Display any errors received from API
}).ajaxError(function(e, xhr, options, error){
	if(xhr.responseJSON && xhr.responseJSON.msg){
		set_feedback(xhr.responseJSON.msg, 'error_message', false, 4000);
		$('#cbox2Content').unmask();
		$('#cboxContent').unmask();
	}

// Display any warning messages
}).ajaxSuccess(function(e, xhr, options, error){
	if(xhr.responseJSON && xhr.responseJSON.warning){
		set_feedback(xhr.responseJSON.warning, 'warning_message', false, 2500);
	}
});

function displayError(model){
	set_feedback(model.validationError, 'error_message', false, 4000);
	return false;
}

// Backbone Models
<?php $this->load->view('food_and_beverage/js/models/modifier'); ?>
<?php $this->load->view('food_and_beverage/js/models/item'); ?>
<?php $this->load->view('food_and_beverage/js/models/customer'); ?>
<?php $this->load->view('food_and_beverage/js/models/cart_item'); ?>
<?php $this->load->view('food_and_beverage/js/models/receipt_item'); ?>
<?php $this->load->view('food_and_beverage/js/models/receipt'); ?>
<?php $this->load->view('food_and_beverage/js/models/payment'); ?>
<?php $this->load->view('food_and_beverage/js/models/order'); ?>
<?php $this->load->view('food_and_beverage/js/models/side'); ?>
<?php $this->load->view('food_and_beverage/js/models/item_side'); ?>
<?php $this->load->view('food_and_beverage/js/models/recent_transaction'); ?>
<?php $this->load->view('food_and_beverage/js/models/register_log'); ?>

// Backbone Collections
<?php $this->load->view('food_and_beverage/js/collections/modifiers'); ?>
<?php $this->load->view('food_and_beverage/js/collections/items'); ?>
<?php $this->load->view('food_and_beverage/js/collections/customers'); ?>
<?php $this->load->view('food_and_beverage/js/collections/cart'); ?>
<?php $this->load->view('food_and_beverage/js/collections/payments'); ?>
<?php $this->load->view('food_and_beverage/js/collections/receipt_cart'); ?>
<?php $this->load->view('food_and_beverage/js/collections/receipts'); ?>
<?php $this->load->view('food_and_beverage/js/collections/sides'); ?>
<?php $this->load->view('food_and_beverage/js/collections/item_sides'); ?>
<?php $this->load->view('food_and_beverage/js/collections/recent_transactions'); ?>

// Backbone Views
<?php $this->load->view('food_and_beverage/js/views/cart_list'); ?>
<?php $this->load->view('food_and_beverage/js/views/cart_list_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/cart_totals'); ?>
<?php $this->load->view('food_and_beverage/js/views/customer_list'); ?>
<?php $this->load->view('food_and_beverage/js/views/customer_list_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/payment_customer_list'); ?>
<?php $this->load->view('food_and_beverage/js/views/edit_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/payment'); ?>
<?php $this->load->view('food_and_beverage/js/views/payment_window'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt_item_list'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt_window'); ?>
<?php $this->load->view('food_and_beverage/js/views/item_sides'); ?>
<?php $this->load->view('food_and_beverage/js/views/item_modifiers'); ?>
<?php $this->load->view('food_and_beverage/js/views/recent_transactions'); ?>
<?php $this->load->view('food_and_beverage/js/views/recent_transaction'); ?>
<?php $this->load->view('food_and_beverage/js/views/recent_transaction_details'); ?>
<?php $this->load->view('food_and_beverage/js/views/add_tip'); ?>
<?php $this->load->view('food_and_beverage/js/views/send_message'); ?>
<?php $this->load->view('food_and_beverage/js/views/register_log'); ?>
<?php $this->load->view('food_and_beverage/js/views/items'); ?>
<?php $this->load->view('food_and_beverage/js/views/authorize_window'); ?>

// If all receipts are now paid, remove table record with all data
// associated with it (close the sale)
App.closeTable = function(waitForModal){
	if(typeof(waitForModal) == 'undefined'){
		waitForModal = true;
	}

	if(!App.receipts.paymentsMade() || App.receipts.allReceiptsPaid()){

		// TODO: replace this with a proper "Table service" model
		$.ajax({
			url: App.api_table,
			type: 'DELETE',

			// If table closed successfully
			success: function(response){

				if(waitForModal){
					$(document).one('cbox_closed', function(e){
						reload_tables(function(){
							fnb.show_tables();
						});
					});
					$.colorbox.close();
					$.colorbox2.close();
				}else{
					reload_tables(function(){
						fnb.show_tables();
					});
				}
				// Clear out existing table data
				App.cart.reset(response.cart);
				App.receipts.reset(response.receipts);
				App.customers.reset(response.customers);
				App.table_name = '';
				$('#table_name').val('');

				// Refresh recent transactions
				$.get(App.api + 'recent_transactions', {'employee_id':App.employee_id}, function(response){
					App.recent_transactions.reset(response);
				},'json');

				$('#menubar_stats').html('Table #');
			}
		});

		return true;
	}
	return false;
};
function open_cash_drawer() {
	<?php $webprnt_ip = $this->config->item('webprnt_ip');?>
	var receipt_data = '';
	receipt_data = webprnt.add_cash_drawer_open(receipt_data);
	webprnt.print(receipt_data,  "http://<?=$webprnt_ip?>/StarWebPrint/SendMessage");
	webprnt.print_all("http://<?=$webprnt_ip?>/StarWebPrint/SendMessage");
}
$(function(){
	// STARTING PRINTING PROCESSES
	setInterval(function(){webprnt.print_all("http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage")}, 3000);
	<?php if ($this->config->item('webprnt_ip') != $this->config->item('webprnt_hot_ip')) { ?>
	setInterval(function(){webprnt.print_all("http://<?=$this->config->item('webprnt_hot_ip')?>/StarWebPrint/SendMessage")}, 3000);
	<?php } ?>
	<?php if ($this->config->item('webprnt_hot_ip') != $this->config->item('webprnt_cold_ip')) { ?>
	setInterval(function(){webprnt.print_all("http://<?=$this->config->item('webprnt_cold_ip')?>/StarWebPrint/SendMessage")}, 3000);
	<?php } ?>

	App.items = new ItemCollection();
	App.receipts = new ReceiptCollection();
	App.cart = new Cart();
	App.sides = new SideCollection();
	App.recent_transactions = new RecentTransactionCollection();
	App.register_log = new RegisterLog(<?php echo json_encode($register_log); ?>);
	App.customers = new CustomerCollection();

	App.Page = {};
	App.Page.cart = new CartListView({collection: App.cart, el: $('#cart_contents')});
	App.Page.cartTotals = new CartTotalsView({collection: App.cart, el: $('#sale_details')});
	App.Page.customers = new CustomerListView({collection: App.customers, el: $('#attached_customers')});
	App.Page.itemEdit = false;

	// Pre-load all available F&B items, cart, and receipts for current table
	App.items.reset(<?php echo json_encode($food_items); ?>);
	App.sides.reset(<?php echo json_encode($sides); ?>);
	App.cart.reset(<?php echo json_encode($cart); ?>);
	App.receipts.reset(<?php echo json_encode($receipts); ?>);
	App.recent_transactions.reset(<?php echo json_encode($recent_transactions); ?>);
	App.customers.reset(<?php echo json_encode($customers); ?>);

	$(document).on('click', '.fnb_logout', function(e){
		fnb.logout();
		e.preventDefault();
		return false;
	});

	// Open sub-category items window
	$('.subcategory').click(function(e){
		var params = {};
		var category = $(this).data('category');
		params.category = category;

		var subcategory = $(this).data('subcategory');
		if(subcategory && subcategory != '-Uncategorized-'){
			params.subcategory = subcategory;
		}else{
			params.subcategory = "";
		}
		var items = new ItemCollection( App.items.where(params) );
		var itemsView = new ItemsView({collection: items});

		$.colorbox2({
			'title': category + ' > '+ subcategory,
			'width': 720,
			'height': 600,
			'html': itemsView.render().el
		});

		return false;
	});

	// Search customers to add to sale
   	$("#customer_search").autocomplete({
		source: '<?php echo site_url("food_and_beverage/customer_search"); ?>',
		delay: 200,
		autoFocus: true,
		minLength: 1,
		select: function(event, ui)
		{
			var attributes = {
				'person_id' : ui.item.person_id,
				'first_name': ui.item.first_name,
				'last_name': ui.item.last_name,
				'email' : ui.item.email,
				'phone': ui.item.phone,
				'photo': ui.item.photo,
				'account_number': ui.item.account_number,
				'member_account_balance': ui.item.member_account_balance,
				'member_account_balance_allow_negative': ui.item.member_account_balance_allow_negative,
				'account_balance': ui.item.account_balance,
				'account_balance_allow_negative': ui.item.account_balance_allow_negative,
				'discount': ui.item.discount
			};
			App.customers.create(attributes);
			customer_flag_fnb(ui.item.status_flag, ui.item.comments);

			if($('#table_name').val() == ''){
				var table_name = ui.item.first_name+' '+ui.item.last_name;
				$('#table_name').val(table_name);
				App.table_name = table_name;
				$.post(App.api_table, {'name':table_name}, function(response){
					reload_tables();
				});
			}

			$(this).val('');
			return false;
		}
	});

	$('#table_name').on('blur', function(e){
		var name = $(this).val();
		$.post(App.api_table, {'name':name}, function(response){
			App.table_name = name;
			reload_tables();
			set_feedback('Table name updated', 'success_message');
		});
	});

	// Open payments window
	$('#payments_button').on('click', function(e){
		var receiptsView = new ReceiptWindowView({collection: App.receipts});

		$.colorbox({
			title: 'Split Payments',
			html: receiptsView.render().el,
			width: 1268,
			height: 750,
			cache: false,
			onClosed: function(){
				receiptsView.remove();
			}
		});

		return false;
	});

	// Open recent transactions window
	$(document).on('click', '.fnb_recent_transactions', function(e){
		e.preventDefault();
		var recentTransactionsView = new RecentTransactionsView({collection: App.recent_transactions});

		$.colorbox2({
			title: 'Recent Transactions',
			html: recentTransactionsView.render().el,
			width: 900,
			height: 600,
			cache: false,
			onClosed: function(){
				recentTransactionsView.remove();
			}
		});
		return false;
	});

	// Send selected items to kitchen
	$('#send_to_kitchen').on('click', function(e){
		var cartItems = App.cart.where({'selected': true});
		var copiedItems = [];

		_.each(cartItems, function(item){

			// Clone the cart item to place in the order object
			var itemCopy = _.clone(item.attributes);
			itemCopy.modifiers = itemCopy.modifiers.toJSON();
			itemCopy.soups = itemCopy.soups.toJSON();
			itemCopy.salads = itemCopy.salads.toJSON();
			itemCopy.sides = itemCopy.sides.toJSON();
			itemCopy = new CartItem(itemCopy);

			copiedItems.push(itemCopy);
		});

		// Create a new order with list of items and save it to database
		var order = new Order({'items': copiedItems}, {'url':App.api_table + 'orders'});
		order.save({}, {success: function(){
			App.orders_made = 1;
		}});

		return false;
	});

	$('#open_cash_drawer').on('click', function(e){
		open_cash_drawer();
		return false;
	});

	// Add food item to cart
	$('body').on('click', 'button.menu_item', function(e){
		$.colorbox2.close();

		var item_id = parseInt($(this).data('item-id'));
		var item = App.items.get(item_id);
		item.set({"line":null});

		var attributes = _.clone(item.attributes);
		attributes.is_new = true;
		var cart_item = new CartItem( attributes );
		App.cart.clearNew();
		App.cart.create(cart_item);

		var cartItemCopy = cart_item.attributes;
		var receipt_item = new ReceiptItem(cartItemCopy);

		var availableReceipt = App.receipts.findWhere({'status':'pending'});
		if(!availableReceipt){
			var availableReceipt = App.receipts.add({date_paid:null});
		}

		availableReceipt.get('items').add(receipt_item);
		e.preventDefault();
	});

	// Select new table
	$('#suspend_sale_button').on('click', function(e){
		fnb.show_tables(true);
		return false;
	});

	// Close register
	$(document).on('click', '.fnb_close_register', function(e){
		e.preventDefault();
		var closeRegisterWindow = new RegisterLogView({model: App.register_log, template: 'close'});

		$.colorbox2({
			title: 'Register Closing Amount',
			html: closeRegisterWindow.render().el
		});

		return false;
	});

	$('#cbox2Content').on('click', 'a.employee', function(e){
		var employee_id = $(this).data('employee-id');
		var employee_name = $(this).data('employee-name');
		var table_num = $(this).data('table-num');
		var table = $('#layout_editor').find('div[data-table="'+ table_num +'"]');

		// Change employee assigned to table
		$.post(App.api + 'service/' + table_num, {employee_id: employee_id}, function(response){

			// Update employee name on table
			table.find('span.employee').html(employee_name);

			// Apply proper table color if employee is changed to person currently logged in
			if(employee_id == App.employee_id){
				table.removeClass('other-active').addClass('my-active');
			}else{
				table.removeClass('my-active').addClass('other-active');
			}
			$.colorbox2.close();
		});

		return false;
	});

	$('#cancel_sale').on('click', function(e){

		// If orders have been sent to kitchen, require manager approval
		// to cancel sale
		if(App.employee_level < 2 && App.orders_made == 1){

			var auth_window = new AuthorizeWindowView({'action':'cancel_sale'});
			$.colorbox2({
				title: 'Manager Approval Required',
				html: auth_window.render().el
			});

		}else{
			if(!confirm('Are you sure you want to cancel this sale?')){
				return false;
			}

			// Attempt to close table, if table fails to close, display error
			if(!App.closeTable(false)){
				set_feedback('Please finish paying receipts or refund payments to cancel sale', 'error_message', 5000);
			}
		}
		e.preventDefault();
	});

	$('div.categories').find('.menu_section_button').first().trigger('click');
    $('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
});

// Refresh table layout content
function reload_tables(callback){
	$.get(SITE_URL + '/food_and_beverage/layout', function(response){
		$('#table_layout').html(response);
		if(typeof(callback) == 'function'){
			callback(response);
		}
	});
}
</script>
<!-- END BackboneJS code -->

<?php $this->load->view("partial/footer"); ?>

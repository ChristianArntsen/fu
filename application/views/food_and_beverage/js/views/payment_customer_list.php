var PaymentCustomerListView = Backbone.View.extend({
	initialize: function(options) {
		this.options = options || {}

		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
	},

	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<a class="fnb_button show_payment_buttons" href="#">Back</a>');
		_.each(this.collection.models, this.addCustomer, this);

		return this;
	},

	addCustomer: function(customer){
		var button = '<a class="fnb_button '+ this.options.type +' payment-customer" data-customer-id="'+ customer.get('person_id') +'">'+
			customer.get('first_name') +' '+ customer.get('last_name') + '</a>';
		this.$el.append(button);
	}
});
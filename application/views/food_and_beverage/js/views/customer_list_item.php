var CustomerListItemView = Backbone.View.extend({
	tagName: "li",
	className: "customer",
	template: _.template( $('#template_customer').html() ),

	events: {
		"click .remove": "removeCustomer"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	removeCustomer: function(event){
		this.model.destroy();
		return false;
	}
});
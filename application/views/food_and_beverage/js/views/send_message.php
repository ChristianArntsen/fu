var SendMessageView = Backbone.View.extend({
	tagName: "div",
	template: _.template( $('#template_send_message').html() ),

	events: {
		"click a.send": "sendMessage"
	},

	render: function() {
		this.$el.html(this.template());
		return this;
	},

	sendMessage: function(event){
		var message = $('#item_message').val();

		if(!message){
			set_feedback('Message can not be empty', 'error_message', false, 3000);
			return false;
		}

		// Clone the cart item to place in the order object
		var itemCopy = _.clone(this.model.attributes);
		itemCopy.modifiers = itemCopy.modifiers.toJSON();
		itemCopy.soups = itemCopy.soups.toJSON();
		itemCopy.salads = itemCopy.salads.toJSON();
		itemCopy.sides = itemCopy.sides.toJSON();
		itemCopy = new CartItem(itemCopy);

		// Create a new order with list of items and save it to database
		var order = new Order({'message':message, 'items': [itemCopy]}, {'url':App.api_table + 'orders'});
		order.save();
		$.colorbox.close();

		return false;
	}
});
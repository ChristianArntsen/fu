var CartTotalsView = Backbone.View.extend({
	tagName: "div",
	id: "register_container",
	template: _.template( $('#template_cart_totals').html() ),

	initialize: function() {
		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
		this.listenTo(this.collection, "change", this.render);
	},

	render: _.debounce(function(){
		var totals = this.collection.getTotals();
		this.$el.html('');
		this.$el.html(this.template(totals));
		return this;
	}, 25)
});
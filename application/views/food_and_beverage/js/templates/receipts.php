<style>
#split_payments div.items {
	width: 425px;
	float: left;
	height: 680px;
	background-color: white;
	border-right: 1px solid #E0E0E0;
}

#split_payments div.receipt-nav {
	height: 80px;
	width: auto;
	display: block;
	float: none;
}

#split_payments div.receipt-nav a.fnb_button {
	font-size: 14px;
}

#split_payments div.receipt-nav > a.fnb_button,
#split_payments div.receipt-nav > div.btn-group {
	margin: 10px;
}

#split_payments div.receipt-nav a.left,
#split_payments div.receipt-nav a.right {
	float: left;
	width: 75px;
	height: 75px;
	margin: 0px;
	padding: 0px;
	background: #d6d6d6;
	background: -moz-linear-gradient(top,  #d6d6d6 0%, #c9c9c9 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d6d6d6), color-stop(100%,#c9c9c9));
	background: -webkit-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: -o-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: -ms-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: linear-gradient(to bottom,  #d6d6d6 0%,#c9c9c9 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d6d6d6', endColorstr='#c9c9c9',GradientType=0 );
	color: #666;
	border: 1px solid #AAA;
	font-size: 40px;
	line-height: 75px;
	text-align: center;
	font-weight: bold;
}
#split_payments div.receipt-nav a.left:active,
#split_payments div.receipt-nav a.right:active {
	background: #E0E0E0 !important;
}

#split_payments div.receipt-nav a.right {
	float: right;
}

#split_payments div.receipt-nav a.left {
	margin-left: -1px;
}

#split_payments div.receipt-container {
	width: 290px;
	display: block;
	float: left;
	margin: 10px 0px;
	padding: 10px;
	overflow: hidden;
	background: none;
}

#split_payments div.new-container {
	width: 100px;
	min-height: 500px;
	display: block;
	float: left;
	margin: 10px;
	width: 86px;
	padding: 0px;
	overflow: hidden;
	background: none;
}
/*
#split_payments div.new-container a.new-ticket {
	border-radius: 40px;
	height: 80px;
	width: 80px;
	font-weight: bold;
	font-size: 60px;
	margin: 0px;
	padding: 0px;
	display: block;
	line-height: 80px;
	float: none;
	margin-top: 190px;
}
*/

#split_payments a.new-receipt {
	width: 150px;
	float: left;
	display: block;
}

#split_payments div.receipt-container a.pay,
#split_payments div.receipt-container a.print {
	width: 100px;
	display: block;
	float: right;
	margin: 10px 0px 0px 0px;
	font-size: 20px;
	font-weight: bold;
}

#split_payments div.receipt-container a.print {
	float: left;
}

#split_payments div.receipt-container a.pay.disabled,
#split_payments div.receipt-container a.print.disabled {
	background: #D9D9D9 !important;
	border: 1px solid #CCC;
	color: #AAA;
	cursor: arrow;
	text-shadow: none !important;
}

#split_payments div.receipt a {
	width: auto;
	float: none;
	display: block;
	padding: 0px 0px 0px 0px;
	line-height: 35px;
	height: 35px;
	margin: 0px 0px 10px 0px;
	position: relative;
}

#split_payments div.receipt a span.seat {
	position: absolute;
	top: 0px;
	bottom: 0px;
	display: block;
	width: 35px;
	line-height: 35px;
	text-align: center;
	background-color: #E0E0E0;
	box-shadow: inset 0px 0px 3px rgba(0,0,0,0.4);
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	font-weight: bold;
	color: #222;
	text-shadow: none;
}

#split_payments div.receipt div.title {
	display: block;
	width: auto;
	overflow: hidden;
	padding-bottom: 5px;
	margin-bottom: 5px;
	border-bottom: 1px solid #CCC;
}

#split_payments div.receipt div.title > span {
	font-size: 14px;
	color: #666;
	font-weight: normal;
	float: left;
	line-height: 16px;
	padding: 5px 0px;
}

#split_payments div.receipt div.title a.fnb_button.delete-receipt {
	float: right;
	padding: 5px 8px;
	height: auto;
	width: auto;
	line-height: 16px;
	margin: 0px;
	background: #d14d4d; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d14d4d', endColorstr='#c03939'); /* for IE */
	background: -webkit-linear-gradient(top, #d14d4d, #c03939);
	background: -moz-linear-gradient(top,  #d14d4d,  #c03939); /* for firefox 3.6+ */
}

#split_payments div.receipt {
	display: block;
	width: auto;
	padding: 10px;
	border: none;
	box-shadow: 0px 0px 3px #777;
	height: 450px;
	background: url("images/backgrounds/texture.png") repeat scroll;
}

#split_payments div.receipt.selected {
	border: 1px solid #CC2222;
	padding: 9px;
}

#split_payments div.receipt span.is-paid {
	display: none;
	font-size: 18px !important;
	font-weight: bold !important;
	color: #CCC;
	float: right !important;
}

#split_payments div.receipt.paid a.delete-receipt {
	display: none;
}

#split_payments div.receipt.paid span.is-paid {
	display: block;
}

#split_payments div.receipt.paid a.item {
	background: #D9D9D9 !important;
	border: 1px solid #BBB !important;
	color: #444 !important;
	cursor: arrow;
	text-shadow: none !important;
}

#split_payments a.item.selected {
	background: #8cc5ea;
	background: -moz-linear-gradient(top,  #8cc5ea 0%, #66a9c4 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#8cc5ea), color-stop(100%,#66a9c4));
	background: -webkit-linear-gradient(top,  #8cc5ea 0%,#66a9c4 100%);
	background: -o-linear-gradient(top,  #8cc5ea 0%,#66a9c4 100%);
	background: -ms-linear-gradient(top,  #8cc5ea 0%,#66a9c4 100%);
	background: linear-gradient(to bottom,  #8cc5ea 0%,#66a9c4 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8cc5ea', endColorstr='#66a9c4',GradientType=0 );
}

#split_payments div.receipt.paid a.item > span.seat {
	box-shadow: none;
	color: #666 !important;
	border-right: 1px solid #BBB;
}

#split_payments .header_cell {
	width: auto !important;
	margin-left: 0px !important;
}

#split_payments div.receipts {
	float: none;
	display: block;
	width: auto;
	height: 575px;
	overflow-x: hidden;
	overflow-y: hidden;
	position: relative;
}

#split_payments div.scroll-content {
	height: 600px;
	width: 5000px;
	display: block;
	overflow: hidden;
	position: relative;
}

#split_payments a.add-to-receipt {
	display: block;
	margin: 0px;
	padding: 5px;
	width: auto;
}

#split_payments table td {
	padding: 15px;
}

#split_payments div.btn-group > a.fnb_button.selected {
	box-shadow: 0px 0px 15px rgba(0, 0, 0, 1) inset;
}

#split_payments div.btn-group {
	overflow: hidden;
	display: block;
	float: left;
	padding: 0px;
}

#split_payments div.btn-group > a.fnb_button {
	margin: 0px !important;
}

#split_payments div.btn-group a.move {
	border-top-right-radius: 0px;
	border-bottom-right-radius: 0px;
}

#split_payments div.btn-group a.split {
	border-top-left-radius: 0px;
	border-bottom-left-radius: 0px;
}
</style>
<script type="text/html" id="template_receipts">
<div class="receipt-nav">
	<a class="left"><</a>
	<a class="fnb_button new-receipt">+ New Receipt</a>
	<div class="btn-group mode" data-mode="move">
		<a class="fnb_button move selected" data-value="move">Move</a>
		<a class="fnb_button split" data-value="split">Split</a>
	</div>
	<a class="fnb_button remove-items">Remove Items</a>
	<a class="right">></a>
</div>
<div class="receipts">
	<div class="scroll-content">
	<!-- Receipts will be inserted here -->
	</div>
<!-- <div class="new-container">
	<a class="fnb_button new-ticket" href="#">+</a>
</div> -->
</div>
</script>

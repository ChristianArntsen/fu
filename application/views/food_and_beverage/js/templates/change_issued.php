<script type="text/html" id="template_change_issued">
<h2 style="font-size: 20px; margin: 15px; text-align: center; color: #444">Change Issued</h2>
<h1 style="font-size: 28px; margin: 15px; text-align: center;"><%-accounting.formatMoney(change_due)%></h1>
<a href="#" class="fnb_button close" style="display: block; float: none; margin: 60px auto 0; width: 100px;">Ok</a>
</script>

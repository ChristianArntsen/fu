<script type="text/html" id="template_send_message">
<div style="padding: 15px; overflow: hidden;">
	<div class="row" style="position: relative; padding-right: 15px">
		<label for="edit_item_comments" style="margin-bottom: 5px; display: block; font-size: 16px;">Message</label>
		<textarea style="display: block; width: 100%; height: 75px;" name="message" id="item_message"></textarea>
	</div>
	<div class="row" style="position: relative;">
		<a class="submit_button send" href="#" style="float: right;">Send</a>
	</div>
</div>
</script>
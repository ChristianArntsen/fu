<style>
fnb_button.set_type {
	height: 30px;
}
</style>
<script type="text/html" id="template_add_tip">
<div class="row" style="padding: 20px; overflow: hidden;">
	<button class="fnb_button type <%if(tip.type == 'cash'){ print('selected') } %>" data-type="cash" style="float: left;">Cash</button>
	<% if(customer_id){ %>
	<button class="fnb_button type <%if(tip.type == 'member_balance'){ print('selected') } %>" data-type="member_balance" data-customer-id="<%-customer_id%>" style="float: left; width: 170px;"><%-App.member_account_name +' Tip' %></button>
	<button class="fnb_button type <%if(tip.type == 'customer_balance'){ print('selected') } %>" data-type="customer_balance" data-customer-id="<%-customer_id%>" style="float: left; width: 170px;"><%-App.customer_account_name +' Tip' %></button>
	<% } %>

	<% if(payments.length > 0){
	_.each(payments, function(payment){
		var is_credit_card = ((payment.invoice_id != 0 && 
				payment.type.search("Giftcard") === -1 && 
				payment.type.search("Bank") === -1) || 
			payment.payment_type.search("Credit Card") !== -1);
		// If any payments are credit cards, display button to tip the card
		if(!is_credit_card){
			return true;
		} 
		%>
		<button class="fnb_button type <%if(tip.invoice_id == payment.invoice_id){ print('selected') } %>" data-type="<%-payment.type%>" data-invoice-id="<%-payment.invoice_id%>" style="float: left;"><%-payment.payment_type%> Tip</button>
	<% }); } %>	
</div>
<div class="row" style="padding: 20px; overflow: hidden;">
	<div class="form_field">
		<label style="width: 90px; float: left; font-weight: bold;">Amount</label>
		<input type="text" name="amount" id="tip_amount" style="width: 100px;" value="<%=accounting.formatMoney(tip.amount, '')%>" />
	</div>
	<div class="form_field" style="overflow: hidden;">
		<label style="width: 90px; float: left; font-weight: bold;">Type</label>
		<input type="hidden" name="type" id="tip_type" value="<%-tip.type%>" />
		<input type="hidden" name="customer_id" id="tip_customer_id" value="<%-tip.customer_id%>" />
		<span id="tip_payment_type" style="font-size: 16px; float: left; line-height: 35px;"><%-tip.payment_type%></span>
	</div>
	<div class="form_field" style="overflow: hidden;">
		<label style="width: 90px; float: left; font-weight: bold;">Recipient</label>
		<form name="employee_search" id='employee_search_form' autocomplete="off">
			<input type="text" id="tip_employee_search" name="search" value="<%-tip.tip_recipient_name%>" placeholder="Search employees..." autocomplete="off" />
			<input type="hidden" name="tip_recipient" id="tip_recipient" value="<%-tip.tip_recipient%>" />
		</form>				
	</div>
	<input type="hidden" name="invoice_id" id="tip_invoice_id" value="<%-tip.invoice_id%>" />
</div>
<div class="row" style="padding: 20px; overflow: hidden;">
	<a href="#" class="save fnb_button" style="display: block; width: 100px;" href="#">Save Tip</a>
</div>
</script>

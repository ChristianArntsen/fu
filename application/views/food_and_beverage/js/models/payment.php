var Payment = Backbone.Model.extend({
	idAttribute: "type",
	defaults: {
		"type": "",
		"amount": 0.00,
		"date": "",
		"card_number": ""
	},

	initialize: function(attrs, options){
		this.set('type', attrs.type.replace('/','-'));
		this.listenTo(this, 'invalid', this.error, this);
	},

	error: function(){
		this.collection.trigger('invalid', this);
		this.destroy();
	},

	parse: function(response, options){
		if(this.collection.get(response.type)){
			this.collection.get(response.type).set(response);
		}

		if(response.sale_id){
			App.receipt.sale_id = "POS "+response.sale_id;
		}

		// If any messages were passed with response, display them
		if(response.msg){
			set_feedback(response.msg, 'warning_message');
		}
		return response;
	},

	validate: function(attributes, options){
		
		if(typeof(attributes.amount) == 'string'){			
			var amount = parseFloat(accounting.unformat(attributes.amount));
		}else{
			var amount = attributes.amount;
		}

		if(amount < 0){
			return "Payment can not be negative";
		}
	}
});

var RegisterLog = Backbone.Model.extend({
	urlRoot: App.api + 'register_log',
	idAttribute: "register_log_id",

	defaults: {
		"shift_start": null,
		"shift_end": null,
		"open_amount": 0.00,
		"close_amount": 0.00,
		"cash_sales_amount": 0.00,
		"change":0,
		"ones":0,
		"fives":0,
		"tens":0,
		"twenties":0,
		"fifties":0,
		"hundreds":0
	}
});
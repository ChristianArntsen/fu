<script>
$.colorbox2.resize();
$('#ct100_MainContent_ifrm').one('load', function(e){
	$('#cbox2LoadedContent').unmask();
	$.colorbox2.resize();
});
function print_mercury_signature_slip(payment) {
	var receipt_data = '';
	var header_data = {
    	course_name:'<?php echo $this->config->item('name')?>',
    	address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
    	address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
    	phone:'<?php echo $this->config->item('phone')?>',
    	employee_name:App.receipt.header.employee_name,
    	customer:App.table_name
    };
    var card_data = {
    	card_type:payment.card_type,
    	masked_account:payment.card_number,
    	auth_code:payment.auth_code,
    	auth_amount:payment.amount.toFixed(2),
    	print_tip_line:'<?php echo $this->config->item('print_tip_line'); ?>',
    	cardholder_name:'<?php echo $cardholder_name; ?>',
    	print_two_signature_slips:'<?php echo $this->config->item('print_two_signature_slips');?>'
    };
	
	receipt_data = webprnt.build_credit_card_slip(receipt_data, card_data, header_data);
	console.log('webprnt.print credit card receipt *************************************************');
	console.log(receipt_data);
	webprnt.print(receipt_data, "http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage");
}
</script>
<a class="fnb_button show_payment_buttons" style="height: 30px; line-height: 30px; display: block; float: none; padding: 10px;" href="#">Back</a>
<h3><? echo $user_message ?></h3>
<div style="text-align: center; height: 425px;">
	<iFrame id='ct100_MainContent_ifrm' src="<?php echo $url?>" height="500px" width="660px" scrolling="auto" frameborder="0" runat="server" style="text-align: center;">
		Your browser does not support iFrames. To view this content, please download and use the latest version of one of the following browsers: Internet Explorer, Firefox, Google Chrome or Safari.
	</iFrame>
</div>

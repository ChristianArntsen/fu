<style type="text/css">
	.pricing_table{
		position:relative;
		border:1px solid #666;
		margin:10px 0;
		padding: 10px;
		width: 480px;
	}
	.inline_button{
		display:inline-block;
		line-height:20px;
		height:20px;
		width:120px;
		padding:4px 2px;
		margin:0;
	}
	.time_input{
		margin-right:5px;
	}
	.price_input{
		margin-right:5px;
		margin-top:5px;
	}
	.delete_button{
		color: red;
		display: inline;
		float: right;
		margin-top: -30px;
		font-size: 12px;
		cursor:pointer;
	}
</style>
<ul id="error_message_box"></ul>
<?=form_open('price_categories/save/new/'.$season_info->season_id,array('id'=>'pricing_form'));?>
<fieldset id="season_basic_info">
<div id="pricing" class="pricing_container" style="min-width:500px;min-height:380px;margin:10px;">
	<? echo form_input(array(
			'name'=>"price_category",
			'id'=>"price_category",
			'placeholder'=>"Price Category Name",
			'style'=>"width:360px;"));
		echo form_hidden('class_id','new');
	?>
	
<? echo form_submit(array(
	'name'=>'submit_details',
	'id'=>'submit_details',
	'value'=>"Save",
	'class'=>'submit_button float_right')
);
?>
</div>
</fieldset>
<?php
echo form_close();
?>
<script type="text/javascript">
	$(document).ready(function(){
		function post_pricing_form_submit(response)
		{
			$('#config_green_fees_form').load('index.php/config/view/season/<?=$season_info->season_id?>/' + response.class_id);
		}
		console.log('adding autocomplete');
		$( "#price_category" ).autocomplete({
			source: "<?php echo site_url("price_categories/get_price_class_suggestions/{$season_info->teesheet_id}/{$season_info->season_id}");?>",
			delay: 10,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui)
			{
				event.preventDefault();
				$('#class_id').val(ui.item.value);
				$('#price_category').val(ui.item.label);
			}
		});
		$('a[rel=delete_pricing]').on('click',function(){
			if (confirm("Are you sure you would like to delete this price category?"))
			{
				$.ajax({
				  type: "POST",
				  url: "index.php/price_categories/delete_price_category/<?=$price_info->class_id?>",
				  complete: function( msg ) {
					 $.colorbox.close();
					 post_pricing_form_submit();
				  }
				});
			}
			return false;
		});
		$('#pricing_form').validate({
			submitHandler:function(form)
			{
				$('input').removeAttr('disabled');
				if ($.trim($('#price_category').val()) != '')
				{
					$(form).mask("<?php echo lang('common_wait'); ?>");
					$(form).ajaxSubmit({
						success:function(response)
						{
							if (response.success == false)
								alert(response.message);
							$.colorbox.close();
							post_pricing_form_submit(response);
						},
						dataType:'json'
					});
				}
				else
					alert('Please enter a price category name');

			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li"
		});
	});
</script>
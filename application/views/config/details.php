<div class="field_row clearfix">
<?php echo form_label(lang('config_company').':<span class="required">*</span>', 'company',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'name',
				'id'=>'name',
				'placeholder'=>lang('config_company'),
				'value'=>$this->config->item('name')));?>
		</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('config_address').':<span class="required">*</span>', 'address',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'address',
				'id'=>'address',
				'placeholder'=>lang('config_address'),
				'value'=>$this->config->item('address')));?>
		</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_address2').':<span class="required">*</span>', 'address',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php
			$state_list = array(''=>'State',
				'AL'=>"AL",
				'AK'=>"AK",
				'AZ'=>"AZ",
				'AR'=>"AR",
				'CA'=>"CA",
				'CO'=>"CO",
				'CT'=>"CT",
				'DE'=>"DE",
				'DC'=>"DC",
				'FL'=>"FL",
				'GA'=>"GA",
				'HI'=>"HI",
				'ID'=>"ID",
				'IL'=>"IL",
				'IN'=>"IN",
				'IA'=>"IA",
				'KS'=>"KS",
				'KY'=>"KY",
				'LA'=>"LA",
				'ME'=>"ME",
				'MD'=>"MD",
				'MA'=>"MA",
				'MI'=>"MI",
				'MN'=>"MN",
				'MS'=>"MS",
				'MO'=>"MO",
				'MT'=>"MT",
				'NE'=>"NE",
				'NV'=>"NV",
				'NH'=>"NH",
				'NJ'=>"NJ",
				'NM'=>"NM",
				'NY'=>"NY",
				'NC'=>"NC",
				'ND'=>"ND",
				'OH'=>"OH",
				'OK'=>"OK",
				'OR'=>"OR",
				'PA'=>"PA",
				'RI'=>"RI",
				'SC'=>"SC",
				'SD'=>"SD",
				'TN'=>"TN",
				'TX'=>"TX",
				'UT'=>"UT",
				'VT'=>"VT",
				'VA'=>"VA",
				'WA'=>"WA",
				'WV'=>"WV",
				'WI'=>"WI",
				'WY'=>"WY");
			echo form_input(array(
				'name'=>'city',
				'id'=>'city',
				'placeholder'=>'City',
				'value'=>$this->config->item('city')));
			  echo form_dropdown('state', $state_list,$this->config->item('state'));
			  echo form_input(array(
				'name'=>'zip',
				'id'=>'zip',
				'placeholder'=>'Zip',
				'size'=>'6',
				'value'=>$this->config->item('zip')));

		?>
		</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('config_phone').':<span class="required">*</span>', 'phone',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'phone',
				'id'=>'phone',
				'placeholder'=>lang('config_phone'),
				'value'=>$this->config->item('phone')));
		?>
		</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_fax').':', 'fax',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'fax',
				'id'=>'fax',
				'placeholder'=>lang('config_fax'),
				'value'=>$this->config->item('fax')));
		?>
		</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('common_email').':<span class="required">*</span>', 'email',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'email',
				'id'=>'email',
				'placeholder'=>lang('common_email'),
				'value'=>$this->config->item('email')));
		?>
		</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_website').':', 'website',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'website',
				'id'=>'website',
				'placeholder'=>lang('config_website'),
				'value'=>$this->config->item('website')));
		?>
		</div>
</div>

<div class="field_row clearfix">
		<?php echo form_label(lang('config_company_logo').':', 'company_logo',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_upload(array(
				'name'=>'company_logo',
				'id'=>'company_logo',
				'value'=>$this->config->item('company_logo')));?>
		</div>
</div>

<div class="field_row clearfix">
		<?php echo form_label(lang('config_delete_logo').':', 'delete_logo',array('class'=>'wide')); ?>
		<div class='form_field'>
				<?php echo form_checkbox('delete_logo', '1');?>
		</div>
</div>
 <div class="field_row clearfix">
		<?php echo form_label('Reservation Email Photo:', 'reservation_email_photo',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_upload(array(
				'name'=>'reservation_email_photo',
				'id'=>'reservation_email_photo',
				'value'=>$this->config->item('reservation_email_photo')));?>
		</div>
</div>

<div class="field_row clearfix">
		<?php echo form_label('Delete Reservation Email Photo:', 'delete_reservation_email_photo',array('class'=>'wide')); ?>
		<div class='form_field'>
				<?php echo form_checkbox('delete_reservation_email_photo', '1');?>
		</div>
</div>
<div id='reservation_email_text_box' style='display:none'>
	<div class="field_row clearfix">
		<div class='form_field'>
			<textarea id='reservation_email_text' name='reservation_email_text' class="wysiwyg"><?php echo $this->config->item('reservation_email_text'); ?></textarea>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#reservation_email_text_box').expandable({title:'Reservation Email Text:'});
	$('textarea.wysiwyg').cleditor({width: 800, height: 250});
</script>

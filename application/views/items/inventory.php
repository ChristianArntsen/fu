<ul id="error_message_box"></ul>
<?php
echo form_close();
echo form_open('items/save_inventory/'.$item_info->item_id,array('id'=>'item_form'));
?>

<table align="center" border="0">
<div class="field_row clearfix">
<tr>
<td>
	<div class='item_name'>
		<?=$item_info->name?>
	</div>
</td>
<td>
	<div class='current_quantity'>
		<?php echo lang('items_current_quantity').': '.$item_info->quantity ?>
	</div>
</td>
</tr>
<tr>
	<td style='width:470px;'>
		<div class='item_number'>
			<?php echo lang('items_item_number').': '.$item_info->item_number?>
		</div>
		<div class='item_category'>
			<?php echo lang('items_category').': '.$item_info->category?>
		</div>
	</td>
	<td>
		<div class='quantity_controls'>
			<span class='pos_neg_buttonset'>
	            <input type='radio' id='negative' name='quantity_sign' value='-1' />
	            <label for='negative' id='quantity_negative_label'>-</label>
	            <input type='radio' id='positive' name='quantity_sign' value='1'  checked/>
	            <label for='positive' id='quantity_positive_label'>+</label>
	        </span>
	 		<?php echo form_input(array(
				'name'=>'newquantity',
				'id'=>'newquantity'
				)
			);?>
		</div>
	</td>
</tr>
</table>
<div class='popup_divider'></div>
	<?php echo form_input(array(
		'name'=>'trans_comment',
		'id'=>'trans_comment',
		'placeholder'=>'Reason / Comment')		
	);?>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
<div class='clear'></div>
<?php 
echo form_close();
?>
<script type='text/javascript'>
	 		
//validation and submit handling
$(document).ready(function()
{		
	$('.pos_neg_buttonset').buttonset();
	$('#negative').button();
	$('#positive').button();
	console.log('just tried to apply buttons');
	var submitting = false;
    $('#item_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				$.colorbox.close();
				post_item_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			newquantity:
			{
				required:true,
				number:true
			}
   		},
		messages: 
		{
			
			newquantity:
			{
				required:"<?php echo lang('items_quantity_required'); ?>",
				number:"<?php echo lang('items_quantity_number'); ?>"
			}
		}
	});
});

	//handle making the number negative
	
	$('#newquantity').keyup(function(event){				
		if (event.keyCode === 189 && $('#quantity_positive_label').hasClass('ui-state-active'))
		{			
			switch_sign();
		}
		
		if ($('#newquantity').val()==='' && $('#quantity_negative_label').hasClass('ui-state-active'))
		{
			switch_sign();
		}
		
		if ($('#newquantity').val() >= 0 && $('#quantity_negative_label').hasClass('ui-state-active'))
		{
			switch_sign();
		}
	});
	
	function switch_sign()
	{		
		if ($('#quantity_negative_label').hasClass('ui-state-active')){
			$('#quantity_negative_label').removeClass('ui-state-active');			
			$('#quantity_positive_label').addClass('ui-state-active');
			$('#newquantity').removeClass('negative_balance');
		}
		else if ($('#quantity_positive_label').hasClass('ui-state-active')){
			$('#quantity_positive_label').removeClass('ui-state-active');
			$('#quantity_negative_label').addClass('ui-state-active');
			$('#newquantity').addClass('negative_balance');
		}
	}
	
	$('.ui-button', '.pos_neg_buttonset').click(function(){
		if($('#quantity_negative_label').hasClass('ui-state-active'))
		{
			$('#newquantity').addClass('negative_balance');
			
			if ($('#newquantity').val().indexOf("-") === -1 )
			{				
				$('#newquantity').val('-' + $('#newquantity').val());
			}
		}
		else
		{
			$('#newquantity').removeClass('negative_balance');
			$('#newquantity').val($('#newquantity').val().replace('-','')); 
		}
		$('#newquantity').focus();		
	});
</script>
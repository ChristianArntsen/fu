<style>
.etsFormGroup { margin: 10px 0; }
#ets_img { margin:40px auto;}

.etsButton {
	background: #3D80B9; /* for non-css3 browsers */
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 22px;
	padding: 5px 0 0 10px;
	text-align: center;
	text-shadow: 0px -1px 0px black !important;
	border-radius: 4px;
	margin-bottom: 8px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}

.etsMessage {
	color: #c44b00;
	font-weight: bold;
	font-size: 20px;
}
</style>

<div style="padding: 15px; background-color: #EFEFEF; overflow: hidden;">
	<?php if(!empty($no_show_policy)){ ?>
	<h4>No Show Policy</h4>
	<div><?php echo html_entity_decode($no_show_policy); ?></div>
	<?php } ?>
	<div style="height: 450px;" id="ets_payment_window">
		<!-- HTML5 Magic -->
		<div id='ets_session_id' data-ets-key="<?php echo trim($session->id); ?>">
			<img id='ets_img' src="http://www.loadinfo.net/main/download?spinner=3875&disposition=inline" alt="">
		</div>
	</div>
	<div id='ets_script_box'>
	</div>
</div>

<script>
if (typeof ETSPayment == 'undefined')
{
	var e = document.createElement('script');
	e.src = "<?php echo ECOM_ROOT_URL ?>/init";
	//document.getElementById("ets_script_box").appendChild(e);
	$('head')[0].appendChild(e);
}
else
{
	ETSPayment.createIFrame();
}
var interval_id = '';
var count = 0;

$(document).ready(function(){
	interval_id = setInterval(add_ets_handlers, 200);
});

function add_ets_handlers()
{
	if ($('#ETSIFrame').length > 0)
	{
		ETSPayment.addResponseHandler("success", function(e){
			$.ajax({
			   type: "POST",
			   url: "<?php echo $url; ?>",
			   data: 'response='+JSON.stringify(e),
			   success: function(response){
					$('#ets_payment_window').html(response);
				},
				dataType:'html'
			 });
		});

		clearInterval(interval_id);
	}
	else if (count > 50)
	{
		clearInterval(interval_id);
	}
	count ++;
}
</script>
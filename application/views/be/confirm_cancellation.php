<?php $this->load->view("partial/course_header"); ?>
<style>
	#menubar_background, #menubar_full{
		background:url('../images/header/header_piece.png') repeat-x 0 -2px transparent;
	}
	#content_area, #home_module_list {
		width:780px;
	}
	.booking_details {
		float:left;
		width:30%;
	}
	.cancellation_text {
		float:left;
		width:60%;
		padding:15px;
		text-align:center;
	}
	#booking_basic_info {
		padding:5px;
	}
	#booking_basic_info table td {
		padding:8px 2px;
	}
	#cancel_my_reservation {
		padding: 10px;
		line-height: 20px;
		margin: 10px;
		font-size: 14px;
		font-weight: bold;
		padding-bottom: 10px;
		color: white;
		background: #1E5A96;
		border: 1px solid #EEE;
		cursor:pointer;
	}
</style>
<div id="course_name"><?php echo $course_info->name?></div>
<div id="thank_you_box"></div>
<div id="home_module_list">
	<div class='content'>
		<div class='booking_details'>
			<fieldset id="booking_basic_info" class="ui-corner-all ui-state-default login_info">
				<legend>Reservation Details</legend>
				    <div class='reservation_info'>
				    	<table>
				    		<tbody>
				    			<?php 
				    			if (count($teetime_info) > 1 || $reservations_module) {
				    				foreach ($teetime_info as $teetime) {
				    						echo "<tr><td>";	
				    						echo ($teetime->side == 'front')?'Simulator 1: ':'Simulator 2: ';
											echo date('g:ia', strtotime($teetime->start+1000000)).' - '.date('g:ia', strtotime($teetime->end+1000000)).' '.date('n/j/y T', strtotime($teetime->start+1000000)).'<br/>';
											echo "</td></tr>";	
									}
								} else { ?>
				    			<tr>
				    				<td>Date:</td>
				    				<td><input type="hidden" value ="<?php echo $start?>" id="hidden_start"/>
				                <?php echo date('m/d/Y', strtotime($teetime_info[0]->start+1000000))?></td>
				    			</tr>
				    			<tr>
				    				<td>Time:</td>
				    				<td><?php echo date('g:i a', strtotime($teetime_info[0]->start+1000000))?></td>
				    			</tr>
				    			<tr>
				    				<td>Holes:</td>
				    				<td><input type="hidden" value ="<?php echo $teetime_info[0]->holes?>" id="hidden_holes"/>
				                <?php echo $teetime_info[0]->holes?></td>
				    			</tr>
				    			<tr>
				    				<td>Players:</td>
				    				<td><div id="player_count" class="be_reserve_buttonset">
									    <?php echo $teetime_info[0]->player_count?>
									</div></td>
				    			</tr>
				    			<?php } ?>
				    		</tbody>
				    	</table>
				</fieldset>
				
				<div class="clear"></div>
			</div>
		</div>
		<div class='cancellation_text'>
	<?php if ($teetime_info[0]->status == 'deleted') { ?>
		<div></div>
		<div>This reservation has already been cancelled</div>
	<?php } else if ($teetime_info->paid_player_count == 0) { ?>
		<form action='index.php/be/cancel_reservation/<?php echo $teetime_id.'/'.$person_id ?>' method='post'>
		<div></div>
		<input type='hidden' name='reservations_module' value='<?=$reservations_module?>'/>
		<?php foreach ($teetime_info as $teetime)
		{
			?>
			<input type='hidden' name='reservation_id[]' value='<?=$teetime->reservation_id?>'/>
			<?php
		}
		?>
		<div>Are you sure you would like to cancel your reservation?</div>
		<input type='submit' value='Cancel My Reservation' id='cancel_my_reservation'/>
		</form>
	<?php } else { ?>
		<div>Your reservation cannot be cancelled.</div>
		<div>Please email or call ForeUP for further assistance at 801.215.9487 or support@foreup.com</div>
	<?php } ?>

    <!--p>An email has been sent to your email address with your teetime details.</p-->
    <br/><br/>
   </div>
</div>
<?php $this->load->view("partial/course_footer"); ?>
<?php
$optionsArray = '';
$optionsMenu = array('' => '- None -');
foreach($modifier['options'] as $opt){
	$optionsArray[] = $opt['label'].' ('.to_currency((float) $opt['price']).')';
	$optionsMenu[$opt['label']] = $opt['label'];
}
$optionsString = implode(', ',$optionsArray);
?>
<tr>
	<td>
		<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][modifier_id]" value="<?php echo $modifier['modifier_id']; ?>" class="modifier_id" />
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][name]", 'value' => $modifier['name'], 'style' => 'width: 200px;', 'class'=>'modifier_name', 'disabled'=>false));?>
		</div>
	</td>
	<td>
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][options]", 'value' => $optionsString, 'style' => 'width: 450px;', 'class'=>'modifier_options', 'disabled'=>false));?>
		</div>
	</td>
	<td>
		<div class='form_field'>
			<?php echo form_dropdown("modifiers[{$modifier['modifier_id']}][default]", $optionsMenu, $modifier['default'], "style='width: 150px;'"); ?>
		</div>
	</td>
	<td>
		<a href="#" class="delete_modifier" style="color: white;">X</a>
	</td>
</tr>
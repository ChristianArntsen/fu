<ul id="error_message_box"></ul>
<?php
echo form_open('sales/update_raincheck/' . $raincheck_id, array('id' => 'raincheck_form'));
?>
<fieldset id="raincheck_basic_info">
    <legend>Raincheck Info</legend>
    <div class="field_row clearfix">
        <?php echo form_label('Customer:', 'customer_name', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'customer_name', 'id' => 'customer_name', 'value' => $customer)); ?>
            <?php echo form_hidden('raincheck_customer_id', $customer_id); ?>
        </div>
    </div>

    <div class="field_row clearfix">
        <?php echo form_label('Expiry Date:', 'expiry_date', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'expiry_date', 'id' => 'expiry_date', 'value' => $expiry_date)); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Green Fee:', 'green_fee', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'green_fee', 'id' => 'green_fee', 'value' => $green_fee, 'onkeyup' => 'javascript:update_total_value();')); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Cart Fee:', 'cart_fee', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'cart_fee', 'id' => 'cart_fee', 'value' => $cart_fee, 'onkeyup' => 'javascript:update_total_value();')); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Tax:', 'tax_val', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'tax_val', 'id' => 'tax_val', 'value' => $tax, 'onkeyup' => 'javascript:update_total_value();')); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Value:', 'value', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_label($total, 'total_value', array('class' => 'wide', 'id'=> 'total_label')); ?>
            <?php echo form_hidden('total_value', $total, false, 'total_value'); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php
        echo form_submit(array(
            'name' => 'submitButton',
            'id' => 'submitButton',
            'value' => 'Update Raincheck',
            'class' => 'submit_button float_right')
        );
        ?>
    </div>
</fieldset>
<?php form_close(); ?>
<script>
    $(document).ready(function() {
        $("#customer_name").autocomplete({
            source: '<?php echo site_url("sales/customer_search/ln_and_pn"); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 0,
            select: function(event, ui)
            {
                event.preventDefault();
                $("#customer_name").val(ui.item.label);
                $('#raincheck_customer_id').val(ui.item.value);
            }
        });

        $('#expiry_date').datetimepicker({
            defaultDate: new Date(),
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm',
            defaultValue: '<?php echo date('Y-m-d'); ?> 12:00:00'
        });
        $('#raincheck_form').validate({
            submitHandler: function(form) {
                $(form).mask("<?php echo lang('common_wait'); ?>");
                $(form).ajaxSubmit({
                    success: function(response) {
                        if (response.success) {
                            set_feedback(response.message, 'success_message', false);
                            $.colorbox.close();
                        } else {
                            set_feedback(response.message, 'error_message', true);
                            $.colorbox.close();
                        }
                    },
                    dataType: 'json'
                });
            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules: {
            },
            messages: {
            }
        });
    });
    
    function update_total_value() {
        var gf_fee = $('#green_fee').val();
        var cart_fee = $('#cart_fee').val();
        var tax_price = $('#tax_val').val();
        var total_price = parseFloat(gf_fee) + parseFloat(cart_fee) + parseFloat(tax_price);
        $('#total_value').val(total_price.toFixed(2));
        $('#total_label').text(total_price.toFixed(2));
    }
</script>
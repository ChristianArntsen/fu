<?php
if($export_excel == 1)
{
	//$excelXml = new Excel_XML();
	//$excelXml->setWorksheetTitle($title);
	$rows = array();
	$row = array();
	$row = array();
	foreach ($headers['summary'] as $header)
	{
		$row[] = strip_tags($header['data']);
	}
	$rows[] = $row;

	foreach ($summary_data as $key=>$datarow)
	{
		$row = array();
		foreach($datarow as $cell)
		{
			$row[] = strip_tags($cell['data']);
		}
		$rows[] = $row;

		$row = array();
		foreach ($headers['details'] as $header)
		{
			$row[] = strip_tags($header['data']);
		}
		$rows[] = $row;

		foreach($details_data[$key] as $datarow2)
		{
			$row = array();
			foreach($datarow2 as $cell)
			{
				$row[] = strip_tags($cell['data']);
			}
			$rows[] = $row;
		}
	}

	//$excelXml->addArray($rows);
	//$excelXml->generateXML($title);
	$content = array_to_csv($rows);

	force_download(strip_tags($title) . '.csv', $content);
	exit;
}

if ($export_excel == 2)
{
?>
<?php
	$columns = count($headers['summary']);
	$cell_style = 'width="'.(765/$columns).'" height="40"';
	$details_columns = count($headers['details']);
	$details_cell_style = 'width="'.(765/$details_columns).'" height="40"';
}
else
{
?>
<style>
	#table_holder {
		width:100%;
	}
</style>
<?php
}
?>
<table id="report_contents">
	<tr>
		<td id="item_table">
			<div id="table_holder" style="width: 100%;">
				<table class="tablesorter report" id="sortable_table">
					<thead>
						<?php if ($export_excel == 2) {?>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center;font-size:20px;font-weight:bold;'>
									<?=$title?>
								</div>
							</th>
						</tr>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center; font-size:14px;'>
									<?=$subtitle?>
								</div>
							</th>
						</tr>
						<?php } ?>
						<tr>
							<?php if ($export_excel!=2 && $export_excel != '') {?><th <?=$cell_style?> ><div class="header_cell header">+<span class="sortArrow">&nbsp;</span></div></th><?php } ?>
							<?php foreach ($headers['summary'] as $header) { ?>
							<th  <?=$cell_style?> align="<?php echo $header['align']; ?>"><div class="header_cell header"><?php echo $header['data']; ?><?php if ($export_excel!=2) {?><span class="sortArrow">&nbsp;</span><?php } ?></div></th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php if (count($summary_data) > 0) { ?>
							<?php foreach ($summary_data as $key=>$row) { ?>
							<tr>
								<?php if ($report_type != 'detailed_sales' && $report_type != '') { ?>
								<td <?=$cell_style?> >
									<?php if (isset($details_data[$key]) && count($details_data > 0)) {
										if ($export_excel == 2) {?>

										<?php } else { ?>
											<a href="<?php echo site_url('reports/sale_details').$row['sale_id']; ?>" class="expand" style="font-weight: bold;"><?php echo ($expanded && count($details_data[$key]) > 0)?'-':'+';?></a>
										<?php } ?>
									<?php } ?>
								</td>
								<?php } ?>
								<?php foreach ($row as $cell) { ?>
								<?php 
									if (str_replace("$","", $cell['data']) < '0' && strpos($cell['data'],'$') !== false) { ?>
										<td  class='giftcard_red' <?=$cell_style?> align="<?php echo $cell['align']; ?>"><?php echo ($cell['data'] != '' ? str_replace('xxxxxxxxxxxx', '', $cell['data']) : ' '); ?></td>
									<?php } else { ?>
										<td  <?=$cell_style?> align="<?php echo $cell['align']; ?>"><?php echo ($cell['data'] != '' ? str_replace('xxxxxxxxxxxx', '', $cell['data']) : ' '); ?></td>
								<?php } ?>
								<?php } ?>
							</tr>
							<tr>
								<td colspan="12" class="innertable" <?php echo ($expanded && count($details_data[$key]) > 0)?'style="display:table-cell;"':'';?>>
									<?php if (isset($details_data[$key]) && count($details_data > 0)) {?>
									<table class="innertable">
										<thead>
											<tr>
												<?php foreach ($headers['details'] as $header) { ?>
												<th  <?=$details_cell_style?> align="<?php echo $header['align']; ?>"><?php echo $header['data']; ?></th>
												<?php } ?>
											</tr>
										</thead>

										<tbody>
											<?php foreach ($details_data[$key] as $row2) { ?>

												<tr>
													<?php foreach ($row2 as $cell) { ?>
													<td  <?=$details_cell_style?> align="<?php echo $cell['align']; ?>"><?php echo $cell['data'] != '' ? $cell['data'] : ' '; ?></td>
													<?php } ?>
												</tr>
											<?php } ?>
										</tbody>
									</table>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td  <?=$cell_style?> colspan='<?php echo count($headers['summary'])+1; ?>' style='text-align:center;'>
									<?=$empty_message?>
								</td>
							</tr>
						<?php } ?>
 					</tbody>
				</table>
			</div>

			<table id="report_summary" class="tablesorter report">
			<?php
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				foreach($overall_summary_data as $name=>$value) {
					if ($name == 'account_charges')
						$label = $cdn;
					else if ($name == 'member_balance')
						$label = $mbn;
					else
						$label = lang('reports_'.$name);
				?>
				<?php $value = ($report == 'play_length' ? $value : to_currency($value)); ?>
				<tr class="summary_row"><td width='382' align='right'><?php echo "<strong>".lang('reports_'.$name). '</strong>: '.$value; ?></td></tr>
			<?php }?>
			</table>
		</td>
	</tr>
</table>

<?php if ($export_excel != 2) { ?>
<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$(".tablesorter a.expand").click(function(event)
	{
		$(event.target).parent().parent().next().find('td.innertable').toggle();

		if ($(event.target).text() == '+')
		{
			$(event.target).text('-');
		}
		else
		{
			$(event.target).text('+');
		}
		return false;
	});

	// Ajax toggle sale details
	$(".tablesorter a.get_details").click(function(event){
		var row = $(this).parents('tr:first');
		var url = $(this).attr('href');

		if ($(event.target).text() == '+'){
			$(event.target).text('-');
		}else{
			$(event.target).text('+');
		}

		if(row.next('tr.details').length > 0){
			row.next('tr.details').toggle();
		}else{
			$(event.target).html('<img src="<?php echo base_url('images/spinner_small.gif'); ?>" />');

			$.get(url, null, function(response){
				row.after(response);
				$(event.target).text('-');
			},'html');
		}
		return false;
	});

});
</script>
<?php } ?>

<div class='payment_column_1'>
	<?php if ($mode == 'sale' || ($mode == 'return' && $this->config->item('mercury_id') == '')) { ?>
	<span id='payment_credit_card' class='payment_button'>Credit Card</span>
	<?php } ?>

	<span id='payment_cash' class='payment_button'>Cash</span>

	<?php if ($mode == 'sale') { ?> 
		<span id='payment_check' class='payment_button'>Check</span>
		<!-- <?php $customer_cards = $this->Customer_credit_card->get($customer_id); 
		//print_r($customer_cards);
			foreach ($customer_cards as $card)
			{?>
				<span id='payment_card' class='payment_button card_on_file'><?=$card['card_type']?> - <?=$card['masked_account']?></span>
			<?php
			}
		?> -->
	<?php }
	    echo form_hidden('payment_type','Cash');
	    echo form_hidden('payment_gc_number','');
	?>
</div>

<div class='payment_column_2'>
    <span id='payment_gift_card' class='payment_button'>Gift Card</span>
	<span id='payment_punch_card' class='payment_button'>Punch Card</span>
	<span id='payment_raincheck' class='payment_button'>Raincheck</span>
	<span id='payment_from_account' class='payment_button payment_button_wide <?= (($mode == 'return' || $cab_allow_negative || $customer_account_balance > 0) ? '' : 'disabled')?>'><?=$cab_name;?> Credit</span>
	<span id='payment_from_member_account' class='payment_button payment_button_wide <?= (($is_member && ($mode == 'return' || $cmb_allow_negative || $customer_member_balance > 0))? '' : 'disabled')?>'><?=$cmb_name;?> Credit</span>
	<span id='payment_billing_account' class='payment_button payment_button_wide <?= (($customer && ($invoice_balance_allow_negative || $invoice_balance > 0))? '' : 'disabled')?>'>Billing Account</span>
	<?php if ($mode == 'sale' && $this->config->item('use_loyalty') && $use_loyalty) { ?>
	<span id='payment_from_loyalty' class='payment_button payment_button_wide <?= ($loyalty_points > 0? '' : 'disabled')?>'><?=lang('customers_loyalty_points')?></span>
	<?php } ?>
</div>

<div class='clear'></div>
<?php echo form_close(); ?>
<script>
$(document).ready(function(){
	$("#payment_credit_card").die('click').live('click',function(e){
		submitting_payment = true;
		$("input:[name=payment_type]").val('Credit Card');
		console.log('payment_credit_card');
		
		if (mercury.is_active() && !e.shiftKey){
			mercury.payment_window();
		}else{
			mercury.add_payment();
		}
	});
	$('#payment_cash').die('click').live('click',function(){
		submitting_payment = true;
		$("input:[name=payment_type]").val('Cash');
		mercury.add_payment();
		<?php if ($this->config->item('cash_drawer_on_cash') && $this->config->item('webprnt') != '1') { ?>
		open_cash_drawer();
		<?php } ?>
	});
	$('#payment_check').die('click').live('click',function(){
		submitting_payment = true;
		$("input:[name=payment_type]").val('Check');
		mercury.add_payment();
	});
	$('#payment_gift_card').die('click').live('click',function(e){
		submitting_payment = true;
		$("input:[name=payment_type]").val('<?=lang("sales_giftcard")?>');
		if (!e.shiftKey)
		{
			if (<?=$this->config->item('use_ets_giftcards')?>)
			{
				mercury.payment_window(false, 'giftcard');
			}
			else
			{
				$('#giftcard_data').show();
				$('#back').show();
				$('#make_payment').hide();
				$.colorbox.resize();
				$("#punch_card_number").focus();
			}
		}
		else
		{
			$('#payment_gc_number').val('----------------');
			mercury.add_payment();
		}
	});
	$('#payment_punch_card').die('click').live('click',function(){
		submitting_payment = true;
		$("input:[name=payment_type]").val('<?=lang("sales_punch_card")?>');
		$('#punch_card_data').show();
		$('#back2').show();
		$('#member_account_info').hide();
		$('#make_payment').hide();
		$.colorbox.resize();
		$("#punch_card_number").focus();
	});
	$('#payment_raincheck').die('click').live('click',function(){
		submitting_payment = true;
		$("input:[name=payment_type]").val('<?=lang("sales_raincheck")?>');
		$('#raincheck_data').show();
		$('#back3').show();
		$('#member_account_info').hide();
		$('#make_payment').hide();
		$.colorbox.resize();
		$("#raincheck_number").focus();
	});
	$('#back, #back2, #back3').die('click').live('click',function(){
		submitting_payment = true;
		$('#giftcard_data').hide();
		$('#punch_card_data').hide();
		$('#raincheck_data').hide();
		$('#back').hide();
		$('#back2').hide();
		$('#back3').hide();
		$('#member_account_info').show();
		$('#make_payment').show();
		$.colorbox.resize();
		$("#amount_tendered").focus();

	})
	<?php if($cab_allow_negative || $customer_account_balance > 0) {?>
	$('#payment_from_account').die('click').live('click',function(){
		$("input:[name=payment_type]").val("<?=$cab_name?>");
		mercury.add_payment();
	});
	<?php } ?>
	<?php if($is_member && ($cmb_allow_negative || $customer_member_balance > 0)) {?>
	$('#payment_from_member_account').die('click').live('click',function(){
		$("input:[name=payment_type]").val("<?=$cmb_name?>");
		mercury.add_payment();
	});
	<?php } ?>
	<?php if($customer && ($invoice_balance_allow_negative || $invoice_balance > 0)){ ?>
	$('#payment_billing_account').die('click').live('click',function(){
		$("input:[name=payment_type]").val("Billing Account Balance");
		mercury.add_payment();
	});
	<?php } ?>		
	<?php if($this->config->item('use_loyalty') && ($loyalty_points > 0) && $mode == 'sale') {?>
	$('#payment_from_loyalty').die('click').live('click',function(){
		$("input:[name=payment_type]").val("<?=lang('sales_loyalty')?>");
		mercury.add_payment();
	});
	<?php } ?>
	$('#payment_card_on_file').die('click').live('click',function(){
		$("input:[name=payment_type]").val('Card on file');
		mercury.add_payment();
	});
})
</script>

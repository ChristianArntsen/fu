<?php $this->load->view("partial/header"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	//exit;
}
?>
<div id="receipt_wrapper" class='sales_receipt'>
	<div id="receipt_header">
		<div id="company_name"><?php echo $this->config->item('name'); ?></div>
		<?php if($this->config->item('company_logo')) {?>
		<div id="company_logo"><?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?></div>
		<?php } ?>
		<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
		<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
		<div id="sale_receipt"><?php echo $receipt_title; ?></div>
		<div id="sale_time"><?php echo $transaction_time ?></div>
	</div>
	<div id="receipt_general_info">
		<?php if(isset($customer))
		{
		?>
			<div id="customer"><?php echo lang('customers_customer').": ".$customer; ?></div>
		<?php
		}
		?>
		<div id="sale_id"><?php echo lang('sales_raincheck_id').": RID ".$raincheck_number; ?></div>
		<div id="employee"><?php echo lang('employees_employee').": ".$employee; ?></div>
		<div id="expirydate"><?php echo lang('expiry_date').": ".$expiry_date; ?></div>
		<div id="teesheet_name"><?php echo lang('teesheet_name').": ".$teesheet; ?></div>
	</div>

	<table id="receipt_items">
	<tr>
	<th style="width:33%;text-align:left;"></th>
	<th style="width:20%;text-align:left;"></th>
	<th style="width:15%;text-align:center;"></th>
	<th style="width:16%;text-align:center;"></th>
	<th style="width:16%;text-align:right;"></th>
	</tr>
	
	<tr>
	<td colspan="4" style='text-align:left;'><?php echo lang('sales_green_fee'); ?></td>
	<td colspan="2" style='text-align:right;'><?php echo to_currency($green_fee); ?></td>
	</tr>
	<tr>
	<td colspan="4" style='text-align:left;'><?php echo lang('sales_cart_fee'); ?></td>
	<td colspan="2" style='text-align:right;'><?php echo to_currency($cart_fee); ?></td>
	</tr>
    <tr><td colspan="6">&nbsp;</td></tr>
	<tr>
	<td colspan="4" style='text-align:left;'><?php echo lang('sales_players'); ?></td>
	<td colspan="2" style='text-align:right;'>x <?php echo ($players); ?></td>
	</tr>
	<tr>
	<td colspan="4" style='text-align:left;'><?php echo lang('sales_subtotal'); ?></td>
	<td colspan="2" style='text-align:right;'><?php echo to_currency(($green_fee + $cart_fee)*$players); ?></td>
	</tr>
	<tr><td colspan="6">&nbsp;</td></tr>
	<tr>
	<td colspan="4" style='text-align:left;'><?php echo lang('sales_tax'); ?></td>
	<td colspan="2" style='text-align:right;'><?php echo to_currency($tax); ?></td>
	</tr>
    <tr><td colspan="6">&nbsp;</td></tr>
	<tr>
	<td colspan="4" style='text-align:left;border-top:2px solid #000000;'><?php echo lang('sales_total_credit'); ?></td>
	<td colspan="2" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($total); ?></td>
	</tr>

    <tr><td colspan="6">&nbsp;</td></tr>

	</table>

	<div id="sale_return_policy">
	<?php echo nl2br($this->config->item('return_policy')); ?>
	</div>
	<div id='barcode'>
	<?php echo "<img src='".site_url('barcode')."?barcode=RID $raincheck_number&text=RID $raincheck_number' />"; ?>
	</div>
	<div id="signature">
	----------------------------------------------
	</div>
</div>
<?php $this->load->view("partial/footer"); ?>

<?php if ($this->Appconfig->get('print_after_sale'))
{
?>
<script type="text/javascript">
$(window).load(function()
{
	window.print();
});
</script>
<?php
}
?>
<table>
	<tr>
		<th>Key combination</th>
		<th>What it does</th>
	</tr>
	<tr>
		<td>Alt `</td>
		<td>Show Quick Key Guide</td>
	</tr>
	<tr>
		<td>Navigation</td>
		<td></td>
	</tr>
	<tr>
		<td>Alt + 1</td>
		<td>Tee Sheet/Reservations</td>
	</tr>
	<tr>
		<td>Alt + 2</td>
		<td>Sales</td>
	</tr>
	<tr>
		<td>Alt + 3</td>
		<td>Inventory</td>
	</tr>
	<tr>
		<td>Alt + 4</td>
		<td>Employees</td>
	</tr>
	<tr>
		<td>Alt + 5</td>
		<td>Customers</td>
	</tr>
	<tr>
		<td>Alt + 6</td>
		<td>Gift Cards</td>
	</tr>
	<tr>
		<td>Alt + 0</td>
		<td>Settings</td>
	</tr>
	<tr>
		<td>Sales Helps</td>
		<td></td>
	</tr>
	<tr>
		<td>Alt + g</td>
		<td>Open Gift Card Balance Window</td>
	</tr>
	<tr>
		<td>Alt + p</td>
		<td>Open Payments Window (Pay Now)</td>
	</tr>
	<tr>
		<td>Alt + p</td>
		<td>Open WebPrnt Print Queue</td>
	</tr>

</table>




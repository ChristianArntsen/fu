<ul id="error_message_box"></ul>
<fieldset id="customer_credit_card_info">
	<?php foreach($credit_cards as $credit_card) { ?>
	<div class="field_row clearfix" id='credit_card_row_<?=$credit_card['credit_card_id']?>'>
		<label><?=$credit_card['card_type']?> <?=$credit_card['masked_account']?></label>
		<div class='form_field'>
		<span class='remove_credit_card' data-credit-card-id='<?=$credit_card['credit_card_id']?>'>Delete</span>
		</div>
	</div>
	<?php } ?>
	<div class="field_row clearfix">
		<label></label>
		<div class='form_field'>
		<span class='add_credit_card'>Add New Card</span>
		</div>
	</div>
	<div class='clear' style='text-align:center'>
	<?php
	echo form_submit(array(
		'name'=>'close',
		'id'=>'close_history',
		'value'=>lang('common_close'),
		'class'=>'submit_button float_right')
	);
	?>
	</div>
</fieldset>
<script>
	$(document).ready(function(){
		$('#close_history').click(function(e){
			e.preventDefault();
			$.colorbox2.close();
		});
		$('.add_credit_card').click(function(){
			// OPEN CREDIT CARD WINDOW
			$.colorbox2({'href':'index.php/customers/open_add_credit_card_window/<?=$customer_id?>', 'width':650});
			// ONCOMPLETE RELOAD WINDOW
		});
		$('.remove_credit_card').click(function(){
			// WARN ABOUT BILLINGS WITH THIS CREDIT CARD ATTACHED
			if (confirm('You are about to remove this credit card from this customer account as well as from any associated billing accounts. Are you sure you want to continue?'))
			{
				var credit_card_id = $(this).attr('data-credit-card-id');
				// REMOVE FROM BILLINGS AND MARK AS DELETED
				$.ajax({
		           type: "POST",
		           url: "index.php/customers/delete_credit_card/<?=$customer_id?>/"+credit_card_id,
		           data: '',
		           success: function(response){
		           		console.dir(response);
						// REMOVE FROM HTML
						if (response.success)
							$('#credit_card_row_'+credit_card_id).remove();
				    },
		            dataType:'json'
		         });
			}
		})
	});
</script>
<style>
	fieldset#customer_credit_card_info div.field_row label {
		width: 305px;
	}
	#close_history {
		margin-bottom: 10px;
	}
	.add_credit_card, .remove_credit_card {
		color:blue;
		cursor:pointer;
		font-size:12px;
	}
</style>
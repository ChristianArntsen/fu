<ul id="error_message_box"></ul>
<fieldset id="customer_history_info">
	<div class="field_row clearfix">
		<?php echo form_label('Customer Name:', 'customer_name'); ?>
		<div class='form_field'>
		<?=$person_info->last_name?>, <?=$person_info->first_name?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Most Recent Reservation:', 'customer_name'); ?>
		<div class='form_field'>
		<?=date('F j, Y, g:i a', strtotime($stats['recent_reservation']+1000000))?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Most Recent No Show:', 'customer_name'); ?>
		<div class='form_field'>
		<?=date('F j, Y, g:i a', strtotime($stats['recent_no_show']+1000000))?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Reservations - '.date('F').':', 'customer_name'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
		'name'=>'customer_name',
		'id'=>'customer_name',
		'class'=>'customer_name',
		'size'=>'10',
		'style'=>'text-align:right',
		'disabled'=>true,
		'value'=>$stats['m_booked'])
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Reservations Player Count - '.date('F').':', 'customer_name'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
		'name'=>'customer_name',
		'id'=>'customer_name',
		'class'=>'customer_name',
		'size'=>'10',
		'style'=>'text-align:right',
		'disabled'=>true,
		'value'=>$stats['m_players_booked'])
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('No Shows - '.date('F').':', 'customer_name'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
		'name'=>'customer_name',
		'id'=>'customer_name',
		'class'=>'customer_name',
		'size'=>'10',
		'style'=>'text-align:right',
		'disabled'=>true,
		'value'=>$stats['m_no_shows'])
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Reservations - '.date('Y').':', 'customer_name'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
		'name'=>'customer_name',
		'id'=>'customer_name',
		'class'=>'customer_name',
		'size'=>'10',
		'style'=>'text-align:right',
		'disabled'=>true,
		'value'=>$stats['m_booked'])
		);?>
		</div>
	</div>
		<div class="field_row clearfix">
		<?php echo form_label('Reservations Player Count - '.date('Y').':', 'customer_name'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
		'name'=>'customer_name',
		'id'=>'customer_name',
		'class'=>'customer_name',
		'size'=>'10',
		'style'=>'text-align:right',
		'disabled'=>true,
		'value'=>$stats['ytd_players_booked'])
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('No Shows - '.date('Y').':', 'customer_name'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
		'name'=>'customer_name',
		'id'=>'customer_name',
		'class'=>'customer_name',
		'size'=>'10',
		'style'=>'text-align:right',
		'disabled'=>true,
		'value'=>$stats['ytd_no_shows'])
		);?>
		</div>
	</div>
	<div class='clear' style='text-align:center'>
	<?php
	echo form_submit(array(
		'name'=>'close',
		'id'=>'close_history',
		'value'=>lang('common_close'),
		'class'=>'submit_button float_right')
	);
	?>
	</div>
</fieldset>
<script>
	$(document).ready(function(){
		$('#close_history').click(function(e){
			e.preventDefault();
			$.colorbox2.close();
		});
	});
</script>
<style>
	fieldset#customer_history_info div.field_row label {
		width: 305px;
	}
	#close_history {
		margin-bottom: 10px;
	}
</style>
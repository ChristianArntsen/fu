<?php
echo form_open('customers/save_passes/',array('id'=>'customer_passes_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_passes">
<legend><?php echo lang("customers_create_new_pass"); ?></legend>

<div class="field_row clearfix">	
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'new_pass',
		'id'=>'new_pass',
		'placeholder'=>lang('customers_pass_name'),
		'size'=>20,
		'value'=>'')
	);?>
	<span class='' id='add_pass_button'><?php echo lang('customers_create_pass')?></span>
	</div>
</div>
<div id='passes_container'>
	<div class="groups_title">	
	<?php echo form_label(lang('customers_passes'), 'passes'); ?>
	</div>
	<div class='groups_subtitle'>
	<?php echo form_label(lang('customers_click_pass_edit'), 'passes'); ?>
	</div>
	
	<?php 
	$first_label = true;
	foreach($passes as $pass)
	{
	?>
		<div class='form_field'  id='row_<?php echo $pass['pass_id']; ?>'>
			<?	echo '<span id="gid_'.$pass['pass_id'].'"><span  class="pass_label">'.$pass['label'].'</span></span>'
				
			//' <span style="color:#ccc;">('.$group['member_count'].' members)</span>';
			?>
			<div class='clear'></div>
		</div>
	<? 
	$first_label = false;
	} ?>
	<div id='clear' class='clear'></div>
</div>
<?php 
/*$first_label = true;
foreach($passes as $pass)
{
?>
	<div class="field_row clearfix" id='row_<?php echo $pass['pass_id']; ?>'>	
<?php echo form_label(($first_label)?lang('customers_passes').':':'', 'passes'); ?>
	<div class='form_field'>
		<?	echo '<span class="delete_item" onclick="passes.delete_pass(\''.$pass['pass_id'].'\', \''.addslashes(str_replace('"', '', $pass['label'])).'\')">Delete</span> '.
		'<span id="gid_'.$pass['pass_id'].'"><span  class="pass_label">'.$pass['label'].'</span></span>'.
		' <span style="color:#ccc;">('.$pass['member_count'].' members)</span>';?>
	</div>
</div>
<? 
$first_label = false;
} */?>

</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>
var passes = {
	add_pass:function() {
		var title = $('#new_pass').val();
		title = addslashes(title.replace('"', ''));
		if (title != '')
		{
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/add_pass/",
	            data: "pass_label="+title,
	            success: function(response){
	             	$('#new_pass').val('');
	           		var html = '<div class="form_field" id="row_'+response.pass_id+'">'+	
							'<span id="gid_'+response.pass_id+'"><span  class="pass_label">'+title+'</span></span>';
						
					$('#passes_container #clear').before($(html));
					passes.add_pass_click_event();
	    			$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	},
	save_pass_name:function(pass_id) {
		var title = ($('#pass_text_'+pass_id).val().replace('"', ''));
		$.ajax({
            type: "POST",
            url: "index.php/customers/save_pass_name/"+pass_id,
            data: "pass_label="+title,
            success: function(response){
            	var el = $('#gid_'+pass_id);
            	console.log(el.html());
				el.html('<span  class="pass_label">'+title+'</span>');
				passes.add_pass_click_event();
            },
            dataType:'json'
        });
	},
	add_pass_click_event:function() {
		$('.pass_label').click(function(e){
			var el = $(e.target).parent();
			var text = $(e.target).html();
			var id = el.attr('id');
			var gid = id.replace('gid_','');
			console.log('id '+id);
			el.html('<span class="delete_item" onclick="passes.delete_pass(\''+gid+'\', \''+addslashes(text.replace('"', ''))+'\')">Delete</span><span class="save_item" onclick="passes.save_pass_name(\''+gid+'\')">Save</span><input id="pass_text_'+gid+'" value="'+text+'"/>');
			//el.html('<span class="save_item" onclick="passes.save_pass_name(\''+gid+'\')">Save</span><input id="pass_text_'+gid+'" value="'+text+'"/>');
		});
		console.log('added');
	},
	delete_pass:function(pass_id, title) {
		var answer = confirm('Are you sure you want to delete pass: '+title+'?');
		if (answer) {
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/delete_pass/"+pass_id,
	            data: "",
	            success: function(response){
					$('#row_'+pass_id).remove();
					$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	}
};

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#add_pass_button').click(function(){
		passes.add_pass();
	});
	$('#new_pass').keypress(function(e){
		if(e.which == 13) {
			e.preventDefault();
			passes.add_pass();
		}
	});
    
    $('#customer_passes_form').validate({
		submitHandler:function(form)
		{
			$.colorbox.close();
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	passes.add_pass_click_event();
});
</script>
<?php
$lang['module_home']='Home';

$lang['module_teesheets']='Tee Sheet';
$lang['module_teesheets_desc']='Manage';

$lang['module_schedules']='Schedules';
$lang['module_schedules_desc']='Manage';

$lang['module_auto_mailers']='Auto Mailers';
$lang['module_auto_mailers_desc']='Manage';

$lang['module_recipients']='Recipients';
$lang['module_recipients_desc']='Manage recipients';

$lang['module_courses']='Courses';
$lang['module_courses_desc']='Manage';

$lang['module_invoices']='Invoices';
$lang['module_invoices_desc']='Manage';

$lang['module_timeclock']='Timeclock';
$lang['module_timeclock_desc']='Clock in/Clock out';

$lang['module_billings']='Billing';
$lang['module_billings_desc']='Manage';

$lang['module_customers']='Customers';
$lang['module_customers_desc']='Add, Update & Search';

$lang['module_suppliers']='Suppliers';
$lang['module_suppliers_desc']='Add, Update & Search';

$lang['module_employees']='Employees';
$lang['module_employees_desc']='Add, Update & Search';

$lang['module_sales']='Sales';
$lang['module_sales_desc']='& Returns Processing';

$lang['module_food_and_beverage']='Food & Beverage';
$lang['module_food_and_beverage_desc']='Sales Portal';

$lang['module_reports']='Reports';
$lang['module_reports_desc']='View & Generate';

$lang['module_items']='Inventory';
$lang['module_items_desc']='Add, Update & Search';

$lang['module_config']='Settings';
$lang['module_config_desc']='& Preferences';

$lang['module_receivings']='Receivings';
$lang['module_receivings_desc']='Process Purchase Orders';

$lang['module_giftcards']='Gift Cards';
$lang['module_giftcards_desc']='Add, Update & Search';

$lang['module_item_kits']='Item Kits';
$lang['module_item_kits_desc']='Add, Update & Search Kits';

$lang['module_marketing_campaigns']='Marketing';
$lang['module_marketing_campaigns_desc']='Campaign Management';

$lang['module_promotions'] = 'Promotions';
$lang['module_promotions_desc'] = 'Add, Update & Search';

$lang['module_dashboards'] = 'Dashboards';
$lang['module_dashboards_desc'] = 'Overall Information';

$lang['module_events'] = 'Events';
$lang['module_events_desc'] = 'Manage Events';

// user modules
$lang['module_reservations'] = 'Reservations';
$lang['module_reservations_desc'] = 'Manage Reservations';

$lang['module_directories'] = 'Directory';
$lang['module_directories_desc'] = 'Member Directory';

$lang['module_calendar'] = 'Events';
$lang['module_calendar_desc'] = 'Events/Calendar';

$lang['module_account'] = 'Account';
$lang['module_account_desc'] = 'Manage Account';

$lang['module_newsletter'] = 'Newsletter';
$lang['module_newsletter_desc'] = 'Read newsletters';

$lang['module_settings'] = 'Settings';
$lang['module_settings_desc'] = 'Change course settings';

$lang['module_quickbooks'] = 'Quickbooks';
$lang['module_quickbooks_desc'] = 'Integrate with Quickbooks';

$lang['module_information'] = 'Information';
$lang['module_information_desc'] = 'Golf Course Information';

$lang['module_tournaments'] = 'Tournaments';
$lang['module_tournaments_desc'] = 'Add & Update';
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * table headers for generat stats
 */
$lang['dashboard_genstat_course_count'] = 'Course Count';
$lang['dashboard_genstat_customer_count'] = 'Customer Count';
$lang['dashboard_genstat_employee_count'] = 'Employee Count';
$lang['dashboard_genstat_teetimes_booked'] = 'Teetimes Booked';
$lang['dashboard_genstat_online_bookings'] = 'Online Bookings';
$lang['dashboard_genstat_total_sales'] = 'Total Sales';
$lang['dashboard_genstat_mercury_sales'] = 'Mercury Sales';

$lang['dashboard_courseinfo_course_name'] = 'Course Name';
$lang['dashboard_courseinfo_last_month_texts'] = 'Last Months Texts';
$lang['dashboard_courseinfo_this_month_texts'] = 'This Months Texts';
$lang['dashboard_courseinfo_last_month_emails'] = 'Last Months Emails';
$lang['dashboard_courseinfo_this_month_emails'] = 'This Months Emails';
$lang['dashboard_courseinfo_using_mercury'] = 'Using Mercury';


$lang['dashboard_no_data_to_display'] = 'No data to display.';


<?php

/**
 * Result container object for the SOAP ->connectionError() method call
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * @license LICENSE.txt 
 * 
 * @package QuickBooks
 * @subpackage Server
 */

/**
 * Result interface
 */
require_once 'QuickBooks/Result.php';

/**
 * Result class for ->closeConnection() SOAP method
 */
class QuickBooks_Result_CloseConnection extends QuickBooks_Result
{
	/**
	 * A message indicating the connection has been closed/update was successful
	 * 
	 * @var string
	 */
	public $closeConnectionResult;
	
	/**
	 * Create a new result object
	 * 
	 * @param string $resp		A message indicating the connection has been closed
	 */
	public function __construct($ticket, $status = null, $wait_before_next_update = null, $min_run_every_n_seconds = null)
	{
		$this->closeConnectionResult = $ticket;
	}
}


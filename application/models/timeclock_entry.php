<?php
class Timeclock_entry extends CI_Model
{
	function is_clocked_in($employee_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$this->db->from('timeclock_entries');
		$this->db->where('employee_id', $employee_id);
		$this->db->limit(1);
		$this->db->order_by('shift_start', 'desc');
		
		$last_login = $this->db->get()->result_array();
		if (isset($last_login[0]) && $last_login[0]['shift_end'] == '0000-00-00 00:00:00')
			return true;
		
		return false;
	}
	function get_info($employee_id, $start_time)
	{
		$this->db->from('timeclock_entries');
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start', $start_time);
		$this->db->limit(1);
		
		return $this->db->get()->result_array();
	}
	function clock_in($employee_id = false, $terminal_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$this->db->insert('timeclock_entries', array('employee_id'=>$employee_id, 'terminal_id'=>$terminal_id, 'shift_start'=>date('Y-m-d H:i:s')));
	}
	
	function clock_out($employee_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_end', '0000-00-00 00:00:00');
		$this->db->update('timeclock_entries', array('shift_end'=>date('Y-m-d H:i:s')));
	}
	function save($employee_id, $start_time) {
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start', $start_time);
		$this->db->update('timeclock_entries', array('shift_start'=>$this->input->post('shift_start'),'shift_end'=>$this->input->post('shift_end'), 'terminal_id'=>$this->input->post('terminal_id')));
	}
	function delete($employee_id, $start_time) {
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start', $start_time);
		$this->db->delete('timeclock_entries');
	}
	function get_all($limit = 1000, $employee_id = false, $start = '1969-12-31', $end = '3000-12-31') {
		$start = date("Y-m-d", strtotime($start)).' 00:00:00';
		$end = date("Y-m-d", strtotime($end)).' 23:59:59';
		$employee_id = ($employee_id ? $employee_id : $this->session->userdata('person_id'));
		$results = $this->db->query("SELECT `shift_start` ,  `shift_end` , CASE WHEN shift_end != '0000-00-00 00:00:00' THEN TIMEDIFF( shift_end, shift_start ) ELSE 0 END AS total, label AS terminal FROM (`foreup_timeclock_entries`) LEFT JOIN foreup_terminals ON foreup_timeclock_entries.terminal_id = foreup_terminals.terminal_id WHERE employee_id = '$employee_id' AND shift_start > '$start' AND shift_start < '$end' ORDER BY  `shift_start` DESC LIMIT $limit");
		return $results;
	}
}
?>
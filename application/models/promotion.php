<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotion extends CI_Model
{
  public function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
    $this->db->from('promotion_definitions');
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("expiration_date", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	public function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('promotion_definitions');
        $this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}
  public function get_last_coupon_id()
  {
      $course_id = $this->session->userdata('course_id');
      $result = $this->db->query('SELECT MAX(id) AS id FROM foreup_promotion_definitions WHERE course_id=\'' . $course_id . '\'');
      $id = $result->result_array();
      
      return $id[0]['id'];
  }      
  public function get_coupon_data($coupon_type_id)
  {
      $result = $this->db->query('SELECT * FROM foreup_promotion_definitions WHERE id='.'\''.$coupon_type_id.'\'');
      $return_array = $result->result_array();
      return $return_array[0];
  }
  public function search($search, $limit=20, $offset = 0)
	{
		$course_id = '';
	    if (!$this->permissions->is_super_admin())
	    {
	      $pr = $this->db->dbprefix('promotion_definitions');
	      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
	    }
	    $this->db->from('promotion_definitions');
	    $this->db->like('lower(name)', strtolower($search));
		$this->db->where("deleted = 0 $course_id");
	    $this->db->order_by("id", "asc");
		// Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		$this->db->limit($limit);
	    return $this->db->get();
	}

  public function get_info($promotion_id)
  {
	$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('promotion_definitions');
	$this->db->where('id',$promotion_id);
	$this->db->where("deleted = 0 $course_id");

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $campaign_id is NOT an campaign
			$campaign_obj=new stdClass();

			//Get all the fields from campaigns table
			$fields = $this->db->list_fields('promotion_definitions');

			foreach ($fields as $field)
			{
				$campaign_obj->$field='';
			}

			return $campaign_obj;
		}
	}

  function get_search_suggestions($search,$limit=25)
  {
		       
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $suggestions = array();

		$this->db->from('promotion_definitions');
		$this->db->like('lower(title)', strtolower($search));
                $this->db->where("deleted = 0 $course_id");
		$this->db->order_by("id", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->title);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
                
    }
    function get_coupon_name_suggestions($search, $limit=25)
    {
        $course_id = "AND course_id='{$this->session->userdata('course_id')}'";
        
        $result = $this->db->query("SELECT *, name AS label, id AS value FROM foreup_promotion_definitions WHERE name LIKE '%$search%' $course_id AND deleted='0' ORDER BY name ASC");
        $suggestions = $result->result_array();
        
        if (count($suggestions) > $limit)
        {
            $suggestions = array_slice($suggestions, 0, $limit);
        }
        return $suggestions;
    }
  /*
   * Determines if a given promotion_id is an promotion
	*/
	function exists( $campaign_id )
	{
		$this->db->from('promotion_definitions');
		$this->db->where('id',$campaign_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
        function is_valid_day_of_week($promotion_id)
        {
            $today = date('l', time());
            
            //log_message('error', "TIME TODAY: $today");
            $result = $this->db->query("SELECT * FROM foreup_promotion_definitions WHERE id='$promotion_id'");
            $row = $result->result_array();  
            
            if ($row[0][$today] == 1)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
  /*
   * inserts a new record
   */
  function save(&$promotion_data,$promotion_id=false)
  {
      //log_message('error', 'PROMOTION: ' . $promotion_data['expiration_date']);
		if (!$promotion_id or !$this->exists($promotion_id))
		{
			if($this->db->insert('promotion_definitions',$promotion_data))
			{
				$promotion_data['id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('id', $promotion_id);
		return $this->db->update('promotion_definitions',$promotion_data);
	}

  /*
	Deletes one promotion
	*/
	function delete($promotion_id)
	{
    /*
     * TO DO: validation checking
     */
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("id = '$promotion_id' $course_id");
		return $this->db->update('promotion_definitions', array('deleted' => 1));
	}

	/*
	Deletes a list of promotions
	*/
	function delete_list($promotion_ids)
	{
    /*
     * TO DO: validation checking
     */
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('id',$promotion_ids);
		return $this->db->update('promotion_definitions', array('deleted' => 1));
 	}
        function is_coupon_redeemed($coupon_id, $coupon_type)
        {
            $result = $this->db->query('SELECT redeemed FROM foreup_promotions WHERE id=\''. $coupon_id . '\' AND coupon_definition_id=\'' . $coupon_type . '\'');
            $array = $result->result_array();          
            if ($array[0]['redeemed'] !== 'N')
            {
                return true;              
            }
            else
            {
                return false;
            }         
        }
}
<?php
class Table_Layout_Object extends CI_Model
{
	function __construct(){
		parent::__construct();
	}

	// Saves a new object
	function save($params){

		$params = elements(array('object_id', 'layout_id', 'label', 'type', 'pos_x',
			'pos_y', 'width', 'height', 'rotation'), $params, null);

		if(empty($params['label'])){
			$params['label'] = null;
		}

		if(empty($params['object_id'])){
			if((int) $params['layout_id'] == 0){
				return false;
			}

			$success = $this->db->insert('table_layout_objects', array(
				'layout_id' => (int) $params['layout_id'],
				'label' => $params['label'],
				'type' => $params['type'],
				'pos_x' => $params['pos_x'],
				'pos_y' => $params['pos_y'],
				'width' => $params['width'],
				'height' => $params['height'],
				'rotation' => $params['rotation'],
				'date_created' => date('Y-m-d h:m:s')
			));
			$object_id = $this->db->insert_id();

		}else{
			if($params['label'] == null){
				$params['label'] = '';
			}
			foreach($params as $key => $value){
				if($value === null){
					unset($params[$key]);
				}
			}
			if($params['label'] == ''){
				$params['label'] = null;
			}
			$object_id = $params['object_id'];
			unset($params['object_id'], $params['layout_id']);
			
			if($params['label']){	
				$this->db->set('label', $params['label']);
			}
			$success = $this->db->update('table_layout_objects', $params, 'object_id = '.(int) $object_id);
		}

		if($success){
			return $object_id;
		}else{
			return false;
		}
	}
	
	function has_open_sale($object_id){
		$this->db->select('sale_id');
		$this->db->from('tables');
		$this->db->join('table_layout_objects', 'table_layout_objects.label = tables.table_id', 'inner');
		$this->db->where('table_layout_objects.object_id', (int) $object_id);
		$this->db->where('tables.course_id', $this->session->userdata('course_id'));
		$row = $this->db->get()->row_array();
		
		if(!empty($row['sale_id'])){
			return (int) $row['sale_id'];
		}else{
			return false;
		}
	}
	
	function object_exists($label = '', $object_id = false){
		
		if($label == '' || $label == null){
			return false;
		}
		$this->db->select('object_id');
		$this->db->from('table_layout_objects');
		$this->db->join('table_layouts','table_layouts.layout_id = table_layout_objects.layout_id');
		$this->db->where('table_layout_objects.label', $label);
		$this->db->where('table_layouts.course_id', (int) $this->session->userdata('course_id'));
		$this->db->group_by('object_id');
		
		$row = $this->db->get()->row_array();
		
		if(!empty($row['object_id'])){
			
			if(!empty($object_id) && $row['object_id'] == $object_id){
				return false;
			}else{
				return (int) $row['object_id'];
			}
		}else{
			return false;
		}
	}
	
	// Retrieve object information
	function get($layout_id, $label = null)
	{
		$this->db->select('*', false);
		$this->db->from('table_layout_objects');
		if(!empty($label)){
			$this->db->where('label', $label);
		}
		$this->db->where('layout_id', (int) $layout_id);
		$result = $this->db->get();

		$data = array();
		if(!empty($label)){
			$data = $result->row_array();
		}else{
			$data = $result->result_array();
		}

		return $data;
	}

	// Delete layout object
	function delete($object_id){

		if(empty($object_id)){
			return false;
		}

		$result = $this->db->delete('table_layout_objects', array('object_id'=>$object_id));
		return $result;
	}
}
?>

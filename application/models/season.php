<?php
class Season extends CI_Model 
{
    /*
	Gets information about a particular teesheet
	*/
	function get_info($season_id)
	{
		if (!$season_id || $season_id < 1) return false;
		
		$this->db->from('seasons');
        $this->db->where("season_id = '$season_id'");

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else return false;
    }
   
    /*
	Returns all the seasons
	*/
	function get_all($teesheet_id = false, $make_array = false)
	{
		$course_id = '';
        $this->db->select('seasons.holiday, seasons.season_id, seasons.default, seasons.start_date, seasons.end_date, 
			seasons.season_name, teesheet.title AS teesheet_title, seasons.teesheet_id');
        $this->db->from('seasons');
        $this->db->join('teesheet', 'teesheet.teesheet_id = seasons.teesheet_id AND `foreup_teesheet`.deleted = 0', 'left');
		$this->db->where('seasons.deleted', 0);
		if(!empty($teesheet_id)){
			$this->db->where('seasons.teesheet_id', $teesheet_id);
		}
		$this->db->where('seasons.course_id', $this->session->userdata('course_id'));
        $this->db->order_by('seasons.teesheet_id', 'ASC');
        $this->db->order_by("seasons.default", "ASC");
        $this->db->order_by("seasons.start_date", "ASC");
        $this->db->order_by("seasons.holiday", "DESC");
		$results = $this->db->get()->result();
		
		if ($make_array === false)
			return $results;
		
		$return[0] = "None";
		foreach ($results as $result)
		{
			$return[$result->season_id] = $result->season_name;
		}
		return $return;
	}
	
	/*
	Inserts or updates a season
	*/
	function save(&$season_data, $season_id=false, $create_default_timeframe = true)
	{
		if (!$season_id || !$this->exists($season_id))
		{
			if(empty($season_data['course_id'])){
				$season_data['course_id'] = (int) $this->session->userdata('course_id');
			}
			
			if($this->db->insert('seasons', $season_data))
			{
				$season_id = $this->db->insert_id();
				$season_data['season_id'] = $season_id;
				
				if($create_default_timeframe){
					$this->create_default_timeframe($season_id);
				}

				return $season_id;
			}
			return false;
		}
		
		$this->db->where('season_id', $season_id);
		$this->db->update('seasons', $season_data);
		
		return $this->db->affected_rows();
	}
	
	/*
	Creates a default timeframe using the default price category for a season
	*/
	function create_default_timeframe($season_id, $course_id = false)
	{
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		
		$this->load->model('Price_class');
		$this->load->model('Pricing');
		$default_price = $this->Price_class->get_default($course_id);
		$class_id = (int) $default_price['class_id'];

		$this->add_price_class($season_id, $class_id);
		
		$timeframe_data = array(
			'class_id'			=> $class_id,
			'season_id'			=> $season_id,
			'timeframe_name'	=> 'Default',
			'monday'			=> 1,
			'tuesday'			=> 1,
			'wednesday'			=> 1,
			'thursday'			=> 1,
			'friday'			=> 1,
			'saturday'			=> 1,
			'sunday'			=> 1,
			'start_time'		=> 0,
			'end_time'			=> 2400,
			'default'			=> 1,
			'active' 			=> 1
		);
		
		if ($this->Pricing->add_timeframe($timeframe_data)){
			return true;
		}
		return false;
	}
	
	function is_default_timeframe($timeframe_id){
		
		$this->db->select('default');
		$this->db->from('seasonal_timeframes');
		$this->db->where('timeframe_id', (int) $timeframe_id);
		$row = $this->db->get()->row_array();
		
		if($row['default'] == 1){
			return true;
		}else{
			return false;
		}
	}	
	
	/*
	Creates a default season for a new teesheet by duplicating the default season from the default teesheet
	*/
	function create_default($teesheet_id, $course_id = false)
	{
		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}
		
		$season_data = array(
			'season_name'	=> 'Default Season',
			'teesheet_id'	=> (int) $teesheet_id,
			'start_date'	=> "0000-01-01",
			'end_date'		=> "0000-12-31",
			'default'		=> 1,
			'course_id'		=> (int) $course_id
		);
		
		// Check if default already exists for the teesheet
		$this->db->select('season_id');
		$query = $this->db->get_where('seasons', array('default' => 1, 'teesheet_id' => $teesheet_id));
		
		if($query->num_rows() > 0){
			$row = $query->row_array();
			return $row['season_id'];
		}
		
		$this->db->insert('seasons', $season_data);
		$season_id = $this->db->insert_id();
		$this->create_default_timeframe($season_id, $course_id);
		
		return $season_id;
	}
	
	/*
	 Gets the default season ID for a teesheet
	*/
	function get_default($teesheet_id)
	{
		$this->db->select('season_id');
		$this->db->from('seasons');
		$this->db->where('default', 1);
		$this->db->where('deleted', 0);
		$this->db->where('teesheet_id', $teesheet_id);
		
		$result = $this->db->get()->result_array();
		return $result[0]['season_id'];
	}
	
	/*
	 Determines if a given season_id is a season
	*/
	function exists($season_id)
	{
		$this->db->from('seasons');
		$this->db->where('season_id',$season_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
	
	/*
	 Deletes a season
	*/
	function delete_season($season_id = false)
	{
		if (empty($season_id)){
			return false;
		}
		
		$this->db->delete('seasons', array('season_id' => $season_id, 'default' => 0));
		$season_deleted = $this->db->affected_rows();
		
		if($season_deleted){
			$this->db->delete('seasonal_timeframes', array('season_id' => $season_id));
			$this->db->delete('season_price_classes', array('season_id' => $season_id));
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * Deletes a price class (including it's timeframes) from a season
	 */ 
	function delete_price_class($season_id, $class_id){
		if(empty($season_id) || empty($class_id)){
			return false;
		}
		
		$season_id = (int) $season_id;
		$class_id = (int) $class_id;
		
		$this->db->query("DELETE season_price_class.*, season_timeframe.*
			FROM foreup_season_price_classes AS season_price_class
			LEFT JOIN foreup_seasonal_timeframes AS season_timeframe
				ON season_timeframe.class_id = season_price_class.class_id
				AND season_timeframe.season_id = season_price_class.season_id
			WHERE season_price_class.season_id = {$season_id}
				AND season_price_class.class_id = {$class_id}");
		
		return $this->db->affected_rows();
	}
	
	/*
	 * Adds a price class to a season
	 */ 
	function add_price_class($season_id, $class_id){
		if(empty($season_id) || empty($class_id)){
			return false;
		}
		
		$season_price_class = array(
			'class_id'			=> $class_id,
			'season_id'			=> $season_id,
			'date_created' 		=> date('Y-m-d H:i:s')
		);
		
		$query = $this->db->get_where('season_price_classes', array('class_id'=>$class_id, 'season_id'=>$season_id));
		if($query->num_rows() > 0){
			return false;
		}
		
		$this->db->insert('season_price_classes', $season_price_class);
		return $this->db->affected_rows();
	}	
	
	/*
	 * Duplicate season data and pricing to a new season
	 */
	function duplicate($season_id, $teesheet_id)
	{
		if(empty($season_id) || empty($teesheet_id)){
			return false;
		}
		$this->load->model('Pricing');
		
		$season_data = $this->get_info($season_id);
		$season_data->teesheet_id = (int) $teesheet_id;
		$season_data->default = 0;
		unset($season_data->season_id);
		$season_data = (array) $season_data;
		
		$new_season_id = $this->save($season_data, false, false);
		
		if(empty($new_season_id)){
			return false;
		}
		
		// Get price classes attached to existing season
		$prices = $this->get_price_classes($season_id);

		// Loop through price classes and copy over timeframes to new season
		foreach ($prices as $price){
			
			$this->add_price_class($new_season_id, $price['price_class_id']);
			
			// Get timeframes of price class
			$timeframes = $this->Pricing->get_timeframes($season_id, $price['price_class_id']);
			
			foreach($timeframes as $timeframe){
				$timeframe = (array) $timeframe;
				
				unset($timeframe['timeframe_id']);
				$timeframe['season_id'] = $new_season_id;
				
				// Add timeframe to season price class
				$this->Pricing->add_timeframe($timeframe);
			}
		}
		
		return $new_season_id;
	}
	
	/*
	 * Get price classes associated with a particular season
	 */
	function get_price_classes($season_id){
		
		if(empty($season_id)){
			return false;
		}
		
		$this->db->select("price.name AS price_class_name, price.default AS price_class_default, 
			COUNT(timeframe_id) AS num_timeframes, GROUP_CONCAT(timeframe.timeframe_id) AS timeframe_ids, 
			GROUP_CONCAT(timeframe.timeframe_name SEPARATOR ', ') AS timeframe_names, timeframe.active,
			price.class_id AS price_class_id", false);
		$this->db->from('season_price_classes AS season_price_class');
		$this->db->join('price_classes AS price', 'price.class_id = season_price_class.class_id', 'inner');		
		$this->db->join('seasonal_timeframes AS timeframe', 'timeframe.class_id = price.class_id AND timeframe.season_id = season_price_class.season_id', 'left');
		$this->db->where('season_price_class.season_id', (int) $season_id);
		$this->db->group_by('season_price_class.class_id');		
		$this->db->order_by('price.default ASC');
		$query = $this->db->get();
		
		$rows = $query->result_array();

		if(!empty($rows)){
			foreach($rows as &$row){
				$row['timeframes'] = array();
				$timeframe_id_array = explode(',', $row['timeframe_ids']);
				$timeframe_name_array = explode(',', $row['timeframe_names']);
				
				foreach($timeframe_id_array as $key => $timeframe_id){
					$timeframe = array();
					$timeframe['timeframe_id'] = (int) $timeframe_id;
					$timeframe['timeframe_name'] = trim($timeframe_name_array[$key]);
					$row['timeframes'][] = $timeframe;
				}
			}
		}

		return $rows;
	}	
	
	/*
	 * Get the season for a certain date
	 */
	function get_by_date($teesheet_id, $date)
	{
		$date = "0000-" . $date;
		
		$this->db->from('seasons')
				->where("teesheet_id", $teesheet_id)
				->where("start_date <=", $date)
				->where("end_date >=", $date)
				->order_by("holiday", "desc")
				->order_by("default", "asc")
				->order_by("end_date", "desc")
				->limit(1);
		
		$query = $this->db->get();
		
		if($query->num_rows()==1)
			return $query->row();
		else
			return false;
	}
}
?>

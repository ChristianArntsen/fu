<?php
class Payment extends CI_Model
{
	function exists($payment_type, $sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where("payment_type", $payment_type);
		$this->db->where('sale_id', $sale_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	function get_all($sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id', $sale_id);
		return $this->db->get();
	}
	function get($sale_id, $type = null)
	{
		$this->db->select('payment_type AS type, payment_amount AS amount, invoice_id, tip_recipient');
		$this->db->from('sales_payments');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('payment_type', $type);
		return $this->db->get()->row_array();
	}
	function add($sales_payments_data)
	{
		if ($sales_payments_data['sale_id'])
		{
	//		$this->db->query("UPDATE foreup_sales SET payment_type = CONCAT(payment_type, '{$sales_payments_data['payment_type']}: \${$sales_payments_data['payment_amount']}<br/>') WHERE sale_id = {$sales_payments_data['sale_id']}");
//			if ($this->exists($sales_payments_data['payment_type'], $sales_payments_data['sale_id']))
			if (!$this->exists($sales_payments_data['payment_type'], $sales_payments_data['sale_id']))
			{
				//$this->db->ignore();
	        	if($this->db->insert('sales_payments',$sales_payments_data) && $this->update_sales_payments($sales_payments_data['sale_id']))
				{
					return true;
				}
				return false;
			}

			$this->db->where('sale_id', $sales_payments_data['sale_id']);
			$this->db->where('payment_type', $sales_payments_data['payment_type']);
			return ($this->db->update('sales_payments', $sales_payments_data)  && $this->update_sales_payments($sales_payments_data['sale_id']));
		}
		else {
			return false;
		}
	}
	function update_sales_payments($sale_id)
	{
		$payment_info = '';
		$payments = $this->get_all($sale_id);
		foreach ($payments->result_array() as $payment)
		{
			$payment_info .= "{$payment['payment_type']}: \${$payment['payment_amount']}<br/>";
		}
		$this->db->where('sale_id',$sale_id);
		return $this->db->update('sales', array('payment_type'=>$payment_info));
	}
}
?>

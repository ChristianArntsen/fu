<?php
class Price_class extends CI_Model 
{
    /*
	Gets information about a particular price class
	*/
	function get($class_id)
	{
		if (!$class_id || $class_id < 1) return false;
		
		$this->db->select('class_id, name, default, color'); 
		$this->db->from('price_classes');
        $this->db->where('class_id', $class_id);

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row_array();
        }
        
        return false;
    }
    
	function get_all()
	{
		$course_id = (int) $this->session->userdata('course_id');
		
		$this->db->select('class_id, name, default, color');
		$this->db->from('price_classes');
		$this->db->where('course_id', $course_id);
        $this->db->order_by('default', 'DESC');		
        $query = $this->db->get();
		
		return $query->result_array();
    }
    
	function get_default($course_id = false){
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		
		$query = $this->db->get_where('price_classes', array('default' => 1, 'course_id' => (int) $course_id));
		return $query->row_array();
	}
	
	function get_customer_price_class($customer_id){
		
		$this->db->select('price_class');
		$this->db->from('customers');
		$this->db->where('person_id', (int) $customer_id);

		$query = $this->db->get();
		$row = $query->row_array();
		
		if(empty($row)){
			return false;
		}
		
		return (int) $row['price_class'];
	}
	
	function create_default($course_id = false){
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		
		$data = array(
			'name' => 'Regular',
			'default' => 1,
			'cart' => 1,
			'course_id' => $course_id
		);
		
		$query = $this->db->get_where('price_classes', array('default' => 1, 'course_id' => $course_id));
		if($query->num_rows() > 0){
			return true;
		}
		
		$this->db->insert('price_classes', $data);
		return $this->db->affected_rows;
	}
    
    /*
	Returns all the price classes for a given season
	*/
	function get_all_from_season($season_id, $limit=10000, $offset=0)
	{
		$this->db->from('price_classes')
			->join('seasonal_timeframes',"seasonal_timeframes.class_id = price_classes.class_id")
			->where("seasonal_timeframes.deleted",0)
			->where("price_classes.deleted",0)
			->where('seasonal_timeframes.season_id',$season_id)
			->group_by('price_classes.class_id')
			->order_by("default", "DESC")
			->order_by("name", "ASC")
			->limit($limit)
			->offset($offset);
		$results = $this->db->get();
		return $results->result();
	}
	
	/*
	 Returns all the unused price classes for a given season
	*/
	function get_available_from_season($season_id)
	{
		if(empty($season_id)){
			return false;
		}
		
		$this->db->select('price.name, price.class_id')
			->from('price_classes AS price')
			->join('season_price_classes AS season_price_class', 'season_price_class.class_id = price.class_id AND season_price_class.season_id = '.(int) $season_id, 'left')
			->where('price.course_id', $this->session->userdata('course_id'))
			->where('price.default', 0)
			->where('season_price_class.season_id IS NULL');
			
		$results = $this->db->get();
		$price_classes = $results->result_array();

		return $price_classes;
	}
	
	/*
	 * Get a list of price classes we're already using this season
	 */
	function get_used($season_id)
	{
		//Get all the different price classes we're already using in the season
		$this->db
			->select('class_id')
			->from('seasonal_timeframes')
			->where('season_id',$season_id)
			->where('deleted',0)
			->group_by('class_id');
		$results = $this->db->get();
		return $results->result();
	}
	
	/*
	 * Get a list of price classes to suggest
	 */
	function get_suggestions($teesheet_id, $season_id, $term)
	{
		if (empty($teesheet_id)) $teesheet_id = 0;
		if (empty($season_id)) $season_id = 0;
		
		//Get all the different price classes that aren't defaults that we're using in the teesheet
		$this->db
			->select("price_category AS label, class_id AS value")
			->from('price_classes')
			->where('teesheet_id',$teesheet_id)
			->where('deleted',0)
			->where('default',0)
			->where('price_category !=','')
			->like('price_category', $term)
			->order_by('price_category','asc');
		$results = $this->db->get();
		$price_classes = $results->result();
		
		$used_classes = $this->get_used($season_id);
		foreach ($used_classes as $row)
			$used[] = $row->class_id;
		
		//Remove any used price classes from the original list
		foreach ($price_classes as $key => $price_class)
		{
			if (in_array($price_class->value, $used))
			{
				unset($price_classes[$key]);
			}
		}
		
		return $price_classes;
	}
	
	/*
	 * Deletes a price class
	 */
	function delete($class_id)
	{
		if (empty($class_id)){
			return false;
		}
		$this->db->delete('price_classes', array('class_id'=>$class_id, 'course_id'=>$this->session->userdata('course_id'), 'default'=>0));
		$price_deleted = $this->db->affected_rows();
		
		// Delete any timeframes associated with the price class
		if($price_deleted){
			$this->db->delete('seasonal_timeframes', array('class_id' => $class_id));
			return true;
		}
		
		return false;
	}
	
	/*
	Inserts or updates a price class
	*/
	function save($class_id = null, $name = null, $default = 0, $color = ''){
		
		if(empty($name)){
			return false;
		}
		$course_id = (int) $this->session->userdata('course_id');
		
		if(empty($class_id)){
			$response = $this->db->insert('price_classes', array('name' => $name, 'default' => (int) $default, 'color' => $color, 'course_id' => $course_id));
			$class_id = (int) $this->db->insert_id();
		
		}else{
			$response = $this->db->update('price_classes', array('name' => $name, 'color' => $color), array('class_id' => $class_id, 'course_id' => $course_id));
		}
	
		return $class_id;
	}

	/*
	 Determines if a given class_id is a price class
	*/
	function exists($class_id)
	{
		$this->db->from('price_classes');
		$this->db->where('class_id',$class_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
}
?>

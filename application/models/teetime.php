<?php
class Teetime extends CI_Model
{
	public $teetime_price_classes = array();

	/*
	Determines if a given teetime_id is an teetime
	*/
	function exists($teetime_id)
	{
		$this->db->from('teetime');
		$this->db->where("TTID", $teetime_id);
		$this->db->where('teesheet_id', $this->session->userdata('teesheet_id') ? $this->session->userdata('teesheet_id') : $this->input->get('schedule_id'));
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	/*
	Returns all the teetimes
	*/
	function get_all($limit=10000, $offset=0)
	{
        $course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('teetime');
		$this->db->where("deleted = 0 $course_id");
        $this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function get_teetime_stats()
	{
		$results = $this->db->query("SELECT count(TTID) AS total, COUNT(DISTINCT foreup_teesheet.course_id) AS booking_courses, SUM(IF(details LIKE '%online booking%', 1, 0)) AS online FROM foreup_teetime JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_teetime.teesheet_id WHERE TTID LIKE '____________________' AND status != 'deleted'");
		return $results->result_array();
	}
	function get_teetime_course_stats()
	{
		$return_array = array();
		$this->load->model('Communication');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$results = $this->db->query("SELECT foreup_courses.name as name, foreup_courses.course_id AS course_id, count(TTID) AS total, SUM(IF(details LIKE '%online booking%', 1, 0)) AS online,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date' AND details LIKE '%online booking%', 1, 0 )) AS online_this_month,
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date' AND details LIKE '%online booking%', 1, 0 )) AS online_last_month,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', 1, 0 )) AS reserved_this_month,
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', 1, 0 )) AS reserved_last_month
			FROM foreup_teetime
			JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_teetime.teesheet_id
			JOIN foreup_courses ON foreup_courses.course_id = foreup_teesheet.course_id
			WHERE TTID LIKE '____________________' AND status != 'deleted'
			GROUP BY foreup_courses.course_id ORDER BY foreup_courses.name");

		foreach($results->result_array() as $result)
			$return_array[$result['course_id']] = $result;

		return $return_array;
	}
	function get_play_length_data($start_date, $end_date, $teesheet_id)
	{
		$results = $this->db->query("SELECT teed_off_time, turn_time, finish_time, TIMESTAMPDIFF(MINUTE, teed_off_time, turn_time) AS half_time, TIMESTAMPDIFF(MINUTE, teed_off_time, finish_time) AS total_time FROM foreup_teetime
			WHERE teed_off_time >= '$start_date 00:00:00' AND teed_off_time <= '$end_date 23:59:59' AND TTID LIKE '____________________' AND status != 'deleted'
			AND teesheet_id = $teesheet_id AND (turn_time != '0000-00-00 00:00:00' OR finish_time != '0000-00-00 00:00:00')
			ORDER BY teed_off_time");

		return $results->result_array();
	}
	function count_all($last_week=false)
	{/*
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('teetime');
		$this->db->where("deleted = 0 $course_id");
        return $this->db->count_all_results();*/
	    $this->db->from('teetime');
	    $this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
	    if (!$this->permissions->is_super_admin())
	            $this->db->where('course_id', $this->session->userdata('course_id'));
	    if($last_week === true)
	        $this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
	    return $this->db->count_all_results();
	}

	/*
	Gets information about a particular teetime
	*/
	function get_info($teetime_id, $include_title = false)
	{
        //Get Tee Sheet title also
        if ($include_title)
	        $this->db->select('teetime.*, teesheet.title AS tee_sheet_title');
		$this->db->from('teetime');
		if ($include_title)
			$this->db->join('teesheet', 'teetime.teesheet_id = teesheet.teesheet_id', 'LEFT');
		$this->db->where("TTID = '$teetime_id'");
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $teetime_id is NOT a teetime
			$teetime_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('teetime');

			foreach ($fields as $field)
			{
				$teetime_obj->$field='';
			}

			return $teetime_obj;
		}
	}
	function switch_sides($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$this->db->query("UPDATE foreup_teetime SET side = CASE WHEN side = 'front' THEN 'back' ELSE 'front' END WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') LIMIT 2");
		//echo $this->db->last_query();
		echo json_encode(array());
	}
    function split_by_time($teetime_id)
    {
        // FIND HOW MANY TIME SLOTS THIS SPANS, THEN SAVE A DUPLICATE TEE TIME FOR EACH SLOT
        $main_id = substr($teetime_id, 0, 20);
        $increment = $this->session->userdata('increment');
        $tti = $tee_time_info = $this->get_info($main_id);
        $rtti = $reround_tee_time_info = $this->get_info($main_id.'b');
		// CALCULATE HOW MANY TIME SLOTS
        //echo 'tt end - '.$tti->end.' - tt start - '.$tti->start.'<br/>';
        $reservation_minutes = (strtotime($tti->end + 1000000) - strtotime($tti->start + 1000000)) / 60;
        //echo 'res min - '.$reservation_minutes.'<br/>';
        $tee_time_count = ceil($reservation_minutes / $increment);
        //echo "TTC ".$tee_time_count;
        if ($tee_time_count > 1)
        {
            $new_reservation_array = array();
            // COPY DATA FROM OLD RESERVATIONS
            $new_tee_time_data = (array) $tti;
            $new_reround_tee_time_data = (array) $rtti;
            //echo 'start - '.$tti->start.' - end - '.$tti->end.'<br/>';

            for ($i = 0; $i < $tee_time_count; $i++)
            {
                // GENERATE NEW TEE TIME ID
                $tee_time_id = $this->generate_id();
                // GENERATE NEW START AND END TIMES
                $asm = $asrm = $additional_minutes = $i * $increment;
                $aem = $aerm = $additional_minutes + $increment;
				$this->clean_time($asm, $tti->start);
				$this->clean_time($asrm, $rtti->start);
				$this->clean_time($aem, $tti->start);
				$this->clean_time($aerm, $rtti->start);
                //echo 'adjusted - '.($tti->start + 1000000).' - add min - '.$additional_minutes.'<br/>';
                //echo '';
                $new_start = date('YmdHi', strtotime($tti->start + 1000000 ." + $asm minutes")) - 1000000;
                $new_end = date('YmdHi', strtotime($tti->start + 1000000 ." + $aem minutes")) - 1000000;
                $r_new_start = date('YmdHi', strtotime($rtti->start + 1000000 ." + $asrm minutes")) - 1000000;
                $r_new_end = date('YmdHi', strtotime($rtti->start + 1000000 ." + $aerm minutes")) - 1000000;
                // ASSIGN IDs AND TIMES
                $new_tee_time_data['TTID'] = $tee_time_id;
                $new_reround_tee_time_data['TTID'] = $tee_time_id.'b';
                $new_tee_time_data['start'] = $new_start;
                $new_tee_time_data['end'] = $new_end;
                $new_reround_tee_time_data['start'] = $r_new_start;
                $new_reround_tee_time_data['end'] = $r_new_end;
                // INSERT NEW TEE TIMES INTO DATABASE
                $this->db->insert('teetime', $new_tee_time_data);
                $this->db->insert('teetime', $new_reround_tee_time_data);
                // PREP RESERVATIONS FOR RETURN
                $new_reservation_array[] = $this->add_teetime_styles($new_tee_time_data);
                $new_reservation_array[] = $this->add_teetime_styles($new_reround_tee_time_data);
            }
            // DELETE ORIGINAL TEE TIME
            $this->db->where('teesheet_id', $this->session->userdata('teesheet_id'));
            $this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
            $this->db->limit(2);
            $this->db->update('teetime', array('status'=>'deleted'));

			$tti = (array) $tti;
			$rtti = (array) $rtti;
			$tti['status'] = 'deleted';
            $rtti['status'] = 'deleted';
            $new_reservation_array[] = $this->add_teetime_styles($tti);
            $new_reservation_array[] = $this->add_teetime_styles($rtti);

            return $new_reservation_array;
        }
        else
            return array();
    }

	function mark_as_teedoff($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$teesheet_id = $this->session->userdata('teesheet_id');

		$this->db->query("UPDATE foreup_teetime
			SET status = CASE
				WHEN `status` = 'deleted' THEN 'deleted'
				ELSE 'teed off'
			END,
			teed_off_time = ".$this->db->escape($date)."
			WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$result = $this->db->get();
		$teetimes = $result->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function mark_as_turned($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET turn_time = ".$this->db->escape($date)." WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();
		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function mark_as_finished($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$teesheet_id = $this->session->userdata('teesheet_id');

		$this->db->query("UPDATE foreup_teetime SET finish_time = ".$this->db->escape($date)." WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();
		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function zero_teed_off($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET teed_off_time = '0000-00-00 00:00:00' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function zero_turn($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET turn_time = '0000-00-00 00:00:00' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function update_credit_card($teetime_id, $cc_data)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $this->session->userdata('teesheet_id');
		return $this->db->query("UPDATE foreup_teetime SET credit_card_id = '{$cc_data['credit_card_id']}', credit_card_type = '{$cc_data['card_type']}' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
	}
	function check_in($teetime_id, $count)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET paid_player_count = paid_player_count + $count WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('status !=', 'deleted');
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	/*
	Checks whether an existing time is still available before saving
	*/
	function check_availability($teetime_data)
	{
		if ($teetime_data['status'] == 'deleted')
			return true;
		//added this to allow the API to use this function as well as online booking
		if (!$teetime_data['teesheet_id']) {
			$teetime_data['teesheet_id'] = $this->session->userdata('teesheet_id');
		}
		//Check if closed or an event
		$this->db->from('teetime');
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teetime_data['teesheet_id']);
		$this->db->where('side', $teetime_data['side']);
		$this->db->where("type !=", 'teetime');
		if ($teetime_data['TTID'])
			$this->db->where("TTID !=", $teetime_data['TTID']);

		$this->db->where("start <=", $teetime_data['start']);
		$this->db->where('end >', $teetime_data['start']);
		$event_result = $this->db->get();
		//echo $this->db->last_query();

		//Check if teetimes spots are available
		$this->db->select('sum(player_count) as total_players');
		$this->db->from('teetime');
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teetime_data['teesheet_id']);
		$this->db->where('side', $teetime_data['side']);
		$this->db->where("(start = {$teetime_data['start']} OR (start < {$teetime_data['start']} AND end > {$teetime_data['start']}))");
		if ($teetime_data['TTID'])
			$this->db->where("TTID !=", $teetime_data['TTID']);
		$this->db->group_by('type');
		$result = $this->db->get()->result_array();

		// echo $this->db->last_query();
		// print_r(array(
			// 'event'=>$event_result->num_rows() > 0?'y':'n',
			// 'num_rows'=>$event_result->num_rows(),
			// 'result'=>$result[0]?'y':'n',
			// 'total_players'=>$result[0]['total_players'],
			// 'player_count'=>$teetime_data['player_count'],
			// 'total'=>$result[0]['total_players']+$teetime_data['player_count'],
			// 'tp'=>($result[0]['total_players'] + $teetime_data['player_count'] > 4)?'y':'n'));
		if ($event_result->num_rows() > 0 OR (isset($result[0]) && ($result[0]['total_players'] + $teetime_data['player_count'] > 4))){
			// echo "FALSE";
			return false;
		}
		else
		{
			// echo "TRUE";
			return true;
		}

	}
	function update_purchased_teetime()
	{
		$teetime_id = $this->session->userdata('teetime_to_purchase');
		$teetime_info = $this->get_info($teetime_id);
		$players = $this->session->userdata('players_to_purchase');
		$carts = $this->session->userdata('carts_to_purchase');
		$invoice_id = $this->session->userdata('invoice_id');
		$json = array();
		$cart_count = ($carts == 1)?$players:0;
		$data = array(
			'teetime_id'=>$teetime_id,
			'teesheet_id'=>$teetime_info->teesheet_id,
			'start'=>$teetime_info->start,
			'end'=>$teetime_info->end,
			'holes'=>$teetime_info->holes,
			'date_booked'=>$teetime_info->date_booked,
			'booker_id'=>$teetime_info->booker_id,
			'player_count'=>$players,
			'paid_player_count'=>$players,
			'carts'=>$cart_count,
			'paid_carts'=>$cart_count,
			'booking_source'=>'online',
			'details'=>'Reserved and Paid For using online booking @ '.date('g:ia n/j T')
		);

		$this->save($data, $teetime_id, $json);
		//echo $this->db->last_query();
		$data['invoice_id'] = $invoice_id;
		unset($data['paid_player_count']);
		unset($data['paid_carts']);
		unset($data['booking_source']);
		unset($data['details']);
		unset($data['TTID']);
		//Save all original data in our teetimes_bartered table
		return $this->db->insert('teetimes_bartered', $data);
		//echo $this->db->last_query();
	}
	
	function save_purchased_teetime($teetime_id, $purchased_players, $purchased_carts, $invoice_id){
		
		$teetime_info = $this->get_info($teetime_id);
		
		$carts = 0;
		if($purchased_carts){
			$carts = $purchased_players;
		}
		
		// Update tee time with paid information
		$updated_teetime = array(
			'player_count' => $purchased_players,
			'carts' => $carts,
			'paid_player_count' => $purchased_players,
			'paid_carts' => $carts,
			'details' => 'Reserved and Paid For using online booking @ '.date('g:ia n/j T')
		);
		$this->db->update('teetime', $updated_teetime, array('TTID' => $teetime_id));
		$this->db->update('teetime', $updated_teetime, array('TTID' => $teetime_id.'b'));
	
		$bartered_teetime = array(
			'teetime_id' => $teetime_id,
			'teesheet_id' => $teetime_info->teesheet_id,
			'start' => $teetime_info->start,
			'end' => $teetime_info->end,
			'player_count' => $purchased_players,
			'holes' => $teetime_info->holes,
			'carts' => $carts,
			'date_booked' => $teetime_info->date_booked,
			'booker_id' => $teetime_info->booker_id,
			'invoice_id' => $invoice_id
		);
		
		// Save bartered tee time data
		$this->db->insert('teetimes_bartered', $bartered_teetime);
		return $this->db->affected_rows(); 	
	}
	
	/*
	Adjust for the 7/8 split
	*/
	function clean_time(&$time, $base_time = 0)
	{
		if ($this->session->userdata('increment') == 7.5)
		{
			$minutes = floor(ceil($time + $base_time)/100)*60+ceil($time + $base_time)%100;
			if ($minutes%15 != 0)
				$time = floor($time);
			else
				$time = ceil($time);
		}
	}
	
	function adjust_7_8_time($time){
		if (fmod($time, 15) != 0){
			$time = floor($time);
		}else{
			$time = ceil($time);
		}
		return $time;		
	}
	
	/*
	Inserts or updates a teetime
	*/
	function save(&$teetime_data,$teetime_id=false, &$json_ready_events=array(), $online_booking = false, $course_id = false)
	{
		// CHECK AVAILABILITY IF WE'RE COMING FROM ONLINE BOOKING
		if ($online_booking)
		{
			if (!$this->check_availability($teetime_data))
				return array('success'=>false, 'message'=>'error', 'send_confirmation'=>'');
			//TODO: Check if already booked maximum per individual
		}
		// IF WE DON'T ALREADY HAVE AN ID, ASSIGN ONE
		if ($teetime_id)
			$cur_teetime_data = $this->get_info($teetime_id);
		else
			$teetime_id = $this->generate_id('tt');
		// ASSIGN ID TO SECOND TEE TIME
		$second_id = $teetime_id.'b';
	    if (strlen($teetime_id) > 20)
            $second_id = substr($teetime_id, 0, 20);
		$second_data = array();

		// FROM ONLINE BOOKING OR API, WE PASS IN COURSE ID, OTHERWISE GET INFO FROM SESSION
        $teesheet_holes = 18;
		if ($course_id) {
			$schedule_id = $teetime_data['teesheet_id'] ? $teetime_data['teesheet_id'] : $teetime_data['schedule_id'];
			$teesheet_data = $this->teesheet->get_info($schedule_id);
			$course_info = $this->course->get_info($course_id);
			$frontnine = $teesheet_data->frontnine;
			$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $teesheet_data->increment);
			if ($dif > 0)
				$frontnine += $teesheet_data->increment - $dif;
			$is_simulator = $teesheet_data->is_simulator;
            $teesheet_holes = $teesheet_data->holes;
		}
		else
		{
			$frontnine = $this->session->userdata('frontnine');
			$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $this->session->userdata('increment'));
			if ($dif > 0)
				$frontnine += ($this->session->userdata('increment') - $dif);
			$is_simulator = $this->session->userdata('is_simulator');
		}


		if ($teetime_data['booking_source'] == 'online' || $teetime_data['booking_source'] == 'api' || $teetime_data['split'] == true) //Not yet adjusting for simulators
		{
			unset($teetime_data['split']);
			$second_data = $teetime_data;
			$teetime_data['TTID'] = $teetime_id;
			$second_data['TTID'] = $second_id;
			$second_data['side'] = 'back';
			if ($this->session->userdata('holes') == 9 || $teesheet_holes == 9)
				$second_data['side'] = 'front';
			$second_data['start'] += $frontnine;
            $second_data['end'] += $frontnine;

			if ($second_data['start']%100 >59)
            	$second_data['start'] += 40;
            if ($second_data['end']%100 >59)
            	$second_data['end'] += 40;
			if ($is_simulator || $teetime_data['holes'] == 9)
				$second_data['status'] = 'deleted';
		}
		else {
			$course_holes = $this->session->userdata('holes');
	        $start = $end = '';
			if (isset($teetime_data['start']))
				$start = $teetime_data['start'];
			else if (isset($cur_teetime_data['start']))
				$start = $cur_teetime_data['start'];
	        if (isset($teetime_data['end']))
				$end = $teetime_data['end'];
			else if (isset($cur_teetime_data['end']))
				$end = $cur_teetime_data['end'];

	        $second_data = $teetime_data;
			$second_data['TTID'] = $second_id;
			if (!isset($teetime_data['status']) && isset($teetime_data['holes']) && $teetime_data['holes'] == 18)
				$second_data['status'] = $cur_teetime_data->status;
			$success = true;
			// Erasing second tee time if there is one since we're only playing 9
	        if (isset($teetime_data['holes']) && $teetime_data['holes'] == 9) {
	            if (strlen($teetime_id) > 20)
					$teetime_data['status'] = 'deleted';
				else
					$second_data['status'] = 'deleted';
			}
	        // Updating secondary tee time
	        $second_side = 'back';
	        if ((isset($cur_teetime_data->side) && $cur_teetime_data->side =='back') ||
		        	(isset($teetime_data['side']) && $teetime_data['side'] == 'back') ||
		        	$course_holes == 9)
	            $second_side = 'front';

	        if (strlen($teetime_id) > 20) {
	            unset($second_data['start']);
			    unset($second_data['end']);
			}
			else {
				$second_data['start'] = $start + $frontnine;
	        	$second_data['end'] = $end + $frontnine;

				if ($second_data['start']%100 >59)
		            $second_data['start'] += 40;
		        if ($second_data['end']%100 >59)
		            $second_data['end'] += 40;
			}
	    	$second_data['side'] = $second_side;
		}
		if (isset($second_data['start']))
			$this->clean_time($second_data['start']);
		if (isset($second_data['end']))
			$this->clean_time($second_data['end']);
        $second_time_available = true;
		if (strlen($teetime_id) == 20 && $second_data['status'] != 'deleted')
        	$second_time_available = $this->check_availability($second_data);

		if ($online_booking)
		{
			if (!$second_time_available)
				return array('success'=>false, 'message'=>'reround_error', 'send_confirmation'=>'');
		}

        $second_data['start_datetime'] = date("Y-m-d H:i:s", strtotime($second_data['start'] + 1000000));
        $second_data['end_datetime'] = date("Y-m-d H:i:s", strtotime($second_data['end'] + 1000000));
		if (!$second_id or !$this->exists($second_id))
		{
            $second_data['date_booked'] = date('Y-m-d H:i:s');
            if($this->db->insert('teetime',$second_data))
        		$success = true;
            else
	        	$success = false;
            if ($teetime_data['booking_source'] == 'standby')
                $json_ready_events[] = $this->add_teetime_styles(array_merge($second_data));
		}
		else
		{
			$this->db->where('TTID', $second_id);
			$this->db->limit(1);
			$success = $this->db->update('teetime',$second_data);
			$json_ready_events[] = $this->add_teetime_styles(array_merge($second_data, (array)$this->get_info($second_data['TTID'])));
		}
		$teetime_data['TTID'] = $teetime_id;
        $teetime_data['start_datetime'] = date("Y-m-d H:i:s", strtotime($teetime_data['start'] + 1000000));
        $teetime_data['end_datetime'] = date("Y-m-d H:i:s", strtotime($teetime_data['end'] + 1000000));
		$json_ready_events[] = $this->add_teetime_styles(array_merge((array)$cur_teetime_data, $teetime_data));
        if ($success && !$this->exists($teetime_id))
		{
			$teetime_data['date_booked'] = date('Y-m-d H:i:s');
			if($this->db->insert('teetime',$teetime_data))
        		$saved = true;
			else
				$saved = false;
		}
		else {
			//echo 'making it to the end';
			$this->db->where('TTID', $teetime_id);
			$this->db->limit(1);
			$saved = $this->db->update('teetime',$teetime_data);
		}
		// Send a confirmation email if we haven't done it yet.
		//echo 'saved: '.$saved.' person_id '.$teetime_data['person_id'];
		//print_r($teetime_data);
		//echo (!isset($teetime_data) || !isset($teetime_data['confirmation_emailed']) || !$teetime_data['confirmation_emailed'])?'true':'false';
		//echo (!isset($cur_teetime_data) || !isset($cur_teetime_data->confirmation_emailed) || !$cur_teetime_data->confirmation_emailed)?'true':'false';

		//echo 'trying to get in';
		//echo 'get in '.$saved && $teetime_data['person_id'] != '' && ((!isset($teetime_data['confirmation_emailed']) || !$teetime_data['confirmation_emailed']) && (!isset($cur_teetime_data) || !isset($cur_teetime_data['confirmation_emailed']) || !$cur_teetime_data['confirmation_emailed']));
		$send_confirmation = '';
		if ($saved && $teetime_data['person_id'] != 0 &&
			(!isset($teetime_data) || (!isset($teetime_data['confirmation_emailed']) || !$teetime_data['confirmation_emailed']) &&
			(!isset($cur_teetime_data) || !isset($cur_teetime_data->confirmation_emailed) || !$cur_teetime_data->confirmation_emailed)) &&
			$this->input->post('send_confirmation_emails'))
		{
			//echo 'insie';
			$send_confirmation =  $teetime_id;
		}

		log_message('error', 'Last database: ' . $this->db->last_query());
		return array('success'=>$saved, 'send_confirmation'=>$send_confirmation, 'second_time_available'=>$second_time_available, 'teetime_id'=>$teetime_id);
	}
	function search($customer_id='')
	{
		$teetime_html = '';
		if ($customer_id == '')
			$teetime_html = "<div class='teetime_result'>No results</div>";
		else {
			$this->db->from("teetime");
			$current_day = (date('Ymd')-100).'0000';
			$teesheet_id = $this->session->userdata('teesheet_id');
			$this->db->where("TTID LIKE '____________________' AND status != 'deleted' AND teesheet_id = $teesheet_id AND start >= $current_day AND (person_id = $customer_id OR person_id_2 = $customer_id OR person_id_3 = $customer_id OR person_id_4 = $customer_id OR person_id_5 = $customer_id)");
			$teetimes = $this->db->get()->result_array();
			foreach ($teetimes as $teetime)
			{
				$date = date("F j, Y, g:i a", strtotime($teetime['start']+1000000));
				$teetime_html .= "<div class='teetime_result'><div>{$date}</div><div>Players: {$teetime['player_count']}</div></div>";
			}
		}
		return $teetime_html;
	}

	function send_reminder_email($email, $subject, $data, $from_email, $from_name)
	{
		$view = $this->load->view("email_templates/reservation_reminder",$data, true);
		send_sendgrid(
				$email,
				$subject,
				$view,//$this->load->view("email_templates/reservation_reminder",$data, true),
				$from_email,
				$from_name
			);
	}

	function send_confirmation_email($email, $subject, $data, $from_email, $from_name)
	{
		send_sendgrid(
			$email,
			$subject,
			$this->load->view("email_templates/reservation_made",$data, true),
			$from_email,//$this->session->userdata('course_email'),
		 	$from_name
		);
		$TTID = substr($data['TTID'], 0, 20);
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET confirmation_emailed = 1 WHERE (TTID = '{$TTID}' OR TTID = '{$TTID}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();
	}

	function get_price_class($teetime_price_class = 1, $customer_price_class = 1, $start_time, $teetime_id){
		$start_time = (int) substr($start_time, 8);
		$not_holiday = true;

		$price_category_index = 1;

		if(!empty($teetime_price_class)){
			$price_category_index = $teetime_price_class;

		}else if($this->config->item('holidays') && $this->Appconfig->is_holiday()){
			$price_category_index = 7;
			$not_holiday = false;

		}else if(!empty($customer_price_class)){
			$price_category_index = $customer_price_class;

		}else if ((int) $this->config->item('early_bird_hours_begin') <= $start_time && (int)$this->config->item('early_bird_hours_end') > $start_time){
			$price_category_index = 2;

		}else if ((int) $this->config->item('morning_hours_begin') <= $start_time && (int)$this->config->item('morning_hours_end') > $start_time){
			$price_category_index = 3;

		}else if ((int) $this->config->item('afternoon_hours_begin') <= $start_time && (int)$this->config->item('afternoon_hours_end') > $start_time){
			$price_category_index = 4;

		}else if ((int) $start_time >= $this->config->item('super_twilight_hour')){
			$price_category_index = 6;

		}else if ((int) $start_time >= $this->config->item('twilight_hour')){
			$price_category_index = 5;
		}
		$this->teetime_price_classes[$teetime_id] = $price_category_index;

		return $price_category_index;
	}

	function add_teetime_styles($teetime_data) {
	
		//echo 'adding teetime styles';
		//return $teetime_data;
		$return_data = array();
		$return_data['id'] = $teetime_data['TTID'];
		$return_data['type'] = $teetime_data['type'];
		$return_data['name'] = $this->teesheet->clean_for_json($teetime_data['title']);		
		$return_data['status'] = $teetime_data['status'];
		$return_data['title'] = $this->teesheet->clean_for_json($teetime_data['title']);
		$return_data['allDay'] = '';
		$return_data['stimestamp'] = $teetime_data['start'];
		$return_data['etimestamp'] = $teetime_data['end'];
        $return_data['start'] = $teetime_data['start'];
		$return_data['end'] = $teetime_data['end'];
        $return_data['side'] = $teetime_data['side'];
		$return_data['player_count'] = $teetime_data['player_count'];
		$return_data['carts'] = $teetime_data['carts'];
		$return_data['backgroundColor']= '';
		if ($teetime_data['holes'] == 18)
			$return_data['borderColor']= '';
		else
			$return_data['borderColor']= '';
		//Add classes for styling
		$booked_class = $cart_class = $checkedin_class = $teed_off_class = $raincheck_class = $online_class = $details_class = '';
		if ($teetime_data['booking_source'])
			$booked_class = 'booked_online';
        if ($teetime_data['carts'] > 0)
            $cart_class = 'carts';
        if (($teetime_data['status'] == 'checked in' || $teetime_data['status'] == 'walked in') && !$this->config->item('sales'))
            $checkedin_class = 'checked_in';
		if ($teetime_data['teed_off_time'] != '0000-00-00 00:00:00')
			$teed_off_class = 'teed_off';
		if ($teetime_data['turn_time'] != '0000-00-00 00:00:00')
			$teed_off_class .= ' mark_turn';
		if ($teetime_data['finish_time'] != '0000-00-00 00:00:00')
			$teed_off_class .= ' mark_finished';
		if ($teetime_data['raincheck_players_issued'] > 0)
			$raincheck_class .= ' rainchecks_issued';
		if ($teetime_data['details'] != '')
			$details_class = 'details';
		if ($teetime_data['booking_source'] == 'online' || $teetime_data['booking_source'] == 'ForeUP App' || $teetime_data['booking_source'] == 'api')
			$online_class = ' online_booking';

		$pc = ($teetime_data['player_count']>5)?5:$teetime_data['player_count'];
		$ppc = ($teetime_data['paid_player_count']>$pc)?$pc:$teetime_data['paid_player_count'];

	    if ($ppc == 0 || $teetime_data['type'] != 'teetime')
            $paid_class = '';
		else if ($this->config->item('simulator'))
			$paid_class =  "paid_4_4";
	    else
            $paid_class = "paid_{$ppc}_{$pc}";
		$return_data['className'] = $teetime_data['type'].' holes_'.$teetime_data['holes'].' players_'.$teetime_data['player_count'].' '.$cart_class.' '.$checkedin_class.' '.$paid_class.' '.$teed_off_class.' '.$raincheck_class.' '.$online_class.' '.$details_class;
		$return_data['className'] .= (strlen($teetime_data['TTID']) > 20 ? ' reround':'');
		$return_data['className'] .= (strlen($teetime_data['TTID']) == 20 ? ' first_nine':'');

		// If teetime is a re-round, use the previous 9 price class
		if(strlen($teetime_data['TTID']) > 20 && !empty($this->teetime_price_classes[substr($teetime_data['TTID'], 0, 20)])){
			$price_class_id = $this->teetime_price_classes[substr($teetime_data['TTID'], 0, 20)];
		}else{
			$price_class_id = $this->get_price_class($teetime_data['default_price_category'], $teetime_data['price_class_1'], $teetime_data['start'], $teetime_data['TTID']);
		}
		$return_data['className'] .= ' price_color_'.$price_class_id;

		return $return_data;

//                $return_data['start'] = $this->format_time_string($teetime->start);
//                $return_data['end'] = $this->format_time_string($teetime->end);

        /*if ($teetime_data->type == 'teetime' && ($teetime_data->status == 'checked in' || $teetime_data->status == 'walked in')) {
            $return_data['backgroundColor'] = '#5c9ccc';
            $return_data['borderColor'] = '#4297d7';
        }
        else if ($teetime_data->type == 'tournament') {
            $return_data['backgroundColor'] = '#5f9b5f';
            $return_data['borderColor'] = '#336133';
        }
        else if ($teetime_data->holes == 18) {
            $return_data['borderColor'] = '#333';
            $return_data['backgroundColor'] = '#445651';
        }*/



	}

	function change_date($teetime_id, $new_date, $new_end_date, $back_new_date, $back_new_end_date, $teesheet_id = '')
	{
		$teesheet_id = ($teesheet_id != '') ? $teesheet_id : $this->session->userdata('teesheet_id');
		//$teetime_info = $this->get_info($teetime_id)->result_array();
		$teetime_id = substr($teetime_id, 0, 20);
		$new_date = date('Ymd', strtotime($new_date.' -1 month'));
		//$json_ready_events = array();
		//$teetime_info[''];
		$primary_teetime_id = substr($teetime_id, 0, 20);
		$secondary_teetime_id = $primary_teetime_id.'b';
		//$this->db->query("UPDATE foreup_teetime SET start = CONCAT('{$new_date}', SUBSTRING(start, 9)), end = CONCAT('{$new_date}', SUBSTRING(end, 9)) WHERE TTID = '{$primary_teetime_id}%' LIMIT 1");
		$this->db->query("UPDATE foreup_teetime SET start = '{$new_date}', end = '{$new_end_date}', teesheet_id = '{$teesheet_id}' WHERE TTID = '{$primary_teetime_id}' LIMIT 1");
		return $this->db->query("UPDATE foreup_teetime SET start = '{$back_new_date}', end = '{$back_new_end_date}', teesheet_id = '{$teesheet_id}' WHERE TTID = '{$secondary_teetime_id}' LIMIT 1");
	}
	/*
	Deletes one teetime
	*/
	function delete($teetime_id, $person_id = '')
	{
		if ($person_id != '')
		{
			//If the teetime is being deleted by a golfer online
			//$this->db->where('booker_id', $person_id);
			$this->db->where('paid_player_count', 0);
		}
		else
			//If the teetime is being deleted from an employee using the system
			$person_id = $this->session->userdata('person_id');

		if ($person_id != '')
	    {
	    	$this->db->where("TTID", substr($teetime_id,0,20));
			$this->db->or_where("TTID", substr($teetime_id,0,20).'b');
			$this->db->limit(2);

		    $this->db->update('teetime', array('status' => 'deleted', 'canceller_id' => $person_id, 'date_cancelled' => date('Y-m-d H:i:s')));
			return $this->db->affected_rows() > 0;
		}
		else
			return false;
    }
	function generate_id($type = 'tt') {
        if ($type == 'tt')
            $prefix = 'TTID_'.date('mdHis');
        else if ($type == 'ts')
            $prefix = 'teesheet_id_';
        else
            $prefix = '';

        $length = 20 - strlen($prefix);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = $prefix;
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }
        return $string;
    }
	function get_stats($view, $year, $month, $day, $dow) {
        $brand = $this->session->userdata("teesheet_id");
        $statsArray = array();
        $eday = $day+1;
        if ($month < 10)
            $month = "0".$month;
        if ($day < 10)
            $day = "0".$day;
        if ($eday < 10)
            $eday = "0".$eday;
        if ($view == 'agendaDay') {
            //get one day stats

            $dayString = $year.$month.$day.'0000';
            $edayString = $year.$month.$eday.'0000';
			if ($this->config->item('sales'))
	            $this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
			else
	            $this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
            $this->db->from('teetime');
            $this->db->where("teesheet_id = '$brand'
                    AND `start` > '$dayString'
                    AND `end` < '$edayString'
                    AND `status` != 'deleted'
                    AND `TTID` NOT LIKE '____________________b'");
            $this->db->group_by('choles');

            $results = $this->db->get();
            foreach($results->result() as $result) {
                if ($result->choles == '9')
                    $statsArray['nine'] = $result;
                else if ($result->choles == '18')
                    $statsArray['eighteen'] = $result;
            }
        }
        else if ($view == 'agendaWeek') {
            //get full week stats
        }
        return json_encode($statsArray);
    }

    /**
     * get all online bookings associated with each course id (for non superadmin)
     */
    public function get_online_bookings_count($last_week = false)
    {
      $course_id = '';
      if (!$this->permissions->is_super_admin())
          $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";

      $this->db->from('teetime');
      $this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
      $this->db->where("lower(booking_source) = 'online' {$course_id}");

      if($last_week === true)
        $this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');

      return $this->db->count_all_results();
    }

    /**
     *
     */
    public function get_bookings($limit = false)
    {
      $this->db->select('teetime.date_booked, teetime.start, teetime.end, teetime.player_count, teetime.holes');
      $this->db->from('teetime');
      $this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
      $this->db->where('status != \'delete\'');
      if($limit !== false) $this->db->limit($limit);
      $this->db->order_by('date_booked', 'desc');

      $query = $this->db->get();

      if($query->num_rows() > 0) return $query->result_array();
      else return array();
    }

	public function get_thank_you_list($teesheet_id)
	{
		// Fetch all tee times and person_ids that qualify for thank yous.
		$nine_hours_ago = date('YmdHi', strtotime('-9 hours'))-1000000;
		$seven_hours_ago = date('YmdHi', strtotime('-7 hours'))-1000000;
		//$seven_hours_ago = date('Ymd2359')-1000000;
		$result = $this->db->query("
			SELECT CONCAT(`person_id`,',', `person_id_2`,',', `person_id_3`,',', `person_id_4`,',', `person_id_5`) AS people_ids
			FROM (`foreup_teetime`)
			WHERE `teesheet_id` = '$teesheet_id'
			AND `type` = 'teetime'
			AND `start` > '$nine_hours_ago'
			AND `start` < '$seven_hours_ago'
			AND `paid_player_count` > 0
			AND `status` != 'deleted'
			AND `thank_you_emailed` = 0
			AND LENGTH(TTID) < 21
			")->result_array();

		// Put all person_ids into array
		$customer_id_list = array();
		print_r($result);
   	    foreach($result AS $customer_ids)
		{
			print_r($customer_ids);
			$customer_ids = explode(',', $customer_ids['people_ids']);
			$customer_id_list = array_merge($customer_id_list, $customer_ids);
		}
		$customer_id_list =  array_unique($customer_id_list);

		// Get all customer email addresses for thank yous
		$email_list = array();
		if (count($customer_id_list) > 0)
		{
			$this->db->select('email');
			$this->db->from('people');
			$this->db->where_in('person_id', $customer_id_list);
			$this->db->limit(count($customer_id_list));
			$emails = $this->db->get()->result_array();

			foreach($emails as $email)
			{
				if ($email['email'] && $email['email'] != '')
					$email_list[] = array('email'=>$email['email']);
			}
		}

		// Mark all tee times we gathered as 'thanked'
		$this->db->query("
			UPDATE (`foreup_teetime`)
			SET `thank_you_emailed` = 1
			WHERE `teesheet_id` = '$teesheet_id'
			AND `type` = 'teetime'
			AND `start` > '$nine_hours_ago'
			AND `start` < '$seven_hours_ago'
			AND `paid_player_count` > 0
			AND `status` != 'deleted'
			AND LENGTH(TTID) < 21
			");

		// Return the email list
		return $email_list;
	}
}
?>

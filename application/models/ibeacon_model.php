<?php
class Ibeacon_model extends CI_Model {
	
	function __construct(){
		parent::__construct();	
	}
	
	// Get ibeacons from database (filtered by not received, terminal, etc)
	function get($filters){
		
		if($filters['terminal_id']){
			$this->db->where('terminal_id', (int) $filters['terminal_id']);
		}
		
		if($filters['course_id']){
			$course_id = $filters['course_id'];
		}else{
			$course_id = $this->session->userdata('course_id');
		}		
		
		$this->db->select("GROUP_CONCAT(beacon.beacon_id) AS beacon_id, beacon.date AS beacon_time, p.person_id, p.first_name, 
			p.last_name, p.email, p.phone_number, c.account_balance, p.email, p.phone_number, c.account_number,
			c.member_account_balance, c.member, c.invoice_balance, c.loyalty_points, p.comments,
			IF(
				ISNULL(image.image_id),
				CONCAT('images/profiles/profile_picture.png'),
				CONCAT('archives/images/', c.course_id, '/thumbnails/', image.filename)
			) AS photo", false);
		$this->db->from('ibeacon AS beacon');
		$this->db->join('people AS p', 'p.person_id = beacon.person_id', 'left');
		$this->db->join('customers AS c', 'c.person_id = beacon.person_id', 'left');
		$this->db->join('images AS image', 'image.image_id = c.image_id', 'left');
		$this->db->where('beacon.received', 0);
		$this->db->where('c.course_id', $course_id);
		$this->db->group_by('c.person_id');
		
		$query = $this->db->get();
		$rows = $query->result_array();
		
		$beacon_ids = array();
		foreach($rows as $row){
			$beacon_ids = array_merge($beacon_ids, explode(',', $row['beacon_id']));
		}
		$this->mark_received($beacon_ids);
		
		return $rows;
	}
	
	// Once a beacon message has been shown to course, this function marks
	// it as received so it won't show up again
	function mark_received($beacon_id){
		
		if(empty($beacon_id)){
			return false;
		}
		
		if(is_array($beacon_id)){
			$this->db->where_in('beacon_id', $beacon_id);
		}else{
			$this->db->where(array('beacon_id' => $beacon_id));
		}
		
		$this->db->update('ibeacon', array('received' => 1));
		return $this->db->affected_rows();
	}
	
	// Find course based on iBeacon major ID
	function get_course_id($major_id){
		
		$this->db->select('course_id');
		$this->db->from('courses');
		$this->db->where('ibeacon_major_id', (int) $major_id);
		$this->db->where('ibeacon_enabled', 1);
		$this->db->limit(1);
		
		$row = $this->db->get()->row_array();
		
		if($row){
			return (int) $row['course_id'];	
		}else{
			return false;
		}
	}
	
	// Find terminal location based on iBeacon minor ID
	function get_terminal_id($course_id, $minor_id){
		
		$this->db->select('terminal_id');
		$this->db->from('terminals');
		$this->db->where('course_id', (int) $course_id);
		$this->db->where('ibeacon_minor_id', (int) $minor_id);
		$this->db->limit(1);
		
		$row = $this->db->get()->row_array();
		
		if($row){
			return (int) $row['terminal_id'];	
		}else{
			return false;
		}
	}	

	function get_user_id($email, $password){
		
		$this->db->select('person_id');
		$this->db->from('users');
		$this->db->where('email', trim($email));
		$this->db->where('password', md5(trim($password)));
		$row = $this->db->get()->row_array();
		
		if($row){
			return (int) $row['person_id'];
		}else{
			return false;			
		}
	}
	
	function user_exists($user_id){
		$this->db->select('person_id');
		$this->db->from('people');
		$this->db->where('person_id', $user_id);
		$row = $this->db->get()->row_array();
		
		if(!empty($row['person_id'])){
			return true;
		}else{
			return false;
		}
	}
	
	// Save a new iBeacon ping from someones mobile device
	function save($uuid, $major_id, $minor_id, $person_id, $course_id, $terminal_id, $type){
		
		$data = array(
			'uuid' => $uuid,
			'major_id' => $major_id,
			'minor_id' => $minor_id,
			'person_id' => $person_id,
			'course_id' => $course_id,
			'terminal_id' => $terminal_id,
			'type' => $type,
			'date' => date('Y-m-d H:i:s'),
			'received' => 0
		);
		
		$this->db->insert('ibeacon', $data);
		
		return $this->db->affected_rows();
	}
}

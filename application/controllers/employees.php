<?php
require_once ("person_controller.php");
class Employees extends Person_controller
{
	function __construct()
	{
		parent::__construct('employees');
		$this->load->model('employee_permissions');
	}

	function index()
	{
		$config['base_url'] = site_url('employees/index');
		$config['total_rows'] = $this->Employee->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);

		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_people_manage_table($this->Employee->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('people/manage',$data);
	}
	/* added for excel expert */
	function excel_export() {
		$data = $this->Employee->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("User Name", "First Name", "Last Name", "E-Mail", "Phone Number", "Address 1", "Address 2", "City", "State", "Zip", "Country", "Comments");
		$rows[] = $row;
		foreach ($data as $r) {
			$row = array(
				$r->username,
				$r->first_name,
				$r->last_name,
				$r->email,
				$r->phone_number,
				$r->address_1,
				$r->address_2,
				$r->city,
				$r->state,
				$r->zip,
				$r->country,
				$r->comments
			);
			$rows[] = $row;
		}

		$content = array_to_csv($rows);
		force_download('employees_export' . '.csv', $content);
		exit;
	}

	/*
	Returns employee table data rows. This will be called with AJAX.
	*/
	function search($offset = 0, $course_id = '')
	{
		$data = array();
		$search=$this->input->post('search');
		$data_rows=get_people_manage_table_data_rows($this->Employee->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $course_id, $offset),$this);
		//echo $data_rows;
		$config['base_url'] = site_url('employees/index');
        $config['total_rows'] = $this->Employee->search($search, 0, $group_id);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);

	}
	function employee_search($type='')
	{
		$suggestions = $this->Employee->get_employee_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}


	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Employee->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	/*
	Loads the employee edit form
	*/
	function view($employee_id=-1)
	{
		$this->load->model('Image');
		$this->load->library('Sendgrid');
		$data = array();
		$data['person_info']=$this->Employee->get_info($employee_id);
		$bounced = '';
		if ($data['person_info']->email != '')
			$bounced = $this->sendgrid->check_bounce($data['person_info']->email);
		$data['bounced'] = $bounced;
		$data['all_modules']=$this->Module->get_all_modules();
		$data['image_thumb_url'] = $this->Image->get_thumb_url($data['person_info']->image_id, 'person');
		$this->load->view("employees/form",$data);
	}

        /*
	Loads the employee activate form
	*/
	function activation_view($employee_id=-1)
	{
            	$data['person_info']=$this->Employee->get_info($employee_id);
		$data['all_modules']=$this->Module->get_all_modules();
		$this->load->view("employees/activate_form",$data);
	}
        /*
         * Allows super admins to login as any existing employee
         */
        function login_override($employee_id = -1) {
            if ($this->permissions->is_super_admin()) {
                $this->db->select('username, password');
                $this->db->from('employees');
                $this->db->where('person_id', $employee_id);
                $this->db->limit(1);
                $result = $this->db->get()->result_array();
                $this->Employee->login($result[0]['username'], $result[0]['password']);

            }
            else {
                return false;
            }
            $this->load->view('home');

        }

        /*
        Activates an admin for a new course
        */
        function activate($employee_id=-1){
            $this->load->model('teesheet');
            $this->load->model('Price_class');
            $course_id = $this->input->post("golf_course");
            $user_level = 3;//admin status
            $this->db->from('employees');
            $this->db->where('course_id', $course_id);
            $result = $this->db->get();
            if ($result->num_rows() > 0) {
                echo json_encode(array('success'=>false,'message'=>lang('employees_error_users_already_exist').' ',
                '   person_id'=>-1));
                return;
            }

            //Create new teesheet
            $teesheet_data = array(
				'course_id'=>$course_id,
				'title'=>'Main Course',
				'holes'=>18,
				'default'=>1
			);
			if ($this->permissions->course_has_module('reservations'))
			{
				$this->schedule->save($teesheet_data);
			}
			else
			{
				$this->teesheet->save($teesheet_data);
			}
			$teesheet_id = $teesheet_data['teesheet_id'];
            //$teesheet_id = $this->Teetime->create($course_id);
            
            //Create default tee times and carts as items in inventory
			$this->Item->create_teetimes($course_id);
			
            $person_data = array(
                'person_id'=>$employee_id
            );
            $employee_data = array(
                'course_id'=>$course_id,
                'teesheet_id'=>$teesheet_id,
                'user_level'=>$user_level,
                'activated'=>1
            );
            $permission_data = $this->input->post("permissions")!=false ? $this->input->post("permissions"):array();
            //$permission_data[] = 'teetimes';
            $permission_data[] = 'config';

            if($this->Employee->save($person_data,$employee_data,$permission_data,$employee_id))
            {
                    //New employee
                    if($employee_id==-1)
                    {
                        echo json_encode(array('success'=>false));
                    }
                    else //previous employee
                    {
                        $course_permission_data = array();
                        foreach ($permission_data as $module)
                            $course_permission_data[$module] = 1;
                        if($this->Course->save($course_permission_data, $course_id))
                            echo json_encode(array('success'=>true,'message'=>lang('employees_successful_updating').' '
                            ,'person_id'=>$employee_id));
                        else
                            echo json_encode(array('success'=>false,'message'=>lang('employees_error_adding_updating').' ',
                        '   person_id'=>-1));

                    }
            }
            else//failure
            {
                    echo json_encode(array('success'=>false,'message'=>lang('employees_error_adding_updating').' ',
                        'person_id'=>-1));
            }
        }

	/*
	Inserts/updates an employee
	*/
	function save($employee_id=-1)
	{
        if ($this->permissions->is_super_admin())
            $course_id = $this->input->post('golf_course');
        else
            $course_id = $this->session->userdata('course_id');
        $teesheet_id = $this->session->userdata('teesheet_id');
        $user_level = $this->input->post('user_level');
        if (!$this->permissions->is_super_admin() && $user_level > 3)
            $user_level = 3;
		$person_data = array(
		'first_name'=>$this->input->post('first_name'),
		'last_name'=>$this->input->post('last_name'),
		'email'=>$this->input->post('email'),
		'phone_number'=>$this->input->post('phone_number'),
		'cell_phone_number'=>$this->input->post('cell_phone_number'),
		'birthday'=>$this->input->post('birthday'),
		'address_1'=>$this->input->post('address_1'),
		'address_2'=>$this->input->post('address_2'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zip'=>$this->input->post('zip'),
		'country'=>$this->input->post('country'),
		'comments'=>$this->input->post('comments')
		);
		$permission_data = $this->input->post("permissions")!=false ? $this->input->post("permissions"):array();
		$detailed_permissions = $this->input->post('employee_permissions')!=false ? $this->input->post('employee_permissions'):array();
		$employee_data=array(
            'activated'=>1
		);
		if (!$this->permissions->is_employee())
		{
			$employee_data['username'] = $this->input->post('username');
			$employee_data['position'] = $this->input->post('position');
			$employee_data['user_level'] = $user_level;
		}
		$employee_data['course_id'] = $course_id;

		//Password has been changed OR first time password set
		if($this->input->post('password')!='' && (!$this->permissions->is_employee() || $employee_id == $this->session->userdata('person_id')))
			$employee_data['password'] = md5($this->input->post('password'));
		if (!$this->permissions->is_employee() || $employee_id == $this->session->userdata('person_id'))
		{
			$employee_data['pin'] = $this->input->post('pin') ? $this->input->post('pin') : null;
        	$employee_data['card'] = $this->input->post('card') ? $this->input->post('card') : null;
        }
        if (!$this->permissions->is_super_admin())
            $employee_data['teesheet_id'] = $teesheet_id;

		if($this->Employee->save($person_data,$employee_data,$permission_data,$employee_id,$detailed_permissions))
		{
			if ($this->config->item('mailchimp_api_key'))
			{
				$this->Person->update_mailchimp_subscriptions($this->input->post('email'), $this->input->post('first_name'), $this->input->post('last_name'), $this->input->post('mailing_lists'));
			}
			//SAVE EMPLOYEES IN ZENDESK
			if (!$this->input->post('zendesk') && strpos(base_url(), 'foreupsoftware.com') !== false)
			{
				// "iat"   => $now,
				// "name"  => $employee_data->first_name.' '.$employee_data->last_name,
				// "email" => $employee_data->email,
				// "external_id" => $this->session->userdata('person_id'),
				// "organization" => $this->session->userdata('course_name'),
				// "tags" => json_encode(array("phone_number"=>$employee_data->phone_number,"position"=>$employee_data->position))
				$course_info = $this->Course->get_info($course_id);
				$this->load->library('Zendesk');
				$zendesk_person_id = $employee_id == -1 ? $employee_data['person_id'] : $employee_id;
				$data = array('user'=>array(
						'external_id'=>$zendesk_person_id,
						'organization'=>$course_info->name,
						'name'=>$person_data['first_name'].' '.$person_data['last_name'],
						'email'=>$person_data['email']
					));
				$response = $this->zendesk->curl('/users.json', json_encode($data), 'POST');
				if ($response->error)
				{
					//SEND EMAIL
					$contents = "Data: ".json_encode($data)."\n Response: ".json_encode($response);
					send_sendgrid('jhopkins@foreup.com', 'Trouble Saving Employee to Zendesk', $contents, 'zendesk@foreup.com', 'ForeUP Zendesk');
				}
				//RECORD THAT WE HAVE A ZENDESK ACCOUNT REGARDLESS IF WE HAD SUCCESS OR NOT
				$this->db->query("UPDATE foreup_employees SET zendesk = 1 WHERE person_id = $zendesk_person_id");
			}
			
			//New employee
			if($employee_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('employees_successful_adding').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$employee_data['person_id']));
			}
			else //previous employee
			{
				echo json_encode(array('success'=>true,'message'=>lang('employees_successful_updating').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$employee_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>($employee_data['error']?$employee_data['error']:lang('employees_error_adding_updating').' '.
			$person_data['first_name'].' '.$person_data['last_name']),'person_id'=>-1));
		}
	}

	function save_image($person_id = null){
		$this->load->model('Image');
		$image_id = $this->input->post('image_id', null);
		if($image_id === null){
			$success = true;
		}else{
			$success = $this->Employee->save_image($person_id, $image_id);
		}
		$url = $this->Image->get_thumb_url($image_id, 'person');

		echo json_encode(array('success'=>true, 'image_id'=>$image_id, 'thumb_url'=>$url));
	}

	/*
	This deletes employees from the employees table
	*/
	function delete()
	{
		$employees_to_delete=$this->input->post('ids');

		if($this->Employee->delete_list($employees_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('employees_successful_deleted').' '.
			count($employees_to_delete).' '.lang('employees_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('employees_cannot_be_deleted'),'sql'=>$this->db->last_query()));
		}
	}

	/*
	Gets one row for a person manage table. This is called using AJAX to update one row.
	*/
	function get_row()
	{
		$person_id = $this->input->post('row_id');
		$data_row=get_person_data_row($this->Employee->get_info($person_id),$this);
		echo $data_row;
	}
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 880;
	}
}
?>

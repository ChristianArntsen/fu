<?php		
require_once ("secure_area.php");
//require_once ("interfaces/idata_controller.php");
class Schedules extends Secure_area //implements iData_controller
{
	function __construct()
	{
		parent::__construct('schedules');
		$this->load->model('schedule');
		$this->load->model('employee');
        $this->load->model('track');
        $this->load->model('course');
        $this->load->helper('url');		
	}
	function index()
	{
		$config['base_url'] = site_url('schedules/index');
		$config['total_rows'] = $this->Schedule->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_schedules_manage_table($this->Schedule->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view("schedules/manage", $data);
	}
    /*
	Inserts/updates a teesheet
	*/
	function save($schedule_id=false)
	{
		$schedule_data = array(
			'schedule_id' => $schedule_id,
			'title' => $this->input->post('name'),
			'type' => $this->input->post('type'),
			'holes' => $this->input->post('holes'),
			'increment' => $this->input->post('increment'),
			'frontnine' => $this->input->post('frontnine'),
			'default' => $this->input->post('default'),
			//'online_booking' => $this->input->post('online_booking'),
			'online_open_time' => $this->input->post('online_open_time'),
			'online_close_time' => $this->input->post('online_close_time'),
			'online_time_limit' => $this->input->post('online_time_limit'),
			'course_id' => $this->session->userdata('course_id')
		);
		
		if($this->schedule->save($schedule_data, $schedule_id))
		{
			$track_array = array();
			$track_ids = $this->input->post('track_id');
			$track_names = $this->input->post('track_name');
			$reround_ids = $this->input->post('reround_track_id');
			$track_online_booking = $this->input->post('track_online_booking');
			foreach($track_names as $index => $track_name)
			{
				$track_id = (isset($track_ids[$index]) ? $track_ids[$index]:false);
				$track_array = array(
					'schedule_id'=>$schedule_data['schedule_id'],
					'title'=>$track_name,
					'reround_track_id'=>$reround_ids[$index],
					'online_booking'=>$track_online_booking[$index]
				);
				$this->track->save($track_array, $track_id);
			}
		$sql = $this->db->last_query();
			if ($this->session->userdata('increment') != $this->input->post('increment'))
	            $this->schedule->adjust_teetimes($schedule_id);
        	if($schedule_id==false)
				$schedule_id = $schedule_data['schedule_id'];
			if ($teesheet_data['default'] == 1)
				$this->schedule->switch_tee_sheet($schedule_id);
			echo json_encode(array('success'=>true,'message'=>lang('teesheets_successful_updating').' '.
			$teesheet_data['title'],'teesheet_id'=>$schedule_id, 'sql'=>$sql));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('teesheets_error_adding_updating').' '.
			$teesheet_data['teesheet_title'],'teesheet_id'=>-1, 'sql'=>$this->db->last_query()));
		}
	}
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_teesheets_manage_table_data_rows($this->teesheet->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		echo $data_rows;
	}
	function suggest()
	{
		
	}
	function get_row()
	{
		$schedule_id = $this->input->post('row_id');
		$data_row=get_schedule_data_row($this->schedule->get_info($schedule_id),$this);
		echo $data_row;
	}
	function get_track_row($schedule_id = false)
	{
		$tracks = '';
		if ($schedule_id)
			$tracks=$this->track->get_all($schedule_id);
		$schedule = $this->schedule->get_info($schedule_id);
		echo json_encode(array('row'=>$this->track->row_html(false, $tracks, $schedule)));
	}
	/*
	This deletes teesheet from the teesheet table
	*/
	function delete()
	{
		$teesheets_to_delete=$this->input->post('ids');
		
		if($this->teesheet->delete_list($teesheets_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	function logout()
	{
        $this->Employee->logout();
	}
    
	/*
	Loads the teesheet edit form
	*/
	function view($schedule_id=-1)
	{
		$data['schedule_info']=$this->schedule->get_info($schedule_id);
		$data['tracks']=$this->track->get_all($schedule_id);
		$this->load->view("schedules/form",$data);
	}
	
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 550;
	}
}
?>
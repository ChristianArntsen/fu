<?php
//This controller is for the purpose of running cron tasks in the system

class Users extends CI_Controller//REST_Controller
{
	
    function __construct()
    {
        parent::__construct('users');
		$this->load->library('name_parser');
		$this->load->helper('email_helper');		
		$this->load->model('user');		
	}
	
	function login()
	{		
		$email = $this->input->get('email');
		$password = $this->input->get('password');
		$facebook_id = $this->input->get('facebook_id');
		$twitter_id = $this->input->get('twitter_id');
				
		if (!$email || $email == '')
			$errors[] = 'Missing parameter: email';	
		else if (!valid_email($email))
			$errors[] = 'Invalid parameter: email';
		if ((!$password || $password == '') && (!$facebook_id || $facebook_id == '') && (!$twitter_id || $twitter_id == ''))
			$errors[] = 'Missing parameter: password OR facebook_id OR twitter_id';			
		if (count($errors)>0)
		{
			// $this->response(array('success'=>false,'error'=>implode(', ', $errors)));
			echo json_encode(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				)	,
				400
			);
			return;
			
		}
		
		$login_data = array(
			'email'=>$email,
			'password'=>$password,
			'facebook_id'=>$facebook_id,
			'twitter_id'=>$twitter_id
		);
		if ($this->user->login($login_data['email'], $login_data['password'])) {
			
			echo json_encode(array(
								'status'=>200,
								'valid_credentials'=>true,
								'user_id'=>$this->session->userdata('customer_id'),
								'first_name'=>$this->session->userdata('first_name'),
								'last_name'=>$this->session->userdata('last_name'),
								'facebook_id'=>$this->session->userdata('facebook_id'),
								'twitter_id'=>$this->session->userdata('twitter_id')
								)
							);
		}
		else 
		{
			echo json_encode(array(
								'status'=>204,
								'valid_credentials'=>false,
								'user_id'=>'',
								'first_name'=>'',
								'last_name'=>'',
								)
							);
		}
	}
	
	function save()
	{
		$person_id = $this->input->get('user_id') ? $this->input->get('user_id') : -1;			
		$name = $this->input->get('name');
		$email = $this->input->get('email');
		$password = $this->input->get('password') ? $this->input->get('password') : '';
		$facebook_id = $this->input->get('facebook_id') ? $this->input->get('facebook_id') : '';
		$twitter_id = $this->input->get('twitter_id') ? $this->input->get('twitter_id') : '';
		
		$email_available = !$this->user->username_exists($email);			
		
		$errors = array();
		if (!$email_available)
			$errors[] = "The email address '$email' is already in use";
		if (!$name || $name == '')
			$errors[] = 'Missing parameter: name';
		if (!$email || $email == '')
			$errors[] = 'Missing parameter: email';	
		else if (!valid_email($email))
			$errors[] = 'Invalid parameter: email';
		if ((!$password || $password == '') && (!$facebook_id || $facebook_id == '') && (!$twitter_id || $twitter_id == ''))
			$errors[] = 'Missing parameter: password OR facebook_id OR twitter_id';			
		if (count($errors)>0)
		{
			// $this->response(array('success'=>false,'error'=>implode(', ', $errors)));			
			echo json_encode(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				)	
			);
			return;
		}
		$first_name;
		$last_name;
		$np = new Name_parser();
		$np->setFullName($name);
		$np->parse();
		if (!$np->notParseable()) {
				$first_name=$np->getFirstName();
				$last_name=(!$np->getLastName() || $np->getLastName() == null || $np->getLastName() == '') ? $first_name : $np->getLastName();
		}
		else {
			$first_name = $name;
			$last_name = $first_name;
		}

		$user_data = array(
			'first_name'=>$first_name,
			'last_name'=>$last_name,
			'email'=>$email	
		);
		
		if ($password != '') $user_data['password'] = md5($password);
		if ($facebook_id != '') $user_data['facebook_id'] = $facebook_id;
		if ($twitter_id != '') $user_data['twitter_id'] = $twitter_id;

		$person_data = array(
			'first_name'=>$first_name,
			'last_name'=>$last_name,
			'email'=>$email,
			'phone_number'=>'',
			'address_1'=>'',
			'address_2'=>'',
			'city'=>'',
			'state'=>'',
			'zip'=>'',
			'country'=>'',
			'comments'=>'created from mobile app'
		);		
		
		//if ($this->user->save($person_id, $user_data, $person_data))
		if ($this->user->api_save($person_data, $user_data, $user_info->person_id))
		{
			echo json_encode(array('status'=>200,'user_saved'=>true
                            ,'user_id'=>(int)$user_data['person_id']));			
		}
		else 
		{
			echo json_encode(array('status'=>204,'user_saved'=>false,
                        'user_id'=>-1));
		}
	}	
    
}

?>

<?php
require_once ("secure_area.php");
class Ibeacon extends Secure_area 
{
	function __construct(){
		parent::__construct();	
	}
	
	function index(){
		
		$this->load->model('ibeacon_model');
		
		$course_id = (int) $this->session->userdata('course_id');
		$terminal_id = (int) $this->session->userdata('terminal_id');
		
		$customers = $this->ibeacon_model->get(array(
			'course_id' => $course_id,
			'terminal_id' => $terminal_id
		));
		
		$customer_account_name = ($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
		$member_account_name = ($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');		
		
		foreach($customers as $customer){
			$customer['customer_account_name'] = $customer_account_name;
			$customer['member_account_name'] = $member_account_name;
			
			echo $this->load->view('ibeacon/customer', $customer);
		}
	}	
}
?>

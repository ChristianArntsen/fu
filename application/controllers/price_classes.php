<?php
require_once ("secure_area.php");
class Price_Classes extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Price_class');
	}

	function delete($class_id = null){
		
		$response = $this->Price_class->delete($class_id);
		
		if(!empty($response))
		{
			echo json_encode(array('success'=>true,'message'=>"Price class successfully deleted"));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'Error deleting the price class'));
		}		
	}
	
	function save($class_id = false){
		$name = $this->input->post('name');
		$color = $this->input->post('color');
		
		if(empty($name)){
			echo json_encode(array('success'=>false, 'message'=>'Price class name is required'));
			return false;
		}
		$class_id = $this->Price_class->save($class_id, $name, 0, $color);
		$price_class = $this->Price_class->get($class_id);
		
		if(!empty($class_id)){
			echo json_encode(array('success'=>true, 'message'=>'Price class saved', 'class_id'=>$class_id, 'price_class'=>$price_class));			
		}else{
			echo json_encode(array('success'=>false, 'message'=>'Error creating price class'));	
		}
	}
}

var Schedule = Backbone.Model.extend({
	idAttribute: 'teesheet_id',
	defaults: {
		title: ''
	},
	
	set: function(attributes, options){
		if(attributes.booking_classes !== undefined && !(attributes.booking_classes instanceof BookingClassCollection)){
			attributes.booking_classes = new BookingClassCollection( attributes.booking_classes );
		}

		return Backbone.Model.prototype.set.call(this, attributes, options);
	}
});

var ScheduleCollection = Backbone.Collection.extend({
	url: function(){
		return BASE_URL + 'courses/' + App.data.filters.get('course_id') + '/schedules';
	},
	model: Schedule
});

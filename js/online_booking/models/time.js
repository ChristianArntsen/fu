var Time = Backbone.Model.extend({
	idAttribute: 'time',
	defaults: {
		players: 4,
		holes: 18,
		carts: false,
		green_fee: 0.00,
		cart_fee: false,
		time: null,
	},
	
	initialize: function(){
		// If pricing has carts built in, force cart booking
		if(App.settings.booking_carts == 2){
			this.set({'carts': true});
		}
	},
	
	canPurchase: function(){
		return this.get('can_purchase') && this.get('players') >= 2;
	}	
});

var TimeCollection = Backbone.Collection.extend({
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.filters.get('course_id') + '/schedules/' + App.data.filters.get('schedule_id') + '/times';
	},
	model: Time,
	
	initialize: function(){
		this.listenTo(this, 'request', this.startLoading);
		this.listenTo(this, 'sync error', this.endLoading);
	},
	
	startLoading: function(){
		App.data.status.set('loading', true);
	},
	
	endLoading: function(){
		App.data.status.set('loading', false);
	},
	
	refresh: function(data){
		if(data){
			var params = data;
		}else{
			var params = _.pick(App.data.filters.attributes, 'time', 'date', 'holes', 'players', 'booking_class');
		}
		params.api_key = API_KEY;
		
		var selectedSchedule = App.data.schedules.findWhere({selected: true});
		var bookingClasses = selectedSchedule.get('booking_classes');
		
		// If the booking class is not set, and the selected schedule has booking classes, do nothing
		if(bookingClasses.length > 0 && !params.booking_class && bookingClasses.length > 0){
			return true;
		}
		
		this.fetch({
			reset: true,
			data: params
		});
	}
});

var NavigationView = Backbone.View.extend({
	className: 'container',
	template: _.template( $('#template_navigation').html() ),
	
	events: {
		'click a.logout': 'logout',
		'click a.login': 'login'
	},
	
	initialize: function(options){
		if(options.userModel){
			this.userModel = options.userModel;
		}
		if(options.courseModel){
			this.courseModel = options.courseModel;
		}

		this.listenTo(this.userModel, 'change:logged_in sync', this.render);
	},
	
    render: function() {
		var data = {};
		data.course = this.courseModel.attributes;
		data.user = this.userModel.attributes;
		
		var html = this.template(data);
		this.$el.html(html);
		this.highlightLink(Backbone.history.fragment);
		return this;
    },
    
    highlightLink: function(route){
		this.$el.find('li').removeClass('active');
		this.$el.find("a[href='#"+route+"']").parent().addClass('active');
		this.$el.find('div.navbar-collapse').addClass('collapse').removeClass('in');
	},
	
	logout: function(event){
		event.preventDefault();
		App.data.user.logout();
	},
	
	login: function(event){
		event.preventDefault();
		var loginView = new LoginView({model: App.data.user});
		loginView.show();		
	}
});

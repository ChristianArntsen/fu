var PurchaseView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_purchase').html() ),
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});

var PurchaseListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped table-responsive purchases',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead><tr><th class="sale">Sale #</th><th class="date">Date</th><th class="items">Items</th><th class="total">Total</th></tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderPurchase, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="5">No purchases available</td></tr>');
	},
	
	renderPurchase: function(purchase){
		this.$el.append( new PurchaseView({model: purchase}).render().el );
	}
});

var GiftCardView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_gift_card').html() ),
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});

var GiftCardListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped table-responsive gift-cards',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead><tr><th class="sale">Gift Card #</th><th class="date">Expiration</th><th class="items">Status</th><th class="total">Balance</th></tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderItem, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="5">No gift cards available</td></tr>');
	},
	
	renderItem: function(item){
		this.$el.append( new GiftCardView({model: item}).render().el );
	}
});

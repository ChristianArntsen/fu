var ReservationConfirmationView = Backbone.View.extend({
	template: _.template( $('#template_confirmation').html() ),
	
	events: {
		'click .pay-now': 'purchaseReservation'
	},
	
	initialize: function(){
		this.listenTo(this.model, 'change:purchased', this.render);
	},
	
    render: function() {

		var attributes = this.model.attributes;
		attributes.user_name = App.data.user.get('first_name') + ' ' + App.data.user.get('last_name');
		attributes.user_email = App.data.user.get('email');
		attributes.course_name = App.data.course.get('name');
		attributes.course_phone = App.data.course.get('phone');
		attributes.foreup_discount_percent = App.data.course.get('foreup_discount_percent');
	
		attributes.can_book_carts = false;
		if(App.settings.booking_carts == 1){
			attributes.can_book_carts = true;
		}
		
		this.$el.html(this.template(attributes));
		return this;
    },
    
    purchaseReservation: function(){
		
		var min_required_paying = 2;
		if(App.settings.minimum_players > min_required_paying){
			min_required_paying = App.settings.minimum_players;
		}

		if(this.model.get('pay_players') < min_required_paying){
			this.model.set('pay_players', min_required_paying);
		}		

		this.model.set({
			'discount_percent':App.data.course.get('foreup_discount_percent')
		});
		
		var purchaseView = new PurchaseReservationView({model: this.model});
		purchaseView.show();
	}
});

<script type="text/html" id="template_receipt">
<% if(status == 'complete'){ var paidClass = ' paid'; var btnClass = ' disabled'; } else { var paidClass = ''; var btnClass = ' blah'; } %>
<div class="receipt<%=paidClass%>">
	<div class="title">
		<span>Receipt #<%-receipt_id%></span>
		<% if(receipt_id != 1){ %>
		<a class="fnb_button delete-receipt">Delete</a>
		<% } %>
		<span class="is-paid">PAID</span>
	</div>
</div>
<% if(items.length == 0){ var btnClass = ' disabled'; } %>
<a class="fnb_button print<%=btnClass%>" href="#">Print</a>
<a class="fnb_button pay<%=btnClass%>" href="#">Pay</a>
</script>

<script type="text/html" id="template_receipt_item">
<span class="seat"><%-seat%></span>
<%-name%>
</script>

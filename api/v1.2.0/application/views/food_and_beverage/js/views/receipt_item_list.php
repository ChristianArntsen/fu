var ReceiptItemListView = Backbone.View.extend({
	tagName: "div",
	initialize: function() {
		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
	},

	render: function() {
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');

		_.each(this.collection.models, this.addItem, this);
		return this;
	},

	addItem: function(item){
		this.$el.append( new ReceiptItemView({model:item}).render().el );
	}
});

<?php echo form_open('food_and_beverage/login',array('id'=>'login_form')) ?>
<div id="fnb_login_contents">
	<div id="form_field_username">	
		<span class='pin_icon'></span>
		<?php echo form_input(array(
		'name'=>'pin', 
		'id'=>'pin', 
		'value'=>'',
		'placeholder'=>lang('food_and_beverage_enter_pin_or_swipe_card'),
		'size'=>'20')); 
		echo form_hidden('pin_or_card');
		?>
	</div>
	<div class='number_pad'>
		<div class='number_pad_number number_one'></div>
		<div class='number_pad_number number_two'></div>
		<div class='number_pad_number number_three'></div>
		<div class='number_pad_number number_four'></div>
		<div class='number_pad_number number_five'></div>
		<div class='number_pad_number number_six'></div>
		<div class='number_pad_number number_seven'></div>
		<div class='number_pad_number number_eight'></div>
		<div class='number_pad_number number_nine'></div>
		<div class='number_pad_number number_login'></div>
		<div class='number_pad_number number_zero'></div>
		<div class='number_pad_number number_backspace'></div>
	</div>
</div>
<?php echo form_close(); ?>


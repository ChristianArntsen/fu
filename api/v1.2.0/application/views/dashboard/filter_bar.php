<script>
	$(document).ready(function(){
	    $('#<?=$container_name?>_start').datepicker({ dateFormat: "yy-mm-dd"});
	    $('#<?=$container_name?>_end').datepicker({ dateFormat: "yy-mm-dd"});
	    $('#<?=$container_name?>_submit').click(function(e){
			e.preventDefault();
			var start = $('#<?=$container_name?>_start').val();
			var end = $('#<?=$container_name?>_end').val();
			var type = $('#<?=$container_name?>_filter_type').val();
			console.log('container <?=$container_name?>');
			console.dir($('#<?=$container_name?>_filter_type'));
			console.log('type '+type);
			$('input[name=start]').each(function() {
				$(this).val(start);
			});
			$('input[name=end]').each(function() {
				$(this).val(end);
			});
			$('select[name=type]').each(function() {
				$(this).val(type);
			});
			$('input[name=submit]').each(function() {
				$(this).submit();
			})
			
			
		});
	})
</script>
<div id="<?=$container_name?>_filters" class='dashboard_filters' style="width: <?=$width?>px; height: 30px;">
	<div class='dashboard_filters_inner'>
		<?php
		echo form_input(array(
			'name'=>'filter_start',
			'id'=>$container_name.'_start',
			'class'=>'filter_date',
			'placeholder'=>'Start date',
			'value'=>$start));
		echo form_input(array(
			'name'=>'filter_end',
			'id'=>$container_name.'_end',
			'class'=>'filter_date',
			'placeholder'=>'End date',
			'value'=>$end));
		echo form_dropdown(
			'filter_type',
			array('hour'=>'Hourly','day'=>'Daily','dayname'=>'Day of Week','week'=>'Weekly','month'=>'Monthly','year'=>'Yearly'), 
			'', 
			'',
			$container_name.'_filter_type');
		echo form_submit(array(
			'name'=>'filter_submit',
			'id'=>$container_name.'_submit',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		?>
	</div>
</div>
<?php
echo form_open('employees/save/'.$person_info->person_id,array('id'=>'employee_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="employee_basic_info">
<legend><?php echo lang("employees_basic_information"); ?></legend>
<?php $this->load->view("people/form_basic_info"); ?>
</fieldset>
<?php if (!$this->permissions->is_employee() || $person_info->person_id == $this->session->userdata('person_id'))
{
?>
<fieldset id="employee_login_info">
<legend><?php echo lang("employees_login_info"); ?></legend>
<?php } ?>
<?php if ($this->permissions->is_super_admin()){?>
<div class="field_row clearfix">
<?php echo form_label(lang('common_golf_course').':<span class="required">*</span>', 'golf_course',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'golf_course',
		'id'=>'golf_course',
		'value'=>$person_info->course_id)
	);?>
	</div>
</div>
<script>
$(document).ready(function(){
     $( "#golf_course" ).autocomplete({
 		source: 'index.php/courses/course_search',
		delay: 10,
 		autoFocus: false,
 		minLength: 0,
 		select: function( event, ui )
 		{
			$( "#golf_course" ).val(ui.item.label);
 			do_search(true);
 		}
	});
})
</script>
<?php }
if (!$this->permissions->is_employee())
{
?>
<div class="field_row clearfix">
<?php echo form_label(lang('employees_user_level').':', 'user_level',array('class'=>'')); ?>
	<div class='form_field'>
	<?php
        $user_level_choices = array('1'=>'Employee', '2'=>'Manager', '3'=>'Admin');
        if ($this->permissions->is_super_admin())
            $user_level_choices = array('1'=>'Employee', '2'=>'Manager', '3'=>'Admin', '5'=>'Super Admin');
        echo form_dropdown('user_level',$user_level_choices,$person_info->user_level);
        ?>
	</div>
</div>
<script>
$(document).ready(function(){
	$('#select-image').colorbox2({'maxHeight':750,'width':1100});

	$(document).unbind('changeImage').bind('changeImage', function(event){
		$.post('<?php echo site_url('employees/save_image'); ?>/<?php echo $person_info->person_id; ?>', {image_id: event.image_id}, function(response){
			$('#image-info img').attr('src', response.thumb_url);
			$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/employees?crop_ratio=1&image_id=' + event.image_id);
			$.colorbox2.close();
		},'json');
	});

	$('#remove-image').click(function(event){
		var url = $(this).attr('href');

		if(confirm('Remove this image?')){
			$.post(url, {image_id:0}, function(response){
				$('#image-info img').attr('src', response.thumb_url);
				$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/employees?crop_ratio=1&image_id=' + response.image_id);
			},'json');
		}
		return false;
	});
})
</script>

<?
}
if (!$this->permissions->is_employee() || $person_info->person_id == $this->session->userdata('person_id'))
{
?>
<div class="field_row clearfix">
<?php echo form_label(lang('employees_position').':', 'position',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'position',
		'id'=>'position',
		'value'=>$person_info->position));?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('employees_username').':<span class="required">*</span>', 'username',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'autocomplete'=>'off',
		'name'=>'username',
		'id'=>'username',
		'value'=>$person_info->username));?>
	</div>
</div>

<?php
$password_label_attributes = $person_info->person_id == "" ? '<span class="required">*</span>':'';
?>

<div class="field_row clearfix">
<?php echo form_label(lang('employees_password').':'.$password_label_attributes, 'password',array()); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'autocomplete'=>'off',
		'name'=>'password',
		'id'=>'password'
	));?>
	</div>
</div>


<div class="field_row clearfix">
<?php echo form_label(lang('employees_repeat_password').':'.$password_label_attributes, 'repeat_password',array()); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'autocomplete'=>'off',
		'name'=>'repeat_password',
		'id'=>'repeat_password'
	));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('employees_pin_number').':', 'repeat_pin',array()); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'pin',
		'id'=>'pin',
		'value'=>$person_info->pin
	));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('employees_card_number').':', 'card',array()); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'card',
		'id'=>'card',
		'value'=>$person_info->card
	));?>
	</div>
</div>
<?php
}
if (!$this->permissions->is_employee())
{?>
<div id='permission_info_box' style='display:none'>
<!--p><?php echo lang("employees_permission_desc"); ?></p-->

<ul id="permission_list">
<?php
foreach($all_modules->result() as $module)
{
    if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
    {
    ?>
    <li>
    <?php echo form_checkbox("permissions[]",$module->module_id,$this->Employee->has_permission($module->module_id,$person_info->person_id)); ?>
    <span class="medium"><?php echo lang('module_'.$module->module_id);?>:</span>
    <span class="small"><?php echo lang('module_'.$module->module_id.'_desc');?></span>
    </li>
    <?php
    }
}
?>
</ul>
<?php
}
//echo $this->session->userdata('user_level');
//echo '<br/>'.$person_info->user_level;
if (!$this->permissions->is_employee())
{?>
</div>
<script type='text/javascript'>
	$('#permission_info_box').expandable({
		title : '<?php echo lang("employees_permission_info");?>'
	});

</script>

<?php
}
?>
<input type='hidden' id='zendesk' name='zendesk' value='<?php echo $person_info->zendesk; ?>'/>
	   		
</fieldset>
<div class='clear'></div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#employee_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
				post_person_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			first_name: "required",
			last_name: "required",
			username:
			{
				required:true,
				minlength: 5
			},

			password:
			{
				<?php
				if($person_info->person_id == "")
				{
				?>
				required:true,
				<?php
				}
				?>
				minlength: 8
			},
			repeat_password:
			{
 				equalTo: "#password"
			},
    		email:
    		{
    			required:true,
    			email:true
    		}
   		},
		messages:
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		username:
     		{
     			required: "<?php echo lang('employees_username_required'); ?>",
     			minlength: "<?php echo lang('employees_username_minlength'); ?>"
     		},

			password:
			{
				<?php
				if($person_info->person_id == "")
				{
				?>
				required:"<?php echo lang('employees_password_required'); ?>",
				<?php
				}
				?>
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			repeat_password:
			{
				equalTo: "<?php echo lang('employees_password_must_match'); ?>"
     		},
     		email:{
     			required:"<?php echo lang('common_email_required'); ?>",
     			email:"<?php echo lang('common_email_invalid_format'); ?>"
     		}
		}
	});
});
</script>
<?php
$cell_style = '';
if($export_excel == 1)
{
	//$excelXml = new Excel_XML();
	//$excelXml->setWorksheetTitle($title);
	$rows = array();
	#row = array();
	$row= array();
	foreach ($headers as $header) 
	{
		$row[] = strip_tags($header['data']);
	}
	$rows[] = $row;
	
	foreach($data as $datarow)
	{
		$row = array();
		foreach($datarow as $cell)
		{
			$row[] = strip_tags($cell['data']);
		}
		$rows[] = $row;
	}
	if ($data_2)
	{
		//$rows[] = '';
		foreach($data_2 as $datarow)
		{
			$row = array();
			foreach($datarow as $cell)
			{
				$row[] = strip_tags($cell['data']);
			}
			$rows[] = $row;
		}
	}
	//$excelXml->addArray($rows);
	//$excelXml->generateXML($title);
	$content = array_to_csv($rows);
	
	force_download(strip_tags($title) . '.csv', $content);
	exit;
}

?>

<?php 
if ($export_excel == 2)
{
?>
<style>
	
</style>
<?php	
	$columns = count($headers);
	$cell_style = 'width="'.(765/$columns).'" height="40"';
}
else 
{
//$this->load->view("partial/header");
?>
<style>
	#table_holder {
		width:100%;
	}
</style>
<?php	
}
?>
<table id="report_contents">
	<tr>
		<td id="item_table">
			<div id="table_holder">
				<table border='0' class="tablesorter report" id="sortable_table">
					<thead>
						<?php if ($export_excel == 2) {?>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center;font-size:20px;font-weight:bold;'>
									<?=$title?>
								</div>
							</th>
						</tr>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center; font-size:14px;'>
									<?=$subtitle?>
								</div>
							</th>
						</tr>
						<?php } ?>
						<tr>
							<?php foreach ($headers as $header) { ?>
							<th <?=$cell_style?> align="<?php echo $header['align'];?>"><div class="header_cell header"><?php echo $header['data']; ?><?php if ($export_excel!=2) {?><span class="sortArrow">&nbsp;</span><?php } ?></div></th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $index => $row) { ?>
						<tr>
							<?php foreach ($row as $cell) { ?>
							<td <?=$cell_style?> class="<?php echo ($is_invoice_report ? 'invoice_report colorbox' : ''); ?>" align="<?php echo $cell['align'];?>" colspan ="<?php echo $cell['colspan'] ? $cell['colspan'] : 1 ;?>"  style ="<?php echo $cell['background'] ? 'background:'.$cell['background'].';' : '' ;?> <?php echo $cell['font-size'] ? 'font-size:'.$cell['font-size'].'px;' : '' ;?>" ><?php echo ($export_excel ==2 ? substr($cell['data'],0,25):$cell['data']); ?></td>
							<?php  } ?>
						</tr>
						<?php } ?>
						<?php foreach ($data_2 as $row) { ?>
						<tr>
							<?php foreach ($row as $cell) { ?>
							<td <?=$cell_style?> align="<?php echo $cell['align'];?>"><?php echo $cell['data']; ?></td>
							<?php } ?>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>	
			<!--div id="report_summary" class="tablesorter report">
			<?php foreach($summary_data as $name=>$value) { ?>
				<div class="summary_row"><?php echo "<strong>".lang('reports_'.$name). '</strong>: '.to_currency($value); ?></div>
			<?php }?>
			</div-->
			<table id="report_summary" class="tablesorter report">
			<?php foreach($summary_data as $name=>$value) { ?>
				<tr class="summary_row"><td width='382' align='right'><?php echo "<strong>".lang('reports_'.$name). '</strong>: '.to_currency($value); ?></td></tr>
			<?php }?>
			</table>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php if ($export_excel != 2) { ?>

<script type="text/javascript" language="javascript">
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(); 
	}
}
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	init_table_sorting();	
});
</script>
<?php } ?>
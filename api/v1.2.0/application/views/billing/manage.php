<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
var credit_cards = {
	close:function() {
		$.colorbox.close();
	},
	show_course:function(course_id) {
		console.log('showing course');
		$.colorbox({
			href:'index.php/credit_cards/view/'+course_id,
			'onComplete':function() {
               		credit_cards.get();
           	},
            'width':650
		});
	},
	get:function(replace_menu) {
		replace_menu = (replace_menu == undefined)?false:true;
		this.loading_mask();
		var course_id = $('#course_id').val();
		$.ajax({
           type: "POST",
           url: "index.php/credit_cards/get",
           data: "course_id="+course_id,
           success: function(response){
           		console.dir(response);
           		if (replace_menu)
           			credit_cards.replace_menu(response);
           		else
           			credit_cards.load(response);
           },
	       dataType:'json'
        });
	},
	load:function (data) {
		$('#saved_credit_cards').html('');
		for (var i in data)
		{
			var cc = data[i];
			var saved_card = "<tr id='card_"+cc.credit_card_id+"'><td><a class='delete_credit_card' href='javascript:credit_cards.delete("+cc.credit_card_id+")'>Remove</a></td><td><a class='charge_credit_card' href='javascript:credit_cards.charge("+cc.credit_card_id+")'>Charge</a></td><td class='name_on_card'>"+cc.cardholder_name+"</td> <td class='card_type'>"+cc.card_type+"</td> <td class='last_four'>"+cc.masked_account+"</td> <td class='expiration'>"+cc.token_expiration+"</td></tr>";
			$('#saved_credit_cards').append(saved_card);
		}
		this.unmask();
		$.colorbox.resize();
	},
	replace_menu:function (data) {
		var menu_html = "<select id='credit_card_id' name='credit_card_id' class='valid'>";
		menu_html += "<option value=''></option>";
		for (var i in data)
		{
			var cc = data[i];
			menu_html += "<option value='"+cc.credit_card_id+"'>"+cc.card_type+" - "+cc.masked_account+"</option>";
		}
		menu_html += "</select>";
		$('#credit_card_id').replaceWith($(menu_html));
	},
	add_item:function() {
		var description = $('#description').val();
		var amount = $('#amount').val();
		$.ajax({
           type: "POST",
           url: "index.php/credit_cards/add_charge_item",
           data: "description="+description+'&amount='+amount,
           success: function(response){
           		console.dir(response);
		    	var item_html = "<tr id='item_"+response.line_number+"'><td class='description'>"+description+"</td><td class='amount'>$"+parseFloat(amount).toFixed(2)+"</td><td><a class='delete_item' href='javascript:credit_cards.delete_item("+response.line_number+")'>Remove</a></td></tr>";
				
				$('#charge_items').append($(item_html));
				$('#description').val('');
				$('#amount').val('');
				credit_cards.update_charge_totals(response.totals);
				$.colorbox.resize();
	       },
	       dataType:'json'
        });
	},
	delete_item:function(line_number) {
		$.ajax({
           type: "POST",
           url: "index.php/credit_cards/delete_charge_item",
           data: "line_number="+line_number,
           success: function(response){
           		console.dir(response);
				$('#item_'+line_number).remove();
		    	credit_cards.update_charge_totals(response.totals);
				$.colorbox.resize();
	       },
	       dataType:'json'
        });
	},
	update_charge_taxes:function() {
		$.ajax({
           type: "POST",
           url: "index.php/credit_cards/update_tax_rate/",
           data: 'tax_rate='+$('#tax_rate').val(),
           success: function(response){
           		console.dir(response);
		        credit_cards.update_charge_totals(response.totals);
			},
            dataType:'json'
         });
	},
	update_charge_totals:function(totals) {
		$('#subtotal').html('$'+totals.subtotal);
		$('#taxes').html('$'+totals.taxes);
		$('#total').html('$'+totals.total);
	},
	delete:function(credit_card_id) {
		$.ajax({
           type: "POST",
           url: "index.php/credit_cards/delete",
           data: "credit_card_id="+credit_card_id,
           success: function(response){
           		console.dir(response);
           		credit_cards.remove(credit_card_id);
           },
	       dataType:'json'
        });
	},
	remove:function(credit_card_id) {
		$('#card_'+credit_card_id).remove();
		$.colorbox.resize();
	},
	charge:function(credit_card_id) {
		$.colorbox({
			href:'index.php/credit_cards/charge/'+credit_card_id,
			width:650
		})
	},
	loading_mask:function() {
		$('#saved_credit_cards').mask("<?php echo lang('common_wait');?>");
	},
	unmask:function() {
		$('#saved_credit_cards').unmask();
	}
};
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':650});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("items/generate_barcodes");?>/'+selected.join('~'));
    });

	$('#generate_barcode_labels').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("items/generate_barcode_labels");?>/'+selected.join('~'));
    });
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		set_feedback(response.message,'success_message',false);
		setTimeout(function(){window.location.reload();}, 2500);
	}
}
</script>

<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view/-1",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new')));
					echo 
					anchor("credit_cards/view/-1",
					lang($controller_name.'_new_credit_card'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new_credit_card')));
				
					echo 
					anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>

			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>
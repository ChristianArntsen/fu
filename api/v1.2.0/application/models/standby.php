<?php
class Standby extends CI_Model
{
    function get_list()
    {
        $teesheet_id = $this->session->userdata('teesheet_id');
		
        $this->db->from('teetime_standbys');
        $this->db->where('teesheet_id', $teesheet_id);
        $this->db->where("time >= CURRENT_DATE AND time < CURRENT_DATE + INTERVAL 1 DAY ORDER BY time ASC" );
		$standbys = $this->db->get();
        //echo $this->db->last_query();
        $standbys = $standbys->result_array();
        
      
        $count = 0;
        foreach ($standbys as $standby)
        {
            
            $standbys[$count++]['time'] = date("g:i a", strtotime($standby['time']));
            
            //$standby_html .= "<div class='standby_entry' id='{$standby['standby_id']}'><div class='teetime_result' id='{$standby['standby_id']}'><div>{$standby['name']} {$date} {$standby['holes']} holes</div><div>Players: {$standby['players']}</div> </div></div>";
            
        }
        log_message('error', 'STANDBY ' . $standbys[0]['time']);
        return $standbys;
    }
    function get_by_id($id)
    {
        
        if ($id === '')
            return '';
        
        $this->db->from('teetime_standbys');
        $this->db->where("standby_id = '$id'");
        $standby_entries = $this->db->get()->result_array();
        $standby_entry = $standby_entries[0];
        //log_message('error', 'Teetime details ' . urlencode($standby_entry['details']));
        //log_message('error', $standby_entry['name'] . " {$standby_entry['time']} {$standby_entry['players']} 
          //  {$standby_entry['holes']} {$standby_entry['details']} {$standby_entry['email']} {$standby_entry['phone']}");
        
        return $standby_entry;
      
    }
    function get_by_id2($id)
    {
        
        if ($id === '')
            return '';
        
        $this->db->from('teetime_standbys');
        $this->db->where("standby_id = '$id'");
        $standby_entries = $this->db->get()->result_array();
        $standby_entry = $standby_entries[0];
        //log_message('error', 'Teetime details ' . urlencode($standby_entry['details']));
        //log_message('error', $standby_entry['name'] . " {$standby_entry['time']} {$standby_entry['players']} 
          //  {$standby_entry['holes']} {$standby_entry['details']} {$standby_entry['email']} {$standby_entry['phone']}");
        
        return "?name=".urlencode($standby_entry['name']) ."&time=". urlencode($standby_entry['time']) . "&players=" .urlencode($standby_entry['players'])."&holes=".url_encode($standby_entry['holes']) . 
                "&details=" . urlencode($standby_entry['details']) . "&email=" . urlencode($standby_entry['email']) . "&phone=".urlencode($standby_entry['phone']) . "&id=" . urlencode($id);
        
      
    }
    function save($name, $email, $phone, $holes, $num_players, $time, $details, $person_id)
    {
    	$teesheet_id = $this->session->userdata('teesheet_id');
        //log_message('error', 'time is: ' . date('Y-m-d G:i', strtotime($time)));
        $this->db->query("INSERT INTO foreup_teetime_standbys (teesheet_id, person_id, name, email, phone, holes, players, time, details)
                                VALUES ('$teesheet_id', '$person_id', '$name', '$email', '$phone', '$holes', '$num_players', '". date('Y-m-d G:i', strtotime($time))."', '$details')");
        
    }
    function edit($name, $email, $phone, $holes, $num_players, $time, $details, $id, $person_id)
    {
        
        $this->db->query("UPDATE foreup_teetime_standbys SET teesheet_id='', person_id='$person_id', name='$name', email='$email', phone='$phone', holes='$holes', players='$num_players', time='" . date('Y-m-d G:i', strtotime($time)) . "', details='$details' WHERE standby_id='$id'");
                                
        
    }
    function delete($id)
    {
        $this->db->query("DELETE FROM foreup_teetime_standbys WHERE standby_id='$id'");
    }
}
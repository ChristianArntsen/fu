<?php
class Tournament_winners extends CI_Model
{
	/*
	Gets tournament items for a particular tournament
	*/
	function get_info($tournament_id)
	{
		$this->db->from('tournament_winners');
		$this->db->where('tournament_id',$tournament_id);
		//return an array of tournament inventory items for a tournament
		return $this->db->get()->result();
	}
	
	/*
	Inserts or updates a tournament's items
	*/
	function save($tournament_winners, $tournament_id)
	{		
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		foreach ($tournament_winners as $row)
		{
			// print_r($row);
			
			//Update tournament_winners table
			if ($row['deleted'] === '1') {	
				$this->db->delete('tournament_winners', array('tournament_winner_id' => $row['tournament_winner_id']));
			} 
			else if ($row['tournament_winner_id'] != "") {
				$insert_row = $row;							
				unset($insert_row['deleted']);	//remove 'deleted' because there is now column on the db table for it			
				unset($insert_row['previous_award_amount']);	//remove 'previous_award_amount' because there is now column on the db table for it
				
				$this->db->where('tournament_winner_id', $insert_row['tournament_winner_id']);
				$this->db->update('tournament_winners',$insert_row);
			} 
			else {
				$insert_row = $row;			
				unset($insert_row['deleted']); //remove 'deleted' because there is now column on the db table for it
				unset($insert_row['previous_award_amount']);	//remove 'previous_award_amount' because there is now column on the db table for it
								
				$this->db->insert('tournament_winners',$insert_row);
			}
			
			
					
		}
					
		if ($this->db->trans_complete()) {
			//Update account transactions table
			foreach ($tournament_winners as $row)
			{
				$add_subtract = (float)$row['amount']	- (float)$row['previous_award_amount'];
				$person_id = $row['person_id'];
				$trans_description = 'Tournament Award Winnings';
				$trans_details = 'Adjustment to Account';
				
				if (!$add_subtract == 0) {
					$this->Account_transactions->save($person_id, $trans_description, $add_subtract, $trans_details);
				}				
				
			}
			
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	/*
	Deletes item kit items given an item kit
	*/
	function delete($tournament_id)
	{
		return $this->db->delete('tournament_inventory_items', array('tournament_id' => $tournament_id)); 
	}
}
?>

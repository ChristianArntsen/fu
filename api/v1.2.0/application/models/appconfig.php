<?php
class Appconfig extends CI_Model 
{
	
	function exists($key)
	{
		$this->db->from('courses');	
                $this->db->select($key);
		$this->db->where('course_id',$this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}
	
	function get_all()
	{
		$this->db->from('courses');
                $this->db->where('course_id', $this->session->userdata('course_id'));
                $this->db->limit(1);
		return $this->db->get();		
	}
	
	function get($key, $course_id = false)
	{
		$query = $this->db->get_where('courses', array('course_id' => ($course_id ? $course_id : $this->session->userdata('course_id'))), 1);
		
		if($query->num_rows()==1)
		{
			return $query->row()->$key;
		}
		
		return "";
		
	}
	
	function save($key,$value)
	{
		$config_data=array(
		$key=>$value
		);
				
		//if (!$this->exists($key))
		//{
		//	return $this->db->insert('app_config',$config_data);
		//}
		
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->update('courses',$config_data);		
	}
	function save_all($config_data)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->update('courses',$config_data);		
	}
	
	function batch_save($data)
	{
		$success=true;
		
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		foreach($data as $key=>$value)
		{
			if(!$this->save($key,$value))
			{
				$success=false;
				break;
			}
                        echo $this->db->last_query();
		}
		
		$this->db->trans_complete();		
		return $success;
		
	}
	function is_holiday($date_string = '')
	{
		if ($date_string == '')
			$date_string = date('Y-m-d');
		else
			$date_string = date('Y-m-d', strtotime($date_string));
		
		$this->db->from('holidays');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('date', $date_string);
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows() == 1);
	}
	function get_holiday_info()
	{
		$this->db->from('holidays');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by('date');
		return $this->db->get()->result_array();
	}
		
	function add_holiday($holiday_label, $holiday_date) 
	{
		return $this->db->insert('holidays', array('label'=>$holiday_label, 'date'=>date('Y-m-d', strtotime($holiday_date)), 'course_id'=>$this->session->userdata('course_id')));
	}
	function update_holiday($holiday_date, $holiday_label, $new_date) 
	{
		$this->db->where('date', $holiday_date);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		return $this->db->update('holidays', array('label'=>$holiday_label, 'date'=>date('Y-m-d', strtotime($new_date))));
	}
	function delete_holiday($holiday_date) 
	{
		$this->db->delete('holidays', array('date'=>date('Y-m-d', strtotime($holiday_date))));
	}
	function delete($key)
	{
		//return $this->db->delete('app_config', array('key' => $key)); 
        $this->db->where("course_id", $this->session->userdata('course_id'));
        return $this->db->update('courses', array($key=>''));
	}
	
	function delete_all()
	{
		//return $this->db->empty_table('app_config'); 
	}
	function get_foreup_image()
	{
		return 'images/header/header_logo.png';
	}
	function get_logo_image($company_logo = '', $course_id = false)
	{
		if ($this->config->item('company_logo') || $course_id)
		{
			$img_id = $this->get('company_logo', $course_id);
			return $img_id ? site_url('app_files/view/'.$img_id) : false;
		}
		else if ($company_logo != '')
		{
			return site_url('app_files/view/'.$company_logo);
		}
		return 'images/header/header_logo.png';
	}
    function get_teetime_types($index)
    {
    	$ttime_array = array('5'=>"18 - Weekday ", '2'=>'9 - Weekday');
		if ($index == 2)
	    	$ttime_array = array('2'=>"9 - Weekday ", '5'=>'18 - Weekday');
    	if ($index == 6)
			$ttime_array = array('6'=>'18 - Weekend', '3'=>'9 - Weekend');
    	if ($index == 3)
			$ttime_array = array('3'=>'9 - Weekend', '6'=>'18 - Weekend');
        $type_array = array();
		foreach ($ttime_array as $index => $teetime_label) {
			for ($i = 1; $i <=30; $i++) {
			    if ($i == 1)
				    $type_array['1_'.$index] = "$teetime_label Regular";
				else if ($i == 2) {
					if ($this->config->item('twilight_hour') != '2399')
			            $type_array['2_'.$index] = "$teetime_label Twilight";
				}
				else if ($i == 3) {
			        if ($this->config->item('super_twilight_hour') != '2399')
			            $type_array['3_'.$index] = "$teetime_label Super Twilight";
				}
				else 
				    if ($this->config->item('price_category_'.$i) != '')
		                $type_array[$i.'_'.$index] = $teetime_label.' '.$this->config->item('price_category_'.$i);
			}
		}
        return $type_array;
    }
	function get_cart_types($index)
    {
    	$cart_array = array(4=>'18 - Cart', 1=>'9 - Cart');
    	if ($index == 1)
			$cart_array = array (1=>'9 - Cart', 4=>'18 - Cart');
		$type_array = array();
		foreach ($cart_array as $index => $cart_label) {
			for ($i = 1; $i <=30; $i++) {
				$item_number = $this->session->userdata('course_id').'_'.$i.'_'.$index;
		    	$cart_info = $this->Item->get_info($this->Item->get_item_id($item_number));
				//echo $cart_info->unit_price;
			//return $cart_info;
				if ((float)$cart_info->unit_price > 0) {
					if ($i == 1)
				        $type_array['1_'.$index] = "$cart_label Regular";
					else if ($i == 2) {
				        if ($this->config->item('twilight_hour') != '2399')
				            $type_array['2_'.$index] = "$cart_label Twilight";
					}
					else if ($i == 3) {
				        if ($this->config->item('super_twilight_hour') != '2399')
				            $type_array['3_'.$index] = "$cart_label Super Twilight";
					}
					else 
				        if ($this->config->item('price_category_'.$i) != '')
			                $type_array[$i.'_'.$index] = $cart_label.' '.$this->config->item('price_category_'.$i);
		    	}
			}
		}
        return $type_array;
    }
}

?>

<?php
class Customer_loyalty extends CI_Model
{
	function get_rates()
	{
		//$this->db->select('loyalty_rate_id, label, type, value, points_per_dollar, dollars_per_point');
		$this->db->from('loyalty_rates');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get();
	}
	function get_points_per_dollar($item)
	{
		$tee_time_index = 0;
		$price_category = 0;
		if ($item['price_category'])
		{
			$tee_time_index = $item['teetime_type'];
			$price_category = $item['price_category'];
		}
		
		if (isset($item['item_id']) || isset($item['item_kit_id']) || isset($item['department']) || isset($item['category']) || isset($item['subcategory']))
		{
			$where = array();		
			$this->db->from('loyalty_rates');
			$this->db->where('course_id', $this->session->userdata('course_id'));
			$tti_sql = ($tee_time_index) ?" AND (`tee_time_index` = $tee_time_index)" : '';
			$pc_sql = ($price_category) ? " AND (`price_category` = $price_category)" : '';
			if (isset($item['item_id']))
				$where[] = "(`type` = 'item' AND `value` = ".$this->db->escape($item['item_id'])." $tti_sql $pc_sql)";
			if (isset($item['item_kit_id']))
				$where[] = "(`type` = 'item_kit' AND `value` = ".$this->db->escape($item['item_kit_id']).")";
			if (isset($item['department']))
				$where[] = "(`type` = 'department' AND `value` = ".$this->db->escape($item['department']).")";
			if (isset($item['category']))
				$where[] = "(`type` = 'category' AND `value` = ".$this->db->escape($item['category']).")";
			if (isset($item['subcategory']))
				$where[] = "(`type` = 'subcategory' AND `value` = ".$this->db->escape($item['subcategory']).")";
			if (count($where) > 0)
				$this->db->where('('.implode(' OR ', $where).')');
			$this->db->order_by('points_per_dollar', "desc");
			
			$result = $this->db->get();
			//echo $this->db->last_query();
			//print_r($result);
			if ($result->num_rows() > 0)
				return $result->row_array();
		}
	
		return array('points_per_dollar'=>0);
	}
	function get_dollars_per_point($item)
	{
		$tee_time_index = 0;
		$price_category = 0;
		if ($item['price_category'])
		{
			$tee_time_index = $item['teetime_type'];
			$price_category = $item['price_category'];
		}
		
		if (isset($item['item_id']) || isset($item['item_kit_id']) || isset($item['department']) || isset($item['category']) || isset($item['subcategory']))
		{
			$where = array();		
			$this->db->from('loyalty_rates');
			$this->db->where('course_id', $this->session->userdata('course_id'));
			$tti_sql = ($tee_time_index) ?" AND (`tee_time_index` = $tee_time_index)" : '';
			$pc_sql = ($price_category) ? " AND (`price_category` = $price_category)" : '';
			 
			if (isset($item['item_id']))
				$where[] = "(`type` = 'item' AND `value` = ".$this->db->escape($item['item_id'])." $tti_sql $pc_sql)";
			if (isset($item['item_kit_id']))
				$where[] = "(`type` = 'item_kit' AND `value` = ".$this->db->escape($item['item_kit_id']).")";
			if (isset($item['department']))
				$where[] = "(`type` = 'department' AND `value` = ".$this->db->escape($item['department']).")";
			if (isset($item['category']))
				$where[] = "(`type` = 'category' AND `value` = ".$this->db->escape($item['category']).")";
			if (isset($item['subcategory']))
				$where[] = "(`type` = 'subcategory' AND `value` = ".$this->db->escape($item['subcategory']).")";
			if (count($where) > 0)
				$this->db->where('('.implode(' OR ', $where).')');
			$this->db->order_by('dollars_per_point', "desc");
			
			$result = $this->db->get();
			if ($result->num_rows() > 0)
				return $result->row_array();
		}

		return array('dollars_per_point'=>0);
	}
	function save(&$loyalty_info, $loyalty_id = false) {
		if (!$loyalty_id)
		{
			return $this->db->insert('loyalty_rates', $loyalty_info);
		}
		else 
		{
			$this->db->where('loyalty_rate_id', $loyalty_id);
			return $this->db->update('loyalty_rates', $loyalty_info);
		}
	}
	function delete($loyalty_id) {
		$this->db->where('loyalty_rate_id', $loyalty_id);
		return $this->db->delete('loyalty_rates');
	}
	function save_transaction($person_id = -1, $trans_description='', $add_subtract='', $trans_details='', $sale_id='', $employee_id)
	{		
		$employee_id=$employee_id?$employee_id:$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_customer_info = $this->Customer->get_info($person_id);
		if ($add_subtract == '')
			$add_subtract = $this->input->post('add_subtract');
		if ($trans_description == '')
			$trans_description = $this->input->post('trans_comment');
		$trans_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_customer'=>$person_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$trans_description,
			'trans_description'=>$trans_details,
			'trans_amount'=>$add_subtract
		);
		
		//Update account balance
		$customer_data = array(
		'loyalty_points'=>$cur_customer_info->loyalty_points + $add_subtract
		);
		$person_data = array(
		'address_2'=>$cur_customer_info->address_2
		);

		$giftcards = array();
		$groups = array();
		$passes = array();
		if ($this->Customer->save($person_data, $customer_data,$person_id, $giftcards, $groups, $passes)) 
		{
			$this->insert($trans_data);
			return true;
		}
		else 
		{			
			return false;
		}
	}	
	
	function insert($loyalty_transaction_data)
	{
		return $this->db->insert('loyalty_transactions',$loyalty_transaction_data);
	}
	
	function get_loyalty_transaction_data_for_customer($customer_id)
	{
		$result = $this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date FROM (`foreup_loyalty_transactions`) WHERE `trans_customer` = '$customer_id' ORDER BY `trans_date` desc");
		return $result->result_array();		
	}
}	
<?php
//This controller is for the purpose of running cron tasks in the system

class Daemon extends CI_Controller//REST_Controller
{
	private $cron_key = 'f0r3upcr0n';
    function __construct()
    {
        parent::__construct('cron');
		$this->load->library('Marketing_recipients_lib');
    	$this->load->library('Twilio');
    	$this->config->load('twilio', TRUE);

		$this->load->model('Customer');
		$this->load->model('Teesheet');
		$this->load->model('Teetime');
		$this->load->model('Communication');
		$this->load->model('Sendhub_account');
		$this->load->library('sendhub');
		$this->load->helper('email_marketing');
	}
	function test_email($cron_key)
	{
		$count = 0;
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			do
			{
				send_sendgrid(
					'jhopkins@foreup.com',
					"Testing out Daemons ".date("Y-m-d h:i:s"),
					'Here is a little bit of text to go with it. Count: '.$count,
					'no-reply@foreup.com',
				 	'ForeUP Daemon Tester'
				);
				// WAIT 4 MINUTES
				sleep(60 * 4);
				$count++;
			} while($count < 4);
		}
	}
	function send_marketing_campaigns($cron_key)
	{
		date_default_timezone_set('America/Chicago');
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			ini_set('memory_limit', '100M');
			$count = 0;
			do
			{
				// CHECK FOR ANY POTENTIALLY UNSENT/ERRORED CAMPAIGNS
				$date = date('Y-m-d 00:00:00');
				$query = $this->db->query("
					SELECT
						foreup_marketing_campaigns.campaign_id AS campaign_id,
						foreup_marketing_campaigns.recipient_count AS scheduled,
						foreup_communications.recipient_count AS sent
					FROM `foreup_communications`
						LEFT JOIN foreup_marketing_campaigns ON foreup_communications.campaign_id = foreup_marketing_campaigns.campaign_id
					WHERE date > '$date'
						AND foreup_marketing_campaigns.recipient_count > 0
						AND foreup_marketing_campaigns. recipient_count -foreup_communications.recipient_count > 10
						AND reported = 0");

				$potentially_unsent_campaigns = $query->result_array();
				if (count($potentially_unsent_campaigns) > 0)
				{
					$pu_campaigns = array();
					$puc_html = '<table><tbody><tr><td>Campaign ID</td><td>Scheduled</td><td>Sent</td></tr>';
					foreach ($potentially_unsent_campaigns as $cpgn)
					{
						$pu_campaigns[] = $cpgn['campaign_id'];
						$puc_html .= "<tr><td>{$cpgn['campaign_id']}</td><td>{$cpgn['scheduled']}</td><td>{$cpgn['sent']}</td></tr>";
					}
					$puc_html .= "</tbody></table>";

					//MARK THEM ALL AS POTENTIALLY UNSENT
					$mc_data = array('reported'=>1);
					$this->db->where_in('campaign_id', $pu_campaigns);
					$this->db->update('marketing_campaigns', $mc_data);
					echo $this->db->last_query();
					send_sendgrid(
						'jhopkins@foreup.com',
						"Potentially unsent Campaigns found",
						$puc_html,
						'marketing@foreup.com',
					 	'ForeUP Marketing'
					);
				}
				// END CHECKING FOR PROBLEM CAMPAIGNS


				$info = $this->Marketing_campaign->get_all_unsent_campaigns();
				//\echo $this->db->last_query().'<br/>'.count($info);
				//print_r($info);
				if (count($info) > 0)
				{
					//echo 'here';
				   	$mrl  = $this->marketing_recipients_lib;
					//echo 'and here';
					//return;
				    foreach($info as $i)
				    {
				    	$this->Marketing_campaign->mark_attempted($i->campaign_id);
				      $course  = $this->Course->get_info($i->course_id);
					  $mrl->reset();
				      $rec = $i->recipients;
				      $rec = unserialize($rec);
					  //echo 'rec<br/>';
					  //print_r($rec);
					  //echo '<br/>';
					  if ($rec['groups'])
					      $mrl->set_groups($rec['groups']);
					  if ($rec['individuals'])
					      $mrl->set_individuals($rec['individuals']);
				      $type = $i->type;
					  log_message('error','daemon right before get_recipients');
				      $recpnts = $mrl->get_recipients($type, $i->course_id);
					  //echo '<BR/>ready to send<br/>';
					  //echo "recipients :";
					  //print_r($recpnts);
					  //echo "<br/><br/>";
					  log_message("error", 'daemon recpnts count '.count($recpnts));
				      if(strtolower($type) == 'email')
				      {
				      	$contents = $this->get_contents($i->campaign_id);
						if ($contents != ''){
					        $this->Marketing_campaign->send_mails($i->campaign_id, $recpnts, $contents, $course, $i->subject);
						}
					  }
				      else
				      {
				        /**
				         * not yet tested, but already setup.
				         */
				        // $this->Marketing_campaign->send_text($i->campaign_id, $recpnts, $i->content);
				        //$this->Marketing_campaign->send_sendhub_text($i->campaign_id, $recpnts, $i->content);
						$this->Sendhub_account->send_group_text($i->campaign_id, $recpnts, $i->content, $i->course_id);
				      }
					  sleep(10);
				    }
				}
				else {
					// WAIT 15 minutes
					sleep(60 * 15);
					//echo '<br/>Waiting<br/>';
				}
				$count++;
			} while($count < 1);
		}
	}
	function service_start_notification($service)
	{
		if ($service == 'send_marketing_campaigns' && (date('H') < 6 || date('H') > 7))
			return;
		if ($service == 'send_tee_time_thank_yous' && (date('H') < 7 || date('H') > 8))
			return;
		if ($service == 'quickbooks_sync' && (date('H') >= 21 || date('H') <= 3))
			return;

		send_sendgrid(
			'jhopkins@foreup.com',
			"Restarted $service Daemon Service ".date("Y-m-d h:i:s"),
			"$service Daemon Service has just restarted",
			'daemon@foreup.com',
		 	'ForeUP Daemon'
		);
	}

	function run_invoice_queue()
	{
		// LOADING LIBRARIES AND MODELS
		$this->load->model('Queue_invoice');
		$this->load->library('zip');
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->model('Invoice');
		$this->load->library('Html2pdf');

		$jobs = $this->Queue_invoice->get_jobs();

		foreach ($jobs as $key => $job) {
			//include member balance or customer credit balances on invoice?
			if ($job['member_balance'] && $job['customer_credit'])
				$batch_type = 'both';
			else if ($job['member_balance'])
				$batch_type = 'member';
			else if ($job['customer_credit'])
				$batch_type = 'customer';

			//general course info
			$course_info = $this->Course->get_info($job['course_id']);

			$customers = $this->Customer->get_negative_balances($batch_type);
			foreach ($customers as $key => $customer) {
				$customers_by_id[$customer->person_id] = $customer;
			}
			$combined_doc = new Html2pdf('P','A4','fr');
			$combined_doc->pdf->SetDisplayMode('fullpage');
			$combined_doc->setDefaultFont('Arial');

			$start_date = $job['start_date'];
			$end_date = $job['end_date'];
			$limit = 10000;
			$offset = 0;

			$billings = $this->Customer_billing->get_all_in_range($start_date, $end_date, $limit, $offset);

			// echo $this->db->last_query();
			foreach ($billings->result_array() as $key => $billing) {
				//check the database for an invoice related to this billing
				$existing_invoice = $this->Invoice->get_recurring_billing_invoice($billing, $start_date, $end_date);
				$customer = $this->Customer->get_info($billing['person_id']);

				if (!$existing_invoice) {
					$invoice_data = array(
							'course_id'=>$billing['course_id'],
							'credit_card_id'=>$billing['credit_card_id'],
							'billing_id'=>$billing['billing_id'],
							'person_id'=>$billing['person_id'],
							'month_billed'=>$start_date
						);

					$this->Invoice->save($invoice_data);

					$invoice_number = $existing_invoice ? $existing_invoice['invoice_number'] : $invoice_data['invoice_number'];
					$item_data = array();
					$calculated_total = 0;
					$items=$this->Customer_billing->get_items($billing['billing_id']);
					// Just saving to invoices
					foreach ($items as $index => $item)
					{
						$item_data[] = array(
							'invoice_id'=> $existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'],
							'line_number'=>$index,
							'description'=>$item['description'],
							'quantity'=>$item['quantity'],
							'amount'=>$item['amount'],
							'tax'=>$item['tax'],
							'pay_account_balance'=>0,
							'pay_member_balance'=>0
						);
						$calculated_total += (int)$item['quantity'] * (float)$item['amount'] * (1 + (float)$item['tax']/100);
					}
					// ADD CUSTOMER ACCOUNT BALANCE
					if (($billing['pay_account_balance'] || $job['customer_credit']) && $customer->account_balance < 0)
					{
						$index++;
						$item_data[] = array(
							'invoice_id'=> $existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'],
							'line_number'=>$index,
							'description'=>($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')),
							'quantity'=>1,
							'amount'=>-$customer->account_balance,
							'tax'=>0,
							'pay_account_balance'=>1,
							'pay_member_balance'=>0
						);
						$calculated_total += -(float)$customer->account_balance;
					}
					// ADD MEMBER ACCOUNT BALANCE
					if (($billing['pay_member_balance'] || $job['member_balance']) && $customer->member_account_balance < 0)
					{
						$index++;
						$item_data[] = array(
							'invoice_id'=>$existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'],
							'line_number'=>$index,
							'description'=>($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')),
							'quantity'=>1,
							'amount'=>-$customer->member_account_balance,
							'tax'=>0,
							'pay_account_balance'=>0,
							'pay_member_balance'=>1
						);
						$calculated_total += -(float)$customer->member_account_balance;
					}

					$invoice_id = $existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'];
					$this->Invoice->save_items($item_data, $invoice_id);

					// Charge credit card and update the invoice
					$invoice_data = array(
						'total'=>$calculated_total
					);
					if (!$existing_invoice) {
						$invoice_data['paid'] = '0.00';
					}

					$this->Invoice->save($invoice_data, $invoice_id);
				} else {
					$item_data = $this->Invoice->get_items($existing_invoice['invoice_id']);
				}
				$course_info = $this->Course->get_info($billing['course_id']);

				$invoice_data = $this->Invoice->get_info($existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id']);

				$data = $invoice_data[0];
				$data['invoice_number'] = $existing_invoice ? $existing_invoice['invoice_number'] : $invoice_number;
				$data['total_due'] = $existing_invoice ? $existing_invoice['total_due'] : $calculated_total;
				$data['total_paid'] = $existing_invoice ? $existing_invoice['paid'] : 0;
				$data['invoice_id'] = $existing_invoice ? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'];
				$data['items']=$item_data;
				$data['person_info']=$customer;
				$data['course_info']=$course_info;
				$data['credit_cards']='';
				$data['popup'] = 1;// = array(
				$data['is_invoice'] = true;
				$data['sent'] = true;
				$data['pdf'] = true;

				$invoice_html = $this->load->view('customers/invoice', $data, true);
				$html2pdf = new Html2pdf('P','A4','fr');

				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->setDefaultFont('Arial');
				$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$combined_doc->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$this->zip->add_data("{$customer->last_name}_{$customer->first_name}_".date('m-Y').".pdf", $html2pdf->Output('', true));

				unset($customers_by_id[$customer->person_id]);

			}

			foreach ($customers_by_id as $customer)
			{
				// Save invoice
				$invoice_data = array(
					'course_id'=>$this->session->userdata('course_id'),
					'person_id'=>$customer->person_id
				);
				$this->Invoice->save($invoice_data);

				$invoice_number = $invoice_data['invoice_number'];

				$item_data = array();
				$calculated_total = 0;
				$index = 0;
				// Just saving to invoices
				if (($batch_type == 'member' || $batch_type == 'both') && $customer->member_account_balance < 0)
				{
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')),
						'quantity'=>1,
						'amount'=>-$customer->member_account_balance,
						'tax'=>0,
						'pay_account_balance'=>0,
						'pay_member_balance'=>1

					);
					$calculated_total += -$customer->member_account_balance;
					$index ++;
				}
				if (($batch_type == 'customer' || $batch_type == 'both') && $customer->account_balance < 0)
				{
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')),
						'quantity'=>1,
						'amount'=>-$customer->account_balance,
						'tax'=>0,
						'pay_account_balance'=>1,
						'pay_member_balance'=>0
					);
					$calculated_total += -$customer->account_balance;
				}
				$this->Invoice->save_items($item_data);
				//print_r($item_data);
				// Charge credit card and update the invoice
				$invoice_id = $invoice_data['invoice_id'];
				$invoice_data = array(
					'total'=>$calculated_total
				);
				$this->Invoice->save($invoice_data, $invoice_id);

				// Email copy of invoice
				if ($email_invoice)
					$this->Invoice->email($invoice_data['invoice_id']);//$invoice_id
				// Add to zip file
				$invoice_data = $this->Invoice->get_info($invoice_id);
				$data = $invoice_data[0];
				$data['invoice_number'] = $invoice_number;
				$data['total_due'] = $calculated_total;
				$data['total_paid'] = 0;
				$data['invoice_id'] = $invoice_id;
				$data['items']=$item_data;
				$data['person_info']=$customer;
				$data['course_info']=$course_info;
				$data['credit_cards']='';
				$data['popup'] = 1;// = array(
				$data['is_invoice'] = true;
				$data['sent'] = true;
				$data['pdf'] = true;
				$invoice_html = $this->load->view('customers/invoice', $data, true);
				$html2pdf = new Html2pdf('P','A4','fr');
				//$html2pdf->setModeDebug();
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->setDefaultFont('Arial');
				$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$combined_doc->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$this->zip->add_data("{$customer->last_name}_{$customer->first_name}_".date('m-Y', strtotime($start_date)).".pdf", $html2pdf->Output('', true));

			}

			$this->zip->add_data(date('m-Y', strtotime($start_date))."_Combined_Invoices".".pdf", $combined_doc->Output('', true));
			if (!$this->zip->download("invoices_".date('m-Y', strtotime($start_date)).".zip")) {

				$no_invoices_data = array(
					'member_balance'=>$member_balance,
					'customer_credit'=>$customer_credit,
					'recurring_billings'=>$recurring_billings,
					'start_on'=>$start_date,
					'end_on'=>$end_date
				);

				$this->load->view('customers/no_invoices',$no_invoices_data);
			}

			return;


		}//end of foreach on jobs


	}

	// function auto_bill_courses()
	// {
		// set_time_limit(0);
		// $product_names = array('1'=>'Software','2'=>'Website','3'=>'Marketing');
		// $this->load->model('Billing');
		// $this->load->model('Credit_card');
		// $this->load->library('Hosted_checkout_2');
		// $HC = new Hosted_checkout_2();
//
		// do {
			// // GET ALL BILLINGS THAT NEED TO BE BILLED. MAKE SURE TO ACCOUNT FOR THE LAST BILLING ATTEMPT VALUE
			// $todays_billings = $this->Billing->get_todays_billings();
			// $todays_billed = $this->Billing->get_todays_billed();
			// echo '<br/>'.$this->db->last_query().'<br/><br/>';
//
			// $todays_billings = $todays_billings->result_array();
			// // IF WE ACTUALLY HAVE RESULTS
			// if (true)
			// {
				// foreach ($todays_billings as $billing)
				// {
					// $this->Billing->mark_as_started($billing['billing_id']);
					// // MARK BILLING AS STARTED
					// //if (in_array($billing['billing_id'], $todays_billed))
					// //{
					// //	echo "<br/>Duplicate billing {$billing['billing_id']}<br/>";
					// //	return;
					// //}
					// $tax_rates = array();//explode(',', $billing['tax_rates']);
					// $contact_emails = array_unique(explode(',', $billing['contact_emails']));
					// $products = explode(',', $billing['products']);
//
					// $credit_card_charges = $product_list = array();
					// $subtotal = 0;
					// $product_list = $this->Billing->get_billing_products($billing['course_id'], $billing['credit_card_id']);
					// foreach ($product_list->result_array() as $product)
					// {
						// $payment_type = ($product['free'] ? 'Free' : ($product['monthly'] ? 'Monthly Payment' : ($product['annual'] ? 'Annual Payment' : '')));
						// $payment_amount = ($product['free'] ? 0 : ($product['monthly'] ? $product['monthly_amount'] : ($product['annual'] ? $product['annual_amount'] : 0)));
						// $subtotal += $payment_amount;
						// $credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>"ForeUP Services - {$product_names[$product['product']]} $payment_type", 'amount'=>$payment_amount);
						// $tax_rates[$product['tax_name']] = array('tax_amount'=>$payment_amount * $product['tax_rate'] / 100, 'tax_rate' => $product['tax_rate']);
					// }
					// $taxes = number_format($billing['annual_tax_amount'] + $billing['monthly_tax_amount'], 2);
					// $total = number_format($subtotal + $taxes, 2);
					// $totals = array('subtotal'=>$subtotal,'taxes'=>$taxes,'total'=>$total);
					// $course_info = $this->Course->get_info($billing['course_id']);
					// $invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
					// $employee = 'Auto Billing';
					// $HC->set_frequency('Recurring');
					// $HC->set_token($billing['token']);
					// $HC->set_cardholder_name($billing['cardholder_name']);
					// $HC->set_invoice($invoice);
					// $HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//Test Credentials
					// $transaction_results = $HC->token_transaction('Sale',$total, '0.00', $taxes);
					// $payment_data = array(
						// 'acq_ref_data'=>(string)$transaction_results->AcqRefData,
						// 'auth_code'=>(string)$transaction_results->AuthCode,
						// 'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
						// 'avs_result'=>(string)$transaction_results->AVSResult,
						// 'batch_no'=>(string)$transaction_results->BatchNo,
						// 'card_type'=>(string)$transaction_results->CardType,
						// 'cvv_result'=>(string)$transaction_results->CVVResult,
						// 'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
						// 'masked_account'=>(string)$transaction_results->Account,
						// 'status_message'=>(string)$transaction_results->Message,
						// 'amount'=>(string)$transaction_results->PurchaseAmount,
						// 'ref_no'=>(string)$transaction_results->RefNo,
						// 'status'=>(string)$transaction_results->Status,
						// 'token'=>(string)$transaction_results->Token,
						// 'process_data'=>(string)$transaction_results->ProcessData
					// );
					// // MARK BILLING AS CHARGED
					// $this->Billing->mark_as_charged($billing['billing_id']);
					// $this->Sale->update_credit_card_payment($invoice, $payment_data);
//
					// $credit_card_id = $billing['credit_card_id'];
					// $credit_card_data = array(
						// 'token'=>$payment_data['token'],
						// 'token_expiration'=>date('Y-m-d', strtotime('+2 years'))
					// );
					// if ($payment_data['status'] == 'Approved')
					// {
						// $this->Credit_card->save($credit_card_data, $credit_card_id);
						// $this->Credit_card->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id']);
					// }
					// $data = array(
						// 'receipt_title'=>'Auto Billing Receipt',
						// 'transaction_time'=>date('Y-m-d'),
						// 'customer'=>$course_info->name,
						// 'contact_email'=>$billing['contact_emails'],
						// 'sale_id'=>$invoice,
						// 'employee'=>$employee,
						// 'items'=>$credit_card_charges,
						// 'product_list'=>$product_list,
						// 'tax_rates'=>$tax_rates,
						// 'totals'=>$totals,
						// 'payment_data'=>$payment_data
					// );
					// $subject = ($payment_data['status'] == 'Approved'?'':'Declined ');
					// $data['status_message'] = ($payment_data['status'] == 'Approved'?'':$payment_data['status_message']);
					// $subject .=  $course_info->name.' Auto Bill';
					// $recipients = array('billing@foreup.com','jhopkins@foreup.com');
					// if ($billing['contact_email'] != '' && $payment_data['status'] == 'Approved')
						// foreach ($contact_emails as $contact_email)
							// $recipients[] = $contact_email;
					// send_sendgrid($recipients, $subject, $this->load->view("billing/receipt_email",$data, true), 'billing@foreup.com', 'billing@foreup.com');
					// // MARK BILLING AS SENT
					// $this->Billing->mark_as_sent($billing['billing_id']);
				// }
//
			// }
			// else
			// {
				// // WAIT UNTIL TOMORROW
				// $time = '5am tomorrow' - $right_now;
				// sleep($time);
			// }
		// }while(false);
	// }




// FROM HERE ON, THESE FUNCTIONS ARE FROM THE ORIGINAL CRON SCRIPT
    function index()
    {
        //echo 'adding item';
        log_message('error', 'We are successfully hitting the daemon controller');
		send_sendgrid(
			'signorehopkins@gmail.com',
			'Testing to see if our system works well',
			'I think it is working just fine, dont you?',
			'testing@foreup.com',//$this->session->userdata('course_email'),
			'ForeUP Test'
		);
		echo 'We are successfully hitting the daemon controller';
    }

	function auto_bill_credit_cards($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			$product_names = array('1'=>'Software','2'=>'Website','3'=>'Marketing');
			$this->load->model('Billing');
			$this->load->model('Credit_card');
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			//echo 'getting into it<br/>';
			$todays_billings = $this->Billing->get_todays_billings();
			$todays_billed = $this->Billing->get_todays_billed();
			echo '<br/>'.$this->db->last_query().'<br/><br/>';
			$todays_billings = $todays_billings->result_array();
			$billing_summary_data = array();

			print_r($todays_billings);

			//echo '<br/>'.$this->db->last_query().'<br/>';
			$total_billed = 0;
			$billed_count = 0;
			$total_failed = 0;
			$failed_count = 0;
			foreach ($todays_billings as $billing)
			{
				if (in_array($billing['billing_id'], $todays_billed))
				{
					echo "<br/>Duplicate billing {$billing['billing_id']}<br/>";
					continue;
				}
				//Start success
				$this->Billing->mark_as_started($billing['billing_id']);

				$tax_rates = array();//explode(',', $billing['tax_rates']);
				//$tax_names = explode(',', $billing['tax_names']);
				$contact_emails = array_unique(explode(',', $billing['contact_emails']));
				$products = explode(',', $billing['products']);

				$credit_card_charges = $product_list = array();
				$subtotal = 0;
				$product_list = $this->Billing->get_billing_products($billing['course_id'], $billing['credit_card_id']);
				echo '<br/><br/>'.$this->db->last_query();
				foreach ($product_list->result_array() as $product)
				{
					$payment_type = ($product['free'] ? 'Free' : ($product['monthly'] ? 'Monthly Payment' : ($product['annual'] ? 'Annual Payment' : '')));
					$payment_amount = ($product['free'] ? 0 : ($product['monthly'] ? $product['monthly_amount'] : ($product['annual'] ? $product['annual_amount'] : 0)));
					$subtotal += $payment_amount;
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>"ForeUP Services - {$product_names[$product['product']]} $payment_type", 'amount'=>$payment_amount);
					$tax_rates[$product['tax_name']] = array('tax_amount'=>$payment_amount * $product['tax_rate'] / 100, 'tax_rate' => $product['tax_rate']);
				}
				echo '<br/>Parsed product list<br/>';
/*
				if ($billing['total_annual_amount'] != '0.00')
				{
				}
				if ($billing['total_monthly_amount'] != '0.00')
				{
					$subtotal += $billing['total_monthly_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'ForeUP Services - Monthly Payment', 'amount'=>$billing['total_monthly_amount']);
				}
				foreach ($products as $product)
					$product_list[] = array('line_number'=>count($credit_card_charges)+1,'description'=>' - '.$product_names[$product], 'amount'=>'');
			*/
				/*if ($billing['annual'] && $billing['annual_month'] == date('n') && $billing['annual_day'] == date('j'))
				{
					$subtotal += $billing['annual_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'ForeUP '.$products[$billing['product']].' Services - Annual Payment', 'amount'=>$billing['annual_amount']);
				}
				if ($billing['monthly'] && $billing['period_start'] <= date('n') && $billing['period_end'] >= date('n') && $billing['monthly_day'] == date('j'))
				{
					$subtotal += $billing['monthly_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'ForeUP '.$products[$billing['product']].' Services - Monthly Payment', 'amount'=>$billing['monthly_amount']);
				}*/
				$taxes = $billing['annual_tax_amount'] + $billing['monthly_tax_amount'];
				$total = $subtotal + $taxes;
				$totals = array('subtotal'=>$subtotal,'taxes'=>$taxes,'total'=>$total);
				//print_r($billing);
				//echo '<br/>';
				$course_info = $this->Course->get_info($billing['course_id']);
				//$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
				$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
				$employee = 'Auto Billing';
				echo '<br/>Added Credit Card Payment<br/>';
				$HC->set_frequency('Recurring');
				$HC->set_token($billing['token']);
				$HC->set_cardholder_name($billing['cardholder_name']);
				$HC->set_invoice($invoice);
				$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//ForeUP Credentials
				//$HC->set_merchant_credentials(config_item('test_mercury_id'),config_item('test_mercury_password'));//Test Credentials
				echo '<br/>Set all credentials<br/>';
				//echo config_item('foreup_mercury_id').' here are the credentials - '.config_item('foreup_mercury_password');
				//echo 'and here === '.$this->config->item('mercury_id').' --- '.$this->mercury_id;
				//return;
				$transaction_results = $HC->token_transaction('Sale',$total, '0.00', $taxes);
				echo '<br/>Ran transaction<br/>';
				$payment_data = array(
					'acq_ref_data'=>(string)$transaction_results->AcqRefData,
					'auth_code'=>(string)$transaction_results->AuthCode,
					'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
					'avs_result'=>(string)$transaction_results->AVSResult,
					'batch_no'=>(string)$transaction_results->BatchNo,
					'card_type'=>(string)$transaction_results->CardType,
					'cvv_result'=>(string)$transaction_results->CVVResult,
					'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
					'masked_account'=>(string)$transaction_results->Account,
					'status_message'=>(string)$transaction_results->Message,
					'amount'=>(string)$transaction_results->PurchaseAmount,
					'ref_no'=>(string)$transaction_results->RefNo,
					'status'=>(string)$transaction_results->Status,
					'token'=>(string)$transaction_results->Token,
					'process_data'=>(string)$transaction_results->ProcessData
				);
				$this->Sale->update_credit_card_payment($invoice, $payment_data);

				$credit_card_id = $billing['credit_card_id'];
				$credit_card_data = array(
					'token'=>$payment_data['token'],
					'token_expiration'=>date('Y-m-d', strtotime('+2 years'))
				);
				//echo "<br/>Status {$payment_data['status']}<br/>";
				if ($payment_data['status'] == 'Approved')
				{
					echo "<br/>Approved<br/>";
					$this->Credit_card->save($credit_card_data, $credit_card_id);
					$this->Credit_card->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id']);
				}
				$data = array(
					'receipt_title'=>'Auto Billing Receipt',
					'transaction_time'=>date('Y-m-d'),
					'customer'=>$course_info->name,
					'contact_email'=>$billing['contact_emails'],
					'sale_id'=>$invoice,
					'employee'=>$employee,
					'items'=>$credit_card_charges,
					'product_list'=>$product_list,
					'tax_rates'=>$tax_rates,
					'totals'=>$totals,
					'payment_data'=>$payment_data
				);
				$subject = ($payment_data['status'] == 'Approved'?'':'Declined ');
				$data['status_message'] = ($payment_data['status'] == 'Approved'?'':$payment_data['status_message']);
				$subject .=  $course_info->name.' Auto Bill';
				$recipients = array('jhopkins@foreup.com');
				if ($payment_data['status'] == 'Approved')
				{
					//Record success
					$this->Billing->mark_as_charged($billing['billing_id']);
					$total_billed += $total;
					$billed_count ++;
					if (count($contact_emails) > 0)
					{
						//Email success
						$this->Billing->mark_as_sent($billing['billing_id']);
						foreach ($contact_emails as $contact_email)
							$recipients[] = $contact_email;
					}
				}
				else {
					$total_failed += $total;
					$failed_count ++;
				}
				$billing_summary_data['summary_data'][] = $data;
				echo $subject;
				print_r($recipients);
				send_sendgrid($recipients, $subject, $this->load->view("billing/receipt_email",$data, true), 'billing@foreup.com', 'billing@foreup.com');
			}			
			$billing_summary_data['total_billed'] = $total_billed;
			$billing_summary_data['total_failed'] = $total_failed;
			$billing_summary_data['billed_count'] = $billed_count;
			$billing_summary_data['failed_count'] = $failed_count;
			
			send_sendgrid(array('billing@foreup.com','jhopkins@foreup.com'), 'Billing Summary', $this->load->view("billing/billing_summary",$billing_summary_data, true), 'billing@foreup.com', 'billing@foreup.com');

		//echo json_encode(array('success'=>true, 'billing_count' => count($todays_billings)));
		//echo json_encode(array('success'=>$payment_data['status']=='Approved'?true:false, 'message'=>"A charge of {$payment_data['amount']} was {$payment_data['status']} for card {$payment_data['masked_account']}", 'item_id'=>0));
		}
	}

	function auto_bill_customers($cron_key, $days = false)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			$count = 0;
			do
			{
				// GET ALL THE BILLINGS UNLESS THEY'VE ALREADY BEEN STARTED
				//echo '<br/>Starting auto_bill_customers<br/>';
				set_time_limit(0);
				ini_set('memory_limit', '100M');
				$this->load->model('Invoice');
				$this->load->model('Customer_billing');
				$this->load->model('Customer_credit_card');
				$this->load->helper('download');
				$this->load->helper('file');
				$this->load->library('zip');
				$this->load->library('Html2pdf');
				//print_r(get_filenames('uploads/invoices'));
				//$filename = 'zipped.zip';
				//$data = read_file('uploads/invoices/invoices_04-2013.zip');
				//force_download($filename, $data);
				//return;
				// GENERATE ALL NEEDED INVOICES FIRST
				$billing_courses = $this->Customer_billing->get_todays_generate_invoices_courses($days);
				print_r($billing_courses->result_array());
				echo '<br/><br/>';
				if ($billing_courses->num_rows() > 0)
				{
					foreach($billing_courses->result_array() as $billing_course)
					{
						$limit = 1000; // There is an attachment limit of 7MB with SendGrid, and around 200 Invoices + Combined Copy will be around 5MB, so we'll keep the limit to 200 to be safe
						$generate_invoices = $this->Customer_billing->get_todays_generate_invoices($billing_course['course_id'], $days, $limit);
						echo $this->db->last_query().'<br/>';
						print_r($generate_invoices->result_array());
						//return;
						// IF WE ACTUALLY HAVE RESULTS, WE"LL GET STARTED"
						if ($generate_invoices->num_rows() > 0)
						{
							$this->zip->clear_data();
							$combined_doc = null;
							$customer_array = array();
							$course_info = $this->Course->get_info($billing_course['course_id']);
							$combined_doc = new Html2pdf('P','A4','fr');
							$combined_doc->pdf->SetDisplayMode('fullpage');
							$combined_doc->setDefaultFont('Arial');

							$success = false;
							$this->db->trans_start();
							echo '<br/>Trans Start<br/>';
							foreach ($generate_invoices->result_array() as $billing)
							{
								$customer = $this->Customer->get_info($billing['person_id']);

								echo "<br/>Billing ID: {$billing['billing_id']} days before : {$billing['generate_days_before']}<br/>";
								$start_on = ($billing['frequency'] == 'monthly') ? date('Y-m-01', strtotime("+{$billing['generate_days_before']} days")) : date("Y-{$billing['month']}-01");
								$end_on = ($billing['frequency'] == 'monthly') ? date('Y-m-31', strtotime("+{$billing['generate_days_before']} days")) : date("Y-{$billing['month']}-31");
								$month = date('m');
								$existing_invoice = $this->Invoice->get_recurring_billing_invoice($billing, $start_on, $end_on);
								echo $this->db->last_query();
								//$existing_invoice = false;
								//$customer = $this->Customer->get_info($billing['person_id']);
								//if no invoice continue as is
								// MARK INVOICE GENERATION AS STARTED
								$this->Customer_billing->mark_invoice_generation_as_started($billing['billing_id']);
								echo "<br/>Marked as started<br/>";

								if (!$existing_invoice) {
									// SAVE INVOICE
									$customer_id = $customer->person_id;
									$invoice_data = array(
										'course_id'=>$billing['course_id'],
										'credit_card_id'=>$billing['credit_card_id'],
										'billing_id'=>$billing['billing_id'],
										'person_id'=>$billing['person_id'],
										'month_billed'=>date('Y-m-d', strtotime("+{$billing['generate_days_before']} days")),
										'day'=>$billing['day'],
										'email_invoice'=>$billing['email_invoice'],
										'employee'=>$billing['employee_id'],
										'include_itemized_sales'=>1//$billing['include_itemized_sales'] ? 1 : 0
									);
									$this->Invoice->save($invoice_data);
									$invoice_id = $invoice_data['invoice_id'];
									$invoice_number = $invoice_data['invoice_number'];

									// SAVE INVOICE ITEMS
									$item_data = array();
									$calculated_total = 0;
									$items = $this->Customer_billing->get_items($billing['billing_id']);

									// Just saving to invoices
									foreach ($items as $index => $item)
									{
										$item_data[] = array(
											'invoice_id'=>$invoice_data['invoice_id'],
											'line_number'=>$index,
											'description'=>$item['description'],
											'quantity'=>$item['quantity'],
											'amount'=>$item['amount'],
											'tax'=>$item['tax'],
											'pay_account_balance'=>0,
											'pay_member_balance'=>0
										);
										$calculated_total += (int)$item['quantity'] * (float)$item['amount'] * (1 + (float)$item['tax']/100);
									}
									// ADD CUSTOMER ACCOUNT BALANCE
									$month_account_balance = $this->Account_transactions->get_month_total($customer_id, $invoice_data['month_billed']);
									if ($billing['pay_account_balance'] && $month_account_balance < 0)
									{
										// GET TOTAL CHARGES FOR LAST MONTH
										$account_balance = -$month_account_balance;
										// $month_total = $this->Account_transactions->get_month_total($customer_id, $invoice_data['month_billed']);
										// $account_balance = $account_balance > $month_total ? $account_balance : $month_total;
										$index++;
										$item_data[] = array(
											'invoice_id'=>$invoice_data['invoice_id'],
											'line_number'=>$index,
											'description'=>'Customer Credit Balance',
											'quantity'=>1,
											'amount'=>$account_balance,
											'tax'=>0,
											'pay_account_balance'=>1,
											'pay_member_balance'=>0
										);
										$calculated_total += -(float)$month_account_balance;
									}
									// ADD MEMBER ACCOUNT BALANCE
									$month_member_account_balance = $this->Member_account_transactions->get_month_total($customer_id, $invoice_data['month_billed']);
									if ($billing['pay_member_balance'] && $month_member_account_balance < 0)
									{
										// GET TOTAL MEMBER CHARGES FOR LAST MONTH
										$member_account_balance = -$month_member_account_balance;
										// $month_total = $this->Member_account_transactions->get_month_total($customer_id, $invoice_data['month_billed']);
										// $account_balance = $account_balance > $month_total ? $account_balance : $month_total;
										$index++;
										$item_data[] = array(
											'invoice_id'=>$invoice_data['invoice_id'],
											'line_number'=>$index,
											'description'=>'Member Account Balance',
											'quantity'=>1,
											'amount'=>$member_account_balance,
											'tax'=>0,
											'pay_account_balance'=>0,
											'pay_member_balance'=>1
										);
										$calculated_total += -(float)$month_member_account_balance;
									}
									$this->Invoice->save_items($item_data);
									// Charge credit card and update the invoice
									$calculated_total = floor($calculated_total*100)/100;

									// NO CHARGING IS TO HAPPEN HERE.... WE ARE JUST GENERATING INVOICES HERE
									//$charged = $this->Customer_credit_card->charge($billing, $calculated_total);

									$invoice_data = array(
										'total'=>$calculated_total,
										'paid'=>'0.00'//($charged)?$calculated_total:'0.00',
										//'credit_card_payment_id'=>$billing['credit_card_payment_id']
									);
									$this->Invoice->save($invoice_data, $invoice_id);
								}//END IF (EXISTING_INVOICE)
								else {
									echo "<br/>Existing invoice<br/>";
									$item_data = $this->Invoice->get_items($existing_invoice['invoice_id']);
								}

								$invoice_data = $this->Invoice->get_info($existing_invoice? $existing_invoice['invoice_id'] : $invoice_id);
								
								$past_due_items = $this->Invoice->get_overdue_items($invoice_data[0]['invoice_id'], $customer->person_id, $invoice_data[0]['month_billed']);
								$data = $invoice_data[0];
								$data['invoice_number'] = $existing_invoice ? $existing_invoice['invoice_number'] : $invoice_number;
								$data['total_due'] = $existing_invoice ? $existing_invoice['total_due'] : $calculated_total;
								$data['total_paid'] = $existing_invoice ? $existing_invoice['paid'] : 0;
								$data['invoice_id'] = $existing_invoice ? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'];
								$data['items']=$item_data;
								$data['past_due_items']=$past_due_items;
								$data['person_info']=$customer;
								$data['course_info']=$course_info;
								$data['credit_cards']='';
								$data['date'] = isset($data['date']) ? $data['date'] : $invoice_data['month_billed'];
								$data['popup'] = 1;// = array(
								$data['is_invoice'] = true;
								$data['sent'] = true;
								$data['credit_cards']= $this->Customer_credit_card->get($data['person_id']);
								$data['include_itemized_sales'] = 1;//$existing_invoice ? ($existing_invoice['include_itemized_sales'] ? $existing_invoice['include_itemized_sales'] : 1) : $invoice_data['include_itemized_sales'];
								$data['pdf'] = true;
								// echo "<div>";
								// print_r($data);
								// echo "</div>";
								echo '<br/>Generate PDF<br/>';
								$invoice_html = $this->load->view('customers/invoice', $data, true);
								$html2pdf = new Html2pdf('P','A4','fr');
								//$html2pdf->setModeDebug();
								$html2pdf->pdf->SetDisplayMode('fullpage');
								$html2pdf->setDefaultFont('Arial');
								$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
								$combined_doc->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
								$cc = $customer_array[$customer->person_id] = (isset($customer_array[$customer->person_id]) ? $customer_array[$customer->person_id] + 1 : 0);
								$this->zip->add_data(str_replace(array('/',' '), '_', $customer->last_name).'_'.str_replace(array('/',' '), '_', $customer->first_name).($cc > 0 ? '_'.$cc : '')."_".date('Y-m-d', strtotime("+{$billing['generate_days_before']} days")).".pdf", $html2pdf->Output('', true));


								// Email copy of invoice
								if ($billing['email_invoice'] && $billing['credit_card_id'] == 0)
								{
									$data['emailing_invoice'] = true;
									$this->Invoice->send_email($invoice_id);
									// MARK BILLING AS CHARGED
									//$this->Customer_billing->mark_as_emailed($billing['billing_id']);
								}
								//unset($customers_by_id[$customer->person_id]);
							}
							echo '<br/>ENDING BATCH<br/>';

							$this->db->trans_complete();
							$success = $this->db->trans_status();

							if ($success) // ONLY IF ALL THE TRANSACTIONS WERE SUCCESSFUL, WE'LL SAVE THE ZIP FILE
							{
								echo '<br/>SAVE INVOICE ZIP<br/>';

								$this->zip->add_data(date('Y-m-d')."_Combined_Invoices".".pdf", $combined_doc->Output('', true));
								$folder_name = "archives/invoices/{$billing['course_id']}";
								if (!is_dir('archives'))
									mkdir('archives', 0777);
								if (!is_dir('archives/invoices'))
									mkdir('archives/invoices', 0777);
								if (!is_dir($folder_name))
									mkdir($folder_name, 0777);
								$path = "{$folder_name}/invoices_".date('Y-m-d').".zip";
								// SAVE ZIPPED PDF INVOICES TO THE GOLF COURSE
								$this->zip->archive($path);
								// EMAIL CLIENT WITH LINK TO INVOICE LINK
								$invoices_html = "<p>$course_info->name</p><p>Invoice Generation has just completed.</p><p>You can download your invoices at <a href='http://mobile.foreupsoftware.com/$path'>http://mobile.foreupsoftware.com/$path</a>";
								$emails = array('jhopkins@foreup.com', $course_info->email, $course_info->billing_email);
								//$billing_emails = explode(',', $course_info->billing_email);
								//foreach($billing_emails as $billing_email)
								send_sendgrid($emails, "Invoices Generated - ".$course_info->name, $invoices_html, 'no-reply@foreup.com', 'no-reply@foreup.com');
							}
						}
					}

				}


				// RUN ALL BILLINGS
				//$todays_billings = $this->Customer_billing->get_todays_billings($days);
				$todays_billings = $this->Invoice->get_todays_invoices_to_bill($days);
				//echo $this->db->last_query();
				//print_r($todays_billings->result_array());
				//return;
				$current_course = '';
				$course = new stdClass();
				$send_course_summary = false;
				$billing_list = array();
				$foreup_billing_list = array();
				echo '<br/><br/>Billings '.$this->db->last_query();
	//print_r($todays_billings->result_array());
	//return;
				// IF WE ACTUALLY HAVE RESULTS
				if ($todays_billings->num_rows() > 0)
				{
					foreach ($todays_billings->result_array() as $billing)
					{
						// MARK BILLING AS STARTED
						$this->Invoice->mark_as_started($billing['invoice_id']);
						// SAVE INVOICE
						$customer = $this->Customer->get_info($billing['person_id']);
						$customer_id = $customer->person_id;

						// SAVE INVOICE ITEMS
						$item_data = array();
						$calculated_total = 0;
						$invoice_id = $billing['invoice_id'];
						$invoice_number = $billing['invoice_number'];
						$items = $this->Invoice->get_items($invoice_id);

						$calculated_total = $billing['total'] - $billing['paid'];
						echo "<br/><br/>Total: ".$billing['total']." Paid: ".$billing['paid']."<br/><br/>";
						echo "<br/><br/>Calculated total: ".$calculated_total."<br/><br/>";
						$charged = $this->Customer_credit_card->charge($billing, $calculated_total);

						if ($charged)
						{
							// MARK INVOICE AS CHARGED
							$this->Invoice->mark_as_charged($billing['invoice_id']);

							$invoice_data = array(
								'paid'=>$calculated_total,
								'credit_card_payment_id'=>$billing['credit_card_payment_id']
							);
							$this->Invoice->save($invoice_data, $invoice_id);
							// MARK INVOICE ITEMS AS PAID IN FULL
							$this->db->query("
								UPDATE foreup_invoice_items
								SET paid_amount = (amount * (1 + tax/100))
								WHERE invoice_id = $invoice_id
								LIMIT 20
							");
							// SET INVOICE ITEMS FOR SALE
							$item_data = $this->Invoice->get_items($invoice_id);
							foreach ($item_data as $key => $invoice_item) {
								$item_data[$key]['payment_amount'] = $invoice_item['amount'];// - $invoice_item['paid_amount'];
							}
							// INITIATE A FULL SALE
							$sales_items = array(
								array(
									'invoice_id'=>$invoice_id,
									'invoice_number'=>$invoice_number,
									'line'=>1,
									'name'=>'INV '+$invoice_number,
									'is_serialized'=>TRUE,
									'quantity'=>1,
						            'discount'=>0,
									'price'=>number_format($calculated_total,2),
									'paid'=>$paid,
									'max_discount'=>0,
									'is_invoice'=>TRUE,
									'invoice_items'=>$item_data
								)
							);
							$sales_payments = array(
								$billing['payment_type']=>
									array(
										'payment_type'=>$billing['payment_type'].' '.str_replace('x', '', $billing['masked_account']),
										'payment_amount'=>$calculated_total,
										'invoice_id'=>$billing['credit_card_payment_id'],
										'customer_id'=>$customer_id
									)
							);
							$sale_id = $this->Sale->save($sales_items, $customer_id,$billing['employee_id'],'Auto Billed',$sales_payments,false,'',$billing['course_id']);
							echo '<br/><br/>sale_id '.$sale_id;
							// SAVE SALES INVOICE
							$sales_invoices_data = array(
								'sale_id'=>$sale_id,
								'invoice_id'=>$item['invoice_id'],
								'line' => $item['line'],
								'quantity_purchased'=>1,
								'invoice_cost_price'=>0,
								'discount_percent'=>0,
								'invoice_unit_price'=>$item['price']
							);

							// Email copy of invoice
							if ($billing['email_invoice'])
							{
								if ($this->Invoice->send_email($invoice_id))
									$this->Invoice->mark_as_emailed($billing['invoice_id']); // MARK BILLING AS CHARGED
							}
						}
						// SEND SUMMARY TO GOLF COURSE
						if ($current_course != $billing['course_id'])
						{
							if ($course->email)
								$this->Customer_billing->send_summary($course->email, $course->name, $billing_list, array(lang('billings_customer'), lang('billings_billing_title'), lang('billings_amount_charged'), lang('billings_charge_successful'), lang('billings_email_sent')));
							// RESET BILLING LIST
							$billing_list = array();
							$send_course_summary = false;
							// UPDATE TO CURRENT COURSE INFO
							$course = $this->Course->get_info($billing['course_id']);
						}
						// ADD NEW INFORMATION INTO BILLING LISTS
						$current_course = $billing['course_id'];
						$billing_list[] = array($customer->last_name.', '.$customer->first_name, 'INV '.$billing['invoice_number'],to_currency($calculated_total),($charged?'Yes':'No'),($billing['email_invoice']?'Yes':'Not Active'));
						$foreup_billing_list[] = array($course->name, $customer->last_name.', '.$customer->first_name, 'INV '.$billing['invoice_number'], to_currency($calculated_total),($charged?'Yes':'No'),($billing['email_invoice']?'Yes':'Not Active'));
					}
					// SEND FULL SUMMARY TO FOREUP AND SEND LAST SUMMARY TO GOLF COURSE
					if ($course->email)
						$this->Customer_billing->send_summary($course->email, $course->name, $billing_list, array(lang('billings_customer'), lang('billings_billing_title'), lang('billings_amount_charged'), lang('billings_charge_successful'), lang('billings_email_sent')));
					$this->Customer_billing->send_summary('jhopkins@foreup.com', 'ForeUP Golf Course', $foreup_billing_list, array(lang('billings_golf_course'), lang('billings_customer'), lang('billings_billing_title'), 'Amount', 'Charged', 'Email'));
				}
				else {
					sleep(10);
					echo '<br/>Waiting<br/>';
				}
				$count++;
			} while($count < 1);
		}
	}

	function send_scheduled_marketing($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			$info = $this->Marketing_campaign->get_all_unsent_campaigns();
		   	$mrl  = $this->marketing_recipients_lib;
			//return;
		    foreach($info as $i)
		    {
		      $course  = $this->Course->get_info($i->course_id);
			  $mrl->reset();
		      $rec = $i->recipients;
		      $rec = unserialize($rec);
			  if ($rec['groups'])
			      $mrl->set_groups($rec['groups']);
			  if ($rec['individuals'])
			      $mrl->set_individuals($rec['individuals']);
		      $type = $i->type;
		      $recpnts = $mrl->get_recipients($type);
			  log_message('error', 'daemon recpnts count '.count($recpnts));
		      if(strtolower($type) == 'email')
		      {
		      	$contents = $this->get_contents($i->campaign_id);
				if ($contents != '')
			        $this->Marketing_campaign->send_mails($i->campaign_id, $recpnts, $contents, $course, $i->subject);
			  }
		      else
		      {
		        /**
		         * not yet tested, but already setup.
		         */
		        // $this->Marketing_campaign->send_text($i->campaign_id, $recpnts, $i->content);
		        $this->Marketing_campaign->send_sendhub_text($i->campaign_id, $recpnts, $i->content);
		      }
		    }
		}
	}

	// this function will generate the html mark up based on the template selected
	private function get_contents($campaign_id)
	{
		$info = $this->Marketing_campaign->get_info($campaign_id, true);

		$tpl = $info->template;
		// get the course info
		$course  = $this->Course->get_info($info->course_id);

		$address = sprintf(
			'%s, %s %s, %s',
			$course->address,
			$course->city,
			$course->state,
			$course->zip
		);

		$name = $course->name;

		$data['logo']   = '';
		$data['title']  = ($info->title);
		$data['name']   = $name;
		$data['address']    = $address;
		//    $data['open_time']  = $open_time;
		$data['phone_number'] = $course->phone;
		$data['support_email'] =  $course->email;//$this->config->item('email');
		$data['website'] =  $course->website;//$this->config->item('website');
		$tpl = strtolower(trim($tpl));
		$base_url = 'http://rc.foreupsoftware.com/';
		$data['tpl']= $base_url."application/views/email_templates/$tpl";
		$data['logo'] = $base_url.urlencode($info->logo_path);
		$data['logo_text'] = '';
		$data['header'] = ''; // not needed yet.
		$data['campaign_id'] = $campaign_id;
		$data['include_ads'] = $course->marketing_include_ads;
		$data['base_url'] = $base_url;
		$data['images'] = $info->images;
		//    $data['header'] = $info->header;

		// Loop through replacement images and append the correct base url
		// to each image path
		if(!empty($data['images']) && is_array($data['images'])){
			foreach($data['images'] as $image_url => $image_data){
				$image_data['url'] = $base_url.$image_data['url'];
				$image_data['thumb_url'] = $base_url.$image_data['thumb_url'];
				$data['images'][$base_url.$image_url] = $image_data;

				unset($data[$image_url]);
			}
		}

		$data_opts = array(
			'campaign_id' => $campaign_id,
			'customer_id' => '__customer_id__',
			'course_id'  => $info->course_id
		);

		$ops = http_build_query($data_opts);
		$ops = str_replace('amp;', '', $ops);
		$link = $base_url.'index.php/subscriptions/unsubscribe?'.$ops;

		$data['contents'] = $info->content;
	    $data['contents_2'] = $info->content_2;
	    $data['contents_3'] = $info->content_3;
	    $data['contents_4'] = $info->content_4;
	    $data['contents_5'] = $info->content_5;
	    $data['contents_6'] = $info->content_6;
		$tpl =
		$contents = $this->load->view("email_templates/{$tpl}/mailer.html", $data, true);

		$contents .= "<div><a href='{$link}' target='_blank'>Unsubscribe</a></div>";

		return $tpl != '' ? trim($contents) : '';
	}

	function send_auto_mailers($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			$this->load->model('Auto_mailer');
			$this->load->model('Recipient');
			$this->load->model('Auto_mailer_campaign');
			$this->load->model('Marketing_campaign');

			$auto_mailers = $this->Auto_mailer->get_all(10000, 0, true)->result_array();
			foreach ($auto_mailers as $auto_mailer)
			{
				$campaigns = $this->Auto_mailer_campaign->get_all($auto_mailer['auto_mailer_id'])->result_array();;
				$course  = $this->Course->get_info($auto_mailer['course_id']);
				foreach($campaigns as $campaign)
				{
					//print_r($campaign);
					$recipients = array();
					$date = date('Y-m-d 00:00:00', strtotime('-'.$campaign['days_from_trigger'].' days'));
					$rcpts = $this->Recipient->get_all($auto_mailer['auto_mailer_id'], $date);
					$index  = $campaign['type'] == 'email' ? 'email' : 'phone_number';
					//echo $this->db->last_query().'<br/><br/>';
					foreach ($rcpts->result_array() as $recipient)
					{
						//if(empty($recipient[$index]) || ($recipient["opt_out_{$index}"] == 'Y')) continue;
				        // making the email as a key will make sure that no email will be sent twice
				        $recipients[strtolower($recipient[$index])] = array(
				            "{$index}" => $recipient[$index],
				            'customer_id' => $recipient['person_id']
				        );
					}

					// get the template ready
					$contents = $this->get_contents($campaign['campaign_id']);
		        	//$this->Marketing_campaign->send_mails($campaign['campaign_id'], $recipients, $contents, $course);

					//echo '$auto_mailer '.$auto_mailer['name'].'<br/><br/>';
					//echo '$campaign_id '.$campaign['campaign_id'].'<br/>';
					//print_r($rcpts);
					//print_r($recipients);
					//print_r($contents);
					//echo '<br/><br/>';
					if ($contents != '')
					{
						if($campaign['type'] == 'email')
					    {
					      	$this->Marketing_campaign->send_mails($campaign['campaign_id'], $recipients, $contents, $course, $campaign['subject']);
						}
					    else
					    {
					        $this->Marketing_campaign->send_text($campaign['campaign_id'], $recipients, $contents);
					    }
					}
				}


			}

		}
	}

	function send_tee_time_thank_yous($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			// Returns list of teesheets with thank you email option checked (includes golf course data)
			date_default_timezone_set('America/Chicago');
			$tee_sheets = $this->Teesheet->get_thank_you_tee_sheets()->result_array();
			echo 'Tee Sheets';
			print_r($tee_sheets);
			//echo '<br/>'.$this->db->last_query().'<br/>';
			if (count($tee_sheets) > 0)
			{
				foreach ($tee_sheets as $tee_sheet)
				{
					//Mark tee sheet as run
					// I need to set everything with the tee sheet using the same timezone... keeps them all unified.
					// remember we are getting all the tee sheets at the same time, but each one needs to fetch tee times in their own timezone
					date_default_timezone_set('America/Chicago');
					$this->Teesheet->mark_next_thank_you_time($tee_sheet['teesheet_id']);

					//Set TimeZone
					date_default_timezone_set($tee_sheet['timezone']);

					//Get tee time individuals to thank
					//And mark individuals as emailed
					$course = $this->Course->get_info($tee_sheet['course_id']);
					$customers_to_thank = $this->Teetime->get_thank_you_list($tee_sheet['teesheet_id']);
					print_r($customers_to_thank);
					//echo $this->db->last_query();
					//print_r($customers_to_thank);
					//Email individuals
					//Save in communications
					$contents = $this->get_contents($tee_sheet['thank_you_campaign_id']);
					if ($contents != '')
					    $this->Marketing_campaign->send_mails($tee_sheet['thank_you_campaign_id'], $customers_to_thank, $contents, $course, 'Thanks for Golfing!');
				}
			}
			else
			{

					//Calculate time until next hour starts
					//Should run exactly on the hour
					$next_hour = strtotime(date('Y-m-d H:00:00', strtotime('+1 hour')));
					$now = time();
					$time_til_next_hour = $next_hour - $now;
					echo 'ttnh '.$time_til_next_hour;
					sleep($time_til_next_hour + 120);
			}
		}
	}

	/* Scans database for changes, queues up those changes to be sent
	 * off to QuickBooks web connector for each course that has
	 * QuickBooks connected.
	 */
	function quickbooks_sync($cron_key)
	{
		if ($cron_key != $this->cron_key){
			return;

		}else{
			echo 'STARTING quickbooks_sync'.PHP_EOL;
			$now = time();
			$nine_o_clock/*PM*/ = strtotime(date('Y-m-d 21:00:00'));
			$three_o_clock/*AM*/ = strtotime(date('Y-m-d 04:00:00'));
			echo 'date '.date('Y-m-d H:i:s').' - ';
			echo 'now '.$now.' - ';
			echo '9pm '.$nine_o_clock.' - ';
			echo '3am '.$three_o_clock.' - '.PHP_EOL;
			if ($now > $nine_o_clock || $now < $three_o_clock)
			{
				echo 'Running script (within times)'.PHP_EOL;
				// Load all courses who have QuickBooks enabled
				$this->load->model('quickbooks');
				echo 'Getting users...'.PHP_EOL;
				$users = $this->quickbooks->get_users();
				echo 'Fetched users...'.PHP_EOL;

				// Loop through each course
				foreach($users as $user){
					$courseId = $user['qb_username'];
					if(empty($courseId)){
						continue;
					}
					$this->benchmark->mark('qb_sync_start');
					echo 'Queuing up course '.$courseId.PHP_EOL.'========================='.PHP_EOL;

					// Sync any new items
					$num_items = $this->quickbooks->queue_items($courseId, 50);
					echo 'Items: '.$num_items.PHP_EOL;

					$num_item_kits = $this->quickbooks->queue_item_kits($courseId, 50);
					echo 'Item Kits: '.$num_item_kits.PHP_EOL;

					$num_customers = $this->quickbooks->queue_customers($courseId, 50);
					echo 'Customers: '.$num_customers.PHP_EOL;

					$num_sales = $this->quickbooks->queue_sales($courseId, null, null, 25);
					echo 'Sales: '.$num_sales.PHP_EOL;

					$num_tips = $this->quickbooks->queue_tips($courseId, 25);
					echo 'Tips: '.$num_tips.PHP_EOL;

					$num_returns = $this->quickbooks->queue_returns($courseId, null, null, 25);
					echo 'Returns: '.$num_returns.PHP_EOL;
					$this->benchmark->mark('qb_sync_end');
					echo 'TOTAL TIME: '.$this->benchmark->elapsed_time('qb_sync_start','qb_sync_end').' seconds'.PHP_EOL.PHP_EOL;
				}
				sleep(60 * 15); // 15 minutes
			}
			else
			{
				$seconds_til_nine = $nine_o_clock - $now;
				sleep($seconds_til_nine); // Restart at 9pm
			}
		}
	}
}
?>

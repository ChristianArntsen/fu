<?php
	function complete_sale($guid = NULL, $sale_date = false, $json_response = false)
	{
		$CI =& get_instance();
	    $CI->load->library('sale_lib');
		$CI->load->model('schedule');
		$CI->load->model('teesheet');
		$CI->load->model('green_fee');
		$CI->load->model('fee');
		$CI->load->model('Image');
		$CI->load->model('Customer_loyalty');

		$message = false;
		$mode = $CI->sale_lib->get_mode();
		$no_receipt = $CI->input->post('no_receipt');
		$data['cart']=$CI->sale_lib->get_basket();
		$data['subtotal']=$CI->sale_lib->get_basket_subtotal();
		$data['taxes']=$CI->sale_lib->get_basket_taxes();
		$data['total']=$CI->sale_lib->get_basket_total();
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format());
		$data['has_online_booking'] = $CI->permissions->course_has_module('reservations') ? $CI->schedule->has_online_booking($CI->session->userdata('course_id')) : $CI->teesheet->has_online_booking($CI->session->userdata('course_id'));
		$data['website'] = $CI->config->item('website');
		$customer_id=$CI->sale_lib->get_customer();
		$teetime_id=$CI->sale_lib->get_teetime();
		$employee_id=$CI->Employee->get_logged_in_employee_info()->person_id;
		$comment = $CI->sale_lib->get_comment();
		$emp_info=$CI->Employee->get_info($employee_id);
		$amount_change = $CI->sale_lib->get_basket_amount_due();
		$data['amount_change']=to_currency($amount_change * -1);
		if ($amount_change != 0)
			$CI->sale_lib->add_payment( 'Change issued', $amount_change);
		$data['payments']=$CI->sale_lib->get_payments(0);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		if($customer_id!=-1)
		{
			$cust_info=$CI->Customer->get_info($customer_id);
			$data['customer']=$cust_info->last_name.', '.$cust_info->first_name;
		}
		// $tournament_without_customers = $CI->sale_lib->tournament_without_customer();
		$missing_or_repeat = $CI->sale_lib->missing_or_repeat_giftcard_number();
		$missing_or_repeat_punch_card = $CI->sale_lib->missing_or_repeat_punch_card_number();
		$location = ($CI->config->item('after_sale_load') == 1)?'sales':($CI->permissions->course_has_module('reservations') ? 'reservations' : 'teesheets');
		if ($missing_or_repeat)
		{
			//$data['error_message'] = lang('sales_missing_or_repeat_giftcard_number');
			$message = array('error'=>true,'text'=>lang('sales_missing_or_repeat_giftcard_number'),'type'=>'error_message','persist'=>false);
			echo json_encode($message);
			return;
			//$data['giftcard_error_line'] = $missing_or_repeat;
		}
		else if ($missing_or_repeat_punch_card)
		{
			//$data['error_message'] = lang('sales_missing_or_repeat_giftcard_number');
			$message = array('error'=>true,'text'=>lang('sales_missing_or_repeat_punch_card_number'),'type'=>'error_message','persist'=>false);
			echo json_encode($message);
			return;
			//$data['giftcard_error_line'] = $missing_or_repeat_punch_card;
		}
		// else if ($tournament_without_customers)
		// {
			// // $data['error_message'] = lang('sales_missing_or_repeat_giftcard_number');
			// $data['error_message'] = 'missing customer';
//
			// //$data['giftcard_error_line'] = $missing_or_repeat;
		// }
		else
		{
			//Record purchased teetimes
			if($teetime_id!=-1)
				$teetime_id = $CI->permissions->course_has_module('reservations') ? $CI->schedule->record_teetime_purchases($data['cart']) : $CI->teesheet->record_teetime_purchases($data['cart']);
			//echo $CI->db->last_query();
			//Record raincheck used
			if ($CI->session->userdata('raincheck_id'))
				if ($CI->Sale->raincheck_redeemed())
					$CI->session->unset_userdata('raincheck_id');
			//SAVE sale to database
			$sale_id_number = $CI->Sale->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'],false,$teetime_id, false, $guid, $sale_date);
			$CI->sale_lib->set_taxable('true');
			$data['sale_id']='POS '.$sale_id_number;
			if ($data['sale_id'] == 'POS -1')
			{
				//$data['error_message'] = lang('sales_transaction_failed');
				$message = array('text'=>lang('sales_transaction_failed'),'type'=>'error_message','persist'=>false);
			}
			else
			{
				if ($CI->sale_lib->get_email_receipt() && !empty($cust_info->email))
				{
					send_sendgrid($cust_info->email, lang('sales_receipt'), $CI->load->view("sales/receipt_email",$data, true), $CI->config->item('email'), $CI->config->item('name'));
				}
			}

			//$CI->email_receipt($sale_id_number);
			//return;
			if (!$CI->config->item('print_after_sale'))
			{

				$sale_id = $CI->sale_lib->get_suspended_sale_id();
				$CI->Sale_suspended->delete($sale_id);
		        $CI->sale_lib->delete_customer_quickbuttons();

				if (!$no_receipt)
		        {   //redirect('sales/receipt/'.$sale_id_number);
		            echo json_encode(array('no_auto_receipt'=>true, 'location'=>'sales/receipt/'.$sale_id_number));
					return;
				}
		        $CI->sale_lib->clear_all_minus_cart();

				if (count($CI->sale_lib->get_cart())==0)
		        {
		            $CI->sale_lib->set_teetime(-1);
					$CI->session->unset_userdata('purchase_override');
		        	$CI->session->unset_userdata('purchase_override_time');
		        }

		        if ($no_receipt){
		            if (count($CI->sale_lib->get_cart())>0)
		            {
		                //redirect('sales', 'location');//    $CI->load->view("sales/register");
			            echo json_encode(array('no_auto_receipt'=>true, 'location'=>'sales'));
						return;
					}
					else
		            {
		                //redirect($location, 'location');
					    echo json_encode(array('no_auto_receipt'=>true, 'location'=>$location));
						return;
					}

		        }
				return;
			}

		}

		$sale_id = $CI->sale_lib->get_suspended_sale_id();
		$CI->Sale_suspended->delete($sale_id);
		$CI->sale_lib->clear_all_minus_cart();
        $CI->sale_lib->delete_customer_quickbuttons();
		if (count($CI->sale_lib->get_cart())==0)
		{
			$CI->sale_lib->set_teetime(-1);
			$CI->session->unset_userdata('purchase_override');
	        $CI->session->unset_userdata('purchase_override_time');
		}
		if ($mode == 'return')
			$CI->sale_lib->clear_all();

		if ($guid !== NULL || $json_response)
		{
			return array('sale_id' => $data['sale_id']);
		}
		else if (count($CI->sale_lib->get_cart())>0)
			$CI->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
        else {
            if ($location == 'sales')
	        	$CI->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
	        else {
	        	echo json_encode(array('location'=>$location, 'receipt_data'=>$data, 'sale_id'=>$data['sale_id']));//redirect("teesheets", 'location');
	        }
        }

        //}
	}

?>
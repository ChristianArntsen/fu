<div id='support-side'>
    <div class='content'>
      <h3>Contact Us</h3>
      <form>
        <h5>Ask a question</h5>
        <textarea rows="10" cols="25" name="content" id="email-content"></textarea>
        <input type="submit" value="Submit" id="support-email-submit"/>
      </form>
      <br/>
      <p>p: <?= $phone_number; ?></p>
      <p>e: <?= $email; ?></p>
      <p style='margin-top:10px;'>Support Hours:</p>
      <p style='text-indent:10px;'>8am - 5pm</p>
      <p style='margin-top:5px;'>Emergency Only Support Hours:</p>
      <p style='text-indent:10px;'>1am - 8am & 5pm - 12am</p>
    </div>
</div>
<div id="dialogue" class="hidden" style="height:100%;width:100%;">
  
</div>
<script type="text/javascript">

$(document).ready(function(){
  $('#support-email-submit').click(function(e){
    e.preventDefault();
    $("#dialogue").dialog({
          modal: true,
          height: 45,
          width: 75,
          zIndex: 999,
          resizable: false,
          title: 'Sending email. Please wait...'
  });
  
    $.ajax({
        url: "<?= site_url('support/send_email'); ?>",
        type: "POST",
        data: {
            content: $('#email-content').val()
        },
        beforeSend: function()
        {
          $("#dialogue").dialog("open");
        },
        success: function( res ) {
          $("#dialogue").dialog("close");
          alert(res);
          window.location.reload();
        },
        dataType:'text'
      });
  });
});
</script>
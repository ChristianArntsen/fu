<?= $superadmin ?>

<div class="wrapper">
  <?= $search_form; ?>
  <div id="breadcrumbs">
    <a href="<?= site_url('support'); ?>">Home</a> › <?= $topic_bc ?>
  </div>  
  <div id="support-main">    
      <div class="support-body">
        <div class="content articles">
          <div class="title">
            <h3><?= $topic_type; ?></h3>
          </div>
          <?php if(count($topics) > 0): ?>
          <ul>
            <?php foreach($topics as $topic): ?>
            <li class="article">
              <h4>
                <a href="<?= site_url("support/get_topic/{$topic['id']}?bread_crumb={$topic['bc']}"); ?>">
                  <span id="post-title-<?= $topic['id'] ?>"><?= $topic['title'] ?></span>
                </a>
              </h4>
              <div><?= $topic['content_el'] ?></div>
              <div class="meta"><?= $topic['date_modified']; ?></div>
              <p><?= $topic['admin_opts']; ?></p>
              <div id="post-content-<?= $topic['id'] ?>" class="hidden"><?= $topic['content'] ?></div>
              <span id="post-type-<?= $topic['id'] ?>" class="hidden"><?= $topic['type'] ?></span>
              <span id="post-topic-<?= $topic['id'] ?>" class="hidden"><?= $topic['topic_id'] ?></span>
              <span id="post-keywords-<?= $topic['id'] ?>" class="hidden"><?= $topic['keywords'] ?></span>
            </li>
            <?php endforeach; ?>
          </ul>
          <?php else: ?>
          <div><center><p><i>"Sorry, we couldn't locate any support for this search. Please rephrase and try again or <a href="#" id="answer_request">request an answer here</a>."  Try searching again or send us an email.</i></p></center></div>
          <?php endif; ?>          
        </div>
    </div>
  </div>
  <?= $side_bar ?>
</div>

<div id="dialog" title="Confirmation Required" class="hidden">
  Are you sure you want to delete the selected article?
</div>

<div id="dialog-form" title="Request an answer" class="hidden">
	<form>
	<fieldset>
		<label for="name">Topic</label><br/>
		<input type="text" name="name" id="question-topic" style="width:250px;"/><br/>
		<label for="question-details">Details</label><br/>
		<textarea rows="5" cols="30" name="question-details" id="question-details" style="width:250px;"></textarea>
	</fieldset>
	</form>
</div>

<script type="text/javascript">
$(document).ready(function()
{
  // reset all values
var reset = function(){
    $('#topic_module').attr('disabled', '');
    $('#articleSubmitButton').removeClass('hidden');
    $('#topic_type').val('');
    $('#topic_module').val('');
    $('#topic_title').val('');
    $('#topic_content').val('');
    $('#topic_id').val('');  
    $('#topic_keywords').val('');
}

reset();

  $('#topic_content').wysiwyg({
      initialContent: '',
      maxHeight:25,
      height:25,
      width: 239,
      controls: {
        html: {visible : true }
      },
      maxWidth:239,
      iFrameClass: 'wysiwyg-title-iframe'
    });
      
  $('.article-edit').click(function(){
      reset();
      var id = $(this)[0].attributes[0].value;
      var id_parts = id.split('-');

      var id = id_parts[0];

      var type = $('#post-type-'+id).html();
      var topic = $('#post-topic-'+id).html();
      var title = $('#post-title-'+id).html();
      var content = $('#post-content-'+id).html();
      var keywords = $('#post-keywords-'+id).html();
      
      $('#topic_type').val(type);     
      $('#topic_title').val(title);
      $('#topic_content').val(content);
      $('#topic_id').val(id);
      $('#topic_module').val(topic);
      $('#topic_keywords').val(keywords);
      $('#topic_content-wysiwyg-iframe').contents().find('body').html(content);
      $('#topic_title').trigger('blur');

  });

  $('#articleSubmitButton').click(function(e){
    e.preventDefault();

    var content = $('#topic_content').val();
    content = $.trim(content);
    var title = $('#topic_title').val();
    title = $.trim(title);
    if(content.length < 1 || title.length < 1)
    {
       alert('Error: Title or Content must not be empty.');
    }
    else
    {
      var ra = '{"list":[';
      for(j=0;j<5;j++)
      {
        var input_id = '#topic_related_articles-' + j;
        var mask_id = '#ra-mask-' + j;
        var id = $(mask_id).html();
        if(id.length > 0) ra = ra + obj_to_string({id:id,title:$(input_id).val()}) + ',';
      }

      ra = ra.substring(0, ra.length-1);
      ra = ra + ']}';
      
      $.ajax({
          url: "<?= site_url('support/save'); ?>",
          type: "POST",
          data: {
              id: $('#topic_id').val(),
              type: $('#topic_type').val(),
              title: title,
              content: content,
              topic_id: $('#topic_module').val(),
              keywords: $('#topic_keywords').val(),
              related_articles: ra
          },
          success: function( res ) {
            alert(res);
            window.location.reload();
          },
          dataType:'text'
        });
      }
  });

  $('#answer_request').click(function(e){
    e.preventDefault();
    
    $( "#dialog-form" ).dialog({
      modal: true,
			buttons: {
				"Submit": function() {
					$.ajax({
            url: "<?= site_url('support/request_answer/'); ?>",
            type: "POST",
            data: {
                topic: $('#question-topic').val(),
                details: $('#question-details').val()
            },
            success: function( res ) {
              alert(res);
            },
            dataType:'text'
          });
          $( this ).dialog( "close" );
				},
				"Cancel": function() {
					$( this ).dialog( "close" );
				}
			}
		});
    $( "#dialog-form" ).removeClass('hidden');
    $( "#dialog-form" ).dialog("open");
    $('#question-topic').val($('#search_term').html());
  });

  $('.article-delete').click(function(){
      var id = $(this)[0].attributes[0].value;
      var id_parts = id.split('-');
      
      $("#dialog").dialog({
      modal: true,
      buttons : {
        "Confirm" : function() {
          //Some ajax here
          $.ajax({
            url: "<?= site_url('support/delete_post/'); ?>",
            type: "POST",
            data: {
                id: id_parts[0]
            },
            success: function( res ) {
              alert(res);
              window.location.reload();
//              window.location.href=window.location.href
            },
            dataType:'text'
          });
          $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $("#dialog").dialog("open");

  });
});
</script>
<div id='software_menu_contents'>
	<div id='software_menu_header'>

	</div>
	<div id='allowed_modules'>
		<div id="home_module_list">
		<?php
			$allowed_modules = ($this->Module->get_allowed_modules($this->session->userdata('person_id')));
			foreach($allowed_modules->result() as $module)
			{
		        if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
		        {

			?>
			<div class="module_item">
				<a href="<?php echo site_url("$module->module_id");?>">
					<span class="module_icon <?php echo $module->module_id;?>_icon"></span>
					<div>
						<div><?php echo lang($module->name_lang_key); ?></div>
						<div class='module_desc'><?php echo lang($module->desc_lang_key);?></div>
					</div>
				</a>
			</div>
			<?php
			}
		        }
			?>
			<div class="module_item">
				<a href="<?php echo site_url("support");?>" target='_blank'>
					<span class="module_icon support_icon"></span>
					<div>
						<div>Support</div>
						<div class='module_desc'>Help section</div>
					</div>
				</a>
			</div>
			<div class='clear'></div>
		</div>
	</div>
</div>
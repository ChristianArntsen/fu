<ul id="error_message_box"></ul>
<?php
	echo form_open('timeclock/save/'.$employee_info->person_id.'/'.str_replace(' ', 'T', $entry[0]['shift_start']),array('id'=>'timeclock_entry_form'));
?>
<fieldset id="timeclock_entry_basic_info">
<legend><?php echo lang("timeclock_entry_basic_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('reports_start_time').':', 'shift_start',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'shift_start',
		'size'=>'24',
		'id'=>'shift_start',
		'value'=>$entry[0]['shift_start'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('reports_end_time').':', 'shift_end',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'shift_end',
		'size'=>'24',
		'id'=>'shift_end',
		'value'=>$entry[0]['shift_end'])
	);?>
	</div>
</div>

<?php
echo form_hidden('delete_entry', 0);
echo form_submit(array(
	'name'=>'delete',
	'id'=>'delete',
	'value'=>lang('common_delete'),
	'class'=>'submit_button float_right')
);
?>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<style>
	#details {
		width:470px;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#shift_start').datetimepicker({
		dateFormat:'yy-mm-dd',
		timeFormat:'HH:mm:ss'
	});
	$('#shift_end').datetimepicker({
		dateFormat:'yy-mm-dd',
		timeFormat:'HH:mm:ss'
	});
	$('#delete').click(function(e){
		e.preventDefault();
		$('#delete_entry').val(1);
		$('#timeclock_entry_form').submit();
	});
	var submitting = false;
    $('#timeclock_entry_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					$.colorbox.close();
	                submitting = false;
	                console.dir(response);
	                reports.generate(0);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
   		},
		messages:
		{
		}
	});
});
</script>
<script>
	$(document).ready(function(){

	})
</script>
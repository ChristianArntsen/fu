<?php $this->load->view("partial/header"); 
//echo 'TSID '.$this->session->userdata('teesheet_id');
//echo 'increment '.$this->session->userdata('increment');
?>
	
<script type='text/javascript'>
//Begin offline mode code /application cache
/*
	var cache = window.applicationCache;

        cache.addEventListener("cached", function () {
            console.log("All resources for this web app have now been downloaded. You can run this application while not connected to the internet");
        }, false);
        cache.addEventListener("checking", function () {
            console.log("Checking manifest");
        }, false);
        cache.addEventListener("downloading", function () {
            console.log("Starting download of cached files");
        }, false);
        cache.addEventListener("error", function (e) {
            console.log("There was an error in the manifest, downloading cached files or you're offline: " + e);
            //console.dir(e);
        }, false);
        cache.addEventListener("noupdate", function () {
            console.log("There was no update needed");
        }, false);
        cache.addEventListener("progress", function () {
            console.log("Downloading cached files");
        }, false);
        cache.addEventListener("updateready", function () {
            cache.swapCache();
            console.log("Updated cache is ready");
            // Even after swapping the cache the currently loaded page won't use it
            // until it is reloaded, so force a reload so it is current.
            window.location.reload(true);
            console.log("Window reloaded");
        }, false);*/
//End offline mode code
	var old = ''; // used for event source auto_updates
	/*
	var source = new EventSource('index.php/teesheets/teesheet_updates/<?php echo $this->session->userdata('course_id');?>');
	
	source.onmessage = function(e)
	{
		var nd = new Date();
		console.log('running '+nd.getTime());
		if(old!=e.data){
			console.log(e.data);
			var  response = eval("("+e.data+")");
	  		var events = $.merge(response.caldata, response.bcaldata);
               Calendar_actions.update_teesheet(events, true, 'auto_update');
			
			old = e.data;
		}
	};
	*/
	var calInfo = {};
    alldata = eval('(<?php echo $JSONData ?>)');
    calInfo.caldata = alldata.caldata;
    calInfo.bcaldata = alldata.bcaldata;
    calInfo.openhour = convertFormat('<?php echo $openhour?>');
    calInfo.closehour = convertFormat('<?php echo $closehour?>');
    calInfo.increment1 = '<?php echo $increment?>';
    calInfo.increment2 = '<?php echo $increment?>';
    calInfo.holes = '<?php echo $holes?>';
    calInfo.frontnine = '<?php echo $fntime?>';
    calInfo.purchase = '<?php echo $purchase?>';
        
    function convertFormat(hourString) {
        var h = parseInt(hourString.slice(0,2), 10);
        var m = parseInt(hourString.slice(2), 10);
        var mz = '';
        if (m < 10)
            mz = 0;
        //console.log(hourString+' '+hourString.slice(0,2)+' '+h+' : '+mz+m);
        return h+':'+mz+m
    }
    function add_new_teetimes() {
    	//console.log('starting add new teetimes');
    	setTimeout('Calendar_actions.add_new_teetimes()', 1);
		//console.log('just set cal ac');
    	//setTimeout('add_new_teetimes()', 15000);
    	//console.log('just reset self');
    }
	var teesheet_controls = {
		change_date:function(date){
			var dow = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			$('#teesheet_day').html(date.getDate());
			$('#teesheet_dow').html(dow[date.getDay()]);
			$('#teesheet_month').html(month[date.getMonth()]+', '+date.getFullYear());
		},
		change_teesheet:function(){
			
		}
	};
    $(document).ready(function(){
        
        $('.standby_entry').live("click", function(){
            
           
           $.ajax({
           type: "POST",
           url: "index.php/teesheets/get_standby",
           data: 'id=' + $(this).attr('id'),
           success: function(response){  
               
                 
                 console.log('response is: ' + response.urlData);
          	 $.colorbox({'href':'index.php/teesheets/view_standby' + response.urlData,'width':1000,'title':'Standby Golfers'});   		
           },
           
           dataType:'json'
         });
    		
          });
        $('.new_standby').click(function(){
            $.colorbox({'href':'index.php/teesheets/view_standby', 'width':1000, 'title':'Standby Golfers'}); 
        });
    	$('.new_note').click(function(){
            $.colorbox({'href':'index.php/teesheets/view_note', 'width':525, 'title':'Add Note'}); 
        });
    	$('#settings_button').click(function(){
            
                
    		$.colorbox({'href':'index.php/config/view/teesheet','width':500,'title':'Teesheet Settings'});
    	});
        
    	var wb = $('#weatherBox ul'); 
    	wb.click(function(){
    		if (wb.hasClass('expanded'))
    		{
    			wb.removeClass('expanded');
        		$('#wbtabs-1').hide();
			}
    		else
    		{
	    		wb.addClass('expanded');
        		$('#wbtabs-1').show();
    		}
    	});
    	var ft = $('#facebook_title');
    	ft.click(function(){
    		if (ft.hasClass('expanded'))
    		{
    			ft.removeClass('expanded');
        		$('#facebook_content').hide();
			}
    		else
    		{
	    		ft.addClass('expanded');
        		$('#facebook_content').show();
    		}
    	})
    	var tst = $('#teetime_search_title'); 
    	tst.click(function(e){
    		if (tst.hasClass('expanded'))
    		{
    			tst.removeClass('expanded');
        		$('#teetime_search_content').hide();
			}
    		else
    		{
	    		tst.addClass('expanded');
        		$('#teetime_search_content').show();
    		}
    	})
        var tstandby = $('#teetime_standby_title'); 
    	tstandby.click(function(e){
    		if (tstandby.hasClass('expanded'))
    		{
    			tstandby.removeClass('expanded');
        		$('#teetime_standby_content').hide();
			}
    		else
    		{
	    		tstandby.addClass('expanded');
        		$('#teetime_standby_content').show();
    		}
    	})
        var note_collapse = $('#notes_title'); 
    	note_collapse.click(function(e){
    		if (note_collapse.hasClass('expanded'))
    		{
    			note_collapse.removeClass('expanded');
        		$('#notes_content').hide();
			}
    		else
    		{
	    		note_collapse.addClass('expanded');
        		$('#notes_content').show();
    		}
    	})
        // LOAD INITIAL NOTES
        $.ajax({
           type: "POST",
           url: "index.php/teesheets/get_notes/5",
           data: '',
           success: function(response){
          	 	$('#note_list').html(response.note_html);
           },
           dataType:'json'
         });     
        
    	$('#month_view').click(function(){
			
                        calendar.fullCalendar( 'changeView', 'month', {
                                    
                                    droppable:true,
                                    editable:true,
                                    drop:function()
                                    {
                                        
                                        var originalEventObject = $(this).data('eventObject');
                
                                        // we need to copy it, so that multiple events don't have a reference to the same object
                                        var copiedEventObject = $.extend({}, originalEventObject);
                
                                        // assign it the date that was reported
                                        copiedEventObject.start = date;
                                        copiedEventObject.allDay = allDay;
                                        calendar.fullCalendar('renderEvent', eventObject, true);
                                    
                                        //alert("DROPPED!");
                                    }
                                    
                                });
                                calendar.droppable();
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
    	$('#week_view').click(function(){
			calendar.fullCalendar( 'changeView', 'agendaWeek' )
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
    	$('#day_view').click(function(){
			calendar.fullCalendar( 'changeView', 'agendaDay' )
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
		$('#previous_day').click(function(){
			calendar.fullCalendar('prev');
			calendarback.fullCalendar('prev');
		})
		$('#today').click(function(){
			calendar.fullCalendar('today');
			calendarback.fullCalendar('today');
		})
		$('#next_day').click(function(){
			calendar.fullCalendar('next');
			calendarback.fullCalendar('next');
		})
    	<?php if ($this->config->item('teesheet_updates_automatically')) { ?>
    		//Only if it's enabled in settings
    		if (!!window.EventSource) {
    			//var old = '';
				var source = new EventSource('index.php/teesheets/teesheet_updates/<?php echo $this->session->userdata('course_id');?>');
				
				source.onmessage = function(e)
				{
					var nd = new Date();
					//console.log('running '+nd.getTime());
					if(old!=e.data){
						//console.log(e.data);
						var  response = eval("("+e.data+")");
				  		var events = $.merge(response.caldata, response.bcaldata);
			               Calendar_actions.update_teesheet(events, true, 'auto_update');
						
						old = e.data;
					}
				};
    		}
    		else {
    			add_new_teetimes();
    		}
    	<?php } ?>
    	$.ajax({
           type: "POST",
           url: "index.php/teesheets/make_5_day_weather",
           data: '',
           success: function(response){
          	 	$('#wbtabs-1').html(response.weather);
   			 	$('#teesheet_weather_box').html(response.todays);
           },
           dataType:'json'
         }); 
    });
    function post_teetime_form_submit(response)
	{
		set_feedback(response.message,'warning_message',false, 5000);
	}
	
</script>
<style>
	#teesheets .fc-event-bg {
		background: #f8f8f8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f8f8f8', endColorstr='#bebebe'); /* for IE */
		background: -webkit-linear-gradient(top, #f8f8f8, #bebebe);
		background: -moz-linear-gradient(top,  #f8f8f8,  #bebebe); /* for firefox 3.6+ */
	}
	#teesheets .teetime {
		background: #f8f8f8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f8f8f8', endColorstr='#bebebe'); /* for IE */
		background: -webkit-linear-gradient(top, #f8f8f8, #bebebe);
		background: -moz-linear-gradient(top,  #f8f8f8,  #bebebe); /* for firefox 3.6+ */
	}
	#teesheets .tournament {
		background: #fff9e8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff9e8', endColorstr='#fccc58'); /* for IE */
		background: -webkit-linear-gradient(top, #fff9e8, #fccc58);
		background: -moz-linear-gradient(top,  #fff9e8,  #fccc58); /* for firefox 3.6+ */
	}
	#teesheets .closed {
		background: #fdf2f2; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdf2f2', endColorstr='#fac1c1'); /* for IE */
		background: -webkit-linear-gradient(top, #fdf2f2, #fac1c1);
		background: -moz-linear-gradient(top,  #fdf2f2,  #fac1c1); /* for firefox 3.6+ */
	}
	#teesheets .event {
		background: #FBEDFF; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FBEDFF', endColorstr='#F2BDFF'); /* for IE */
		background: -webkit-linear-gradient(top, #FBEDFF, #F2BDFF);
		background: -moz-linear-gradient(top,  #FBEDFF,  #F2BDFF); /* for firefox 3.6+ */
	} 
	#teesheets .league {
		background: #bddeff; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ecfaf1', endColorstr='#b8ffd6'); /* for IE */
		background: -webkit-linear-gradient(top, #ecfaf1, #b8ffd6);
		background: -moz-linear-gradient(top,  #ecfaf1,  #b8ffd6); /* for firefox 3.6+ */
	}
	#teesheets .teetime div.fc-event-inner {
		border: 1px solid #929292;
	}
	#teesheets .tournament div.fc-event-inner {
		border: 1px solid #B18106;
	}
	#teesheets .closed div.fc-event-inner {
		border: 1px solid #B10606;
	}
	#teesheets .event div.fc-event-inner {
		border: 1px solid #993399;
	}
	#teesheets .league div.fc-event-inner {
		border: 1px solid #396;
/*		border: 1px solid #1AB106;*/
	}
	#teesheets .paid_1_1 .fc-event-bg, #teesheets .paid_2_2 .fc-event-bg, #teesheets .paid_3_3 .fc-event-bg, #teesheets .paid_4_4 .fc-event-bg, #teesheets .paid_5_5 .fc-event-bg {
		background: #edf8ff; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#edf8ff', endColorstr='#bddeff'); /* for IE */
		background: -webkit-linear-gradient(top, #edf8ff, #bddeff);
		background: -moz-linear-gradient(top,  #edf8ff,  #bddeff); /* for firefox 3.6+ */
		/*background:-webkit-gradient(linear, left top, right top, color-stop(0%,#ecfaf1), color-stop(100%,#b8ffd6))*/
	}
	#teesheets .paid_4_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(80%,#bddeff), color-stop(80%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_3_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(60%,#bddeff), color-stop(60%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(40%,#bddeff), color-stop(40%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(20%,#bddeff), color-stop(20%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_3_4 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(75%,#bddeff), color-stop(75%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_4 .fc-event-bg, #teesheets .paid_1_2 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(50%,#bddeff), color-stop(50%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_4 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(25%,#bddeff), color-stop(25%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_3 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(67%,#bddeff), color-stop(67%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_3 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(34%,#bddeff), color-stop(34%,transparent), color-stop(100%,transparent))
	}
	<?php if ($holes == 18) {?>
		#teesheets #frontnine, #teesheets #backnine {
			width:50%;
		}
	<?php } else { ?>
		#teesheets #frontnine {
			width:100%;
		}
	<?php } ?>
</style>
<div class="wrapper">
    <div class="column">
    	<div id='teetime_search_box'>
    		<div id='teetime_search_title'>Search<span class='icon minimize_icon'></span></div>
    		<div id='teetime_search_content' style='display:none'>
    			<input type='text' placeholder='Customer Name' value='' id='teetime_search_name'/>
    			<input type='hidden' value='' id='teetime_search_id'/>
    			<div id='teetime_search_results'>
    				<div class='teetime_result'>
	    				No results
	    			</div>
    			</div>
    		</div>
    	</div>
        <div id="teetime_standby_box">
         	<div id='teetime_standby_title' class='expanded'>Standby<span class='icon minimize_icon'></span></div>
        	<div id='teetime_standby_content'>
	        	<ul class='expanded'>	        
                    <li>	
		    			<input class="new_standby" type="button" value="Add New Standby">		    	
                        <div id="teetime_populate_standby_list" style="overflow:auto; max-height: 300px">
                            <div class='teetime_result'>
                            No Standby Golfers
                            </div>
                        </div>
                    </li>
                </ul>
	        </div>			        	
    	</div>   
        <div id="notes_box">
         	<div id='notes_title' class='expanded'>Notes<span class='icon minimize_icon'></span></div>
        	<div id='notes_content'>
	        	<ul class='expanded'>	        
                    <li>	
		    			<input class="new_note" type="button" value="Add Note">		    	
                        <div id="note_list" style="overflow:auto; max-height: 300px">
                        	<?php $this->load->view('employees/notes', array('notes',$notes)); ?>
                        </div>
                    </li>
                </ul>
	        </div>			        	
    	</div>   
        <div id="datepicker"></div>
        <?php if (!$this->config->item('simulator')) {?>
        <div id="weatherBox" class="weatherBox">
            <ul class='expanded'>
                <!--li class="stats"><a href="#tabs-1">Players</a></li-->
                <li class="weather"><a href="#wbtabs-1">Weather</a><span class='icon minimize_icon'></span></li>
            </ul>
            <!--div id='tabs-1' class="stats">

            </div-->
            <div id='wbtabs-1'>
                <?php //echo $fdweather; ?>
            </div>
        </div>
        <?php } ?>
        <!--div id="stats">
            <ul>

            </ul>

        </div-->
         <div class="facebook_messages logged_in">
         	<div id='facebook_title'>Facebook<span class='icon minimize_icon'></span></div>
        	<div id='facebook_content' style='display:none'>
				<div id="selected_page" class="truncated"></div>
	        	<ol id="selectable">
	
				</ol>
				<textarea id="message" rows="8" maxlength="150" placeholder="Enter message to post to page"></textarea>
		    	<input class="facebook_login" type="button" value="Login">		    	
		    	<input class="post facebook_post" type="button" value="Post to Page">         	  
	        	<!--input class="post" type="button" value="Post" style='display:none'/>
			  	<input class="login" type="button" value="Login"/>
			  	<input class="logout" type="button" value="Logout" style='display:none'/-->
			  	<span id='facebook_logout'>Logout</span>
			</div>			        	
    	</div>
        <div id='settings_button' class='ui-widget-content'>
        	<span id='settings_icon'></span> Teesheet Settings
        </div>
    </div>
    <?php if ($current_teesheet == '') { ?>
    	<div style='padding-top:30px; font-size:18px;'>
    	No default teesheet available, please create one under settings.
    	</div>
    <?php } else { ?>
    <div id="main" class="container">
    	<div id='teesheet_controls'>
    		<div id='course_selector'>
    		<?php if ($tsMenu != '') {?>
	            <form action="" method="post">
	                    <?php echo $tsMenu; ?>
	                    <input type="submit" value="changets" name="changets" id="changets" style="display:none"/>
	            </form>
	        <?php } ?>
	       </div>
	       <a id='print_button'>
	       		<div id='teesheet_download_button'>&nbsp;</div>
	       </a>
    	   <div id='teesheet_view_buttons'>
	       		<ul>
	       			<!--li id='month_view'>Month</li-->
	       			<li id='week_view'>Week</li>
	       			<li id='day_view' class='last selected'>Day</li>
	       		</ul>
	       </div>
	    </div>
    	<div id='teesheet_info'>
    		<div id='teesheet_date_box'>
    			<div id='teesheet_day'>
    				
    			</div>
    			<div class='date_info'>
    				<div id='teesheet_dow'>
    					
    				</div>
    				<div id='teesheet_month'>
    					
    				</div>
    			</div>
    			<div class='clear'></div>
    		</div>
    		<div class='line_spacer'>
    			
    		</div>
    		<div id='teesheet_weather_box'>
    			<div id='weather_icon'>
    				
    			</div>
    			<div id='current_temp'>
    				
    			</div>
    			<div id='low_high'>
    				<div id='high'></div>
    				<div id='low'></div>
    			</div>
    		</div>
    		<div id='teesheet_day_navigator'>
    			<ul>
	       			<li id='previous_day'>Previous</li>
	       			<li id='today'>Today</li>
	       			<li id='next_day' class='last'>Next</li>
	       		</ul>
		    </div>
    	</div>
    	<div class="inner clearfix">
        <?
            if ($user_id) {?>
            <? } ?>
            <div class="content">
                <div id="frontnine">
                    <div id='calendar'></div>
                </div>
                <div id="backnine">
                    <div id='calendarback' style='width:100%; display:none'></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<!-- Right Click Menu -->
<div class='contextMenu' id='myMenu' style='display:none'>
	<ul>
	    <li id="switch"><!--img src="../../images/pieces/switch.png" /-->Switch Sides</li>
	    <li id="repeat">Repeat Event</li>
	    <li id="move">Change Day/Time</li>            
	    <li id="teed_off">Tee'd Off</li>            
	    <li id="mark_turn">Mark the Turn</li>            
	    <li id="mark_finished">Mark as Done</li> 
    	<li id="check_in_1">Check In 1</li>
    	<li id="check_in_2">Check In 2</li>
    	<li id="check_in_3">Check In 3</li>
    	<li id="check_in_4">Check In 4</li>
    	<li id="check_in_5">Check In 5</li>
	    <!--li id="raincheck">Issue Rain Check</li-->            
	</ul>
</div>
<div id="feedback_bar"></div>
<div id="teetime_info_bar"></div>
<div id="fb-root"></div>

<?php $this->load->view("partial/footer"); ?>
<script>
   
var teesheet = {
	teetime:{
		search:function(){
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/teetime_search",
	           data: 'customer_id='+$('#teetime_search_id').val(),
	           success: function(response){
	           		$('#teetime_search_results').html(response.search_results);
	           },
	           dataType:'json'
	         }); 
		}
	}
}

var standby = {
	standby_time:{
		list_standby_golfers:function(){
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/teetime_standby_get_list",
	           data: '',
	           success: function(response){
	           		var standby_html = '';
                                if (response.length == 0)
                                {
                                    standby_html = '<div class=\'teetime_result\'>No Standby Golfers</div>';
                                }
                                
                                //console.log('length: ' + response.length);
                                
                                $.each(response, function(data){
                                    
                                            standby_html += "<div class='standby_entry' id='";
                                            standby_html += response[data].standby_id + 
                                            "'><div class='teetime_result' id='" + 
                                            response[data].standby_id + "'><div>" +
                                            response[data].name + ' </br>' + 
                                            response[data].time + '</br> ' + 
                                            response[data].holes + ' Holes ' +
                                            
                                            response[data].players + ' Players' + "</div> </div>";
                                    
                                         standby_html += '</div>';
                                    
                                });
                
                                   //console.log(standby_html);
                                    
                                
                                $('#teetime_populate_standby_list').html(standby_html);
                                
                                
                                //$( ".standby_entry" ).draggable({ containment: "#content_area_wrapper", scroll: false, revert:true, start: function(e) { $(e).css('z-index', 1000)} , stop: function(e) { $(e).css('z-index', 0)}  });
                                $('.standby_entry').draggable({zIndex:1000, containment:'#content_area_wrapper', 
                                    helper:'clone', appendTo:'body'
                              });
                                
	           },
	           dataType:'json'
	         }); 
		}
                
	}
        
}
var standby_edit = {
    standby_golfer_edit:{
        edit_standby:function(){
    
        }
    }
}
var teetime_html = "<?php //echo (preg_replace('/\s+/',' ',str_replace(array(chr(10),chr(13),'\r\n','\t','\r','\n'), '', ($this->config->item('simulator')) ? $this->load->view("teetimes/form_simulator",$data,true) : $this->load->view("teetimes/form",$data,true))));?>";
	
	/*JOEL CREATE THE ARRAY HERE*/
	//FACEBOOK CODE
	window.my_config = 
	{
		access_token : '',
		page_id : '',
		name : '',		
		user_accounts : '',
		<?php if (base_url() == 'http://localhost:8888/') { ?>
		app_id : '317906091649413', // app_id for LOCALHOST testing only
		app_secret : '5a47cce8b870df193418cbdc1490913b', //app_secret for LOCALHOST testing only
		<?php } else if (base_url() == 'http://dev.foreupsoftware.com') { ?>
		app_id : '583489044999943', // App ID for ForeUP Dev
		app_secret : '5f8b249b9473fd74b13814be106d45ad', //app_secret for ForeUP Dev
		<?php } else if (base_url() == 'http://mobile.foreupsoftware.com') { ?>
		app_id : '398875546898011', // App ID for Mobile.ForeUP Dev
		app_secret : '31d25e25007a63720f3f2e26fce3160a', //app_secret for Mobile.ForeUP Dev
		<?php } else { ?>
		app_id : '411789792224848', // App ID for ForeUP Master
		app_secret : 'f69cd64778c32cb7dda1c1ec6960e1b0', //app_secret for ForeUP Master
		<?php } ?>
		extended_access_token : '',
		can_post_with_extended_access_token : false
	};	
	
	var facebook_obj = {
		logout: function(){	
			FB.logout(function(response){});
		}
	}
	
	$(document).ready(function(){
        
                standby.standby_time.list_standby_golfers();
		var teetime_search_name = $('#teetime_search_name');
		teetime_search_name.focus(function() { $(this).val('') });
		teetime_search_name.autocomplete({
			source: '<?php echo site_url("teesheets/customer_search/ln_and_pn"); ?>',
			delay: 10,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui)
			{
				event.preventDefault();
				teetime_search_name.val(ui.item.label);
				$("#teetime_search_id").val(ui.item.value);
				teetime_search_name.blur();
				teesheet.teetime.search();                               
			}
		});
                
		$('#teesheetMenu').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
		<?php if ($this->session->userdata('is_simulator')) { ?>
	    $('#calendar th.fc-col0').html('Sim 1');
	    $('#calendarback th.fc-col0').html('Sim 2');
		<?php } else { ?>
	    $('#calendar th.fc-col0').html('Front');
	    $('#calendarback th.fc-col0').html('Back');
		<?php } ?>
	
	/**********************************************************************************
	 **********************************FACEBOOK CODE***********************************
	 **********************************************************************************/	
	function update_my_config()
	{
		
		window.my_config.page_id = '<?php echo $this->session->userdata('facebook_page_id'); ?>';
		window.my_config.name = '<?php echo $this->session->userdata('facebook_page_name'); ?>';
		window.my_config.extended_access_token = '<?php echo $this->session->userdata('facebook_extended_access_token'); ?>';	
		if (window.my_config.extended_access_token !== '') {
			window.my_config.can_post_with_extended_access_token = true;			
		} 		
		update_facebook_widget_appearance();
	}
	
	function update_facebook_widget_appearance()
	{
		if (window.my_config.can_post_with_extended_access_token)
		{
			$('.facebook_post').show();
			$('.facebook_login').hide();
			$('#facebook_logout').show();
			$('#message').show();
		} else {
			$('.facebook_post').hide();
			$('.facebook_login').show();
			$('#facebook_logout').hide();
			$('#message').hide();
		}
	}

	
	update_my_config();
					
	/*Load Facebook SDK*/
	  window.fbAsyncInit = function() {    
	    FB.init({
	      appId      : window.my_config.app_id,
	      channelUrl : '<?php echo site_url();?>', // Channel File
	      status     : true, // check login status
	      cookie     : true, // enable cookies to allow the server to access the session
	      xfbml      : true  // parse XFBML
	    });		    
	  };
		  
	  (function(d){
	     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement('script'); js.id = id; js.async = true;
	     js.src = "http://connect.facebook.net/en_US/all.js";
	     ref.parentNode.insertBefore(js, ref);
	   }(document));
	   
	 
	$('.post').live('click', function(){
		if (window.my_config.can_post_with_extended_access_token)
		{						
			post_facebook_message();
		}
		else
		{
			login();			
		}						
	});
	
	//EVERYTHING STARTS HERE  
	 /********1********/
	$('.facebook_login').live('click', function(){
		login();
	});
	
	$('#facebook_logout').click(function(){
		//the user was already logged out of their official facebook session right after logging in. This method just clears their facebook settings from the database
		clear_facebook_settings();
	});
	
	/********2********/
	function login(){		
		FB.login(function(response) {
	        if (response.authResponse) {	            
				//get an extended access token from facebook which will be stored in our database and used to make future posts
	            fetch_long_lived_access_token(response.authResponse.accessToken);
	        } else {
	            console.log("canceled");
	        }
		},{scope: 'manage_pages publish_stream'});//http://stackoverflow.com/questions/8717388/the-user-hasnt-authorized-the-application-to-perform-this-action
	}		
	
	function clear_facebook_settings()
	{
		var URL = '<?php echo site_url("teesheets/clear_facebook_settings");?>';
			$.post(URL, function(response) {		    
			    window.my_config.extended_access_token = '';
			    window.my_config.can_post_with_extended_access_token = false;
			    window.my_config.page_id = '';
				window.my_config.name = '';	
								
				update_facebook_widget_appearance();				
	    	});	
	}
	
	/********3********/
       
	function fetch_long_lived_access_token(short_lived_access_token)
	{	
		var URL;
		URL = "https://graph.facebook.com/oauth/access_token?client_id=" + window.my_config.app_id + "&client_secret=" + window.my_config.app_secret + "&grant_type=fb_exchange_token&fb_exchange_token=" + short_lived_access_token;								
		
		$.post(URL, function(response) {

		    var token_elements = response.split('=');
		    extended_access_token = token_elements[1].split('&');
		    window.my_config.extended_access_token = extended_access_token[0];
		    window.my_config.can_post_with_extended_access_token = true;
		    
			//post the facebook extended access token to server
			URL = '<?php echo site_url("teesheets/update_facebook_access_token");?>';
			data = {extended_access_token: window.my_config.extended_access_token};						
			
			$.post(URL, data, function(response) {

		    });	
		    
		    //show a colorbox that will allow them to select which page is the golf course pages. NOTE: this pop up shows them all pages for which they are administrator		    
		    $.colorbox({'href':'index.php/teesheets/facebook_page_select/width~300', 'title':"Select the Course Page", 'overlayClose':false, 				
				onClosed: function(){
					update_facebook_widget_appearance();
				}				
			});		 
	    });
	    
	    return;
	}		
	/********5********/
	function post_facebook_message(options)
	{
		var message, URL, success_message;
		message = $('#message').val();
		URL = "https://graph.facebook.com/" + window.my_config.page_id + "/feed?access_token=" + window.my_config.extended_access_token + "&message=" + message;				
		
		$('.facebook_messages').mask("<?php echo lang('common_wait'); ?>");
		
		$.post(URL, function(response) {	      
	      
	    })
	    .success(function(){
	    	$('.facebook_messages').unmask();
	    	$('#message').val('');
	    	
	    	success_message = "Successfully Posted to " + window.my_config.name + " Facebook Page";	
	    	
	    	//functioned defined in other script
	    	set_feedback(success_message, 'success_message', false);    	
	    })
	    .error(function() { 
   			 $('.facebook_messages').unmask();
   			 login();
		});		
	}		
});
</script>

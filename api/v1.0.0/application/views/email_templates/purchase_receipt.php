<div lang='en' style='padding:0;margin:0'>
  <table width='100%' cellspacing='0' cellpadding='0' border='0'>
    <tbody>
      <tr>
        <td style='background-color:#e9eff2;padding:30px 15px 0'>
          <table width='710' cellspacing='0' cellpadding='0' border='0' align='center' style='font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;color:#333'>
            <tbody>
              <tr>
                <td style='background-color:#63839b'>
                <!--a target='_blank' href='<?php echo $link_url; ?>' style='color:#ffffff;display:block'-->
                <div style='display:block; height:52px; width:710px;'></div>
                <!--img width='710' height='52' style='border:0;display:block' src='<?php echo $header_image; ?>' alt='<?php echo $course_name;?>'-->
                  <!--/a-->
                </td>
              </tr>
              <tr>
                <td style='background-color:#fff;padding:25px 40px 14px'>
                  <table width='600' cellspacing='0' cellpadding='0' border='0' align='center' style='margin:0 auto'>
                    <tbody>
                      <tr>
                        <td width='600' valign='middle' height='36' style='padding:0 0 25px'>
                          <h1 style='font-weight:normal;font-size:19px;line-height:1.2;margin:0'>
					           Thank you for your purchase.
					      </h1>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#ecf2f5'>
                      <tbody>
                        <tr>
                          <td width='25%' valign='top'>
                            <table cellspacing='0' cellpadding='0' border='0'>
                              <tbody>
                                <tr>
                                  <td valign='top' style='padding:15px 10px 15px 15px'>
                                  <!--a target='_blank' style='text-decoration:none' href='<?php echo $link_url; ?>'-->
                                    <img width='159' height='111' style='display:block;border:none' src='http://ForeUP.com/img/email_golf_course.png' alt=''>
                                      <!--/a-->
                                    </td>
                                    <td valign='top' style='padding:15px 15px 15px 0'>
                                      <div style='line-height:15px'>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                            <td width='75%' valign='top'>
                              <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                <tbody>
                                  <tr>
                                    <td style='border-bottom:1px solid #666666; padding:7px 0px' colspan=2>Reservation Details</td>
                                  </tr>
                                  <tr>
                                    <td valign='top' nowrap='' style='padding:10px 0 15px 15px;font-size:13px;width:200px;'>
                                    <!--a target='_blank' style='color:#5a7b93;text-decoration:none' href='<?php echo $link_url; ?>'-->
                                      <span style='font-size:15px;font-weight:bold;color:#333;text-decoration:none'>Golf Course</span>
                                      <br>
                                      <?php echo $course_name;?>
                                        <!--/a-->
                                      </td>
                                      <td valign='top' nowrap='' style='padding:10px 15px 15px 15px;font-size:13px;width:200px;'>
                                      <!--a target='_blank' style='color:#5a7b93;text-decoration:none' href='<?php echo $link_url; ?>'-->
                                        <span style='font-size:15px;font-weight:bold;color:#333;text-decoration:none'>Details</span>
                                        <br>
                                        <?php 
                                        	if ($simulator_times) {
                                        		foreach($simulator_times['front'] as $timeslot)
												{
													echo ($timeslot['side'] == 'front')?'Simulator 1: ':'Simulator 2: ';
													echo date('g:ia', strtotime($timeslot['start']+1000000)).' - '.date('g:ia', strtotime($timeslot['end']+1000000)).' '.date('n/j/y T', strtotime($timeslot['start']+1000000)).'<br/>';
												}
                                        		foreach($simulator_times['back'] as $timeslot)
												{
													echo ($timeslot['side'] == 'front')?'Simulator 1: ':'Simulator 2: ';
													echo date('g:ia', strtotime($timeslot['start']+1000000)).' - '.date('g:ia', strtotime($timeslot['end']+1000000)).' '.date('n/j/y T', strtotime($timeslot['start']+1000000)).'<br/>';
												}
                                        	}
											else {
												echo "<div>Date: $booked_date</div>";	
												echo "<div>Time: $booked_time</div>";	
												echo "<div>Holes: $booked_holes</div>";	
												echo "<div>Players: $booked_players</div>";	
												echo "<div>Carts: $booked_carts</div>";	
											}
                                        ?>
                                          <!--/a-->
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                          <td width='25%' valign='top'>
                            
                          </td>
                            <td width='75%' valign='top'>
                              <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                <tbody>
                                  <tr>
                                    <td style='border-bottom:1px solid #666666; padding:7px 0px' colspan=2>Payment Details</td>
                                  </tr>
                                  <tr>
                                    <td valign='top' nowrap='' style='padding:10px 0 15px 15px;font-size:13px;width:200px;'>
                                    <!--a target='_blank' style='color:#5a7b93;text-decoration:none' href='<?php echo $link_url; ?>'-->
                                      <span style='font-size:15px;font-weight:bold;color:#333;text-decoration:none'>Billing Info</span>
                                      <br>
	                                      <?php 
	                                      	//Customer name
											//Customer email
	                                    	//Card type
	                                    	//Purchase date
	                                    	//Confirmation number 
	                                      	echo "<div>$customer_name</div>";	
											echo "<div>$customer_email</div>";	
											echo "<div>Paid with: $card_type</div>";	
											echo "<div>Paid on: $current_date</div>";	
											echo "<div>Confirmation #: $confirmation_number</div>";	
										  ?>
                                      </td>
                                      <td valign='top' nowrap='' style='padding:10px 15px 15px 15px;font-size:13px;width:200px;'>
                                      <!--a target='_blank' style='color:#5a7b93;text-decoration:none' href='<?php echo $link_url; ?>'-->
                                        <span style='font-size:15px;font-weight:bold;color:#333;text-decoration:none'>Details</span>
                                        <br>
                                        <?php
                                        	//Price
                                        	//Discount
                                        	//Subtotal - add later
                                        	//Taxes and fees - add later
                                        	//Total 
                                    		echo "<div>Price: $$subtotal</div>";	
											echo "<div>Discount: $$discount</div>";	
											echo "<div><b>Total: $$total</b></div>";	
										?>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td style='background-color:#fff;padding:22px 40px 15px;border-top:1px solid #E4ECF0'>
                          <table cellspacing='0' cellpadding='0' border='0' style="width:100%; background-color:#ecf2f5" >
                            <tbody>
                              <tr>
                                <td valign='middle' style='padding:0 0 0 15px'>
                                  <table cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                      <tr>
                                        <td></td>
                                        <td style='padding:5px'>
            							If you have any questions or concerns, you may contact ForeUP at support@foreup.com
                                      </td>
                                      <td></td>
                                    </tr>
                                </tbody>
                              </table>
                            </td>
                              <td valign='middle' style='padding:0 0 0 15px;font-size:15px;line-height:19px'>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                         <tr>
                          <td>
                            <div style='padding:0 5px'>
                              <div style='min-height:2px;line-height:2px;font-size:2px;background-color:#e2e7e7;clear:both'>
                            </div></div>
                          </td>
                        </tr>
                          <tr>
                            <td style='font-size:11px;line-height:16px;color:#aaa;padding:25px 40px'>
                            <!--div style='font-family:'Helvetica Neue', Arial, Helvetica, sans-serif;margin-top:5px;font-size:11px;color:#666666'>
							  If you believe this email was sent in error, you may <a target='_blank' style='color:#6d90a9;text-decoration:none' href='<?php echo $error_link;?>'>click here</a> to report it
							</div-->
                            <!--div style='font-family:'Helvetica Neue', Arial, Helvetica, sans-serif;margin-top:5px;font-size:11px;color:#666666'>
						        To unsubscribe from receiving further emails from <?php echo $course_name;?>, <a target='_blank' style='color:#6d90a9;text-decoration:none' href='<?php echo $unsubscribe_link;?>'>click here</a>.
						    </div-->
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

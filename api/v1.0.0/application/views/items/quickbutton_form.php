<ul id="error_message_box"></ul>
<?php
echo form_open('items/save_quickbutton/'.$quickbutton_info->quickbutton_id,array('id'=>'quickbutton_form'));
//print_r($quickbutton_items);
?>
<fieldset id="quickbutton_basic_info">
<legend><?php echo lang("items_quickbutton_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('items_quickbutton_name').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>($quickbutton_info->display_name)?$quickbutton_info->display_name:'New Button')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_quickbutton_item').':<span class="required">*</span>', 'quickbutton_item',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'quickbutton_item',
		'id'=>'quickbutton_item',
		'width'=>30
		)
	);
	echo form_hidden('quickbutton_items');
	echo form_hidden('delete_quickbutton');
	?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo lang("items_quickbutton_more_items"); ?>
</div>
<div class="field_row clearfix">
	<script>
		quickbuttons.item_ids = eval('(<?php echo $item_ids; ?>)');
	</script>
	<a id='demo_button' class='quickbutton new_quickbutton' href='index.php/sales'><?php echo ($quickbutton_info->display_name)?$quickbutton_info->display_name:'New Button';?></a>
	<?php
		if (count($quickbutton_items) > 1)
		{
			echo "<div id='demo_menu' class='quickbutton_menu_box'style='display:block;'>";
				foreach($quickbutton_items as $item)
				{
					$item_id_pieces = explode('_', $item['item_id']);
					$green_fee_type = (count($item_id_pieces) > 1)?' '.$green_fee_types['price_category_'.$item_id_pieces[1]]:'';
					if ($item['item_id'] != '')
						echo "<a title='Remove' id='demo_button_{$item['item_id']}' class='quickbutton new_quickbutton' href='javascript:quickbuttons.remove(\"{$item['item_id']}\")' item_id='{$item['item_id']}'>{$item['i_name']}{$green_fee_type}</a>";
					else
						echo "<a title='Remove' id='demo_button_KIT_{$item['item_kit_id']}' class='quickbutton new_quickbutton' href='javascript:quickbuttons.remove(\"KIT_{$item['item_kit_id']}\")' item_id='KIT_{$item['item_kit_id']}'>{$item['ik_name']}{$green_fee_type}</a>";
				}
			echo "</div></div>";
		}
		else 
		{
			echo "<div id='demo_menu' class='quickbutton_menu_box'>	</div>";
			foreach ($quickbutton_items as $item)
			{
				if ($item['item_id'] != '')
					echo "<div id='demo_single_name'>({$item['i_name']})</div>";
				else
					echo "<div id='demo_single_name'>({$item['ik_name']})</div>";
			}
		}
	?>
	<div class='clear'></div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
echo form_submit(array(
	'name'=>'delete',
	'id'=>'delete',
	'value'=>lang('common_delete'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<style>
	div.quickbutton_menu_box {
		left:14em;
	}
	#demo_menu .new_quickbutton {
		background: url(../images/pieces/x_red2.png) no-repeat 10px 8px, -webkit-linear-gradient(top, #349ac5, #4173b3);
	}
	#demo_menu {
		position:inherit;
		margin:auto;
	}
	#demo_button {
		margin:auto;
		margin-bottom:5px;
	}
	#submit {
		float:right;
	}
	#demo_single_name {
		text-align:center;
	}
</style>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	//$('#quickbutton_form').keypress(function(e){
    //  	e.preventDefault();
    //  	if(e.which == 13){
    //   		$('#submit').click();
    //   	}
    //});
    $('#demo_menu').sortable();
	$('#submit').click(function(event) {
		//If none, the error
		if (quickbuttons.item_id_count() == 0)
		{
			event.preventDefault();
			alert('You must include at least one inventory item before saving');
		}
		//If one, then grab from quickbutton id array
		else if (quickbuttons.item_id_count() == 1)
		{
			var item_ids = quickbuttons.item_ids;
			var id_array = [];
			for (var i in item_ids)
				id_array.push(i);
		}
		//If multiple, then grab from sortable list
		else
		{
			var item_ids = $('#demo_menu').sortable('toArray');//quickbuttons.item_ids;
			var id_array = [];
			for (var i in item_ids)
				id_array.push(item_ids[i].substring(12));
		}
		console.log('assigning quickbutton_items - '+id_array.join('|'));
		$('input:[name=quickbutton_items]').val(id_array.join('|'));
	});
	$('#delete').click(function(){
		console.log('assigning delete');
		$('input:[name=delete_quickbutton]').val(1);
	});
	$('#name').keyup(function(event){
		$('#demo_button').html($(event.target).val());
	});
	$( "#quickbutton_item" ).autocomplete({
		source: '<?php echo site_url("sales/item_search"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			ui.item.value = ui.item.value.replace(' ','_');
			event.preventDefault();
			var id_count = quickbuttons.item_id_count();
			console.log('id_count '+id_count);
			console.dir(quickbuttons.item_ids);
			var price_index = (ui.item.price_index != undefined)?'_'+ui.item.price_index:'';
			var teetime_type = (ui.item.teetime_type != undefined)?'_'+ui.item.teetime_type:'';
			quickbuttons.item_ids[ui.item.value+price_index+teetime_type] = ui.item.label;
			console.dir(ui.item);
			if (id_count == undefined || id_count == 0)
			{
				$('#demo_button').html(ui.item.label);
				if ($('#name').val() == 'New Button')
					$('#name').val(ui.item.label);
			}
			else
			{
				$('#demo_menu').show();
				var item_ids = quickbuttons.item_ids;
				$('#demo_menu').html('');
				for (i in item_ids)
					$('#demo_menu').append("<a title='Remove' id='demo_button_"+i+"'class='new_quickbutton' href='javascript:quickbuttons.remove(\""+i+"\")' item_id='"+i+"'>"+item_ids[i]+"</a>");
			}
			$.colorbox.resize();
			$("#quickbutton_item" ).val('');
			$("#quickbutton_item").focus();
			//$("input[name=price_index]").val(ui.item.price_index);
			//$("input[name=teetime_type]").val(ui.item.teetime_type);
			//$("#add_item_form").submit();
		}
	});

	var submitting = false;
    $('#quickbutton_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			//console.dir(form);
			var tab = $('.quickbutton_tab.selected').attr('id').replace('qb_tab_','');
			$(form).ajaxSubmit({
			data:{tab:tab},
			success:function(response)
			{
				console.dir(response);
				if (response.action == 'deleted')
					quickbuttons.delete(response.quickbutton_id);
				else if (response.action == 'saved')
					quickbuttons.update(response.quickbutton_id, response.html);
				$.colorbox.close();
			//	post_item_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required"
   		},
		messages:
		{
			name:"<?php echo lang('items_quickbutton_label_required'); ?>"

		}
	});
});
</script>
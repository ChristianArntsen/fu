<style>
#modifiers, #modifiers td {
	background-color: white;
}

#modifiers td {
    background-color: white;
    border-bottom: 1px solid #B2B2B2;
    border-right: 1px solid #B2B2B2;
    color: #555555;
    font-size: 14px;
    height: 30px;
    padding: 0 6px;
    vertical-align: middle;
}

#modifiers th {
    background: url("<?php echo base_url(); ?>images/backgrounds/paper_edge_gray.png") repeat-x scroll 0 -2px #CCCCCC;
    border-right: 1px solid #BBBBBB;
    color: #333333;
    font-size: 16px;
    height: 30px;
    padding: 0 6px;
    text-align: left;
}

#modifiers tr.editing td {
	background-color: #F0F0F0 !important;
	padding: 5px !important;
}	

#add_modifier .submit_button {
	margin: 18px 0px 0px 0px !important;
	float: left;
}

#add_modifier label {
	display: block;
	line-height: 11px;
	padding: 4px 0px;
	font-size: 12px;
}

#add_modifier .form_field {
	float: left;
	margin-right: 10px;
}

#add_modifier input {
	display: block;
}	

#add_modifier {
	padding: 10px;
	overflow: hidden;
}

.form_field.name input {
	width: 250px;
}

.form_field.options input {
	width: 375px;
}

.form_field.price input {
	width: 60px;
}
</style>
<form method="post" id="add_modifier" action="<?php echo site_url('modifiers/save'); ?>">
	<div class='form_field'>
		<label>Name</label>
		<input style="width: 230px;" type="text" name="name" value="" />
	</div>
	<div class='form_field'>
		<label>Options <em><small>(Separate with commas)</small></em></label>
		<input style="width: 350px;" type="text" name="options" value="" />
	</div>
	<div class='form_field'>
		<label>Price</label>
		<input type="text" name="default_price" value="" style="width: 75px;" />
	</div>
	<input type="submit" class="submit_button" value="Add New" />
</form>
<table id="modifiers">
	<thead class="fixedHeader">
		<tr>
			<th style="width: 275px;">Name</th>
			<th style="width: 400px;">Options</th>
			<th style="width: 75px;">Price</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>			
		<?php if(!empty($modifiers)){ ?>
		<?php foreach($modifiers as $key => $modifier){ ?>
		<tr data-modifier-id="<?php echo $modifier['modifier_id']; ?>">
			<td>
				<div class='form_field name'>
					<span class="data" data-name="name"><?php echo $modifier['name']; ?></span>
				</div>
			</td>
			<td>
				<div class='form_field options'>
					<span class="data" data-name="options"><?php if(!empty($modifier['options'])){ ?><?php echo implode(',', $modifier['options']); ?><?php } ?></span>
				</div>
			</td>				
			<td>
				<div class='form_field price'>
					<span class="data" data-name="default_price"><?php echo $modifier['default_price']; ?></span>
				</div>
			</td>
			<td class="edit">
				<a href="#" title="Edit this modifier" class="edit_modifier">Edit</a>
			</td>			
			<td class="delete">
				<a href="#" class="delete_modifier" title="Delete this modifier" style="color: red;">X</a>
			</td>
		</tr>
		<?php } }else{ ?>
		
		<?php } ?>
	</tbody>
</table>
<script>
$(function(){
	$('#add_modifier').submit(function(e){
		var url = $(this).attr('action');
		var data = $(this).serialize();
		
		$.post(url, data, function(response){
			$('#modifiers').append(response);
			$.colorbox.resize();
		}, 'html');
		
		$(this)[0].reset();
		
		return false;
	});
	
	$('a.delete_modifier').live('click', function(e){
		var row = $(this).parents('tr');
		var modifierId = $(this).parents('tr').find('input.modifier_id').val();
		var url = '<?php echo site_url('modifiers/delete'); ?>' + '/' + modifierId;
		
		$.post(url, null, function(response){
			if(response.success){
				row.remove();
				$.colorbox.resize();
			}
		},'json');
		
		return false;
	});

	$('a.edit_modifier').live('click', function(e){
		var row = $(this).parents('tr');
		row.addClass('editing');
		row.find('td.delete').remove();
		row.find('td.edit').attr('colspan',2);
		
		row.find('span.data').each(function(index, val){
			var fieldName = $(this).attr('data-name');
			var fieldData = $(this).text();
			var textField = $('<input type="text" name="'+fieldName+'" value="'+fieldData+'" />');
			
			$(this).replaceWith(textField);
		});
		
		row.find('a.edit_modifier').replaceWith('<a href="#" class="save_modifier">Save</a>');
		
		return false;
	});
	
	$('a.save_modifier').live('click', function(e){
		var row = $(this).parents('tr');
		var data = row.find('input').serialize();
		
		$.post('<?php echo site_url('modifiers/save'); ?>/' + row.attr('data-modifier-id'), data, function(response){
			row.replaceWith(response);
			$.colorbox.resize();
		},'html');
		
		return false;
	});	
});
</script>
<?php
require_once ("secure_area.php");

class Modifiers extends Secure_area
{
    function __construct(){
        parent::__construct();
		$this->load->model('modifier');
    }

    function index(){
		$modifiers = $this->modifier->get();
		$data['modifiers'] = $modifiers;
		$this->load->view('modifiers/manage', $data);
    }

	function search(){
		$query = $this->input->get('term');
		$rows = $this->modifier->search($query);

		echo json_encode($rows);
	}

	function delete($modifierId = null){
		$success = $this->modifier->delete($modifierId);
		echo json_encode(array('success'=>$success));
	}

	function delete_item($itemId = null, $modifierId = null){
		$success = $this->modifier->delete_from_item($itemId, $modifierId);
		echo json_encode(array('success'=>$success));
	}

	function save($modifierId = null){
		$data = $this->input->post();
		$data['options'] = explode(',', $data['options']);

		$modifierId = $this->modifier->save($data, $modifierId);
		$modifiers = $this->modifier->get($modifierId);
		$data['modifier'] = $modifiers[0];
		$this->load->view('modifiers/modifier', $data);
	}

	function save_item($itemId = null, $modifierId = null){
		$modifierData = array(
			'item' => array(
				'item_id' => $itemId
			)
		);

		$success = $this->modifier->save($modifierData, $modifierId);
		$modifiers = $this->modifier->get($modifierId, $itemId);
		$data['modifier'] = $modifiers[0];
		$this->load->view('modifiers/item_modifier', $data);
	}
}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ("secure_area.php");

class Dashboards extends Secure_area
{
	public $widget_types;
	private $default_dashboards = array();

	function __construct()
	{
		parent::__construct('dashboards');

		$this->lang->load('dashboard');
		$this->load->model('Teetime');
		$this->load->model('Dashboard');
		$this->load->model('Bartered_teetimes');
		$this->load->helper('string');

		$revenue_dashboard['dashboard_id'] = 'revenue';
		$revenue_dashboard['name'] = 'Revenue';
		$revenue_dashboard['editable'] = false;
		$revenue_dashboard['widgets'][] = array(
			'name' => 'Revenue/Profit',
			'height' => 320,
			'width' => 960,
			'pos_x' => 0,
			'pos_y' => 0,
			'type' => 'line',
			'widget_id' => 'revenue'
		);

		$revenue_dashboard['widgets'][] = array(
			'name' => 'Revenue by Tee Time Type',
			'height' => 320,
			'width' => 460,
			'pos_x' => 0,
			'pos_y' => 340,
			'type' => 'pie',
			'widget_id' => 'revenue_by_teetime_type'
		);

		$revenue_dashboard['widgets'][] = array(
			'name' => 'Revenue by Department',
			'height' => 320,
			'width' => 480,
			'pos_x' => 480,
			'pos_y' => 340,
			'type' => 'bar',
			'widget_id' => 'revenue_by_department'
		);

		$revenue_dashboard['widgets'][] = array(
			'name' => 'Top Revenue Items',
			'height' => 320,
			'width' => 960,
			'pos_x' => 0,
			'pos_y' => 680,
			'type' => 'column',
			'widget_id' => 'revenue_by_item'
		);

		$overall_dashboard['dashboard_id'] = 'overall';
		$overall_dashboard['name'] = 'Overall';
		$overall_dashboard['editable'] = false;
		$overall_dashboard['widgets']['teetime_sales'] = array(
			'name' => 'Teetime Sales',
			'height' => 320,
			'width' => 960,
			'pos_x' => 0,
			'pos_y' => 0,
			'type' => 'line',
			'widget_id' => 'teetime_sales'
		);

		$overall_dashboard['widgets']['total_teetime_sales'] = array(
			'name' => 'Total Teetime Sales',
			'height' => 100,
			'width' => 470,
			'pos_x' => 0,
			'pos_y' => 340,
			'type' => 'number',
			'widget_id' => 'total_teetime_sales'
		);

		$overall_dashboard['widgets']['total_bartered_teetimes'] = array(
			'name' => 'Total Bartered Teetime Sales',
			'height' => 100,
			'width' => 470,
			'pos_x' => 490,
			'pos_y' => 340,
			'type' => 'number',
			'widget_id' => 'total_bartered_teetimes'
		);

		$overall_dashboard['widgets']['bartered_teetime_sales'] = array(
			'name' => 'Bartered Teetime Sales',
			'height' => 320,
			'width' => 960,
			'pos_x' => 0,
			'pos_y' => 460,
			'type' => 'line',
			'widget_id' => 'bartered_teetime_sales'
		);

		$overall_dashboard['widgets']['payments_processed'] = array(
			'name' => 'Payments Processed',
			'height' => 320,
			'width' => 960,
			'pos_x' => 0,
			'pos_y' => 920,
			'type' => 'line',
			'widget_id' => 'payments_processed'
		);

		$overall_dashboard['widgets']['payments_total'] = array(
			'name' => 'Total Payments',
			'height' => 100,
			'width' => 460,
			'pos_x' => 0,
			'pos_y' => 800,
			'type' => 'number',
			'widget_id' => 'payments_total'
		);

		if($this->permissions->is_super_admin()){
			$this->default_dashboards['overall'] = $overall_dashboard;
		}else{
			$this->default_dashboards['revenue'] = $revenue_dashboard;
		}
	}

	public function index($dashboard_id = null)
	{
		$data['dashboards'] = array_merge($this->default_dashboards, $this->Dashboard->get());

		if(empty($dashboard_id) && current($data['dashboards'])){
			$active_dashboard = current($data['dashboards']);
			$dashboard_id = $active_dashboard['dashboard_id'];
		}

		$data['widgets'] = array();
		// If custom dashboard created by user
		if(!empty($dashboard_id) && is_numeric($dashboard_id)){
			$data['widgets'] = $this->Dashboard->get_widgets($dashboard_id);

		// If ID is a string, its a default permanant dashboard
		} else if(!is_numeric($dashboard_id)){
			$data['widgets'] = $this->default_dashboards[$dashboard_id]['widgets'];
		}

		$this->load->model('Course');
		$data['courses'] = array();
		$this->Course->get_managed_course_ids($data['courses']);

		if(!empty($data['courses'])){
			$courses = array_values($data['courses']);
			$data['active_course'] = $courses[0];
		}else{
			$data['active_course'] = $this->session->userdata('course_id');
		}

		$data['active_dashboard'] = $dashboard_id;
		$this->load->view('dashboard/dashboard', $data);
	}

	public function widget($widget_id)
	{
		if(empty($widget_id)){
			echo json_encode(array('success'=>false));
			return false;
		}
		$course_id = $this->input->get('course_id') ? $this->input->get('course_id') : $this->session->userdata('course_id');
		// If request is for default pre-made widget
		if(!is_numeric($widget_id)){
			$this->load->model('Course');
			$time_period = $this->input->get('time');
			//$course_id = $this->input->get('course_id');
			$time_data = $this->get_time_data($time_period);

			$this->Dash_data->formats[$time_data['group_by']] = $time_data['format'];

			//if(!empty($course_id) && $this->Course->is_managing_course($course_id)){
				$this->Dash_data->course_id = $course_id;
			//}

			// Select proper widget and return the data
			switch($widget_id){
				case 'revenue':
					$categories = $this->Dash_data->create_time_categories($time_data['group_by'], $time_data['start'], $time_data['end']);
					$widget = array(
						'name' => 'Revenue/Profit',
						'categories' => $categories,
						'data' => $this->Dash_data->fetch_data('revenue', array('profit', 'revenue'), '', 0, $time_data['group_by'],null, $categories, 'line', $time_data['start'], $time_data['end']),
						'height' => 320,
						'width' => 980,
						'pos_x' => 0,
						'pos_y' => 0,
						'type' => 'line',
						'widget_id' => 'revenue'
					);
				break;
				case 'revenue_by_teetime_type':
					$rows = $this->Dash_data->revenue(array('revenue'), 'teetime_type', $time_data['start'], $time_data['end']);
					$categories = $this->Dash_data->create_categories($rows, 'teetime_type');
					$data = $this->Dash_data->structure_results('revenue', $rows, array('revenue'), 'teetime_type',null, $categories, 'pie');

					$widget = array(
						'name' => 'Revenue by Tee Time Type',
						'categories' => $categories,
						'data' => array(array('name'=>'Revenue by Tee Time Type', 'data'=>$data, 'number_type'=>'money')),
						'height' => 320,
						'width' => 460,
						'pos_x' => 0,
						'pos_y' => 340,
						'type' => 'pie',
						'widget_id' => 'revenue_by_teetime_type'
					);
				break;
				case 'revenue_by_department':
					$rows = $this->Dash_data->revenue(array('revenue'), 'department', $time_data['start'], $time_data['end']);
					$categories = $this->Dash_data->create_categories($rows, 'department');

					$data = array('name'=>'Revenue', 'data'=>array(), 'number_type'=>'money');
					foreach($rows as $row){
						$data['data'][] = (float) $row['revenue'];
					}

					$widget = array(
						'name' => 'Revenue by Department',
						'categories' => $categories,
						'data' => array($data),
						'height' => 320,
						'width' => 520,
						'pos_x' => 480,
						'pos_y' => 340,
						'type' => 'bar',
						'widget_id' => 'revenue_by_department'
					);
				break;
				case 'revenue_by_item':
					$rows = $this->Dash_data->revenue(array('revenue'), 'item', $time_data['start'], $time_data['end']);
					$categories = $this->Dash_data->create_categories($rows, 'item');

					$data = array('name'=>'Revenue', 'data'=>array(), 'number_type'=>'money');
					foreach($rows as $row){
						$data['data'][] = (float) $row['revenue'];
					}

					$widget = array(
						'name' => 'Top Revenue Items',
						'data' => array($data),
						'categories' => $categories,
						'height' => 320,
						'width' => 960,
						'pos_x' => 0,
						'pos_y' => 680,
						'type' => 'column',
						'widget_id' => 'revenue_by_item'
					);
				break;
				case 'teetime_sales':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = $this->Dash_data->create_time_categories($time_data['group_by'], $time_data['start'], $time_data['end']);

					$data = array('name'=>'Revenue', 'data'=>array(), 'number_type'=>'money');

					$widget = $this->default_dashboards['overall']['widgets']['teetime_sales'];
					$widget['data'] = $this->Dash_data->fetch_data('revenue', array('revenue'), '', 0, $time_data['group_by'], null, $categories, 'line', $time_data['start'], $time_data['end'], array('teetimes'), false);
					$widget['categories'] = $categories;
				break;
				case 'total_teetime_sales':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = array('total');

					$widget = $this->default_dashboards['overall']['widgets']['total_teetime_sales'];
					$widget['data'] = $this->Dash_data->fetch_data('revenue', array('revenue'), '', 0, 'total',null, $categories, 'number', $time_data['start'], $time_data['end'], array('teetimes'), false);
					$widget['categories'] = $categories;
				break;
				/*
				case 'bartered_teetime_sales':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = $this->Dash_data->create_time_categories($time_data['group_by'], $time_data['start'], $time_data['end']);

					$widget = $this->default_dashboards['overall']['widgets']['bartered_teetime_sales'];
					$widget['data'] = $this->Dash_data->fetch_data('revenue', array('revenue'), '', 0, $time_data['group_by'], $categories, 'line', $time_data['start'], $time_data['end'], array('bartered_teetimes'), false);
					$widget['categories'] = $categories;
				break;

				case 'total_bartered_teetimes':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = array('total');

					$widget = $this->default_dashboards['overall']['widgets']['total_bartered_teetimes'];
					$widget['data'] = $this->Dash_data->fetch_data('revenue', array('revenue'), '', 0, 'total', $categories, 'number', $time_data['start'], $time_data['end'], array('bartered_teetimes'), false);
					$widget['categories'] = $categories;
				break; */
				case 'bartered_teetime_sales':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = $this->Dash_data->create_time_categories($time_data['group_by'], $time_data['start'], $time_data['end']);

					$widget = $this->default_dashboards['overall']['widgets']['bartered_teetime_sales'];
					$widget['data'] = $this->Dash_data->fetch_data('bartered_teetime_sales', array('amount'), '', 0, $time_data['group_by'], 'course', $categories, 'line', $time_data['start'], $time_data['end'], array('bartered_teetimes'), false);
					$widget['data'] = array_merge($widget['data'], $this->Dash_data->fetch_data('bartered_teetime_sales', array('amount'), '', 0, $time_data['group_by'], null, $categories, 'line', $time_data['start'], $time_data['end'], array('bartered_teetimes'), false));
					$lastKey = count($widget['data']) - 1;
					$widget['data'][$lastKey]['name'] = 'Total';

					$widget['categories'] = $categories;
				break;

				case 'total_bartered_teetimes':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = array('total');

					$widget = $this->default_dashboards['overall']['widgets']['total_bartered_teetimes'];
					$widget['data'] = $this->Dash_data->fetch_data('bartered_teetime_sales', array('amount'), '', 0, 'total',null, $categories, 'number', $time_data['start'], $time_data['end'], array('bartered_teetimes'), false);
					$widget['categories'] = $categories;
				break;
				case 'payments_processed':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = $this->Dash_data->create_time_categories($time_data['group_by'], $time_data['start'], $time_data['end']);

					$widget = $this->default_dashboards['overall']['widgets']['payments_processed'];
					$widget['data'] = $this->Dash_data->fetch_data('payments_processed', array('ets', 'mercury'), '', 0, $time_data['group_by'],null, $categories, 'line', $time_data['start'], $time_data['end'], null, false);
					$widget['categories'] = $categories;
				break;
				case 'payments_total':
					if(!$this->permissions->is_super_admin()){
						return false;
					}
					$categories = array('total');

					$widget = $this->default_dashboards['overall']['widgets']['payments_total'];
					$widget['data'] = $this->Dash_data->fetch_data('payments_processed', array('ets', 'mercury'), '', 0, 'total', null,$categories, 'number', $time_data['start'], $time_data['end'], null, false);
					$widget['categories'] = $categories;
				break;
			}

		}else{
			//$course_id = $this->input->get('course_id');
			//if(!empty($course_id) && $this->Course->is_managing_course($course_id)){
				$this->Dash_data->course_id = $course_id;
				//echo $this->Dash_data->course_id.'course_id';
			// }
			// else {
				// $this->Dash_data->course_id = $this->session->userdata('course_id');
			// }
			$widget = $this->Dashboard->get_widget($widget_id);

			$dates = $this->Dash_data->get_dates($widget['period'], $widget['metrics'][0]['relative_period_num']);
			$widget['categories'] = $this->Dash_data->create_time_categories($widget['data_grouping'], $dates['start'], $dates['end']);
			$widget['data'] = array();
//$widget['course_id'] = $course_id;
			if($widget['type'] == 'pie'){
				$widget['data'][0]['data'] = array();
				$widget['data'][0]['name'] = $widget['name'];
			}

			foreach($widget['metrics'] as $metric){
				if($widget['type'] != 'pie'){
					$widget['data'] = array_merge($widget['data'], $this->Dash_data->fetch_data($metric['metric_category'], array($metric['metric']), $widget['period'], $metric['relative_period_num'], $widget['data_grouping'], $widget['categories'], $widget['type']));
				}else{
					$widget['data'][0]['data'] = array_merge($widget['data'][0]['data'], $this->Dash_data->fetch_data($metric['metric_category'], array($metric['metric']), $widget['period'], $metric['relative_period_num'], $widget['data_grouping'], $widget['categories'], $widget['type']));
				}
			}
			//echo $this->db->last_query();
		}

		header('Content-Type: application/json');
		echo json_encode($widget);
	}

	function delete()
	{
		$dashboard_id = $this->input->post('dashboard_id');
		$deleted = $this->Dashboard->delete($dashboard_id);

		header('Content-Type: application/json');
		if($deleted){
			echo json_encode(array('success'=>true, 'dashboard_id'=>$dashboard_id));
		}else{
			echo json_encode(array('success'=>false, 'message'=>'Error deleting dashboard'));
		}
	}

	function save_widget($widget_id = null)
	{
		$widget_data = $this->input->post();

		if(empty($widget_data['name'])){
			echo json_encode(array('success'=>false, 'message'=>'Error, widget name is required'));
			return false;
		}

		if(empty($widget_data['metrics'])){
			echo json_encode(array('success'=>false, 'message'=>'Error, please select some metrics'));
			return false;
		}

		if($widget_data['type'] == 'pie' || $widget_data['type'] == 'number'){
			$widget_data['data_grouping'] = 'total';
		}
		$widget = $this->Dashboard->save_widget($widget_id, $widget_data);

		header('Content-Type: application/json');
		if($widget){
			echo json_encode(array('success'=>true, 'widget'=>$widget));
		}else{
			echo json_encode(array('success'=>false, 'message'=>'Error saving widget'));
		}
	}

	function delete_widget()
	{
		$widget_id = $this->input->post('widget_id');
		$deleted = $this->Dashboard->delete_widget($widget_id);

		header('Content-Type: application/json');
		if($deleted){
			echo json_encode(array('success'=>true, 'widget_id'=>$widget_id));
		}else{
			echo json_encode(array('success'=>false, 'message'=>'Error deleting widget'));
		}
	}

	function save_widget_position($widget_id)
	{
		$position = $this->input->post();
		$this->Dashboard->save_widget_position($widget_id, $position);
	}

	function save_widget_size($widget_id)
	{
		$size = $this->input->post();
		$this->Dashboard->save_widget_size($widget_id, $size);
	}

	function form()
	{
		$this->load->view('dashboard/form', $data);
	}

	function widget_form($widget_id = null)
	{
		$this->load->model('Dash_Data');
		if(!empty($widget_id)){
			$data['widget'] = $this->Dashboard->get_widget($widget_id);
		}
		$data['metrics'] = $this->Dash_Data->get_metrics();
		$data['categories'] = $this->Dash_Data->get_categories();
		$data['dashboard_id'] = $this->input->get('dashboard_id');

		$this->load->view('dashboard/widget_form', $data);
	}

	function save($dashboard_id = null)
	{
		$success = $this->Dashboard->save($this->input->post(), $dashboard_id);

		if(!$success){
			echo json_encode(array('success'=>false, 'message'=>'Dashboard name is required'));
		}else{
			echo json_encode(array('success'=>true, 'dashboard_id'=>$success));
		}
	}

	private function get_time_data($time_period = null)
	{
		if(empty($time_period)){
			$time_period = 'past_year';
		}

		switch($time_period){
			case 'today':
				$end = date('Y-m-d');
				$start = date('Y-m-d');
				$group_by = 'hour';
				$format = 'ga';
			break;
			case 'past_7_days':
				$end = date('Y-m-d');
				$start = date('Y-m-d', strtotime('-7 days'));
				$group_by = 'day';
				$format = 'D jS';
			break;
			case 'past_30_days':
				$end = date('Y-m-d');
				$start = date('Y-m-d', strtotime('-30 days'));
				$group_by = 'day';
				$format = 'j';
			break;
			case 'past_year':
				$end = date('Y-m-d');
				$start = date('Y-m-d', strtotime('-1 year'));
				$group_by = 'month';
				$format = 'M y';
			break;
			case 'all_time':
				$start = null;
				$end = null;
				$group_by = 'year';
				$format = 'Y';
			break;
		}

		$response['start'] = $start;
		$response['end'] = $end;
		$response['group_by'] = $group_by;
		$response['format'] = $format;

		return $response;
	}

	/*
	function mercury()
	{
		$data['mercury_stats'] = $this->get_mercury_stats();
		$data['mcs'] = $this->get_mercury_course_stats();

    	$this->load->view('dashboard/mercury', $data);
	}

	function teetimes()
	{
		$data['teetime_stats'] = $this->get_teetime_stats();
		$data['tcs'] = $this->get_teetime_course_stats();
		//echo '<br/><br/>';
		//print_r($data['tcs']);
		$this->load->view('dashboard/teetimes', $data);
	}

	function text_message()
	{
		$data['com_stats'] = $this->get_communication_stats();
		$data['bill_stats'] = $this->get_billing_stats();
	}
	//created by Joel for testing but currently not working

	function generate_chart($type = 'month', $data_source = 'revenue', $graph_type = 'line', $start = '', $end = '', $width = 900, $height = 300)
	{
		// $container_name = random_string('alpha', 10);
		// $data = array();
//
		// $template = 'dashboard/line_bar_area_charts';
		// $data['start'] = ($start != '') ? date('Y-m-d', strtotime($start)) : date('Y-m-d', strtotime('-1 year'));
		// $data['end'] = ($end != '') ? date('Y-m-d', strtotime($end)) : date('Y-m-d');
 		// $data['type'] = $type;
		// $data['data_source'] = $data_source;
		// $data['graph_type'] = $graph_type;
		// $data['container_name'] =  $container_name;
		// $data['width'] = $width;
		// $data['height'] = $height;
		// $data['yAxis'] = $data_source == 'revenue' ? 'Dollars ($)' : ($data_source == 'tee_time' ? 'Count' : 'Units');
		// print_r($data);
		// echo $template;
		// $this->load->view($template, $data);
		$data = array();
		$data['container_name'] = random_string('alpha', 10);
		$data['start'] = date('Y-m-d', strtotime('-1 year'));
		$data['end'] = date('Y-m-d');
		$data['type'] = 'month';
		$data['data_source'] = 'missed_tee_time';
		$data['graph_type'] = 'pie';
		$data['container_name'] = $chart_ids[] = random_string('alpha', 10);
		$data['width'] = 200;
		$data['height'] = 200;
		$data['yAxis'] = 'Dollars ($)';
		$data['static'] = true;
		$data['test'] = true;
		$this->load->view('dashboard/small_widget_charts', $data);
	}

	function generate_table ($type = 'month', $data_source = 'tee_time_table', $graph_type = 'line', $start = '', $end = '', $width = 900, $height = 300)
	{
		$data = array();

		$data['start'] = ($start != '') ? date('Y-m-d', strtotime($start)) : date('Y-m-d', strtotime('-1 year'));
		$data['end'] = ($end != '') ? date('Y-m-d', strtotime($end)) : date('Y-m-d');
 		$data['type'] = $type;
		$data['data_source'] = $data_source;
		$data['graph_type'] = $graph_type;
		$data['container_name'] = random_string('alpha', 10);
		$data['container_name_2'] = random_string('alpha', 10);
		$data['width'] = $width;
		$data['height'] = $height;
		$data['yAxis'] = $data_source == 'revenue' ? 'Dollars ($)' : ($data_source == 'tee_time' ? 'Count' : 'Units');
		$data['test'] = true;
		$this->load->view('dashboard/table', $data);
	}

	function generate_stats ($controller_name)
	{
		switch ($controller_name)
		{
			case 'teesheets':
				$this->tee_sheet_data();
				break;
			case 'sales':
				$this->pos_data();
				break;
			case 'customers':
				$this->customer_data();
				break;
		}
	}

	function tee_times($type = 'monthly', $width = 900, $height = 300)
	{
		$params = $this->input->post('params');
		$container_name = random_string('alpha', 10);
		$data['type'] = $type;
		$data['container_name'] =  $container_name;
		$data['width'] = $width;
		$data['height'] = $height;
		$this->load->view($template, $data);
	}
	function mercury_widget()
	{
		$data = array();
		$this->load->view('dashboard/mercury_widget', $data);
	}
	function text_widget()
	{
		$data = array();
		$this->load->view('dashboard/text_widget', $data);
	}
	*/
	/*
	 * these functions are for development only
	 */
	function no_shows()
	{
		$results = $this->Dash_data->fetch_missed_tee_time_data();
		echo "<pre>";
		print_r($results);
		echo "</pre>";

	}
	function players()
	{
		$results = $this->Dash_data->fetch_players_data();
		echo "<pre>";
		print_r($results);
		echo "</pre>";
	}
	function average_group_size()
	{
		$results = $this->Dash_data->fetch_group_size_data();
		echo "<pre>";
		print_r($results);
		echo "</pre>";
	}
	//return data in groups rather than individual functions like above
	function tee_sheet_data()
	{
		$results = $this->Dash_data->fetch_tee_sheet_data();
		echo json_encode($results);
	}
	function pos_data()
	{
		$results = $this->Dash_data->fetch_pos_data();
		echo json_encode($results);
	}
	function customer_data()
	{
		$results = $this->Dash_data->fetch_customer_data();
		echo json_encode($results);
	}
	/*
	 * end of development functions
	 */



	function fetch_players_data()
	{
		echo json_encode($this->Dash_data->fetch_players_data());
	}
	function fetch_missed_tee_time_data()
	{
		//$this->load->model('Dashboard');
		echo json_encode($this->Dash_data->fetch_missed_tee_time_data());
	}
	function fetch_revenue_data()
	{
		//$this->load->model('Dashboard');
		echo json_encode($this->Dash_data->fetch_revenue_data());
	}
	function fetch_tee_time_data()
	{
		//$this->load->model('Dashboard');
		echo json_encode($this->Dash_data->fetch_tee_time_data());
	}
	function fetch_tee_time_table_data()
	{
		echo json_encode($this->Dash_data->fetch_tee_time_table_data());
	}
	function fetch_mercury_data()
	{
		//$this->load->model('Dashboard');
		echo json_encode($this->Dash_data->fetch_mercury_data());
	}
	function fetch_text_data()
	{
		//$this->load->model('Dashboard');
		echo json_encode($this->Dash_data->fetch_text_data());
	}
	function emails()
	{
		$data['com_stats'] = $this->get_communication_stats();
		$data['bill_stats'] = $this->get_billing_stats();

	}
	function get_communication_stats()
	{
		$this->load->model('Communication');
		$com_stats = $this->Communication->get_stats();
		return $com_stats;
	}
	function get_billing_stats()
	{
		$bill_stats = $this->Billing->get_monthly_limits();
		return $bill_stats;
	}
	function get_teetime_stats()
	{
		$tts = $this->Teetime->get_teetime_stats();
		$btts = $this->Billing->get_teetime_stats();
		$teetime_stats = array_merge($tts[0], $btts);
		return $teetime_stats;
	}
	function get_teetime_course_stats()
	{
		$tts = $this->Teetime->get_teetime_course_stats();
		$tcs = $this->Billing->get_teetime_course_stats();
		foreach ($tts as $course_id => $tt)
			$tcs[$course_id] = (isset($tcs[$course_id]))?array_merge($tcs[$course_id], $tt):$tt;
		return $tcs;
	}
	function get_mercury_stats()
	{
		return $this->Sale->get_mercury_stats();
	}
	function get_mercury_course_stats()
	{
		return $this->Sale->get_mercury_course_stats();
	}
  public function get_general_stats()
  {
    $data = array();

    $total = new StdClass();

    $total->pivot = 'Total';
    $total->course_count = number_format($this->Course->count_all());
    $total->customer_count = number_format($this->Customer->count_all());
    $total->employee_count = number_format($this->Employee->count_all());
    $total->teetimes_booked = number_format($this->Teetime->count_all());
    $total->online_bookings = number_format($this->Teetime->get_online_bookings_count());
    $total->total_sales = '$' . number_format($this->Sale->get_total_sales(), 2, '.', ',');
    $total->mercury_sales = '$' . number_format($this->Sale->get_total_mecury_sales(), 2, '.', ',');
    $data[] = $total;

    $weekly = new StdClass();

    $weekly->pivot = 'Past Week';
    $weekly->course_count = number_format($this->Course->count_all());
    $weekly->customer_count = number_format($this->Customer->count_all());
    $weekly->employee_count = number_format($this->Employee->count_all());
    $weekly->teetimes_booked = number_format($this->Teetime->count_all(true));
    $weekly->online_bookings = number_format($this->Teetime->get_online_bookings_count(true));
    $weekly->total_sales = '$' . number_format($this->Sale->get_total_sales(true), 2, '.', ',');
    $weekly->mercury_sales = '$' . number_format($this->Sale->get_total_mecury_sales(true), 2, '.', ',');
    $data[] = $weekly;

    return $data;
  }

  private function get_paid_teetimes()
  {
    $data = array();

    $total = new StdClass();
    $total->sold_teetimes = number_format($this->Bartered_teetimes->get_total_sold_tee_times());
    $total->potential_teetimes = '';
    $total->revenue = '';
    $total->potential_revenue = '';
    $total->most_sold = '';
    $total->most_earned = '';

    $data['total'] = $total;

    $weekly = new StdClass();
    $weekly->sold_teetimes = '';
    $weekly->potential_teetimes = '';
    $weekly->revenue = '';
    $weekly->potential_revenue = '';
    $weekly->most_sold = '';
    $weekly->most_earned = '';

    $data['weekly'] = $weekly;

    return $data;
  }
}//EO Dashboard class
//EOF
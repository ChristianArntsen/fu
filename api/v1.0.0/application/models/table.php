<?php
class Table extends CI_Model
{
	function get_all($order_by_table_id = false)
	{
		$this->db->from('tables');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		if ($order_by_table_id)
			$this->db->order_by('table_id');
		else
			$this->db->order_by('sale_id');
		return $this->db->get();
	}

	public function get_info($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get();
	}

	function exists($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function get_id_by_table_number($table_number)
	{
		$this->db->from('tables');
		$this->db->where('table_id',$table_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row_array();

		if ($query->num_rows()==1)
			return $result['sale_id'];
		else
			return false;
	}

	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$success = $this->db->update('tables',$sale_data);

		return $success;
	}

	function save($items,$customer_id,$employee_id,$comment,$payments,$sale_id=false,$table_id=false)
	{
		if (!$sale_id || $sale_id == -1)
		{
			$sale_id = $this->get_id_by_table_number($table_id);
		}

		$payment_types='';
		foreach($payments as $payment_id=>$payment)
		{
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
		}

		$sales_data = array(
			'sale_time' => date('Y-m-d H:i:s'),
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'payment_type'=>$payment_types,
			'comment'=>$comment,
			'deleted' => 0,
			'course_id'=>$this->session->userdata('course_id'),
			'table_id'=>$table_id
		);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		if ($sale_id)
		{
			$this->db->delete('table_payments', array('sale_id' => $sale_id));
			$this->db->delete('table_items_taxes', array('sale_id' => $sale_id));
			$this->db->delete('table_items', array('sale_id' => $sale_id));
			$this->db->delete('table_item_kits_taxes', array('sale_id' => $sale_id));
			$this->db->delete('table_item_kits', array('sale_id' => $sale_id));
			$this->db->delete('table_items_modifiers', array('sale_id' => $sale_id));

			$this->db->where('sale_id', $sale_id);
			$this->db->update('tables', $sales_data);
		}
		else
		{
			$this->db->insert('tables',$sales_data);
			$sale_id = $this->db->insert_id();
		}
		//echo $this->db->last_query();
		foreach($payments as $payment_id=>$payment)
		{
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$payment['payment_type'],
				'payment_amount'=>$payment['payment_amount']
			);
			$this->db->insert('table_payments',$sales_payments_data);
		}

		foreach($items as $line=>$item)
		{
			if (isset($item['item_id']))
			{
				$cur_item_info = $this->Item->get_info($item['item_id']);

				$sales_items_data = array
				(
					'sale_id'=>$sale_id,
					'item_id'=>$item['item_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'serialnumber'=>$item['serialnumber'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_cost_price' => $cur_item_info->cost_price,
					'item_unit_price'=>$item['price'] + $item['modifier_total']
					);

				$this->db->insert('table_items',$sales_items_data);

				// Insert any modifiers
				if(!empty($item['modifiers'])){
					foreach($item['modifiers'] as $key => $modifier){
						$modifierData = array();
						$modifierData['sale_id'] = $sale_id;
						$modifierData['item_id'] = $item['item_id'];
						$modifierData['line'] = $item['line'];
						$modifierData['modifier_id'] = $modifier['modifier_id'];
						$modifierData['selected_option'] = $modifier['selected_option'];

						if(empty($modifier['selected_price'])){
							$modifier['selected_price'] = '0.00';
						}

						$modifierData['selected_price'] = $modifier['selected_price'];

						$this->db->insert('table_items_modifiers', $modifierData);
					}
				}
			}
			else
			{
				$cur_item_kit_info = $this->Item_kit->get_info($item['item_kit_id']);

				$sales_item_kits_data = array
				(
					'sale_id'=>$sale_id,
					'item_kit_id'=>$item['item_kit_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_kit_cost_price' => $cur_item_kit_info->cost_price,
					'item_kit_unit_price'=>$item['price']
				);

				$this->db->insert('table_item_kits',$sales_item_kits_data);
			}

			$customer = $this->Customer->get_info($customer_id);
 			if ($customer_id == -1 or $customer->taxable)
 			{
				if (isset($item['item_id']))
				{
					foreach($this->Item_taxes->get_info($item['item_id']) as $row)
					{
						$this->db->insert('table_items_taxes', array(
							'sale_id' 	=>$sale_id,
							'item_id' 	=>$item['item_id'],
							'line'      =>$item['line'],
							'name'		=>$row['name'],
							'percent' 	=>$row['percent'],
							'cumulative'=>$row['cumulative']
						));
					}
				}
				else
				{
					foreach($this->Item_kit_taxes->get_info($item['item_kit_id']) as $row)
					{
						$this->db->insert('table_item_kits_taxes', array(
							'sale_id' 		=>$sale_id,
							'item_kit_id'	=>$item['item_kit_id'],
							'line'      	=>$item['line'],
							'name'			=>$row['name'],
							'percent' 		=>$row['percent'],
							'cumulative'	=>$row['cumulative']
						));
					}
				}
			}
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}

		return $sale_id;
	}

	function delete($sale_id)
	{
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->delete('table_receipts', array('sale_id' => $sale_id));
		$this->db->delete('table_receipt_items', array('sale_id' => $sale_id));
		$this->db->delete('table_payments', array('sale_id' => $sale_id));
		$this->db->delete('table_items_taxes', array('sale_id' => $sale_id));
		$this->db->delete('table_items_modifiers', array('sale_id' => $sale_id));
		$this->db->delete('table_items', array('sale_id' => $sale_id));
		$this->db->delete('table_item_kits_taxes', array('sale_id' => $sale_id));
		$this->db->delete('table_item_kits', array('sale_id' => $sale_id));
		$this->db->delete('tables', array('sale_id' => $sale_id));
		//$this->db->where('sale_id', $sale_id);
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->update('sales_suspended', array('deleted' => 1));


		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	function get_sale_items($sale_id)
	{
		$this->db->from('table_items');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_sale_item_modifiers($sale_id){

		if(empty($sale_id)){
			return false;
		}

		$this->db->select('m.modifier_id, m.name, s.selected_price, m.default_price AS price,
			m.options, s.line, s.item_id, s.selected_option AS selected_option', false);
		$this->db->from('modifiers AS m');
		$this->db->join('table_items_modifiers AS s', 's.modifier_id = m.modifier_id', 'inner');
		$this->db->where(array('s.sale_id' => $sale_id));
		$this->db->order_by('s.line');

		$query = $this->db->get();
		$modifiers = $query->result_array();
		$rows = array();

		// Loop through rows and decode JSON options into array
		foreach($modifiers as $key => $row){

			if(!empty($row['options'])){
				$row['options'] = json_decode($row['options'], true);
				$row['options'][] = 'no';
			}
			// If no options set, just default to 'yes' or 'no'
			if(empty($row['options']) || empty($row['options'][0])){
				$row['options'] = array('yes','no');
			}

			$rows[$row['line']][$row['modifier_id']] = $row;
		}

		return $rows;
	}

	function get_sale_total($sale_id)
	{
		$items = $this->get_sale_items($sale_id)->result_array();
		$item_kits = $this->get_sale_item_kits($sale_id)->result_array();

		$total = 0;
		foreach ($items as $item)
			$total += $item['quantity_purchased'] * $item['item_unit_price'] * (100 - $item['discount_percent']) / 100;
		foreach ($item_kits as $item_kit)
			$total += $item_kit['quantity_purchased'] * $item_kit['item_kit_unit_price'] * (100 - $item_kit['discount_percent']) / 100;
		return $total;
	}

	function get_sale_item_kits($sale_id)
	{
		$this->db->from('table_item_kits');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('table_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_customer($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		return $this->Customer->get_info($this->db->get()->row()->customer_id);
	}

	function get_comment($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get()->row()->comment;
	}
}
?>

-- Move all usernames and passwords from customer table to user table
INSERT IGNORE INTO `foreup_users` (`id`, `username`, `password`)
SELECT person_id AS id, username, password FROM `foreup_customers`;

-- Drop username and password columns from customer table
ALTER TABLE `foreup_customers`
  DROP `username`,
  DROP `password`;
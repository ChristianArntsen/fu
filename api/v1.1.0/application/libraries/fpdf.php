<?php
require('fpdf/fpdf.php');

class PDF extends FPDF
{
	private $start_date = '';
	private $end_date = '';
	private $header_labels = array();
	
	function SetDates($start_date = '', $end_date = '')
	{
		$this->start_date = date('F j, Y', strtotime($start_date));
		$this->end_date = date('F j, Y', strtotime($end_date));
	}
	function SetHeaderLabels($header_labels) {
		$this->header_labels = $header_labels;
	}
	//PageHeader
	function Header() 
	{
		$this->SetFontStyle('title');
		$CI = get_instance();
		$course_name = $CI->session->userdata('course_name');
		// Framed title
	    $this->Cell(550,20,$course_name,0,0,'C');
	    // Line break
	    $this->Ln(20);
		$this->SetFontStyle('sub_title');
		$this->Cell(550,16,'Detailed Departments Report',0,0,'C');	
		$this->Ln(15);
		if ($this->start_date != '') {
			$this->SetFontStyle('normal');
			$this->Cell(275,16,'Starting date: '.$this->start_date,0,0,'C');
		}
		if ($this->end_date != '') {	
			$this->SetFontStyle('normal');
			$this->Cell(275,16,'Ending date: '.$this->end_date,0,0,'C');
		}
		$this->Ln(15);
		
		$default_header = array('w'=>100,'h'=>20,'v'=>'','b'=>0,'l'=>0,'a'=>'L');
		// Header
		$h = $this->header_labels;
		$this->SetFontStyle('header');
		foreach($h as $cell) {
			//In case our cell data is missing any values
			$c = $cell;
			$c = array_merge($default_header, $c);
			$this->Cell($c['w'],$c['h'],$c['v'],$c['b'],$c['l'],$c['a']);
		}
		$this->Ln();
	}
	//PageFooter
	function Footer()
	{
	    // Go to 1.5 cm from bottom
	    $this->SetY(-15);
		$this->SetFontStyle('footer');
	    // Print centered page number
	    $this->Cell(0,10,'Page '.$this->PageNo().' Printed '.date('F j, Y').' using ForeUP',0,0,'C');
	}
	
// Load data
function LoadData($file)
{
	// Read file lines
	$lines = file($file);
	$data = array();
	foreach($lines as $line)
		$data[] = explode(';',trim($line));
	return $data;
}

// Simple table
function BasicTable($header, $data)
{
	// Header
	foreach($header as $col)
		$this->Cell(40,7,$col,1);
	$this->Ln();
	// Data
	foreach($data as $row)
	{
		foreach($row as $col)
			$this->Cell(40,6,$col,1);
		$this->Ln();
	}
}
function SetFontStyle($style = '')
{
	switch($style){
		case 'normal':
			$this->SetFont('Arial','',8);
			break;
		case 'header':
			$this->SetFont('Arial','B',11);
			break;
		case 'title':
			$this->SetFont('Arial','B',16);
			break;
		case 'sub_title':
			$this->SetFont('Arial','B',14);
			break;
		case 'header_text':
			$this->SetFont('Arial','',12);
			break;
		case 'footer':
			    // Select Arial italic 8
	    	$this->SetFont('Arial','I',8);
			break;
		
	}
}

// Better table
function ImprovedTable($header, $data)
{
	//$default_header = array('w'=>100,'h'=>20,'v'=>'','b'=>0,'l'=>0,'a'=>'L');
	$default_cell = array('w'=>100,'h'=>20,'v'=>'','b'=>0,'l'=>0,'a'=>'L');
	//pass in (w)idth, (h)eight, (a)lignment, (b)orders, (v)alue, (p)adding etc with $header and $data
	// Column widths
	// Header
	//$h = $header;
	$d = $data;
	//$this->SetFontStyle('header');
	//foreach($h as $cell) {
		//In case our cell data is missing any values
	//	$c = $cell;
	//	$c = array_merge($default_header, $c);
	//	$this->Cell($c['w'],$c['h'],$c['v'],$c['b'],$c['l'],$c['a']);
	//}
	//$this->Ln();
	// Data
	$this->SetFontStyle('normal');
	foreach($d as $row)
	{
		foreach($row as $cell_index => $cell)
		{
			$c = $cell;
			$c = array_merge($default_cell, $c);
			if ($c['p'] > 0) {
				$this->Cell($c['p']);
				$c['w'] -= $c['p'];
			}
			$this->Cell($c['w'],$c['h'],$c['v'],$c['b'],$c['l'],$c['a']);
		}
//		$this->Cell($w[1],0,$row[1],'LR');
	//	$this->Cell($w[2],0,number_format($row[2]),'LR',0,'R');
		//$this->Cell($w[3],0,number_format($row[3]),'LR',0,'R');
		$this->Ln();
	}
	// Closing line
	//$this->Cell(array_sum($w),0,'','T');
}

// Colored table
function FancyTable($header, $data)
{
	// Colors, line width and bold font
	$this->SetFillColor(255,0,0);
	$this->SetTextColor(255);
	$this->SetDrawColor(128,0,0);
	$this->SetLineWidth(.3);
	//$this->SetFont('','B');
	$pdf->SetFont('Arial','',14);
	
	// Header
	$w = array(40, 35, 40, 45);
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
	$this->Ln();
	// Color and font restoration
	$this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetFont('');
	// Data
	$fill = false;
	foreach($data as $row)
	{
		$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
		$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
		$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
		$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
	}
	// Closing line
	$this->Cell(array_sum($w),0,'','T');
}
}

/*$pdf = new PDF();
// Column headings
$header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
// Data loading
$data = $pdf->LoadData('countries.txt');
$pdf->SetFont('Arial','',14);
$pdf->AddPage();
$pdf->BasicTable($header,$data);
$pdf->AddPage();
$pdf->ImprovedTable($header,$data);
$pdf->AddPage();
$pdf->FancyTable($header,$data);
$pdf->Output();*/
?>

<?php
$lang['suppliers_new']='New Supplier';
$lang['suppliers_supplier']='Supplier';
$lang['suppliers_update']='Update Supplier';
$lang['suppliers_confirm_delete']='Are you sure you want to delete the selected suppliers?';
$lang['suppliers_none_selected']='You have not selected any suppliers to delete';
$lang['suppliers_error_adding_updating'] = 'Error adding/updating supplier';
$lang['suppliers_successful_adding']='You have successfully added supplier';
$lang['suppliers_export']='Export Suppliers';
$lang['suppliers_import']='Import Suppliers';
$lang['suppliers_successful_updating']='You have successfully updated supplier';
$lang['suppliers_successful_deleted']='You have successfully deleted';
$lang['suppliers_one_or_multiple']='supplier(s)';
$lang['suppliers_cannot_be_deleted']='Could not deleted selected suppliers, one or more of the selected suppliers has sales.';
$lang['suppliers_basic_information']='Supplier Information';
$lang['suppliers_account_number']='Account #';
$lang['suppliers_company_name']='Company Name';
$lang['suppliers_company_name_required'] = 'Company Name is a required field';
$lang['suppliers_download_excel_import_template'] = 'Download Import Excel Template (CSV)';
$lang['suppliers_format_account_numbers'] = 'Your import has failed due to improperly formatted account numbers. Please use number format and attempt reupload.';
$lang['suppliers_most_imported_some_failed'] = 'Most suppliers imported. But some were not, here is list of their CODE';
$lang['suppliers_import_successfull'] = 'Import Suppliers successful';
?>
<?php
class menu extends CI_Model
{
    function add_menu($start_time, $end_time, $title)
    {       
        $this->db->insert('menus', array('title'=>trim($title), 'start_time'=>$start_time, 'end_time'=>$end_time, 'course_id'=>$this->session->userdata('course_id')));
        
        return $this->db->insert_id();
    }
    function save_menu_name($start_time, $end_time, $title) 
    {
        $this->db->where('menu_id', $group_id);
	return $this->db->update('menus', array( 'title'=>$title, 'start_time'=>$start_time, 'end_time'=>$end_time,'course_id'=>$this->config->item('course_id')));
    }
    function delete_menu($menu_id)
    {      
        $this->db->query("DELETE FROM foreup_menus WHERE menu_id='$menu_id'");
    }
    function get_menu_info($customer_it = '', $search = false)
    {
        $result = $this->db->query("SELECT * FROM foreup_menus WHERE course_id='" . $this->config->item('course_id')."' GROUP BY menu_id");
        return  $result->result_array();
    }
    // Return information for which menu items are associated with this menu_id
    function get_menu_data($menu_id)
    {
        $result = $this->db->query("SELECT title FROM foreup_menus WHERE menu_id='$menu_id'");
        $menu_name_result = $result->result_array();
        $menu_name = $menu_name_result[0]['title'];
            //log_message('error', 'MENU NAME: '. $menu_name);
        
        $course_id = $this->config->item('course_id');
        $result = $this->db->query("SELECT item_id FROM foreup_menu_items WHERE menu_id='$menu_id'");
        $checked_item_ids = $result->result_array();
        
        $item_id_checked = array();       
        $category_list = array();
        
        
        foreach ($checked_item_ids as $key=>$id)
        {           
            $item_id_checked[$checked_item_ids[$key]['item_id']] = 1;               
        }
        
        $result = $this->db->query("SELECT * FROM foreup_items WHERE course_id='$course_id' AND food_and_beverage='1' AND deleted='0'");
        $all_available_items = $result->result_array();
        foreach ($all_available_items as $item)
        {
            
            //this assumes that there will always be a category
            $category_list['menu_data'][$item['category']]['item_id'][] = $item['item_id'];
            $category_list['menu_data'][$item['category']]['item_name'][] = $item['name'];
            if (!isset($item_id_checked[$item['item_id']]))
            {              
                $item_id_checked[$item['item_id']] = 0;
            }           
        }
        
        $category_list['item_checks'] = $item_id_checked;
        $category_list['menu_name'] = $menu_name;
        $category_list['menu_id'] = $menu_id;
        return $category_list;
        
    }
    
}        


?>

<ul id="error_message_box"></ul>
<?php
echo form_open('teesheets/save_booking_class/'.$booking_class_info->booking_class_id,array('id'=>'booking_class_form'));
?>
<fieldset id="booking_class_basic_info">
<legend><?php echo lang("teesheets_booking_class_basic_info"); ?></legend>
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_teesheet_title').':', 'bc_name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'bc_name',
			'size'=>'20',
			'id'=>'bc_name',
			'value'=>$booking_class_info->name)
		);?>
	</div>
	</div><div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_active').':', 'active',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_radio("active",0,$booking_class_info->active==1?FALSE:TRUE); ?>
		Off
		<?php echo form_radio("active",1,$booking_class_info->active==1?TRUE:FALSE); ?>
		On
		</div>
	</div> 
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_price_class').':', 'bc_price_class'); ?>
		<div class='form_field'>
		<?php
			echo form_dropdown('bc_price_class', $price_classes, $booking_class_info->price_class);
		?>
		</div>
	</div>

	<div class="field_row clearfix">	
    <?php echo form_label(lang('config_online_open_time').':', 'booking_class_open_time',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('booking_class_open_time', array(
                    '0000'=>'12:00am',
                    '0030'=>'12:30am',
                    '0100'=>'1:00am',
                    '0130'=>'1:30am',
                    '0200'=>'2:00am',
                    '0230'=>'2:30am',
                    '0300'=>'3:00am',
                    '0330'=>'3:30am',
                    '0400'=>'4:00am',
                    '0430'=>'4:30am',
                    '0500'=>'5:00am',
                    '0530'=>'5:30am',
                    '0600'=>'6:00am',
                    '0630'=>'6:30am',
                    '0700'=>'7:00am',
                    '0730'=>'7:30am',
                    '0800'=>'8:00am',
                    '0830'=>'8:30am',
                    '0900'=>'9:00am',
                    '0930'=>'9:30am',
                    '1000'=>'10:00am',
                    '1030'=>'10:30am',
					'1100'=>'11:00am',
					'1130'=>'11:30am',
					'1200'=>'12:00pm',
					'1230'=>'12:30pm',
					'1300'=>'1:00pm',
					'1330'=>'1:30pm',
					'1400'=>'2:00pm',
					'1430'=>'2:30pm',
					'1500'=>'3:00pm',
					'1530'=>'3:30pm',
					'1600'=>'4:00pm',
                    '1630'=>'4:30pm',
                    '1700'=>'5:00pm',
                    '1730'=>'5:30pm',
                    '1800'=>'6:00pm',
                    '1830'=>'6:30pm',
                    '1900'=>'7:00pm',
                    '1930'=>'7:30pm',
                    '2000'=>'8:00pm',
                    '2030'=>'8:30pm',
                    '2100'=>'9:00pm',
                    '2130'=>'9:30pm',
                    '2200'=>'10:00pm',
                    '2230'=>'10:30pm',
					'2300'=>'11:00pm',
					'2330'=>'11:30pm',
					'2400'=>'12:00am'),
                    $booking_class_info->online_open_time);
            ?>
            <span class="settings_note">
                (First available online tee time)
            </span>
        </div>
    </div>
    <div class="field_row clearfix">	
    <?php echo form_label(lang('config_online_close_time').':', 'booking_class_close_time',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('booking_class_close_time', array(
                    '0000'=>'12:00am',
                    '0030'=>'12:30am',
                    '0100'=>'1:00am',
                    '0130'=>'1:30am',
                    '0200'=>'2:00am',
                    '0230'=>'2:30am',
                    '0300'=>'3:00am',
                    '0330'=>'3:30am',
                    '0400'=>'4:00am',
                    '0430'=>'4:30am',
                    '0500'=>'5:00am',
                    '0530'=>'5:30am',
                    '0600'=>'6:00am',
                    '0630'=>'6:30am',
                    '0700'=>'7:00am',
                    '0730'=>'7:30am',
                    '0800'=>'8:00am',
                    '0830'=>'8:30am',
                    '0900'=>'9:00am',
                    '0930'=>'9:30am',
                    '1000'=>'10:00am',
                    '1030'=>'10:30am',
					'1100'=>'11:00am',
					'1130'=>'11:30am',
					'1200'=>'12:00pm',
					'1230'=>'12:30pm',
					'1300'=>'1:00pm',
					'1330'=>'1:30pm',
					'1400'=>'2:00pm',
					'1430'=>'2:30pm',
					'1500'=>'3:00pm',
					'1530'=>'3:30pm',
					'1600'=>'4:00pm',
                    '1630'=>'4:30pm',
                    '1700'=>'5:00pm',
                    '1730'=>'5:30pm',
                    '1800'=>'6:00pm',
                    '1830'=>'6:30pm',
                    '1900'=>'7:00pm',
                    '1930'=>'7:30pm',
                    '2000'=>'8:00pm',
                    '2030'=>'8:30pm',
                    '2100'=>'9:00pm',
                    '2130'=>'9:30pm',
                    '2200'=>'10:00pm',
                    '2230'=>'10:30pm',
					'2300'=>'11:00pm',
					'2330'=>'11:30pm',
					'2400'=>'12:00am'),
                    $booking_class_info->online_close_time);
            ?>
                <span class="settings_note">
                    (Last available online tee time)
                </span>
            </div>
    </div>
	<div class="field_row clearfix">	
    <?php echo form_label(lang('config_online_booking_protected').':', 'online_booking_protected',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_dropdown('online_booking_protected', array(
                '0'=>'Off',
                '1'=>'On'),
                $booking_class_info->online_booking_protected);
        ?>
        </div>
    </div>
        
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_days_in_booking_window').':', 'bc_days_in_booking_window',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php 
				echo form_dropdown('bc_days_in_booking_window', array('7'=>'7', '10'=>'10', '14'=>'14'), $booking_class_info->days_in_booking_window, 'id="bc_days_in_booking_window"'); 
			?>
		</div>
	</div>  
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_minimum_players').':', 'bc_minimum_players',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php 
				echo form_dropdown('bc_minimum_players', array('4'=>'4', '3'=>'3', '2'=>'2', '1'=>'1'), $booking_class_info->minimum_players, 'id="bc_minimum_players"'); 
			?>
		</div>
	</div>  
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_limit_holes').':', 'bc_limit_holes',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php 
				echo form_dropdown('bc_limit_holes', array('0'=>'No Limit', '9'=>'9 only', '18'=>'18 only'), $booking_class_info->limit_holes, 'id="bc_limit_holes"'); 
			?>
		</div>
	</div>  
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_booking_carts').':', 'bc_booking_carts',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php 
				echo form_dropdown('bc_booking_carts', array('0'=>'Off', '1'=>'On'), $booking_class_info->booking_carts, 'id="bc_booking_carts"'); 
			?>
		</div>
	</div>  
	<?php echo form_hidden('teesheet_id', $teesheet_id)?>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#booking_class_form').validate({
		submitHandler:function(form)
		{
			if ($.trim($('#bc_name').val()) != '')
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						$('#online_booking_classes').html(response);
						$.colorbox2.close();
						submitting = false;
					},
					dataType:'html'
				});
			}
			else
				alert('Please enter a booking class name');

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
});
</script>
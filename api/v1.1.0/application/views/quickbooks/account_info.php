<script>
$(function(){
	$('#refresh-quickbooks-accounts').off().on('click', function(e){
		var url = $(this).attr('href');
		$.get(url, null, function(response){
			if(response.success){
				set_feedback(response.message,'success_message',false);		
			}else{
				set_feedback(response.message,'error_message',true);		
			}			
		},'json');
		return false;
	});

	$('#disable_quickbooks').off().on('click', function(e){
		if(confirm('Are you sure you want to disable QuickBooks?')){
			var url = $(this).attr('href');
			$.get(url, null, function(response){
				if(response.success){
					set_feedback(response.message,'success_message',false);
					$('#quickbooks_account').html('');
					$('#enable_quickbooks').show();
				}else{
					set_feedback(response.message,'error_message',true);		
				}			
			},'json');
		}

		return false;
	});	
});
</script>
<a title="Download QWC File" class="terminal_button" style="width: 155px; display: block; margin: 0px;" href="<?php echo site_url('qb/create_qwc'); ?>">Download QWC File</a>
<a href="<?php echo site_url('qb/refresh_accounts'); ?>" style="position: absolute; right: 10px; top: 10px;" id="refresh-quickbooks-accounts">Refresh Account List</a>
<div class="field_row clearfix">	
	<?php echo form_label('QWC Username:', 'qb_username',array('class'=>'wide')); ?>
	<?php echo form_input(array(
		'name'=>'qb_username',
		'id'=>'qb_username',
		'placeholder'=>'',
		'value'=>$quickbooks['qb_username'],
		'disabled'=>true));?>
	<input type="hidden" name="qb_username" value="<?php echo $quickbooks['qb_username']; ?>" />
</div>
<div class="field_row clearfix">	
	<?php echo form_label('QWC Password:', 'qb_password',array('class'=>'wide')); ?>
	<?php echo form_input(array(
		'name'=>'qb_password',
		'id'=>'qb_password',
		'placeholder'=>'',
		'value'=>$quickbooks['qb_password'],
		'disabled'=>true));?>	
</div>
<div class="field_row clearfix">	
	<?php echo form_label('Company File Location:', 'qb_company_file',array('class'=>'wide')); ?>
	<?php echo form_input(array(
		'name'=>'qb_company_file',
		'id'=>'qb_company_file_location',
		'placeholder'=>'',
		'style'=>'width: 400px;',
		'value'=>$quickbooks['qb_company_file']));?>	
</div>
<div class="field_row clearfix">
	<?php echo form_label('Sales Account', 'qb_sales_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_sales_account', $quickbooks['accounts'], $quickbooks['sales_account']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Redemption Account', 'qb_redemption_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_redemption_account', $quickbooks['accounts'], $quickbooks['redemption_account']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Tips Account', 'qb_tips_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_tips_account', $quickbooks['accounts'], $quickbooks['tips_account']); ?>
	</div>
</div>
<?php 
echo form_submit(array(
	'name'=>'submit_quickbooks',
	'id'=>'submit_quickbooks',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
<a id="disable_quickbooks" href="<?php echo site_url('qb/disable'); ?>" style="color: #AA0000; float: right;">Disable QuickBooks</a>
<?php if ($course) { ?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo lang('login_reset_password'); ?></title>
</head>
<body>
<?php echo lang('login_you_requested_password_reset').' '.$course->name; ?><br />
<?php echo lang('login_please_click_link'); ?> <br /><br/>
<?php echo anchor('customer_login/reset_password_enter_password/'.$reset_key, lang('login_reset_password')); ?><br/><br/>
<?php echo lang('login_thank_you'); ?><br/><br/>
<?php echo lang('login_if_you_have_questions').' '.$course->name;?><br/>
<?php echo lang('login_at_phone').' '.$course->phone; ?><br/><br/>

</body>
</html>	
<?php } else { ?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo lang('login_reset_password'); ?></title>
</head>
<body>
<?php echo lang('login_reset_password_message'); ?><br /><br />
<?php echo anchor('customer_login/reset_password_enter_password/'.$reset_key, lang('login_reset_password')); ?>
</body>
</html>
<?php } ?>
<?php
echo form_open('customers/save_email/'.$customer_id.'/'.$type,array('id'=>'add_email_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_email">
<legend></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('new_email').':', 'email'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'id'=>'new_email',
		'size'=>'40')
	);?>
	</div>
</div>

</fieldset>
<div class='clear' style='text-align:center'>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</div>
<?php 
echo form_close();
?>

<script>
$(document).ready(function()
{
	$('#new_email').select();
	
	var submitting = false;
    $('#add_email_form').validate({    	    	
		submitHandler:function(form)
		{			
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{			
				console.log(response);	
				
				$('#add_customer_email').addClass('hidden');
				$('#email_chckbox').removeClass('hidden');
				$('#email_container').html(response.email);
				$.colorbox2.close();	
				// customer.billing.create(response.type, response.person_id);			
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{						
    		email: "email"
   		},
		messages: 
		{     		
     		email: "<?php echo lang('common_email_invalid_format'); ?>"
		}
	});
});
	
</script>
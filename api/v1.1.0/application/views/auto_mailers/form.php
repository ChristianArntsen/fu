<ul id="error_message_box"></ul>
<?php
echo form_open('auto_mailers/save/'.$auto_mailer->auto_mailer_id,array('id'=>'auto_mailer_form'));
?>
<style>
	.delete_campaign {
		width:20px;
		height:20px;
		display:block;
		background:url(../images/pieces/x_red3.png) transparent;
		margin-right:5px;
		cursor:pointer;
	}
</style>
<fieldset id="auto_mailer_basic_info">
<legend><?php echo lang("auto_mailers_basic_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('common_name').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>$auto_mailer->name,
		'size'=>35)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('auto_mailers_marketing_campaigns').'', 'department',array('class'=>'wide')); ?>
	<div class='form_field'>
<?php echo form_label(lang('auto_mailers_marketing_campaign').'', 'department',array('class'=>'wide', 'style'=>'width:210px;')); ?>
<?php echo form_label(lang('auto_mailers_event').'', 'department',array('class'=>'wide', 'style'=>'width:100px;')); ?>
<?php echo form_label(lang('auto_mailers_days_after_event').'', 'department',array('class'=>'wide', 'style'=>'width:90px;')); ?>
		<button id='add_campaign_row' class='submit_button float_right'>Add</button>

	</div>
</div>
<div id='campaign_row_container'>

<?php 
$c_count = 0;
foreach ($campaigns as $campaign)
{
	$c_count++;
	?>
<div class="field_row clearfix campaign_row" id='campaign_row_<?=$c_count?>'>
<?php echo form_label(/*lang('auto_mailers_marketing_campaigns').*/'', 'department',array('class'=>'wide', 'style'=>'width:148px;')); ?>
	<div class='form_field'>
	<?php //echo form_dropdown('category', $this->Item->get_categories(), $item_info->category);?>
	<table style='margin-left:148px; width:470px;'>
		<tbody>
			<tr>
				<td style='width:20px;'>
					<span class='delete_campaign' onclick='remove_campaign_row(<?=$c_count?>)'></span>
				</td>
				<td style='width:210px;'>
				<?php echo form_input(array(
					'name'=>'campaign_name_'.$c_count,
					'id'=>'campaign_name_'.$c_count,
					'value'=>$campaign['name'],
					'size'=>35)
				);?>
				<?php echo form_hidden('campaign_id_'.$c_count, $campaign['campaign_id']); ?>
				</td>
				<td style='width:150px;'>
				<?php echo form_dropdown('trigger_'.$c_count, array(1=>'on subscribe'), $campaign['trigger']);?>
				</td>
				<td style='width:90px;'>
				<?php echo form_input(array('name'=>'days_after_'.$c_count, 'id'=>'days_after_'.$c_count, 'value'=> $campaign['days_from_trigger'], 'size'=>6));?>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
<script>
	$( "#campaign_name_<?=$c_count?>").autocomplete({
		source: "<?php echo site_url('marketing_campaigns/suggest');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function( event, ui ) 
 		{
 			event.preventDefault();
 			console.log('selecting');
 			console.dir(ui.item);
			$("#campaign_name_<?=$c_count?>").val(ui.item.label);
			$("#campaign_id_<?=$c_count?>").val(ui.item.value);
		}
	});
</script>
<?php } 

$c_count++;
?>

<div class="field_row clearfix campaign_row" id='campaign_row_<?=$c_count?>'>
<?php echo form_label(/*lang('auto_mailers_marketing_campaigns').*/'', 'department',array('class'=>'wide', 'style'=>'width:148px;')); ?>
	<div class='form_field'>
	<?php //echo form_dropdown('category', $this->Item->get_categories(), $item_info->category);?>
	<table style='margin-left:148px; width:470px;'>
		<tbody>
			<tr>
				<td style='width:20px;'>
					<span class='delete_campaign' onclick='remove_campaign_row(<?=$c_count?>)'></span>
				</td>
				<td style='width:210px;'>
				<?php echo form_input(array(
					'name'=>'campaign_name_'.$c_count,
					'id'=>'campaign_name_'.$c_count,
					'value'=>$campaign->campaign_id,
					'size'=>35)
				);?>
				<?php echo form_hidden('campaign_id_'.$c_count, $campaign->campaign_id); ?>
				</td>
				<td style='width:150px;'>
				<?php echo form_dropdown('trigger_'.$c_count, array(1=>'on subscribe'), $campaign->trigger);?>
				</td>
				<td style='width:90px;'>
				<?php echo form_input(array('name'=>'days_after_'.$c_count, 'id'=>'days_after_'.$c_count, 'value'=> 0, 'size'=>6));?>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
</div>
<script>
	$( "#campaign_name_<?=$c_count?>").autocomplete({
		source: "<?php echo site_url('marketing_campaigns/suggest');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function( event, ui ) 
 		{
 			event.preventDefault();
 			console.log('selecting');
 			console.dir(ui.item);
			$("#campaign_name_<?=$c_count?>").val(ui.item.label);
			$("#campaign_id_<?=$c_count?>").val(ui.item.value);
		}
	});
</script>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>

<?php
echo form_close();
?>
<script type='text/javascript'>
console.log('really really before error');
</script>
<script type='text/javascript'>
console.log('really before error');
function add_additional_campaign_row() {
	var campaign_rows = $('.campaign_row');
	console.dir(campaign_rows);
	var cr_count = campaign_rows.length;
	var nr = cr_count + 1;
	console.log('cr_count '+cr_count);
	var html = '<div class="field_row clearfix campaign_row" id="campaign_row_'+nr+'">'+
				'<label for="department" class="wide" style="width:148px;"></label>	<div class="form_field">'+
					'<table style="margin-left:148px; width:470px;">'+
						'<tbody>'+
							'<tr>'+
								'<td style="width:20px;">'+
									'<span class="delete_campaign" onclick="remove_campaign_row('+nr+')"></span>'+
								'</td>'+
								'<td style="width:210px;">'+
									'<input type="text" name="campaign_name_'+nr+'" value="" id="campaign_name_'+nr+'" size="35">'+				
									'<input type="hidden" id="campaign_id_'+nr+'" name="campaign_id_'+nr+'" value="82">'+
								'</td>'+
								'<td style="width:150px;">'+
								'<select id="trigger_'+nr+'" name="trigger_'+nr+'">'+
									'<option value="1">on subscribe</option>'+
								'</select>'+
								'</td>'+
								'<td style="width:90px;">'+
								'<input type="text" name="days_after_'+nr+'" value="0" id="days_after_'+nr+'" size="6" class="valid">'+
								'</td>'+
							'</tr>'+
						'</tbody>'+
					'</table>'+
				'</div>'+
			'</div>';
		$('#campaign_row_container').append(html);
		$( "#campaign_name_"+nr).autocomplete({
			source: "<?php echo site_url('marketing_campaigns/suggest');?>",
			delay: 10,
			autoFocus: false,
			minLength: 0,
			select: function( event, ui ) 
	 		{
	 			event.preventDefault();
	 			console.log('selecting');
	 			console.dir(ui.item);
				$("#campaign_name_"+nr).val(ui.item.label);
				$("#campaign_id_"+nr).val(ui.item.value);
			}
		});
		$.colorbox.resize();
}
function remove_campaign_row(row_number) {
	$('#campaign_id_'+row_number).val('');
	$('#campaign_row_'+row_number).hide();
	$.colorbox.resize();
}
//validation and submit handling
$(document).ready(function()
{
	console.log('before error?');
	$('#add_campaign_row').click(function(e){
		e.preventDefault();
		add_additional_campaign_row();
	});
	
	var submitting = false;
    $('#auto_mailer_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
				//post_item_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required"
   		},
		messages:
		{
			name:"<?php echo lang('auto_mailers_name_required'); ?>"
		}
	});
});
</script>
<script>
	window.billing_config = 
	{
		customer_credit : '',
		member_balance : '',
		cp : '',
		bc : '',
		member_balance_nickname : '',
		customer_credit_nickname : '',
		base_url : ''	
	};
	
	window.billing_config.customer_credit = '<?php echo ($person_info->account_balance); ?>';
	window.billing_config.member_balance = '<?php echo ($person_info->member_account_balance); ?>';
	window.billing_config.member_balance_nickname = '<?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')));  ?>';
	window.billing_config.customer_credit_nickname = '<?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')));  ?>';
	window.billing_config.base_url = "/index.php/customers/load_invoice/0/0/0/0/";
	$(document).ready(function(){
		customer.billing.initialize_controls();
		customer.billing.initialize_invoice_table_controls();
		console.log('finished initializing');
	});
</script>
<fieldset id="billing_info">
	<div id='invoice_time_options'>
		<div id='time_options_box'>
			<?php if ($is_invoice) { ?>
			<div id='first_row_options' class='time_options_row'>
				<div id='time_components'>
					<div>
						Send Date:
						<input id="send_date" name="send_date" value='<?=$invoice['send_date'];?>' placeholder="Send On" type='text' />
					</div>								
					<div>
						Due/Bill Date:
						<input id="due_date" name="due_date" value='<?=$invoice['due_date'];?>' placeholder="Due On" type='text' />
					</div>								
				</div>								
				<div id='save_invoice'>Save</div>
				<!-- <div id='advanced_options'>advanced</div> -->
			</div>	
			<?php } else { ?>
			<div id='first_row_options' class='time_options_row'>
				<div id='time_components'>
					<div>
						Title:
						<input id="billing_title" name="billing_title" value='<?=$billing['title'];?>' placeholder="Billing Title" type='text' />
					</div>								
					<div>
						Frequency: 
						<input type='hidden' id='frequency' name='frequency' value='one_time'/>
						<select id='frequency_selector'>
							<option value='monthly'>Monthly</option>
							<option value='yearly'>Yearly</option>
						</select>
					</div>
					<div>
						Bill on: 
						<select id="bill_month" name="bill_month" class="valid" style='display:none'><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option></select> 
						<select id="bill_day" name="bill_day"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
					</div>
					<div id='advanced_options'>advanced</div>	
				</div>								
				<div id='save_invoice'>Save</div>
				<!-- <div id='advanced_options'>advanced</div> -->
			</div>
			<div id='second_row_options' class='time_options_row'>
				<div>
					Gen. Inv. Days Before:
					<input id="generate_days_before" name="generate_days_before" type="text" size=2 class="" value="<?=$billing['generate_days_before']?>">
				</div>
				<div>
					<input type='hidden' id='customer_id' name='customer_id' value='<?=$person_info->person_id;?>'/>									
					Delayed Start Date:
					<input id="start_date" name="start_date" type="text" placeholder="Start Date" class="">
				</div>
				<div id='month_range'>
					between:
					<select id="start_month" name="start_month" class="valid"><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option></select> 
					<select id="end_month" name="end_month" class="valid"><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12" selected>Dec</option></select> 
				</div>																	
			</div>
			<?php } ?>
		</div>							
	</div>
</fieldset>
<ul id="error_message_box"></ul>
<?php
echo form_open('schedules/save/'.$schedule_info->schedule_id,array('id'=>'schedule_form'));
?>
<fieldset id="schedule_basic_info">
<legend><?php echo lang("schedules_basic_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('schedules_name').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'size'=>'20',
		'id'=>'name',
		'value'=>$schedule_info->title)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('schedules_default').':', 'default',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("default",1,$schedule_info->default); ?>
	</div>
</div>    
<div class="field_row clearfix">	
<?php echo form_label(lang('schedules_type').':', 'type',array('class'=>'wide')); ?>
            <div class='form_field'>
        <?php echo form_dropdown('type', array(
                'tee_sheet'=>'Tee Sheet',
				'bays'=>'Bays',
                'lessons'=>'Lessons'),
                $schedule_info->type);
        ?>
        </div>
        <script>
        	$('#type').change(function(){
    			$('#track_rows .form_field input[name=track_employee[]]').each(function(){
					$(this).addClass('hidden');
				});
    			$('#track_rows .form_field select[name=reround_track_id[]]').each(function(){
					$(this).addClass('hidden');
				});
        	
        		var type = $(this).val();
        		if (type == 'tee_sheet')
        		{
        			$('#frontnine_box').show();
    				$('#track_rows .form_field select[name=reround_track_id[]]').each(function(){
						$(this).removeClass('hidden');
					});
        		}
        		else
        		{
        			$('#frontnine_box').hide();
        			if (type == 'lessons')
        			{
        				$('#track_rows .form_field input[name=track_employee[]]').each(function(){
        					$(this).removeClass('hidden');
        				});
        			}
        		}	
        		$.colorbox.resize();
        	})
        </script>
</div>
<div class="field_row clearfix" id='frontnine_box' <?php echo ($schedule_info->type && $schedule_info->type != 'tee_sheet' ? "style='display:none;'":'') ?>>
<?php echo form_label(lang('schedules_frontnine').':', 'frontnine',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('frontnine', array(
            '140'=>'1 hour 40 min',
            '145'=>'1 hour 45 min',
            '150'=>'1 hour 50 min',
            '155'=>'1 hour 55 min',
            '200'=>'2 hours',
            '205'=>'2 hours 5 min',
            '210'=>'2 hours 10 min',
            '215'=>'2 hours 15 min',
            '220'=>'2 hours 20 min',
            '225'=>'2 hours 25 min',
            '230'=>'2 hours 30 min'),
            $schedule_info->frontnine);
        ?>
	</div>
</div>

<!--div class="field_row clearfix">
<?php echo form_label(lang('schedules_holes').':', 'holes',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('holes', array('9'=>'9','18'=>'18'), $schedule_info->holes, 'id="holes"');?>
	</div>
</div-->
<div class="field_row clearfix">	
<?php echo form_label(lang('schedules_increment').':', 'increment',array('class'=>'wide')); ?>
            <div class='form_field'>
        <?php echo form_dropdown('increment', array(
                '5'=>'5 min',
                '6'=>'6 min',
                '7'=>'7 min',
                '7.5'=>'7/8 min',
                '8'=>'8 min',
                '9'=>'9 min',
                '10'=>'10 min',
                '11'=>'11 min',
                '12'=>'12 min',
                '13'=>'13 min',
                '14'=>'14 min',
                '15'=>'15 min',
                '20'=>'20 min',
                '30'=>'30 min',
				'45'=>'45 min',
                '60'=>'60 min'),
                ($schedule_info->increment?$schedule_info->increment:'10'));
        ?>
            <span class="settings_note">
                (How often each reservation starts)
            </span>
        </div>
</div>
<div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_open_time').':', 'online_open_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_open_time', array(
                        '0000'=>'12:00am',
                        '0030'=>'12:30am',
                        '0100'=>'1:00am',
                        '0130'=>'1:30am',
                        '0200'=>'2:00am',
                        '0230'=>'2:30am',
                        '0300'=>'3:00am',
                        '0330'=>'3:30am',
                        '0400'=>'4:00am',
                        '0430'=>'4:30am',
                        '0500'=>'5:00am',
                        '0530'=>'5:30am',
                        '0600'=>'6:00am',
                        '0630'=>'6:30am',
                        '0700'=>'7:00am',
                        '0730'=>'7:30am',
                        '0800'=>'8:00am',
                        '0830'=>'8:30am',
                        '0900'=>'9:00am',
                        '0930'=>'9:30am',
                        '1000'=>'10:00am',
                        '1030'=>'10:30am',
						'1100'=>'11:00am',
						'1130'=>'11:30am',
						'1200'=>'12:00pm',
						'1230'=>'12:30pm',
						'1300'=>'1:00pm',
						'1330'=>'1:30pm',
						'1400'=>'2:00pm',
						'1430'=>'2:30pm',
						'1500'=>'3:00pm',
						'1530'=>'3:30pm',
						'1600'=>'4:00pm',
                        '1630'=>'4:30pm',
                        '1700'=>'5:00pm',
                        '1730'=>'5:30pm',
                        '1800'=>'6:00pm',
                        '1830'=>'6:30pm',
                        '1900'=>'7:00pm',
                        '1930'=>'7:30pm',
                        '2000'=>'8:00pm',
                        '2030'=>'8:30pm',
                        '2100'=>'9:00pm',
                        '2130'=>'9:30pm',
                        '2200'=>'10:00pm',
                        '2230'=>'10:30pm',
						'2300'=>'11:00pm',
						'2330'=>'11:30pm',
						'2400'=>'12:00am'),
                        $schedule_info->online_open_time);
                ?>
                    <span class="settings_note">
                        (First available online reservation)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_close_time').':', 'online_close_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_close_time', array(
                        '0000'=>'12:00am',
                        '0030'=>'12:30am',
                        '0100'=>'1:00am',
                        '0130'=>'1:30am',
                        '0200'=>'2:00am',
                        '0230'=>'2:30am',
                        '0300'=>'3:00am',
                        '0330'=>'3:30am',
                        '0400'=>'4:00am',
                        '0430'=>'4:30am',
                        '0500'=>'5:00am',
                        '0530'=>'5:30am',
                        '0600'=>'6:00am',
                        '0630'=>'6:30am',
                        '0700'=>'7:00am',
                        '0730'=>'7:30am',
                        '0800'=>'8:00am',
                        '0830'=>'8:30am',
                        '0900'=>'9:00am',
                        '0930'=>'9:30am',
                        '1000'=>'10:00am',
                        '1030'=>'10:30am',
						'1100'=>'11:00am',
						'1130'=>'11:30am',
						'1200'=>'12:00pm',
						'1230'=>'12:30pm',
						'1300'=>'1:00pm',
						'1330'=>'1:30pm',
						'1400'=>'2:00pm',
						'1430'=>'2:30pm',
						'1500'=>'3:00pm',
						'1530'=>'3:30pm',
						'1600'=>'4:00pm',
                        '1630'=>'4:30pm',
                        '1700'=>'5:00pm',
                        '1730'=>'5:30pm',
                        '1800'=>'6:00pm',
                        '1830'=>'6:30pm',
                        '1900'=>'7:00pm',
                        '1930'=>'7:30pm',
                        '2000'=>'8:00pm',
                        '2030'=>'8:30pm',
                        '2100'=>'9:00pm',
                        '2130'=>'9:30pm',
                        '2200'=>'10:00pm',
                        '2230'=>'10:30pm',
						'2300'=>'11:00pm',
						'2330'=>'11:30pm',
						'2400'=>'12:00am'),
                        $schedule_info->online_close_time);
                ?>
                    <span class="settings_note">
                        (Last available online reservation)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_time_limit').':', 'online_time_limit',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_time_limit', array(
                        '120'=>'2 hours',
                        '180'=>'3 hours',
                        '240'=>'4 hours'),
                        $schedule_info->online_time_limit);
                ?>
                    <span class="settings_note">
                        (Last available online reservation)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">
			<?php echo form_label(($index > 0 ? '' : lang('schedules_tracks').':'), 'name',array('class'=>'wide')); ?>
			<div class='form_field'>
				<div id='add_track'>Add Track</div>
			</div>
		</div>
		<div id='track_rows'>
   		<?php foreach ($tracks->result_object() as $track) { 
   			echo $this->track->row_html($track, $tracks, $schedule_info); ?>
		<?php }	?>
		</div>
        <!--div class="field_row clearfix">
	<?php echo form_label(lang('schedules_online_booking').':', 'online_booking',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_checkbox("online_booking",0,$schedule_info->online_booking==1?FALSE:TRUE); ?>
		Off
		<?php echo form_radio("online_booking",1,$schedule_info->online_booking==1?TRUE:FALSE); ?>
		On
		</div>
	</div-->    


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#add_track').click(function(){
		//add row
		$.ajax({
	        url: "<?= site_url("schedules/get_track_row/$schedule_info->schedule_id"); ?>",
	        type: "POST",
	        data: {},
	        success: function( data ) {
	        	$('#track_rows').append(data.row);
	            //update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
	            $.colorbox.resize();
	        },
	        dataType:'json'
	    });
	});
	var submitting = false;
    $('#schedule_form').validate({
		submitHandler:function(form)
		{
			if ($.trim($('#name').val()) != '')
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
						post_schedule_form_submit(response);
		                submitting = false;
					},
					dataType:'json'
				});
			}
			else
				alert('Please enter a schedule name');

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
});
</script>
<?php $this->load->view("partial/header");
$printer_name = $this->config->item('updated_printing')?'foreup':'foreup';
?>
<style>
div.pay_box {
	display: block;
	float: none;
	overflow: hidden;
}

#cart_section {
	overflow: hidden;
}

div.keypad {
	overflow: hidden;
	position: absolute;
	height: 190px;
	padding-top: 10px;
	width: 280px;
	background-color: white;
	box-shadow: 2px 2px 8px #222;
	z-index: 999999;
}

div.keypad div.number_pad {
	overflow: hidden;
	margin: 0px auto;
	padding: 0px;
	width: 260px;
	display: block;
}

div.keypad input {
	margin: 10px;
	display: block !important;
	width: auto !important;
}

#cbox2Content {
	background: none repeat scroll 0 0 #EFEFEF;
}

#cbox2title {
    background: -moz-linear-gradient(center top , #3CACD2, #3090C3, #2474B4) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-radius: 0 0 0 0;
    border-top: 1px solid #9FD7E9;
    color: #FFFFFF;
    font-size: 16px;
    font-weight: lighter;
    padding-left: 20px;
    text-align: left;
    text-shadow: -1px -1px 0 #000000;
}

#splitpayment_register .reg_item_seat {
	width: 6%;
}

#splitpayment_register .reg_item_name {
	width: 42%;
}

th.reg_item_name {
    width: 38%;
}

th.reg_item_seat, td.reg_item_seat {
	width: 10%;
	background-color: #F0F0F0;
	font-weight: bold;
}

th.reg_item_seat {
	text-indent: 0px !important;
}

th.reg_item_seat > div {
	margin-left: -6px;
}

td > a.edit_item.fnb_button {
	margin: 2px !important;
	padding: 0px 0px !important;
	line-height: 34px !important;
	height: 34px !important;
	float: right !important;
	width: 60px !important;
	text-align: center !important;
}

#register td.reg_item_total {
	padding-right: 0px;
	padding: 2px;
}

#register td {
	vertical-align: top !important;
	padding-top: 2px;
}

#register_box {
	max-height: 700px;
}

tbody > tr > td * {
	font-size: 14px;
}

tbody > tr.selected {
	background-color: #E2EEF6 !important;
}

tbody > tr.ordered, tbody > tr.paid {
	background-color: #E5E5E5 !important;
}

tbody > tr.ordered a.edit_item,
tbody > tr.paid a.edit_item {
	visibility: hidden !important;
}

tbody td.reg_item_name > div {
	font-weight: bold;
}

tbody td.reg_item_name ul.modifiers {
	display: block;
	margin: 0px;
	padding: 0px;
}

ul.modifiers > li {
	padding: 4px 0px 4px 0px;
	font-size: 12px;
	line-height: 14px;
	display: block;
}

/* Table layout styles */
#layout_editor {
	display: block;
	position: relative;
	padding: 0px;
	margin: 0px;
	margin-top: 1px;
	height: 616px;
	width: auto;
	background: none;
	overflow: hidden;
}

#layout_editor div.palette {
	right: 0px;
	width: 150px;
	height: 616px;
	padding-top: 15px;
	background: white;
	border-left: 1px solid #DDD;
	z-index: 1;
	float: right;
}

#layout_editor div.tab_content {
	width: 992px;
	height: 590px;
	display: block;
	overflow: hidden;
	float: left;
}

#layout_editor div.tab_content > div {
	display: none;
}

#layout_editor div.tab_content > div.active {
	display: block;
}

div.palette > div.drag {
	margin: 25px auto 0px auto;
}

div.floor_layout > div.drag {
	position: absolute !important;
}

div.object {
	width: 75px;
	height: 75px;
}

div.object > span.label,
div.object > span.employee,
div.object > input {
	display: block;
	text-align: center;
	top: 50%;
	width: 100%;
	float: none;
	position: absolute;
	margin-top: -7px;
}

div.object > input {
	margin: -12px auto 0px auto !important;
	padding: 2px !important;
	float: none;
	display: block;
	left: 15%;
	right: 15%;
}

div.object > span.label {
	font-weight: bold;
	font-size: 14px;
}

div.object > span.employee {
	margin-top: 14px;
}

div.object > input {
	width: 60% !important;
	color: black;
	margin: 0 auto;
	margin-top: -12px;
}

div.object:hover > a.delete-object {
	display: block;
}

div.palette div.object:hover > a.delete-object {
	display: none !important;
}

div.object > a.delete-object {
	display: none;
	position: absolute;
	font-size: 20px;
	background-color: white;
	border: 1px solid #CCC;
	padding: 0px 5px 0px 5px;
	line-height: 20px;
	color: red;
	top: -4px;
	left: -4px;
}

/* Table SVG Styles */
div.object:hover {
	cursor: pointer;
}

div.object:hover .table,
div.object:hover .seat  {
	/*fill: red; */
}

div.object .table {
	fill: #FFFFFF;
}

div.object .seat {
	fill: #FFFFFF;
}

div.object.my-active .table,
div.object.my-active .seat {
	fill: #3483C9 !important;
}

div.object.my-active div.box {
	background-color: #3483C9 !important;
}

div.object.my-active span {
	color: white;
}

div.object.other-active span.label {
	color: #444
}

div.object.other-active .table,
div.object.other-active .seat {
	fill: #EFEFEF !important;
}

div.object.other-active div.box {
	background-color: #B71212 !important;
}

div.object > div.box {
	display: block;
	float: none;
	position: absolute;
	padding: 0px;
	margin: 0px;
	background-color: white;
	border: 1px solid black;
	top: 0px;
	left: 0px;
	right: 0px;
	bottom: 0px;
	width: 100%;
}

div.object.drag {
	width: 75px;
	height: 75px;
	position: relative;
}

div.floor_layout > div.object {
	background-size: 100% 100%;
	background-position: center center;
	background-repeat: no-repeat;
}

#layout_editor div.booth {
	background-image: url('images/restaurant_layout/booth.svg');
	width: 100px;
	height: 100px;
	display: block;
}

#layout_editor div.table_square_4 {
	background-image: url('images/restaurant_layout/table_square_4.svg');
	width: 100px;
	height: 100px;
	display: block;
}

#layout_editor div.table_round_4 {
	background-image: url('images/restaurant_layout/table_round_4.svg');
	width: 100px;
	height: 100px;
	display: block;
}

div.floor_layout {
	width: auto;
	height: 590px;
	display: block;
	background: none;
	z-index: 5;
	overflow: hidden;
}

/* Layout tabs */
#layout_editor ul.tabs {
	display: block;
	width: auto;
	height: 60px;
	background-color: #AAA;
	overflow: hidden;
	margin: 0px;
	padding: 0px;
}

#layout_editor ul.tabs > li.tab {
	width: auto;
	display: block;
	float: left;
	list-style-type: none;
	margin: 14px 10px 0px 0px;
	padding: 0px;
}

#layout_editor ul.tabs > li.tab a {
	display: block;
	background-color: #E0E0E0;
	width: auto;
	padding: 15px 35px;
	font-size: 14px;
	color: #333;
	text-decoration: none;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
}

#layout_editor ul.tabs > li.tab.active a {
	background-color: #EFEFEF;
	font-weight: bold;
	box-shadow: 1px 1px 3px #555;
}

#layout_editor .button {
	display: block;
	float: left;
	margin: 0px 5px 0px 0px;
	background: #349AC5;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top, #349AC5, #4173B3);
	color: white;
	font-size: 14px;
	font-weight: normal;
	height: 32px;
	line-height: 32px;
	padding: 0px 20px;
	width: auto;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}

#layout_editor .button.new {
	float: left !important;
	margin-top: 15px;
	background: #4dad47;
	background: -moz-linear-gradient(top,  #4dad47 0%, #398235 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4dad47), color-stop(100%,#398235));
	background: -webkit-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -o-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -ms-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: linear-gradient(to bottom,  #4dad47 0%,#398235 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4dad47', endColorstr='#398235',GradientType=0 );
}

#colorbox input, #colorbox2 input {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #B1B1B1;
    border-radius: 5px;
    box-shadow: 0 2px 12px -6px #000000 inset;
    color: #666464;
    font-family: "Quicksand", Helvetica, sans-serif;
    font-size: 20px;
    font-weight: lighter;
    line-height: 20px;
    padding: 5px;
}
</style>
<script>
function set_feedback(text, classname, keep_displayed, time_override)
{
	if(text!='' && text!=undefined)
	{
		$('#feedback_bar').removeClass();
		$('#feedback_bar').addClass(classname);
		$('#feedback_bar').html(text+"<span class='message_close_box'></span>");
		$('#feedback_bar').slideDown(250);
		var text_length = text.length;
		var text_lengthx = 0;
		if (time_override != ''&& time_override != undefined)
			text_lengthx = time_override;
		else
			text_lengthx = text_length*50;

		if(!keep_displayed)
		{
			$('#feedback_bar').show();

			setTimeout(function()
			{
				$('#feedback_bar').slideUp(250, function()
				{
					$('#feedback_bar').removeClass();
				});
			},text_lengthx);
		}
	}
	else
	{
		$('#feedback_bar').hide();
	}
}

// JQuery - Keypad plugin
(function ($) {

	var template = '<div class="keypad" style="display: none; position: absolute;">' +
		'<div class="number_pad">' +
			'<div class="number_pad_number number_one" data-char="1"></div>' +
			'<div class="number_pad_number number_two" data-char="2"></div>' +
			'<div class="number_pad_number number_three" data-char="3"></div>' +
			'<div class="number_pad_number number_four" data-char="4"></div>' +
			'<div class="number_pad_number number_five" data-char="5"></div>' +
			'<div class="number_pad_number number_six" data-char="6"></div>' +
			'<div class="number_pad_number number_seven" data-char="7"></div>' +
			'<div class="number_pad_number number_eight" data-char="8"></div>' +
			'<div class="number_pad_number number_nine" data-char="9"></div>' +
			'<div class="number_pad_number number_login" data-char="enter"></div>' +
			'<div class="number_pad_number number_zero" data-char="0"></div>' +
			'<div class="number_pad_number number_backspace" data-char="del"></div>' +
		'</div>' +
	'</div>';

	function format_number(chars, formatMoney){
		var number = chars.replace(/[^0-9]+/g, '');

		if(formatMoney){
			number = number / 100;
			var number = accounting.formatMoney(number, '');
		}
		return number;
	}

	function add_character(input, character){
		var characters = input.val() + character;
		var formatted = format_number(characters, input.data('settings').formatMoney);

		input.val(formatted);
		input.focus();
		return true;
	}

	function remove_character(input){
		var characters = input.val();
		characters = characters.substring(0, characters.length - 1);
		var formatted = format_number(characters,  input.data('settings').formatMoney);

		input.val(formatted);
		input.focus();
		return true;
	}

	function close(keypad){
		keypad.hide();
		return value;
	}

	function focus_on_end(input){

		// If this function exists...
		if (input[0].setSelectionRange) {
		  // ... then use it (Doesn't work in IE)

		  // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
		  var len = input.val().length * 2;
		  input[0].setSelectionRange(len, len);

		} else {
		// ... otherwise replace the contents with itself
		// (Doesn't work in Google Chrome)

			input.val(input.val());
		}

		// Scroll to the bottom, in case we're in a tall textarea
		// (Necessary for Firefox and Google Chrome)
		input.scrollTop = 999999;
	}

    $.fn.keypad = function( options ) {
        // Default options
        var settings = $.extend({
            position: 'left',
            formatMoney: true
        }, options );

        $(document).bind('click', function(e){
			if(!$(e.target).hasClass('has-keypad')
				&& !$(e.target).hasClass('keypad')
				&& !$(e.target).hasClass('number_pad_number')){
				$('div.keypad').hide();
			}
		});

		// Place hidden keypad in page body
		var keypad = $(template);
		keypad.attr('id', 'jquery_keypad');
		$('#jquery_keypad').remove();
		$('body').append(keypad);

		keypad.on('click', '.number_pad_number', function(e){
			var character = $(this).attr('data-char');
			var currentField = $.fn.keypad.currentField;

			if(character == 'enter'){
				close(keypad);

			}else if(character == 'del'){
				remove_character(currentField);

			}else{
				add_character(currentField, character);
			}
			return false;
		});

        // Attach keypad code to element on page
        return this.each(function(){

			$(this).addClass('has-keypad');
			$(this).data('settings', settings);
			$(this).click(function(e){

				$.fn.keypad.currentField = $(this);

				var height = parseInt($(this).height(), 10);
				var width = parseInt($(this).width(), 10);
				var padY = parseInt($(this).css('padding-top'), 10) + parseInt($(this).css('padding-bottom'), 10);
				var padX = parseInt($(this).css('padding-left'), 10) + parseInt($(this).css('padding-right'), 10);

				if(settings.position == 'right'){
					var top = $(this).offset().top + height + padY - 200;
					var left = $(this).offset().left + width + padX + 5;

				}else if(settings.position == 'left'){
					var top = $(this).offset().top + height + padY - 200;
					var left = $(this).offset().left - 200 - width + padX + 5;

				}else if(settings.position == 'bottom'){
					var top = $(this).offset().top + height + padY + 5;
					var left = $(this).offset().left + padX - 10;

				}else if(settings.position == 'top'){
					var top = $(this).offset().top + padY - 5;
					var left = $(this).offset().left + padX - 10;
				}

				$('div.keypad').css({'top':top, 'left':left}).show();
				focus_on_end( $(this) );
			});

			$(this).on('focus', function(event){
				focus_on_end( $(this) );
			});
		});
    };
}( jQuery ));
</script>
<script type='text/javascript' src='js/keyboard/jquery.keyboard.js'></script>
<script type='text/javascript' src='js/keyboard/qwerty.js'></script>
<script type='text/javascript' src='js/keyboard/form.js'></script>
<script type='text/javascript' src='js/keyboard/jquery.selection.js'></script>
<script type='text/javascript' src='js/fnb.js'></script>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fnb.css?<?php echo APPLICATION_VERSION; ?>" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/keyboard.css?<?php echo APPLICATION_VERSION; ?>" />
<div style='display: none;'>
	<div id='fnb_login_container'>
		<?php $this->load->view('food_and_beverage/login');?>
	</div>
</div>
<?php
// If not logged in to F&B, display login window
if (!$this->session->userdata('fnb_logged_in')) { ?>
<script>
$(document).ready(function(){
	fnb.login.show();
});
</script>

<?php
// If no table is selected, show table layout
} else if (empty($table_number)){ ?>
<script>
	$(document).ready(function(){
		fnb.show_tables();
	});
</script>
<?php }else if(!empty($table_number)){ ?>
<script>
	$(document).ready(function(){
		$('#menubar_stats').html('Table #<?php echo $table_number; ?>');
	});
</script>
<?php } ?>
<div id='sales_register_holder'>
	<div id='cart_section'>
		<div class='pay_box'>
			<a id="send_to_kitchen" class="fnb_button" href="#">Send to Kitchen</a>
			<div id='payments_button' class='fnb_button'>
				Pay Now
			</div>
			<?php
			// Only show this part if there is at least one payment entered.
			//if(count($payments) > 0)
			{
					?>
				<div id="finish_sale">
					<?php echo form_open("food_and_beverage/complete",array('id'=>'finish_sale_form')); ?>
					<?php
					$process_sale = '';
					if ($payments_cover_total && $amount_due == '0.00')
					{
						echo '<h1>Change: '.$amount_due.'</h1>';
						echo '<script>$("body").mask("'.lang("sales_completing_sale").'");</script>';
						$process_sale = '$(document).ready(function(){	mercury.complete_sale();});';
					}
					else if ($payments_cover_total && $mode == 'sales')
					{
						echo '<script>$(document).ready(function(){mercury.complete_sale_window();});</script>';
					}
					?>
					</form>
				</div>
			<?php }	?>
			<span id='open_cash_drawer' class=' payment_button_medium fnb_button'>Cash Drawer</span>
		</div>
		<div id="register_container" class="sales">
			<table>
				<!-- food_and_beverage/return hidden controls -->
               <tr>
                       <td>
	                       <div style='display:none'>
	                               <?php echo form_open("food_and_beverage/change_mode",array('id'=>'mode_form')); ?>
	                               <span><?php echo lang('sales_mode') ?></span>
	                               <?php
	                                       echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"  id="mode"');
	                                       echo form_close();                                               ?>
	                               </div>
               			</td>
               </tr>

		    	<tr>
		    		<td id="register_items_container" class='<?=$mode?>'>
						<div id="table_top" class="table_top_sale">

						</div>
						<!-- Register area -->
						<div class='fixed_top_table'>
							<div class='header-background'></div>

						<div id="register_holder">
							<div id="register_box">
								<table id="register" class="items">
									<!-- table header with labels -->
									<thead>
										<tr>
											<th class="reg_item_seat" id="reg_item_seat"><div class="header_cell header">Seat<span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_name" id="reg_item_name"><div class="header_cell header"><?php echo lang('sales_item_name'); ?><span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_price" id="reg_item_price"><div class="header_cell header"><?php echo lang('sales_price'); ?><span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_discount" id="reg_item_discount"><div class="header_cell header"><?php echo lang('sales_discount'); ?><span class="sortArrow">&nbsp;</span></div></th>
											<th class="reg_item_total" id="reg_item_total"><div class="header_cell header">&nbsp;<span class="sortArrow">&nbsp;</span></div></th>
										</tr>
									</thead>
									<!-- Items in the cart -->
									<tbody id="cart_contents">
									</tbody>
								</table>
							</div>
						    <div id='sale_details'>
				            	<table>
				            		<tr>
				            			<td>
							            	<div class='subtotal_box'>
					                        	<div class="left float"><?php echo lang('sales_sub_total'); ?>:</div>
						                        <div id="basket_total" class="right float"><?php echo to_currency($basket_subtotal); ?></div>
						                        <div class='clear'></div>
					                        </div>
					                        <div id="items_in_basket" class="left"><?php echo $items_in_basket; ?> Items</div>
							            </td>
						                <td>
							                <div id='taxes_holder'>
						                        <?php foreach($basket_taxes as $name=>$value) { ?>
						                        <div>
						                            <div class="right register_taxes"><?php echo to_currency($value); ?></div>
						                            <div class="left register_taxes"><?php echo $name; ?></div>
						                            <div class='clear'></div>
						                        </div>
						                        <?php }; ?>
							                </div>
							                <div id='taxable_box'>
							                	<div class='right'><?php echo form_checkbox(array('name'=>'is_taxable', 'id'=>'is_taxable', 'checked'=>$taxable_checked, 'onclick'=>"fnb.toggle_taxable(this)", "$taxable_disabled"=>"$taxable_disabled")); ?></div>
							                	<div class="left"><?php echo lang('sales_taxable'); ?></div>
							                    <div class='clear'></div>
						                    </div>
							                <div id="register_total">
					                            <div id="basket_final_total" class="right"><?php echo to_currency($basket_total); ?></div>
						                        <div class="left"><?php echo lang('sales_total'); ?>:</div>
					                        </div>
						                </td>
			          				</tr>
			          			</table>
				            </div>
						</div>
						</div>
					</td>
				</tr>
			</table>
			<div id='totals_box'>
				<!-- <table id="sales_items">
	                <?php foreach($payments as $payment) {?>
	                    <?php if (strpos($payment['payment_type'], lang('sales_giftcard'))!== FALSE) {?>
	                <tr>
                        <td class="left"><?php echo $payment['payment_type']. ' '.lang('sales_balance') ?>:</td>
                        <td class="right"><?php echo to_currency($this->Giftcard->get_giftcard_value(end(explode(':', $payment['payment_type']))) - $payment['payment_amount']);?></td>
	                </tr>
	                    <?php }?>
	                <?php }?>
                </table>
     			<?php //$this->load->view('food_and_beverage/payments_list');?>
     			<div class='clear'></div>-->
			</div>
		</div>
	</div>
	<!-- Start primary column -->
	<div class='main_column'>
		<div id="Payment_Types" >
				<div class='pay_box'>
						<a class='small_button new_quickbutton fnb_button' id='suspend_sale_button' href='#'>
							<span id='payment_suspend_sale' class=' payment_button_medium'><?=lang('food_and_beverage_select_table');?></span>
						</a>
					<!--span id='payment_suspend_sale' class='payment_button payment_button_wide'>Suspend Sale</span-->
					<?php if ($this->session->userdata('user_level') < 2) { ?>
					<!-- <span id='override_sale' class=' payment_button_medium fnb_button'>Override</span> -->
					<?php } ?>
					<?php if ($this->config->item('track_cash')) { ?>
						<?php echo anchor(site_url('food_and_beverage/closeregister?continue=home'),
							"<span id='payment_suspend_sale' class=' payment_button_medium fnb_button'>".lang('sales_close_register')."</span>"); ?>
					<?php } ?>
			   			<a class='small_button new_quickbutton fnb_button' id='fnb_logout' href='javascript:void(0)'>
							<span id='payment_log_out' class=' payment_button_medium'>Logout</span>
						</a>
					<div class='clear'></div>
				</div>
				<div class='clear'></div>
			</div>
		<div class='item_menus'>
			<?php
				$menu_sections = array();
				$menu_section_header = '';
				$menu_section_html = '';
				$current_section = '';
				$first_section = false;
				if (count($items) > 0)
				{
					foreach ($items as $item)
					{
						// Hack to skip sides from displaying, should be doing
						// this at DB level...
						if($item['is_side'] == 1){
							continue;
						}
						$menu_section_name = trim($item['category']) != '' ? $item['category'] : 'Uncategorized';
						$menu_section_name = strtolower(str_replace(' ', '_', $menu_section_name));
						if (!$menu_sections[$menu_section_name])
							$menu_sections[$menu_section_name] = array();
						$menu_sections[$menu_section_name]['section'] = $item['category'];
						if (!$menu_sections[$menu_section_name]['items'])
							$menu_sections[$menu_section_name]['items'] = array();
						$menu_sections[$menu_section_name]['items'][] = $item;
					}
					foreach ($menu_sections as $section_id => $menu_section)
					{
						$first_section = ($first_section ? $first_section : $section_id);
						$menu_section_header .= "<div id='section_$section_id' class='menu_section_button fnb_button' onclick='fnb.show_section(\"{$section_id}\")'>{$menu_section['section']}</div>";
						$menu_section_html .= "<div id='section_{$section_id}_contents' class='menu_section_item_holder' style='display:none'>";
						foreach ($menu_section['items'] as $item)
							$menu_section_html .= "<div data-item-id='{$item['item_id']}' class='menu_item fnb_button'>{$item['name']}</div>";
						$menu_section_html .= '<div class="clear"></div></div>';
					}
				}
			?>
			<div id='menu_section_button_holder'>
				<?=$menu_section_header?>
			</div>
			<div id='menu_section_item_holder'>
				<?=$menu_section_html?>
			</div>
			<script>
				fnb.show_section('<?=$first_section?>');
			</script>
		</div>

		<?php if ($this->config->item('print_after_sale')) { ?>
		<div style='height:1px; width:1px; overflow: hidden'>
		       <applet name="jZebra" code="jzebra.PrintApplet.class" archive="<?php echo base_url()?>/jzebra.jar" width="100" height="100">
		           <param name="printer" value="<?=$printer_name?>">
		           <!-- <param name="sleep" value="200"> -->
		       </applet>
		</div>
		<?php } ?>
		<div class='contextMenu' id='myTransactionMenu' style='display:none'>
			<ul>
			    <li id="print_transaction_receipt">Print Receipt</li>
			    <?php for ($i = 0; $i <5; $i++) {
			   		//$rec_tran = $recent_transactions[$i];
			    ?>
			    <li id="add_tip_<?=$i?>">Add Tip -</li>
			    <?php } ?>
			    <li id="add_tip">Add Tip</li>
			</ul>
		</div>
		<div id="feedback_bar"></div>
	</div>
	<div class='clear'></div>
</div>

	<script>
	var giftcard_swipe = false;
		$(document).ready(function(){
			var gcn = $('#giftcard_number');
			gcn.keydown(function(event){
				// Allow: backspace, delete, tab, escape, and enter
		        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
		             // Allow: Ctrl+A
		            (event.keyCode == 65 && event.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (event.keyCode >= 35 && event.keyCode <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
		            	console.log('kc '+event.keyCode);
		            var kc = event.keyCode;
		            if (giftcard_swipe && !((kc >= 48 && kc <=57) || (kc >= 96 && kc <= 105) || kc == 13)) //Allow numbers only and enter
		            {
		            	console.log('numbers only');
	            		event.preventDefault();
	            	}
	            	else if (kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/|| (event.shiftKey && kc == 53) /*percentage sign*/)
		            {
		            	console.log('blocking special characters');
		            	giftcard_swipe = true;
		                event.preventDefault();
		            }
		        }
			});
			//gcn.focus();
		})
	</script>

<script type="text/javascript" language="javascript">
<?php
if(isset($error))
{
	echo "set_feedback('$error','error_message',false);";
}

if (isset($warning))
{
	echo "set_feedback('$warning','warning_message',false);";
}

if (isset($success))
{
	echo "set_feedback('$success','success_message',false);";
}
?>
var mercury = {
	is_active:function(){
		return <?php echo (($this->config->item('mercury_id')!='' && $this->config->item('mercury_password')!='') || $this->config->item('ets_key')!='')?1:0; ?>;
	},
	payment_window:function(declined){
		declined = declined==undefined?false:declined;
		//$.colorbox.close();

		var receipt_id = $('#payment_receipt_id').val();
		var amount_tendered = Number($('#amount_tendered').val());
		var receipt_due_amount = get_receipt_total(receipt_id);
		var total_due_amount = parseFloat($('#due_amount').split("$")[1]);
		var total_tax = 0;

		/*
		if ($('.register_taxes.right')[0]) {
			total_tax = Number($($('.register_taxes.right')[0]).html().replace(/[^0-9\.]+/g,""));
			if ($('.register_taxes.right')[1])
				total_tax += Number($($('.register_taxes.right')[1]).html().replace(/[^0-9\.]+/g,""));
		}
		var total_in_cart = Number($('#basket_total').html().replace(/[^0-9\.]+/g,""));
		*/

		if (amount_tendered > receipt_due_amount){
			alert('Error: Charging more than due.');
		}else if (amount_tendered <= 0){
			alert('Error: Amount must be greater than 0.');
		}else{
			$.colorbox({href:'index.php/food_and_beverage/open_payment_window/POS/Sale/OneTime/'+amount_tendered.toFixed(2)+'/'+(Number(total_tax)).toFixed(2)+'/'+declined,'width':702,'height':600});
		}
	},
	init_payments_window:function(receipt_id){
		$('#amount_tendered').focus();
		$('#amount_tendered').select();

		var receipt = $('div.receipt[data-receipt-id="'+receipt_id+'"]');
		var receiptTotal = get_receipt_total(receipt_id);

		// Only show payments that pertain to this receipt (in payment window)
		$('div.payments_and_tender').find('form').hide();
		$('div.payments_and_tender').find('form[data-receipt-id="'+receipt_id+'"]').show();

		$('#amount_tendered').val(receiptTotal.toFixed(2));
		$('#payment_html_contents div.due_amount').html('$' + receiptTotal.toFixed(2));
		$('#payment_receipt_id').val(receipt_id);
	},
	payments_window:function(receipt_id){
		var submitting = false;
		$.colorbox2({
			inline:true,
			href:'#payment_html_contents',
			title:'Add Payment',
			onComplete:function(){
				mercury.init_payments_window(receipt_id);

				$('#add_payment_form').validate({
				    submitHandler:function(form)
				    {
				        if (submitting) return;
				        submitting = true;
				        $('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
				        $(form).ajaxSubmit({
					        success:function(response)
					        {
					        	$('#giftcard_number').val('');
					        	$('#giftcard_data').hide();
								$('#punch_card_number').val('');
								$('#payment_receipt_id').val('');
					        	$('#punch_card_data').hide();
					        	$('#member_account_info').show();
								$('#make_payment').show();
						        $.colorbox.resize();

					            submitting = false;

								// Update receipts with new payment amounts
					            if(response.receipt_payments){
									$.each(response.receipt_payments, function(id, value){
										$('div.receipt[data-receipt-id="'+id+'"]').attr('data-payment-amount', value);
									});
								}
								var amount_due_receipt = get_receipt_total(receipt_id);

								if(amount_due_receipt == 0)
								{
									$('div.receipt[data-receipt-id="'+receipt_id+'"]').addClass('paid').siblings('a.pay').addClass('disabled');

		                      	    $('#cboxLoadedContent').unmask();
		                      	    $('#giftcard_data').hide();
									$('#punch_card_data').hide();
						        	$('#member_account_info').show();
									$('#make_payment').show();

									if(response.payments_cover_total){
										$.get('<?php echo site_url('food_and_beverage/complete'); ?>/' + receipt_id, null, function(completed_sale_response){
											complete_sale(completed_sale_response);
										},'json');
									}else{
										$.colorbox2.close();
										fnb.update_page_sections(response);
									}
								}
								else
								{
		                      	    $('#cboxLoadedContent').unmask();
		                      	    $('#giftcard_data').hide();
									$('#punch_card_data').hide();
						        	$('#member_account_info').show();
									$('#make_payment').show();

									fnb.update_page_sections(response);
									mercury.init_payments_window(receipt_id);
									$.colorbox2.resize();
						        }
						        $('#cboxLoadedContent').unmask();
					        },
				            dataType:'json'
				        });
				    },
			       errorLabelContainer: "#error_message_box",
			       wrapper: "li"
			    });
			},
			width:500
		});
	},
	giftcard_window:function(){
		$.colorbox({href:'index.php/food_and_beverage/open_giftcard_window/'+$('#amount_tendered').val(),'width':400,'title':'Charge Gift Card', onComplete:function(){$('#giftcard_number').focus()}});
	},
	close_window:function(){
		$.colorbox.close();
	},
	add_payment:function() {
		console.log('trying add_payment');
	    $('#add_payment_form').submit();



	    console.log('finished add_payment');
	    //$('#submit').click();
	},
	delete_payment:function(payment_type, payment_amount, receipt_id) {
		var answer = true;
		if (payment_type == 'Cash' ||
			payment_type == 'Check' ||
			payment_type == 'Gift Card' ||
			payment_type == 'Punch Card' ||
			payment_type == 'Debit Card' ||
			payment_type == 'Credit Card')

			//window.location = 'index.php/food_and_beverage/delete_payment/'+payment_type;
			answer = true;
		else {
			var answer = confirm('Refund '+payment_amount+' to '+payment_type);
			//if (answer)
			//	window.location = 'index.php/food_and_beverage/delete_payment/'+payment_type;
		}

		if (answer)
		{
			$('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
			$.ajax({
               type: "POST",
               url: "index.php/food_and_beverage/delete_payment/",
               data: {
               		payment_id:payment_type,
               		receipt_id:receipt_id
               },
               success: function(response){
               		$('.payments_and_tender').replaceWith(response.payments);
					$('#amount_tendered').val(response.amount_due).select();
               		$('#cboxLoadedContent').unmask();
               		$.colorbox.resize();
			   },
			   dataType:'json'
            });
        }
	},
	complete_sale_window:function(){
		$.colorbox2({href:"index.php/food_and_beverage/complete_sale_window",width:650,height:170});
	},
	complete_sale:function(){
		//submitting_sale = true;
		$("#finish_sale_form").submit();
	},
	encrypted_data:'',
    encrypted_key:'', //if it is one at a time
    purchase:'1.00'

};

var submitting_sale = false;
var submitting_payment = false;
var has_cc_payment = 0;
sale_id = <?php echo (int) $sale_id; ?>;

$(document).ready(function()
{
	// Send to kitchen
	$('#send_to_kitchen').live('click', function(e){
		var items = {};
		var rows = $('#register').find('tr.selected:not(.ordered)');
		if(rows.length <= 0){
			set_feedback('No items selected to order', 'error_message', false);
			return false;
		}

		rows.each(function(index, val){
			var line = $(this).attr('data-line');
			var item_id = $(this).attr('data-item-id');

			items['items['+line+'][line]'] = line;
			items['items['+line+'][item_id]'] = item_id;
		});

		$.post('<?php echo site_url(); ?>/food_and_beverage/save_ticket', items, function(response){
			rows.addClass('ordered').removeClass('selected');
			set_feedback('Items ordered successfully', 'success_message', false);
		},'json');

		return false;
	});

	// ********* Begin split payments code *********

	// Create new receipt
	$('#split_payments a.new-ticket').live('click', function(){
		var newReceiptId = parseInt($('.receipt-container').length) + 1;
		$('#split_payments div.receipt').removeClass('selected');
		$('div.receipts div.scroll-content').append(
			'<div class="receipt-container">'+
			'<div class="receipt" data-receipt-id="'+newReceiptId+'">'+
				'<div class="title">'+
					'<span>Receipt #'+newReceiptId+'</span>'+
					'<a class="fnb_button delete-receipt">Delete</a>'+
					'<span class="is-paid">PAID</span>'+
				'</div>'+
			'</div>'+
			'<a class="fnb_button pay disabled">Pay Now</a></div>'
		);
		var numReceipts = parseInt($('div.scroll-content div.receipt-container').length);

		if(numReceipts % 2 == 1){
			var receiptScroll = (numReceipts - 1) * 310;
		}else{
			var receiptScroll = (numReceipts - 2) * 310;
		}

		// Scroll to newly created receipt
		$('#split_payments div.scroll-content').css("left", -receiptScroll);
		return false;
	});

	// Add item to selected receipt
	$('#split_payments a.add-to-receip').live('click', function(){
		var name = $(this).text();
		var lineNum = $(this).attr('data-line');
		var itemId = $(this).attr('data-item-id');
		var price = $(this).attr('data-price');
		var seat = $(this).attr('data-seat');
		var targetReceipt = $('div.receipt.selected');
		var receipt_id = targetReceipt.attr('data-receipt-id');

		if(targetReceipt.hasClass('paid')){
			return false;
		}

		// Add item to proper receipt
		if(targetReceipt.find('a.item[data-line='+lineNum+']').length <= 0){
			$('div.receipt.selected').append('<a class="fnb_button item" data-line="'+ lineNum +'" data-item-id="'+ itemId +'" data-price="'+ price +'" href="#">'+
			'<span class="seat">'+seat+'</span>'+
			name +
			'</a>');

			// Post item to server to save it in back end
			$.post('<?php echo site_url('food_and_beverage/add_receipt_item'); ?>/'+receipt_id,
				{line:lineNum, item_id:itemId, sale_id:sale_id},
				function(response){

			});
			targetReceipt.siblings('a.pay').removeClass('disabled');
		}
		return false;
	});

	// Select a receipt
	$('#split_payments div.receipt').live('click', function(){
		var targetReceipt = $(this);
		var receipt_id = targetReceipt.attr('data-receipt-id');

		var selectedRows = $('#splitpayment_cart_contents').find('tr.selected:not(.ordered)');
		var items = {};
		var receiptHtml = '';

		if(targetReceipt.hasClass('paid') || selectedRows.length == 0){
			return false;
		}

		// Loop through selected rows and create array of item IDs
		selectedRows.each(function(index, val){
			var name = $(this).find('td.reg_item_name div').text();
			var lineNum = $(this).attr('data-line');
			var itemId = $(this).attr('data-item-id');
			var price = $(this).attr('data-price');
			var seat = $(this).attr('data-seat');

			// Skip any selected items already in receipt
			if(targetReceipt.find("a[data-line='"+lineNum+"']").length == 0){
				items['items['+lineNum+'][line]'] = lineNum;
				items['items['+lineNum+'][item_id]'] = itemId;

				receiptHtml += '<a class="fnb_button item" data-line="'+ lineNum +'" data-item-id="'+ itemId +'" data-price="'+ price +'" href="#">'+
					'<span class="seat">'+seat+'</span>'+
						name +
					'</a>';
			}
		});
		targetReceipt.append(receiptHtml);
		items['sale_id'] = sale_id;

		// Post item to server to save it in back end
		$.post('<?php echo site_url('food_and_beverage/add_receipt_item'); ?>/' + receipt_id, items, function(response){

		});
		targetReceipt.siblings('a.pay').removeClass('disabled');
		selectedRows.removeClass('selected');

		return false;
	});

	// Scroll receipts RIGHT
	$('#split_payments a.right').live('click', function(e){
		var curVal = $('div.scroll-content').position().left;
		var numReceipts = parseInt($('div.scroll-content div.receipt-container').length);
		var totalReceiptWidth = numReceipts * 310;

		if(Math.abs(curVal - 620) < totalReceiptWidth){
			$('div.scroll-content').css("left", curVal - 620);
		}
		return false;
	});

	// Scroll receipts LEFT
	$('#split_payments a.left').live('click', function(e){
		var curVal = $('div.scroll-content').position().left;

		if(curVal <= -620){
			$('div.scroll-content').css("left", curVal + 620);
		}else{
			$('div.scroll-content').css("left", 0);
		}
		return false;
	});
	// ********* End split payments code **********

	//Prevent people from leaving the page without completing sale
	$('#menubar_navigation td a').click(function(e){
		if (($('.reg_item_top').length > 0 || $('#payment_contents').length > 0) &&
			!confirm('You have not completed this sale. Are you sure you want to leave this page?')){
			e.preventDefault();
		}
	});

	//Print receipt if in queue
	$('.colbox').colorbox({
		width:550,
		href:function(){
			return $(this).attr('href')+'/'+$('#amount_tendered').val();
		}
	});
	//$('input:checkbox:not([safari])').checkbox();
	//$('ul.sf-menu').superfish();
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('#new_customer_button').colorbox({'maxHeight':700, 'width':550});
	$('.edit_customer').colorbox({'maxHeight':700, 'width':550});
	$('#quickbutton_menu').qtip({
	   content: {
	      text: $('#quickbutton_section'),
	   },
	   show: {
	      event: 'click'
	   },
	   hide: {
	      event: 'click'
	   },
	   position: {
	      my: 'top center',  // Position my top left...
	      at: 'bottom center', // at the bottom right of...
	      target: $('#quickbutton_menu'), // my target
	      adjust: {
	          method: 'none shift'
	      }
	   }
	});
	<?php if(!(count($payments) > 0 && $payments_cover_total)) { ?>
	$('#item').focus();
	<?php } ?>
	$('#comment').change(function()
	{
		$.post('<?php echo site_url("food_and_beverage/set_comment");?>', {comment: $('#comment').val()});
	});

	$('#email_receipt').change(function()
	{
		$.post('<?php echo site_url("food_and_beverage/set_email_receipt");?>', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
	});


    $("#finish_sale_button").click(function()
    {
		if (submitting_sale)
			console.log('already submitting');
		else
    	{
			submitting_sale = true;
    		$('#finish_sale_form').submit();
    	}
    });
    $('#finish_sale_button').dblclick(function(){});
    $("#finish_sale_no_receipt_button").click(function()
    {
        $('#no_receipt').val(1);
		if (submitting_sale)
			console.log('already submitting');
		else
		{
			submitting_sale = true;
			$('#finish_sale_form').submit();
		}
    });
    $('#finish_sale_no_receipt_button').dblclick(function(){});
	$('#open_cash_drawer').click(function(){
       open_cash_drawer();
    });
    $('#override_sale').click(function(){
    	fnb.override();
    });
    <?php if ($this->config->item('print_after_sale')) { ?>
       var submitting = false;
    $('#finish_sale_form').validate({
               submitHandler:function(form)
               {
					complete_sale(form);
               },
               errorLabelContainer: "#error_message_box",
               wrapper: "li"
       });
<?php } ?>
    $("#cancel_sale_button").click(function()
    {
    	if (confirm('<?php echo lang("sales_confirm_cancel_sale"); ?>'))
    	{
            $('#cancel_sale_form').submit();
    	}
    });
    $("#delete_button").click(function()
    {
    	if (confirm('<?php echo lang("sales_confirm_cancel_sale"); ?>'))
    	{
            //$('#delete_form').submit();
            fnb.delete_checked_lines();
    	}
    });

	$("#payment_types").change(checkPaymentTypeGiftcard).ready(checkPaymentTypeGiftcard);
	var ct = $('#column_transactions');
	ct.click(function(){
    	if (ct.hasClass('expanded'))
		{
			ct.removeClass('expanded');
    		$('#recent_transactions').hide();
		}
		else
		{
    		ct.addClass('expanded');
    		$('#recent_transactions').show();
		}
	});
	var ssh = $('#suspended_sales_header');
    ssh.click(function(){
    	if (ssh.hasClass('expanded'))
		{
			ssh.removeClass('expanded');
    		$("#suspended_sales").hide();
		}
		else
		{
    		ssh.addClass('expanded');
    		$("#suspended_sales").show();
		}
	});
    <?php if ($receipt_data) {
		?>
	 	console.log('attempting to print receipt');

		//*setTimeout(function(){*/print_receipt("<?=$receipt_data?>", true)/*}, 1000)*/;
 	<?php }
 	echo $process_sale;
	?>
});

function complete_sale(response){

	fnb.update_page_sections(response);
	submitting = false;

	//Old Print method... to be phased out... has issues when printing
	<?php if ($this->config->item('print_sales_receipt')) { ?>
	//Print receipt
	if ($('#no_receipt').val() != 1)
	{
		var receipt_data = '';
		// Items
		var data = response.receipt_data;

		has_cc_payment = 0;
		var receipt_data = build_receipt(data);
		console.log('print_two_other <?=$this->config->item('print_two_receipts_other')?>');
		console.log('print_two <?=$this->config->item('print_two_receipts')?>');
		console.log('has_cc_payment'+(has_cc_payment ? 'yes' : 'no'));
		if (('<?=$this->config->item('print_two_receipts_other')?>' == '1' && !has_cc_payment) || (has_cc_payment && '<?=$this->config->item('print_two_receipts')?>' == '1')) {
			for(var i = 1; i <= 2; i++)
				print_receipt(receipt_data, false, data.sale_id, response.receipt_data);
		}
		else
		{
			print_receipt(receipt_data, false, data.sale_id, response.receipt_data);
		}
	}
	<?php } ?>

	$('#menubar_stats').html('');
	fnb.show_tables();
	$.colorbox2.close();
}

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value", "");
		fnb.add_item(response.item_id);
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		if ($("#select_customer_form").length == 1)
		{
			$("#customer").attr("value",'');
			selectCustomer(response.person_id)
		}
		else
		{
			window.location = '<?php echo site_url('food_and_beverage/index');?>';
		}
	}
}

function checkPaymentTypeGiftcard()
{
	if ($("#payment_types").val() == "<?php echo lang('sales_giftcard'); ?>")
	{
		$("#amount_tendered").val('');
		$("#amount_tendered").focus();
	}
	else
	{

	}
}
function findPrinter() {
//     var applet = document.jZebra;
  //  if (applet != null) {
        // Searches for locally installed printer with "zebra" in the name
    //}
}
function monitorFinding() {
	var applet = document.jZebra;
	if (applet != null) {
	   if (!applet.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = applet.getPrinterName();
              console.log(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            console.log("Applet not loaded!");
        }
      }
function build_receipt(data){

	var receipt_data = '';
	var cart = data.cart;
    //Itemized
    for (var line in cart)
    {
        receipt_data += add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n';
    	if (parseInt(cart[line].discount) > 0)
    		receipt_data += add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))
    }
    // Totals
    receipt_data += '\n\n'+add_white_space('Subtotal: ','$'+parseFloat(data.subtotal).toFixed(2))+'\n';
    var taxes = data.taxes;
    for (var tax in taxes)
           receipt_data += add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n';

       receipt_data += add_white_space('Total: ','$'+parseFloat(data.total).toFixed(2))+'\n';

    // Payment Types
    receipt_data += '\nPayments:\n';
    var payments = data.payments;
    for (var payment in payments)
    {
	    has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
	    receipt_data += add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n';
    }
       // Change due
       receipt_data += '\n'+add_white_space('Change Due: ',(data.amount_change))+'\n';
    <?php if ($this->config->item('print_tip_line')) { ?>
    receipt_data += '\n\n'+add_white_space('Tip: ','$________.____');
    receipt_data += '\n\n'+add_white_space('TOTAL CHARGE: ','$________.____');
    receipt_data += '\n\n\nX_____________________________________________\n';
	<?php } ?>
    receipt_data += chr(27)+chr(97)+chr(49);
	receipt_data += "\x1Dh" +chr(80);  // ← Set height
    receipt_data += "\n\n";// Some spacing at the bottom
    var sale_num = data.sale_id.replace('POS ', '');
    var len = sale_num.length;
	if (len == 3)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+sale_num;
	else if (len == 4)
        receipt_data += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 5)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 6)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 7)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 8)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 9)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 10)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // Sale id and barcode
    receipt_data += '\n\nSale ID: '+data.sale_id+'\n';

    return receipt_data;
}
function print_receipt(receipt, add_delay, sale_id, data) {
	try
	{
		add_delay = add_delay == undefined ? false : add_delay;
		var applet = document.jZebra;
		if (applet != null)
		{
	    	{
	    		applet.findPrinter("<?=$printer_name?>");

		       if (add_delay) {
			       	var t = new Date();
		    		var ct = 0;
		   			while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {

			        }
			        setTimeout(function(){

				        // Send characters/raw commands to applet using "append"
				        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify

				        applet.append("<?php echo $this->config->item('name')?>\n");
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify
				        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
				        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
				        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
				        applet.append("<?php echo $this->config->item('phone')?>\n\n");
				        applet.append(chr(27) + chr(97) + chr(48));// Left justify
				        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
				        var d = new Date();
				        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
				        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
				        var ap = (d.getHours() < 12)?'am':'pm';
				        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");

				        applet.append(receipt);
				        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
				        applet.append(chr(27) + chr(105)); //Cuts the receipt

				        // Send characters/raw commands to printer
				        //applet.forceAccept();
				        applet.print();
				        open_cash_drawer();
				    }, 1);
			    }
			    else
			    {
			    	console.log('not timing out');
					// Send characters/raw commands to applet using "append"
			        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
			        applet.append(chr(27)+chr(64));//Resets the printer
			        //applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
			        applet.append(chr(27) + "\x61" + "\x31"); // center justify

			        applet.append("<?php echo $this->config->item('name')?>\n");
			        applet.append(chr(27)+chr(64));//Resets the printer
			        applet.append(chr(27) + "\x61" + "\x31"); // center justify
			        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
			        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
			        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
			        applet.append("<?php echo $this->config->item('phone')?>\n\n");
			        applet.append(chr(27) + chr(97) + chr(48));// Left justify
			        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
			        <?php //if ($customer != '') { ?>
			        if (data != undefined && data.customer != undefined)
				        applet.append("Customer: <?php //echo $customer; ?>"+data.customer+"\n");
			        <?php //} ?>
			        var d = new Date();
			        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
			        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
			        var ap = (d.getHours() < 12)?'am':'pm';
			        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");
			        //applet.append("<?php echo $cab_name.': '.$customer_account_balance; ?>\n");
			        //applet.append("<?php echo $cmb_name.': '.$customer_member_balance; ?>\n");
			        <?php if ($giftcard) { ?>
			        //applet.append("Giftcard Balance: <?php echo $giftcard_balance; ?>\n");
			        <?php } ?>

			        applet.append(receipt);
			        //applet.appendImage('/images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        //applet.appendImage('../../images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        <?php if ($this->config->item('online_booking')) { ?>
		        	applet.append("\nAnd remember, you can book your\nnext tee time online at\n<?=$this->config->item('website')?>");
		        	<?php } ?>

			        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
			        applet.append(chr(27) + chr(105)); //Cuts the receipt

			        // Send characters/raw commands to printer
			        //applet.forceAccept();
			        applet.print();
			        open_cash_drawer();
		        }
			}
		}
	}
	catch(err)
	{
		set_feedback('Unable to print receipt at this time. Please print manually','error_message',false);
	}
}
function print_postscript_receipt(receipt, customer) {
    var applet = document.jZebra;
    if (applet != null) {
    	if ($.isFunction(applet.findPrinter))
    	{
	        applet.findPrinter("<?=$printer_name?>");
	   		while (!applet.isDoneFinding()) {
	           // Wait
	           console.log('not done finding yet');
	        }
	        // Send characters/raw commands to applet using "append"
	        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
	        applet.appendHTML("<html>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('name')?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('phone')?>')+"</div><br/>");
	        applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>')+"</div>");
	        if (customer != undefined && customer != '')
	        	applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Customer: '+customer)+"</div>");
	        var d = new Date();
	        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
	        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
	        var ap = (d.getHours() < 12)?'am':'pm';
	        applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Date: '+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap)+"</div><br/>");

	        applet.appendHTML(receipt);
	        applet.appendHTML("<br/>");
	        applet.appendHTML('<div style="font-size:6px;">______________________________________________</div>');
	        //applet.appendImage('/images/test/test.png', "ESCP");
	        //applet.append("\r\n");
	        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
	        //applet.append("\r\n");
	        //applet.appendImage('../../images/test/test.png', "ESCP");
	        //applet.append("\r\n");

	        // Send characters/raw commands to printer
	        //applet.forceAccept();
	        console.log('Printing HTML');

			<?php if ($this->config->item('online_booking')) { ?>
	        applet.appendHTML("<br/><div style='text-align:center;'>And&nbsp;remember,&nbsp;you&nbsp;can&nbsp;book&nbsp;your<br/>next&nbsp;tee&nbsp;time&nbsp;online&nbsp;at<br/><?=$this->config->item('website')?></div>");
	        <?php } ?>
	        applet.printHTML();
	        console.log('Done Printing HTML');
        }
    }
}
function hexdec (hex_string) {
    // Returns the decimal equivalent of the hexadecimal number
    //
    // version: 1109.2015
    // discuss at: http://phpjs.org/functions/hexdec
    // +   original by: Philippe Baumann
    // *     example 1: hexdec('that');
    // *     returns 1: 10
    // *     example 2: hexdec('a0');
    // *     returns 2: 160
    hex_string = (hex_string + '').replace(/[^a-f0-9]/gi, '');
    return parseInt(hex_string, 16);
}
function update_recent_transaction(transaction_id)
{
    $('#transaction_'+transaction_id).remove();
}
function delete_recent_transaction(transaction_id)
{
    $('#transaction_'+transaction_id).remove();
}
function add_nbsp(string) {
	return string.replace(' ', '&nbsp;');
}
function chr(i) {
      return String.fromCharCode(i);
}
function open_cash_drawer() {
    var applet = document.jZebra;
    if (applet != null) {
    	applet.findPrinter("<?=$printer_name?>");
        applet.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
        //applet.forceAccept();
        applet.print();
    }
}
function boldAndCenter() {
    var applet = document.jZebra;
    if (applet != null) {
        applet.append(chr(27) + chr(69) + "\r");  // bold on
        applet.append(chr(27) + "\x61" + "\x31"); // center justify
    }
}
function add_white_space(str_one, str_two)
{
       var width = 42;
       var strlen_one = str_one.length;
       var strlen_two = str_two.length;
       var white_space = '';
       var white_space_length = 0;
       if (strlen_one + strlen_two >= width)
               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
       else
               white_space_length = width - (strlen_one + strlen_two);

       for (var i = 0; i < white_space_length; i++)
               white_space += ' ';
       return str_one+white_space+str_two;
}

// FNB Overridden functions, because FNB JS is currently minified
fnb.update_page_sections = function(response) {

	if (response) {

		if(response['sale_id']){
			sale_id = response.sale_id;
		}

		if (response['message']) {
			set_feedback(response['message']['text'], response['message']['type'], response['message']['persist']);
		}
		if (response['register_box']) {
			$("#register_box").html(response['register_box']);
		}
		if (response['basket_info']) {
			fnb.update_basket_totals('all', response['basket_info']);
		}
		if (response['customer_info_filled']) {
			$("#customer_info_filled").html(response['customer_info_filled']);
		}
		if (response['payment_window']) {
			$("#make_payment").html(response['payment_window']);
		}
		if (response['payments']){
			$('.payments_and_tender').replaceWith(response.payments);
			$('.colbox').colorbox();
		}
		if (response['suspended_sales']) {
			$("#suspended_sales").html(response['suspended_sales']);
		}
		if (response['table_top']) {
			$("#table_top").html(response['table_top']);
		}
		if (response['amount_due']){
			$('#amount_tendered').val(response.amount_due).select();
		}

		if (response['suspend_button_title']) {
			fnb.setup_suspend_button('', response['suspend_button_title']);
		}
		if (response['split_payments']) {
			$("#split_payments_html").html(response['split_payments']);
		}

		if (response['is_cart_empty']) {
			// remove payment type values
			$(".payments_and_tender :form").each(function() {
				$(this).remove();
			});
		}
		if (response['table_top']) {
			if (response['mode'] == 'sale')
			{
				$("#register_items_container").removeClass('return');
				$('#mode').val('sale');
				$("#payments_button").html('Pay Now');
			}
			else
			{
				$('#mode').val('return');
				$("#register_items_container").addClass('return');
				$("#payments_button").html('Return Now');
			}
			$("#table_top").html(response['table_top']);
		}
		if (response['table_number'])
		{
			$('#menubar_stats').html('Table #'+response['table_number']);
		}
		return true;
	} else {
		document.location = document.location;
	}
	return false;
};

function get_receipt_total(receiptId){
	var numSplits = {};
	var receiptTotal = 0;
	var receipt = $('div.receipt[data-receipt-id="'+ receiptId +'"]');

	// Loop through all items on all receipts, count items split across
	// multiple receipts
	$('div.receipt-container').find('a.item').each(function(index, val){
		var key = $(this).attr('data-item-id') + "-" + $(this).attr('data-line');

		if(numSplits[key]){
			numSplits[key] += 1;
		}else{
			numSplits[key] = 1;
		}
	});

	// Add up the items of desired receipt, figuring in which items are split
	receipt.find('a.item').each(function(index, val){
		var key = $(this).attr('data-item-id') + "-" + $(this).attr('data-line');
		var price = parseFloat($(this).attr('data-price'));

		var splitPrice = round(parseFloat(price / numSplits[key]), 2);
		receiptTotal += splitPrice;
	});

	// Calculate actual total due
	var total = round(receiptTotal, 2);
	if(receipt.attr('data-payment-amount')){
		var paymentTotal = parseFloat(receipt.attr('data-payment-amount'));
		total = receiptTotal - paymentTotal;
	}

	var cartTotal = parseFloat($('#register_container div.due_amount').text().split("$")[1]);

	// If this is the last receipt being paid and there is a remainder
	// cent, add it to the receipt.
	var remainder = round((cartTotal - total), 2);
	if(remainder == -0.01 || remainder == 0.01){
		total += parseFloat(remainder);
	}

	return round(total, 2);
}
</script>

<div id="table_layout" style="display: none;">
	<?php $this->load->view('food_and_beverage/layout'); ?>
</div>

<!-- Backbone Templates -->
<?php $this->load->view('food_and_beverage/js/templates/edit_item'); ?>
<?php $this->load->view('food_and_beverage/js/templates/cart_item'); ?>
<?php $this->load->view('food_and_beverage/js/templates/cart_totals'); ?>
<?php $this->load->view('food_and_beverage/js/templates/receipts'); ?>
<?php $this->load->view('food_and_beverage/js/templates/receipt'); ?>
<?php $this->load->view('food_and_beverage/js/templates/payment_window'); ?>
<?php $this->load->view('food_and_beverage/js/templates/payment'); ?>
<?php $this->load->view('food_and_beverage/js/templates/item_sides'); ?>
<?php $this->load->view('food_and_beverage/js/templates/item_modifiers'); ?>
<?php $this->load->view('food_and_beverage/js/templates/item_soup_salad'); ?>

<script>
// Currency/number format settings
accounting.settings = {
	currency: {
		symbol : "$",   // default currency symbol is '$'
		format: "%s%v", // controls output: %s = symbol, %v = value/number
		decimal : ".",  // decimal point separator
		thousand: ",",  // thousands separator
		precision : 2   // decimal places
	},
	number: {
		precision : 0,  // default precision on numbers is 0
		thousand: ",",
		decimal : "."
	}
};

// Underscore init, custom functions
_.mixin({
	capitalize: function(str) {
		if(typeof(str) !== 'string'){ return str; }
		return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
	},

	round: function(number, precision) {
		if(typeof(number) != 'number'){
			number = parseFloat(number);
		}
		var multiplier = Math.pow( 10, precision );
		return Math.round( number * multiplier ) / multiplier;
	}
});

var App = {};
App.table_num = <?php echo (int) $table_number; ?>;
App.api_root = "index.php/api/food_and_beverage/service/";
App.api_table = App.api_root + App.table_num + '/';
App.api_key = "no_limits";

App.item_categories = {
	1: "Cook Temp",
	2: "Condiments",
	3: "Customize",
	4: "Soup/Salad",
	5: "Sides"
};

// Override ajax function to always pass API key
$(document).ajaxSend(function(e, xhr, options){
    xhr.setRequestHeader("api_key", App.api_key);

// Display any errors received from API
}).ajaxError(function(e, xhr, options, error){
	if(xhr.responseJSON && xhr.responseJSON.msg){
		set_feedback(xhr.responseJSON.msg, 'error_message', false, 4000);
	}
});

// Backbone Models
<?php $this->load->view('food_and_beverage/js/models/modifier'); ?>
<?php $this->load->view('food_and_beverage/js/models/item'); ?>
<?php $this->load->view('food_and_beverage/js/models/cart_item'); ?>
<?php $this->load->view('food_and_beverage/js/models/receipt_item'); ?>
<?php $this->load->view('food_and_beverage/js/models/receipt'); ?>
<?php $this->load->view('food_and_beverage/js/models/payment'); ?>
<?php $this->load->view('food_and_beverage/js/models/order'); ?>
<?php $this->load->view('food_and_beverage/js/models/side'); ?>
<?php $this->load->view('food_and_beverage/js/models/item_side'); ?>

// Backbone Collections
<?php $this->load->view('food_and_beverage/js/collections/modifiers'); ?>
<?php $this->load->view('food_and_beverage/js/collections/items'); ?>
<?php $this->load->view('food_and_beverage/js/collections/cart'); ?>
<?php $this->load->view('food_and_beverage/js/collections/payments'); ?>
<?php $this->load->view('food_and_beverage/js/collections/receipt_cart'); ?>
<?php $this->load->view('food_and_beverage/js/collections/receipts'); ?>
<?php $this->load->view('food_and_beverage/js/collections/sides'); ?>
<?php $this->load->view('food_and_beverage/js/collections/item_sides'); ?>

// Backbone Views
<?php $this->load->view('food_and_beverage/js/views/cart_list'); ?>
<?php $this->load->view('food_and_beverage/js/views/cart_list_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/cart_totals'); ?>
<?php $this->load->view('food_and_beverage/js/views/edit_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/payment'); ?>
<?php $this->load->view('food_and_beverage/js/views/payment_window'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt_item_list'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt_item'); ?>
<?php $this->load->view('food_and_beverage/js/views/receipt_window'); ?>
<?php $this->load->view('food_and_beverage/js/views/item_sides'); ?>
<?php $this->load->view('food_and_beverage/js/views/item_modifiers'); ?>

// If all receipts are now paid, remove table record with all data
// associated with it (close the sale)
App.closeTable = function(){
	if(App.receipts.allReceiptsPaid()){

		// TODO: replace this with a proper "Table service" model
		$.ajax({
			url: App.api_table,
			type: 'DELETE',

			// If table closed successfully
			success: function(response){
				$(document).one('cbox_closed', function(e){
					reload_tables(function(){
						fnb.show_tables();
					});
				});
				$.colorbox.close();
				$.colorbox2.close();

				// Clear out existing table data
				App.cart.reset(response.cart);
				App.receipts.reset(response.receipts);
				$('#menubar_stats').html('Table #');
			}
		});
	}
};

$(function(){
	App.items = new ItemCollection();
	App.receipts = new ReceiptCollection();
	App.cart = new Cart();
	App.sides = new SideCollection();

	App.Page = {};
	App.Page.cart = new CartListView({collection: App.cart, el: $('#cart_contents')});
	App.Page.cartTotals = new CartTotalsView({collection: App.cart, el: $('#sale_details')});

	// Pre-load all available F&B items, cart, and receipts for current table
	App.items.reset(<?php echo json_encode($food_items); ?>);
	App.sides.reset(<?php echo json_encode($sides); ?>);
	App.cart.reset(<?php echo json_encode($cart); ?>);
	App.receipts.reset(<?php echo json_encode($receipts); ?>);

	// Open payments window
	$('#payments_button').on('click', function(e){
		var receiptsView = new ReceiptWindowView({collection: App.receipts});

		$.colorbox({
			title: 'Split Payments',
			html: receiptsView.render().el,
			width: 1268,
			height: 750,
			cache: false
		});

		return false;
	});

	// Send selected items to kitchen
	$('#send_to_kitchen').on('click', function(e){
		var cartItems = App.cart.where({'selected':true});
		var data = [];
		_.each(cartItems, function(item){
			data.push({'item_id': parseInt(item.get('item_id')), 'line': parseInt(item.get('line'))});
		});
		var order = new Order({'items':data});
		order.save();
		return false;
	});

	// Add food item to cart
	$('div.item_menus').on('click', 'div.menu_item', function(e){
		var item_id = parseInt($(this).data('item-id'));
		var item = App.items.get(item_id);
		item.set({"line":null});

		var cart_item = new CartItem(item.attributes);
		App.cart.create(cart_item);

		var cartItemCopy = cart_item.attributes;
		var receipt_item = new ReceiptItem(cartItemCopy);

		var availableReceipt = App.receipts.findWhere({'status':'pending'});
		if(!availableReceipt){
			var availableReceipt = App.receipts.add({date_paid:null});
		}

		availableReceipt.get('items').add(receipt_item);
	});

	// Select new table
	$('#suspend_sale_button').on('click', function(e){
		fnb.show_tables(true);
		return false;
	});
});

// Refresh table layout content
function reload_tables(callback){
	$.get(SITE_URL + '/food_and_beverage/layout', function(response){
		$('#table_layout').html(response);
		if(typeof(callback) == 'function'){
			callback(response);
		}
	});
}
</script>
<!-- END BackboneJS code -->

<?php $this->load->view("partial/footer"); ?>
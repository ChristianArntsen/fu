var ReceiptItemView = Backbone.View.extend({
	tagName: "a",
	className: "fnb_button item",
	template: _.template( $('#template_receipt_item').html() ),

	events: {
		"click": "selectItem"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		if(this.model.get('selected')){
			this.$el.addClass('selected');
		}else{
			this.$el.removeClass('selected');
		}
		return this;
	},

	selectItem: function(event){

		if( $(event.currentTarget).parents('div.receipt').hasClass('paid') ){
			return false;
		}

		if(this.model.get('selected')){
			this.model.set({"selected":false});
			this.$el.removeClass('selected');
		}else{
			this.model.set({"selected":true});
			this.$el.addClass('selected');
		}

		return false;
	}
});

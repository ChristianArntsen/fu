var ItemSidesView = Backbone.View.extend({
	tagName: "div",
	className: "sides",

	initialize: function(attributes, options){
		this.options = {};
		this.options.category_id = attributes.category_id;
		this.options.number_soups = attributes.number_soups;
		this.options.number_salads = attributes.number_salads;
		this.options.number_sides = attributes.number_sides;
	},

	events: {
		"click a.side": "selectSide",
		"click a.no_side": "removeSide",
		"click a.back": "back"
	},

	render: function() {
		data = {};
		data.category_id = this.options.category_id;

		// If section is 'Soups/Salads'
		if(data.category_id == 4){
			this.template = _.template( $('#template_item_soup_salad').html() );
			data.number_soups = this.model.get('number_soups');
			data.number_salads = this.model.get('number_salads');
			data.salads = this.model.get('salads');
			data.soups = this.model.get('soups');

		// Otherwise section is just 'Sides'
		}else{
			this.template = _.template( $('#template_item_sides').html() );
			data.number_sides = this.model.get('number_sides');
			data.sides = this.model.get('sides');
		}

		this.$el.html(this.template(data));
		return this;
	},

	selectSide: function(event, sideType){
		var button = $(event.currentTarget);

		// Mark button as 'selected'
		button.parents('div.side').find('a').removeClass('selected');
		button.addClass('selected');
		var side_id = button.data('side-id');
		var position = button.data('position');
		var type = button.data('type');

		// Retrieve the selected side and assign it the proper side 'position'
		var side = App.sides.get(side_id);
		var itemSide = new ItemSide(side.attributes);
		itemSide.set('position', position);

		// Add the side to item
		if(type == 'side'){
			var sideCollection = this.model.get('sides');
		}else if(type == 'salad'){
			var sideCollection = this.model.get('salads');
		}else if(type == 'soup'){
			var sideCollection = this.model.get('soups');
		}

		if(sideCollection){
			sideCollection.add(itemSide, {merge: true});
		}
		return false;
	},

	removeSide: function(event){
		var button = $(event.currentTarget);

		// Mark button as 'selected'
		button.parents('div.side').find('a').removeClass('selected');
		button.addClass('selected');
		var position = button.data('position');
		var type = button.data('type');

		// Create a new side model that represents 'no side'
		var side = new ItemSide({'position':position, 'item_id':0, 'tax':0, 'total':0, 'name':'None', 'price':0});

		// Get appripriate collection to modify
		if(type == 'side'){
			var sideCollection = this.model.get('sides');
		}else if(type == 'salad'){
			var sideCollection = this.model.get('salads');
		}else if(type == 'soup'){
			var sideCollection = this.model.get('soups');
		}

		if(sideCollection){
			sideCollection.add(side, {'merge':true});
		}

		return false;
	}
});
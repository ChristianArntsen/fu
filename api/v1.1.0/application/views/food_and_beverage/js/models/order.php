var Order = Backbone.Model.extend({
	url: App.api_table + 'orders',
	idAttribute: "ticket_id",
	defaults: {
		"ticket_id": null,
		"items": null,
		"printed": 0,
		"deleted": 0,
		"completed": 0,
		"date_created": null,
		"date_ordered": null,
		"date_completed": null
	},

	initialize: function(attributes, options){
		// When order is sent to server successfully, mark items in cart
		// as "ordered"
		this.listenTo(this, 'sync', this.markItems, this);
	},

	markItems: function(order){
		// Loop through each item in order, mark as ordered
		_.each(order.get('items'), function(orderItem){
			var item = App.cart.get(orderItem.line);
			item.set('is_ordered', true);
		});
	},

	validate: function(){
		var complete = true;

		// Loop through each item in order and check if they are complete
		_.each(this.get('items'), function(orderItem){
			var item = App.cart.get(orderItem.line);
			if(!item.isComplete()){
				complete = false;
			}
		});

		if(!complete){
			return 'Some items in order need finished';
		}
	}
});
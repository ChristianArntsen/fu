var Side = CartItem.extend({
	idAttribute: "item_id",
	defaults: {
		"name": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"price": 0.00,
		"tax": 0,
		"total": 0,
		"subtotal": 0,
		"modifiers": [],
		"taxes": [],
		"view_category": 0
	},

	// Reset "set" method back to default from CartItem
	set: function(attributes, options){
		Backbone.Model.prototype.set.call(this, attributes, options)
	},

	initialize: function(){
		var modifierCollection = new ModifierCollection(this.get('modifiers'));

		this.set({
			modifiers: modifierCollection
		});
	}
});
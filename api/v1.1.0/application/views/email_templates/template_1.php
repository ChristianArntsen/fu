<html lang="en"><head>
<title>America Template</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">

/* This is to help Hotmail a bit with correct rendering */

.ReadMsgBody
{ width: 100%; width: 100%; background-color: #f2f5f5;}
.ExternalClass
{width: 100%; width: 100%; background-color: #f2f5f5;}
body { width: 100%; background-color: #f2f5f5;}

</style>
</head>
<body bgcolor="#f2f5f5" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="644" bgcolor="#f2f5f5" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<!-- To increase the space, adjust this value -->
		<td height="25">
		</td>
	</tr>
</tbody></table>

<!-- Okay, in this Table, we create some space inside the Top table -->

<table width="644" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td height="20">
		</td>
	</tr>
</tbody></table>

<!-- Here we create the top, where we put our Logo and Social Media Icons -->
<table width="644" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td width="31" height="34"></td>
		<td width="451" height="34">
		
			<!-- The Logo -->
			<a href="#"><img src="http://www.premiumstuff.net/demo/america/green/light/images/logo.jpg" border="0" alt="Logo"></a>
		
		</td>
		<td>
			<a href="#"><img src="http://www.premiumstuff.net/demo/america/green/light/images/google_icon.jpg" width="44" height="34" border="0" style="display: block;" alt="Google+"></a></td>
		<td>
			<a href="#"><img src="http://www.premiumstuff.net/demo/america/green/light/images/twitter_icon.jpg" width="44" height="34" border="0" style="display: block;" alt="Twitter"></a></td>
		<td>
			<a href="#"><img src="http://www.premiumstuff.net/demo/america/green/light/images/facebook_icon.jpg" width="43" height="34" border="0" style="display: block;" alt="Facebook"></a></td>
		<td width="31" height="34"></td>
	</tr>
</tbody></table>

<!-- Again, to create some space from below -->
<table width="644" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<!-- To increase the space, adjust this value -->
		<td height="20">
		</td>
	</tr>
</tbody></table>
<!-- End Top -->

<!-- Here we create our Header, it's a Image -->
<table bgcolor="#000000" width="644" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td>
			<!-- Header Image, replace header.jpg in the 'images' folder -->
			<a href="#"><img src="http://www.premiumstuff.net/demo/america/green/light/images/header.jpg" border="0" alt="Header"></a>
		</td>
	</tr>
</tbody></table>

<!-- Let's code our Ribbon shall we? -->
<table width="658" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td>
			<!-- The top border of the Ribbon -->
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/ribbon_border_top.jpg" width="658" height="5" style="display: block;" alt="">
		</td>
	</tr>
</tbody></table>

<!-- The Ribbon content. I have used a animated image to present the quotes. Follow this tutorial to learn how. -->
<table width="658" bgcolor="#0d6e04" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td width="658" align="center">		
			
			<!-- Quote -->	
			<span style="font-family: Georgia, serif; font-size: 18px; color: #FFFFFF; line-height: 32px; font-weight: normal;"><i>"This is a static Quote." ~ John Do</i></span>
			
		</td>
	</tr>
</tbody></table>

<!-- The bottom border of the Ribbon -->
<table width="658" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td>
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/ribbon_border_bottom.jpg" width="658" height="5" style="display: block;" alt="">
		</td>
	</tr>
</tbody></table>

<!-- The bottom of the Header -->
<table width="644" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td>
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/header_border_bottom.jpg" width="644" height="32" style="display: block;" alt="">
		</td>
	</tr>
</tbody></table>

<!-- Let's create the most important thing, the tables that hold the content -->

<!-- Padding-top -->
<table width="644" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<!-- To increase the space, adjust this value -->
		<td height="25">
		</td>
	</tr>
</tbody></table>

<!-- Column 1 -->
<table width="644" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td rowspan="2" width="28" height="60"></td>
		<td>
		
			<!-- Column Image 1 -->
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/column_image_1.jpg" width="78" height="78" border="0" style="display: block;" alt="Image 1">
			
		</td>
		<td rowspan="2" width="168" height="60" valign="top">
			
			<!-- The Title -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: bold; font-size: 18px; color: #161616; line-height: 32px;">We are friendly.</span><br>
			
			<!-- Text -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: normal; font-size: 12px; color: #686868; line-height: 19px;">This e-mail template is extremely user friendly. We have commented our Photoshop files &amp; code as possible as we can.</span>
			
			</td>
			
		<!-- To create the space between the two columns, also to create a subtile border between -->
		<td rowspan="2" width="41" height="60"></td>
			
			<!-- Change the HEX value for a different color of border -->
			<td rowspan="2" bgcolor="#f1f1f1" width="1" height="60"></td>
			
		<td rowspan="2" width="42" height="60"></td>
		
		<td>
			
			<!-- Column Image 2 -->
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/column_image_2.jpg" width="78" height="78" border="0" style="display: block;" alt="Image 2">
			
		</td>
		<td rowspan="2" width="180" height="60" valign="top">
		
			<!-- The Title -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: bold; font-size: 18px; color: #161616; line-height: 32px;">We push forward.</span><br>
			
			<!-- Text -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: normal; font-size: 12px; color: #686868; line-height: 19px;">It's impossible to convince customers to buy your product with boring white e-mail templates. Not anymore with America!</span>
		
		</td>
		<td rowspan="2" width="28" height="60"></td>
	</tr>
	<tr>
		<td width="78" height="60"></td>
		<td width="78" height="60"></td>
	</tr>
</tbody></table>

<!-- Padding-bottom of the columns -->
<table id="Table_01" width="644" height="3" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td width="315" height="3" bgcolor="#FFFFFF"></td>
			<!-- If you have changed the border color, please change here as well. -->
			<td width="1" height="30" bgcolor="#f1f1f1"></td>
		<td width="328" height="3" bgcolor="#FFFFFF"></td>
	</tr>
</tbody></table>

<!-- End 2 Columns Table -->

<!-- Let's create another two columns. You may also copy the other two as many as you like -->

<!-- Column 3 -->
<table width="644" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td rowspan="2" width="28" height="60" alt=""></td>
		<td>
		
			<!-- Column Image 3 -->
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/column_image_3.jpg" width="78" height="78" border="0" style="display: block;" alt="Image 3"></td>
		<td rowspan="2" width="168" height="60" valign="top">
			
			<!-- The Title -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: bold; font-size: 18px; color: #161616; line-height: 32px;">We like coffee.</span><br>
			
			<!-- Text -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: normal; font-size: 12px; color: #686868; line-height: 19px;">And we also like great Design. We try to push email templates forward as far as possible. Just take a look at those pixels!</span>
			
			</td>
		<td rowspan="2" width="41" height="60"></td>
		<td rowspan="2" bgcolor="#f1f1f1" width="1" height="60"></td>
		<td rowspan="2" width="42" height="60"></td>
		<td>
			
			<!-- Column Image 4 -->
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/column_image_4.jpg" width="78" height="78" style="display:block;" alt="Image 4"></td>
		<td rowspan="2" width="180" height="60" valign="top">
		
			<!-- The Title -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: bold; font-size: 18px; color: #161616; line-height: 32px;">Save time.</span><br>
			
			<!-- Text -->
			<span style="font-family: Helvetica, Arial, Sans-serif; font-weight: normal; font-size: 12px; color: #686868; line-height: 19px;">Save time by letting letting us do your work. We love what we do. You can <a style="text-decoration: none; color: #0d6e04;" href="mailto:support@premiumstuff.net">contact us here</a>.</span>
		
		</td>
		<td rowspan="2" width="28" height="60"></td>
	</tr>
	<tr>
		<td width="78" height="60"></td>
		<td width="78" height="60"></td>
	</tr>
</tbody></table>

<!-- Padding-bottom of the columns -->
<table width="644" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td height="25">
		</td>
	</tr>
</tbody></table>
<!-- End 2 Columns -->

<!-- Last word, were, once again, try to convince our customers -->
<table width="644" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td width="35" height="100"></td>
		<td width="581" height="100" align="center">
		
		<span style="font-family: Helvetica, Arial, Sans-serif; font-size: 24px; color: #161616; font-weight: bold; line-height: 42px;">
		
			<!-- Headline -->
			So champ, what are you waiting for?
			
		</span><br>
		
		
		<span style="font-family: Georgia, serif; font-size: 14px; color: #525252; line-height: 20px;">
		
			<!-- Text under the headline -->
			<i>7 Color Schemes, 2 Versions, Editable Photoshop files, Stretchable, Easy to Customize, Tightly Coded, Awesome Designed, Future Proof, Well Documented, Flawless Support. Ready to roll?</i></span><br><br><br>
			
			<!-- The mighty Download Button -->
			<a href="#"><img src="http://www.premiumstuff.net/demo/america/green/light/images/button.jpg" border="0"></a><br><br>
		
		</td>
		<td width="35" height="100"></td>
	</tr>
</tbody></table>
<!-- End Last Word -->

<!-- Footer -->
<table width="658" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td>
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/footer.jpg" style="display: block;">
		</td>
	</tr>
</tbody></table>

<!-- Important, the Subscribe and Unsubscribe link and additional information -->
<table width="644" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td width="35" height="100"></td>
		<td width="581" align="center">
		
			<!-- Your Logo to gain trust -->
			<img src="http://www.premiumstuff.net/demo/america/green/light/images/logo_footer.jpg"><br>
		
		<span style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; color: #525252; line-height: 15px;">
		
			<!-- The Additional Information -->
			This is a one-time email sent to <a style="color: #0d6e04;" href="#">johndo@email.com</a>. If you wish to receive more emails by <a style="color: #0d6e04;" href="#">Company Name</a>, please take a few seconds to<br><br>
			
			<!-- Subscribe and Unsubscribe -->
			<a style="color: #0d6e04;" href="mailto:support@premiumstuff.net">Subscribe</a>
			
		</span><br><br>
		</td>
		<td width="35" height="100"></td>
	</tr>
</tbody></table>
<!-- End Subscribe and Unsubscribe -->


</body></html>
<ul id="error_message_box"></ul>
<?php
echo form_open('upload/crop_image/'.$file->image_id,array('id'=>'crop_form'));
?>
<img src='<? echo base_url()?>uploads/<?=$file->filename?>' id='crop_image' />
<!--div class="field_row clearfix">
<?php echo form_label(lang('items_is_giftcard').':', 'is_giftcard',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'is_giftcard',
		'id'=>'is_giftcard',
		'value'=>1,
		'checked'=>($item_info->is_giftcard)? 1 : 0)
	);?>
	</div>
</div-->
<input type='hidden' id='x' name='x' val='0'/>
<input type='hidden' id='y' name='y' val='0'/>
<input type='hidden' id='w' name='w' val='0'/>
<input type='hidden' id='h' name='h' val='0'/>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	console.log('setting up validate');
	var submitting = false;
    $('#crop_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$('#crop_image').attr('src', '<? echo base_url()?>uploads/'+response.filename);
				//$.colorbox.close();
				//post_item_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
	   		},
		messages:
		{
			}
	});
});
</script>
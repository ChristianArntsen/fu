<html>
  <head>
    <!--script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.load("jquery", "1.4.2");
      google.setOnLoadCallback(load_revenue_data);
      function drawChart(data) {
        var data = google.visualization.arrayToDataTable(data.data);

        var options = {
          title: 'Monthly Revenues and Profit',
          width:1200,
          vAxis:{format:'$#,###.##'}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      function load_revenue_data() {
      	$.ajax({
           type: "POST",
           url: "/index.php/dashboard/fetch_revenue_data/",
           data: '',
           success: function(response){
           		//console.dir(response.headers);
           		console.dir(response.data);
           		drawChart(response);
		    },
            dataType:'json'
         });
      }
    </script-->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
    	$.ajax({
           type: "POST",
           url: "/index.php/dashboard/fetch_tee_time_data/<?=$day_of_week?>",
           data: '',
           success: function(response){
           	console.dir(response);
		        chart = new Highcharts.Chart({
		            chart: {
		                renderTo: <?=$container_name?>,
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: '<?=$title?>',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: <?=$categories?>
		            },
		            yAxis: {
		                title: {
		                    text: ''
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: response
		        });
		    },
            dataType:'json'
        });
    });
});


		</script>

  </head>
  <body>
  	<script src="/js/highcharts.js"></script>
	<script src="/js/exporting.src.js"></script>
	<script type="text/javascript" src="/js/gray.js"></script>
	<div id="<?=$container_name?>" style="width: <?=$width?>px; height: <?=$height?>px;"></div>
  </body>
</html>
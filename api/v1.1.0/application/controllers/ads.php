<?php
// Email Ad Engine
class Ads extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Ad');
    }

	function index()
	{
		//		$this->Ad->display();
	}

	function view($campaign_id = 0, $person_id = 0)
	{
		$this->Ad->view($campaign_id, $person_id);
	}

	function click($campaign_id = 0, $person_id = 0)
	{
		$ad_info = $this->Ad->click($campaign_id, $person_id);
		redirect($ad_info['destination_url']);
	}
}